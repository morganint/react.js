import {
  CLEAR_FLINK_ADMIN_DATA,
  SET_TEACHER_LEARNING_CENTER,
  SET_TEACHER_REPORT_GROUPS,
  CREATE_REPORT_GROUP,
  EDIT_REPORT_GROUP,
  DELETE_REPORT_GROUP,
  CHANGE_SELECTED_REPORT_GROUP,
  CHANGE_SELECTED_FAMILY
} from './types';
import _ from 'lodash';

import { axiosAPI as axios } from 'utils';
import { withLoading } from 'utils';

const filter = 'flinkAdmin';

export const clearFlinkAdminData = () => ({
  filter,
  type: CLEAR_FLINK_ADMIN_DATA
});

export const changeSelectedReportGroupId = id => ({
  filter,
  type: CHANGE_SELECTED_REPORT_GROUP,
  payload: id
});

export const changeSelectedFamilyId = id => ({
  filter,
  type: CHANGE_SELECTED_FAMILY,
  payload: id
});

export const setLearningCenter = () => (dispatch, getState) => {
  const {
    common: { learningCenters },
    auth: { user }
  } = getState();

  const learningCenter = _.find(learningCenters, {
    _id: user.learningCenterId
  });

  dispatch({
    filter,
    type: SET_TEACHER_LEARNING_CENTER,
    payload: learningCenter
  });
};

export const getReportGroup = () => (dispatch, getState) =>
  withLoading(dispatch, async () => {
    const {
      auth: { user }
    } = getState();

    try {
      const res = await axios.get(
        `/api/flink-admin/report-groups/by-teacher-id/${user._id}`
      );

      dispatch({
        filter,
        type: SET_TEACHER_REPORT_GROUPS,
        payload: res.data
      });
    } catch (err) {
      console.log(err);
    }
  });

export const createReportGroup = data => dispatch =>
  withLoading(dispatch, async () => {
    try {
      const res = await axios.post('/api/flink-admin/report-groups/add', data);

      dispatch({
        filter,
        type: CREATE_REPORT_GROUP,
        payload: res.data
      });

      return { success: true, reportGroup: res.data };
    } catch (err) {
      return { success: false, err: err.response.data };
    }
  });

export const editReportGroup = data => dispatch =>
  withLoading(dispatch, async () => {
    try {
      const res = await axios.put('/api/flink-admin/report-groups/edit', data);

      dispatch({
        filter,
        type: EDIT_REPORT_GROUP,
        payload: res.data
      });

      return { success: true, reportGroup: res.data };
    } catch (err) {
      return { success: false, err: err.response.data };
    }
  });

export const deleteReportGroup = id => dispatch =>
  withLoading(dispatch, async () => {
    try {
      const res = await axios.delete(`/api/flink-admin/report-groups/${id}`);

      if (res.data.success) {
        dispatch({
          filter,
          type: DELETE_REPORT_GROUP,
          payload: id
        });
      }

      return res.data;
    } catch (err) {
      return { success: false, err: err.response.data };
    }
  });

// export const editStudent = data => dispatch =>
//   withLoading(dispatch, async () => {
//     const result = {};

//     try {
//       const res = await axios.post(
//         `/api/teacher/students/edit?id=${data._id}`,
//         data
//       );

//       dispatch({
//         filter,
//         type: EDIT_STUDENT,
//         payload: res.data
//       });

//       result.success = true;

//       return result;
//     } catch (err) {
//       result.success = false;
//       result.err = err.response.data;

//       return result;
//     }
//   });

// export const fetchStudents = id => dispatch =>
//   withLoading(dispatch, async () => {
//     try {
//       const res = await axios.get(`/api/teacher/students/list?teacherId=${id}`);

//       dispatch({
//         filter,
//         type: SET_STUDENTS,
//         payload: res.data
//       });
//     } catch (err) {
//       console.log(err);
//     }
//   });

// export const deleteStudent = id => dispatch =>
//   withLoading(dispatch, async () => {
//     try {
//       const res = await axios.delete(`/api/teacher/students/byId?id=${id}`);

//       if (res.data.success) {
//         dispatch({
//           filter,
//           type: DELETE_STUDENT,
//           payload: id
//         });
//       }
//     } catch (err) {
//       console.log(err);
//     }
//   });
