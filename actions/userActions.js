import { axiosAPI as axios } from 'utils';
import { setAuthFromToken } from 'actions/authActions';
import { withLoading } from 'utils';

export const changePassword = data => dispatch =>
  withLoading(dispatch, async () => {
    try {
      const res = await axios.post('/api/users/change-password', data);

      dispatch(setAuthFromToken(res.data.token));

      return { success: true };
    } catch (e) {
      return e.response.data;
    }
  });
