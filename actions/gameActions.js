import _ from 'lodash';

import { axiosAPI as axios, withLoading } from 'utils';
import {
  SHOW_STATS,
  SET_CURRENT_ACTIVITY,
  OPEN_ACTIVITY_HELP,
  CLOSE_ACTIVITY_HELP,
  OPEN_TEAM_ACTIVITY_HELP,
  CLOSE_TEAM_ACTIVITY_HELP,
  GAME_INITIALIZED,
  RESTART_ACTIVITY,
  SHOW_GAME_PARENT_ADVICE,
} from 'actions/types';
import API from 'api';

const getActivityTemplate = (activity, activityTemplates) => {
  return _.find(activityTemplates, { _id: activity.templateId });
};

const filter = 'game';

export const setCurrentActivity = (
  activity,
  activityTemplates,
  options = {}
) => (dispatch) =>
  withLoading(dispatch, async () => {
    // if null passed
    if (!activity) {
      return dispatch({
        filter,
        type: SET_CURRENT_ACTIVITY,
        payload: null,
      });
    }

    // if activity object passed
    if (typeof activity === 'object') {
      return dispatch({
        filter,
        type: SET_CURRENT_ACTIVITY,
        payload: { currentActivity: activity, ...options },
      });
    }

    // if activity ID passed
    if (typeof activity === 'string') {
      const payload = await getActivityData(activity, activityTemplates);

      return dispatch({
        filter,
        type: SET_CURRENT_ACTIVITY,
        payload: { currentActivity: payload, ...options },
      });
    }
  });

export const getActivityData = (id, activityTemplates) => {
  const result = {};

  return axios
    .get(`api/flink-play/activities/${id}`)
    .then((res) => {
      result.activity = res.data;
      result.template = getActivityTemplate(res.data, activityTemplates);

      return API.activity.getActivityData(id, result.template);
    })
    .then((data) => {
      if (!data) return null;

      result.data = data;

      return result;
    })
    .catch((err) => {
      console.log(err);
      return null;
    });
};

export const openActivityHelp = () => ({
  filter,
  type: OPEN_ACTIVITY_HELP,
});

export const closeActivityHelp = () => ({
  filter,
  type: CLOSE_ACTIVITY_HELP,
});

export const openActivityTeamHelp = () => ({
  filter,
  type: OPEN_TEAM_ACTIVITY_HELP,
});

export const closeActivityTeamHelp = () => ({
  filter,
  type: CLOSE_TEAM_ACTIVITY_HELP,
});

export const restartActivity = () => ({
  filter,
  type: RESTART_ACTIVITY,
});

export const setGameInitialized = () => ({
  filter,
  type: GAME_INITIALIZED,
});

export const showStats = (payload) => ({
  filter,
  type: SHOW_STATS,
  payload,
});

export const showGameParentAdvice = (payload) => ({
  filter,
  type: SHOW_GAME_PARENT_ADVICE,
  payload,
});
