import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';

import {
  getFlinkPlayData,
  showVideo,
  setMenuButtons,
  resetFlinkPlay,
  showInitialSettings,
  // removeLearnerReports,
  closeActivity,
} from 'actions/flinkPlayActions';
import { showError } from 'actions/statusActions';
import { logout } from 'actions/authActions';
import { LOGOUT, SETTINGS } from 'consts/buttons';
import {
  Help,
  Settings,
  Character,
  ThemeBackground,
  MenuButtons,
  Modals,
  ParentAdvice,
  VideoAnimation,
} from 'components/flink-play';
import { filesURL } from 'config';

import MainMenu from 'pages/FlinkPlay/flink-play-pages/MainMenu/MainMenu';
import MenuGroup from 'pages/FlinkPlay/flink-play-pages/MenuGroup/MenuGroup';
import FamilyReports from 'pages/FlinkPlay/flink-play-pages/FamilyReports/FamilyReports';
import { TemplateWrapper } from "components/flink-play";
import {
  // msToHMS,
  mobileAndTabletCheck,
} from 'utils';

import classes from './FlinkPlay.module.scss';

export class FlinkPlay extends Component {
  state = { solutionInitialized: false };

  async componentDidMount() {
    mobileAndTabletCheck() &&
      document.body.addEventListener('click', this.requestFullscreen);

    const {
      getFlinkPlayData,
      setMenuButtons,
      showError,
      logout,
      showInitialSettings,
    } = this.props;

    try {
      await getFlinkPlayData();
    } catch (err) {
      console.log(err);
      return showError({
        message: (err && err.error) || 'Something goes wrong',
        cb: logout,
      });
    }

    setMenuButtons([LOGOUT, SETTINGS]);

    this.showFamilyVideo().then(showInitialSettings);
  }

  componentDidUpdate(prevProps) {
    const { solution: prevSolution } = prevProps.flinkPlay;
    const { solution } = this.props.flinkPlay;

    // if (prevSolution && solution && prevSolution._id !== solution._id) {
    // }

    if (
      (!prevSolution && solution) ||
      (prevSolution && solution && prevSolution._id !== solution._id)
    ) {
      this.setState({ solutionInitialized: false });
      this.initSolution();
    }
  }

  componentWillUnmount() {
    this.props.resetFlinkPlay();

    document.body.removeEventListener('click', this.requestFullscreen);
    if (
      document.fullscreenElement ||
      document.webkitFullscreenElement ||
      document.mozFullScreenElement
    ) {
      document.exitFullscreen();
    }
  }

  requestFullscreen = () => {
    if (
      document.fullscreenElement ||
      document.webkitFullscreenElement ||
      document.mozFullScreenElement
    ) {
      return;
    }

    const rootElem = document.getElementById('root');

    if (rootElem.requestFullScreen) {
      rootElem.requestFullScreen();
    } else if (rootElem.mozRequestFullScreen) {
      rootElem.mozRequestFullScreen();
    } else if (rootElem.webkitRequestFullScreen) {
      rootElem.webkitRequestFullScreen();
    }
  };

  initSolution = () => {
    this.showIntroAnimation().then(() => {
      // Show Report Screen
      this.setState({ solutionInitialized: true });
    });
  };

  showFamilyVideo = () => {
    return new Promise((resolve) => {
      const {
        flinkPlay: { learner },
        showVideo,
        loginLang,
      } = this.props;

      if (learner.settings || !learner.isAdmin) {
        return resolve();
      }

      const src = `${filesURL}/Files/animations/FamilyLearning/${loginLang.name}/FamilyLearningVideo.mp4`;

      showVideo({
        src,
        callback: () => {
          showVideo(null);
          resolve();
        },
      });
    });
  };

  showIntroAnimation = () => {
    return new Promise((resolve) => {
      const { flinkPlay, showVideo } = this.props;

      const {
        solutionLocale,
        learner: {
          settings: { autoAnimation },
        },
        solution: { animations },
      } = flinkPlay;

      if (
        !autoAnimation ||
        !animations ||
        !animations.introduction ||
        !animations.introduction[solutionLocale.code]
      ) {
        return resolve();
      }

      const src = `${filesURL}/Files/animations/Introduction/${
        solutionLocale.name
      }/${animations.introduction[solutionLocale.code]}`;

      showVideo({
        src,
        callback: () => {
          showVideo(null);
          resolve();
        },
      });
    });
  };

  render() {
    const {
      closeActivity,
      flinkPlay: {
        help,
        video,
        isDataLoaded,
        showSettings,
        activity,
        // learner,
        solution,
        currentActivityGroup,
        familyReportsOpened,
        solutionMenu,
      },
    } = this.props;

    if (!isDataLoaded) return null;

    const { solutionInitialized } = this.state;

    return (
      <div className={classes.wrapper}>
        {solution && solutionMenu && (
          <>
            <ThemeBackground />
            <Character />
          </>
        )}

        <div className={classes.content}>
          <Modals />
          <ParentAdvice />

          {help && <Help />}
          {showSettings && <Settings />}
          {video && <VideoAnimation />}

          {/* <AppearTransition show={showSettings}>
            <Settings />
          </AppearTransition> */}

          {activity ? (
            <TemplateWrapper activity={activity} closeHandler={closeActivity} />
          ) : (
            <>
              <AppearTransition
                show={
                  solutionInitialized &&
                  !familyReportsOpened &&
                  !currentActivityGroup
                }
              >
                <MainMenu />
              </AppearTransition>

              <AppearTransition
                show={
                  solutionInitialized &&
                  !familyReportsOpened &&
                  currentActivityGroup
                }
              >
                <MenuGroup />
              </AppearTransition>

              <AppearTransition
                show={solutionInitialized && familyReportsOpened}
              >
                <FamilyReports />
              </AppearTransition>

              <MenuButtons />
            </>
          )}
        </div>
      </div>
    );
  }
}

const AppearTransition = ({ show, children }) => {
  return (
    !!show && (
      <CSSTransition
        in={!!show}
        appear
        timeout={{
          appear: 500,
        }}
        classNames={{
          appear: classes.appear,
          appearActive: classes.appearActive,
          // appearDone: "appearDone"
        }}
      >
        <div>{children}</div>
      </CSSTransition>
    )
  );
};

FlinkPlay.propTypes = {
  flinkPlay: PropTypes.object.isRequired,
  showVideo: PropTypes.func.isRequired,
  resetFlinkPlay: PropTypes.func.isRequired,
  setMenuButtons: PropTypes.func.isRequired,
  getFlinkPlayData: PropTypes.func.isRequired,
  showInitialSettings: PropTypes.func.isRequired,
  closeActivity: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
};

const mapStateToProps = ({ flinkPlay, status }) => ({
  loginLang: status.lang,
  flinkPlay,
});

export default connect(mapStateToProps, {
  // removeLearnerReports,
  getFlinkPlayData,
  showError,
  closeActivity,
  logout,
  showVideo,
  resetFlinkPlay,
  showInitialSettings,
  setMenuButtons,
})(FlinkPlay);
