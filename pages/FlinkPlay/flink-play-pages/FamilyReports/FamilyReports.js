import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { GO_BACK, PARENT_ADVICE } from 'consts/buttons';
import _ from 'lodash';
import styled from 'styled-components';

import { MainMenuBoard } from 'components/flink-play';
import { setMenuButtons, showFamilyReports } from 'actions/flinkPlayActions';
import { showInfo } from 'actions/statusActions';
import { getFamilyMembersByFamily } from 'actions/familiesActions';
import { imagesURL } from 'config';

const prevBtnImg = `${imagesURL}/TemplateGraphics/e-Book/arrow_left.png`;
const nextBtnImg = `${imagesURL}/TemplateGraphics/e-Book/arrow_right.png`;

class FamilyReports extends Component {
  state = {};

  async componentDidMount() {
    const {
      setMenuButtons,
      family,
      solutions,
      showInfo,
      getFamilyMembersByFamily,
      currentLearnerId,
    } = this.props;

    // Set menu button
    setMenuButtons([{ type: GO_BACK, onClick: this.exit }, PARENT_ADVICE]);

    if (!family) {
      console.log('No family');
      return this.exit();
    }

    const familyMembers = await getFamilyMembersByFamily(family._id);

    if (!familyMembers) {
      console.log('No family members');
      return this.exit();
    }

    const members = familyMembers.filter((member) => {
      // Without loged in learner
      if (member._id === currentLearnerId) return false;

      // Without learners that haven't reportSolution field
      if (!member.reportSolution) return false;

      const solution = _.find(solutions, { _id: member.reportSolution });

      if (!solution || !solution.solutionMenu) return false;

      return true;
    });

    if (!members.length) {
      showInfo({
        message:
          'There is no other members in your family, or report solutions not assigned with them',
      });
      return this.exit();
    }

    this.setState({ members, activeMemberIdx: 0 });
  }

  exit = () => {
    const { showFamilyReports } = this.props;
    showFamilyReports(false);
  };

  changeActiveMemberIdx = (newIdx) => {
    this.setState({ activeMemberIdx: newIdx }, () => {
      // Reset scroll to bottom
      this.scrollbarRef.current && this.scrollbarRef.current.scrollToBottom();
    });
  };

  scrollbarRef = React.createRef();

  render() {
    const { translate, solutions } = this.props;
    const { members, activeMemberIdx } = this.state;

    if (!members) return null;

    const activeMember = members[activeMemberIdx];

    const solution = _.find(solutions, {
      _id: activeMember.reportSolution,
    });

    const isFirst = activeMemberIdx === 0;
    const isLast = activeMemberIdx === members.length - 1;

    const memberName = `${activeMember.firstname} ${activeMember.lastname}`;

    const props = {
      title: memberName,
      subtitle: solution.name,
      solutionMenu: solution.solutionMenu,
      translate,
      reports: activeMember.reports,
      scrollbarRef: this.scrollbarRef,
    };

    return (
      <MainMenuBoard {...props}>
        {!isFirst && (
          <PrevButton
            onClick={() => this.changeActiveMemberIdx(activeMemberIdx - 1)}
          >
            <img src={prevBtnImg} alt="" />
          </PrevButton>
        )}
        {!isLast && (
          <NextButton
            onClick={() => this.changeActiveMemberIdx(activeMemberIdx + 1)}
          >
            <img src={nextBtnImg} alt="" />
          </NextButton>
        )}
      </MainMenuBoard>
    );
  }
}

FamilyReports.propTypes = {
  family: PropTypes.object.isRequired,
  setMenuButtons: PropTypes.func.isRequired,
  showFamilyReports: PropTypes.func.isRequired,
  getFamilyMembersByFamily: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  showInfo: PropTypes.func.isRequired,
  solutions: PropTypes.array.isRequired,
  currentLearnerId: PropTypes.string.isRequired,
};

const mapStateToProps = ({
  flinkPlay: { family, solutionTranslate, learner, product },
}) => ({
  family,
  translate: solutionTranslate,
  currentLearnerId: learner._id,
  solutions: product.solutions,
});

export default connect(mapStateToProps, {
  setMenuButtons,
  showFamilyReports,
  getFamilyMembersByFamily,
  showInfo,
})(FamilyReports);

const PrevButton = styled.button`
  cursor: pointer;
  position: absolute;
  top: 50%;
  transform: translate(-100%, -50%);
  left: 0;

  img {
    height: 5vmin;
  }
`;

const NextButton = styled.button`
  cursor: pointer;
  position: absolute;
  top: 50%;
  transform: translate(100%, -50%);
  right: 0;

  img {
    height: 5vmin;
  }
`;
