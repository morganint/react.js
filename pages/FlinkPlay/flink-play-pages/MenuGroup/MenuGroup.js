import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import PropTypes from 'prop-types';

import { getGroupElements } from 'utils';
// import { imagesURL } from "config";
import { setMenu, openMenuItem } from 'actions/flinkPlayActions';
import { MenuGroupView } from 'components/flink-play';

class MenuGroup extends Component {
  state = { items: null };

  componentDidMount() {
    this.setItems();
  }

  componentDidUpdate(prevProps) {
    const { currentActivityGroup } = this.props;

    if (prevProps.currentActivityGroup !== currentActivityGroup) {
      this.setItems();
    }
  }

  setItems = () => {
    const { currentActivityGroup, solutionMenu, setMenu, menus } = this.props;

    if (!currentActivityGroup) return;

    const activityGroup = _.find(solutionMenu.activityGroups, {
      _id: currentActivityGroup,
    });

    if (!activityGroup) {
      return this.setState({ items: [], activityGroup });
    }

    // Set Menu
    const menu = _.find(menus, { _id: activityGroup.menuId });
    setMenu(menu);

    const items = getGroupElements(
      activityGroup,
      solutionMenu.activityGroups,
      solutionMenu.activities
    );

    this.setState({ items, activityGroup });
  };

  render() {
    const {
      translate,
      currentMenu,
      openMenuItem,
      reports = {},
      teamReports,
      previousActivityId,
    } = this.props;

    const { items } = this.state;

    if (!items) return null;

    const title = currentMenu && translate(currentMenu.labelString);

    return (
      <MenuGroupView
        title={title}
        items={items}
        reports={reports}
        activityClickHandler={(id) => openMenuItem(id, false)}
        groupClickHandler={(id) => openMenuItem(id, true)}
        highlight={previousActivityId}
        teamReports={teamReports}
      />
    );
  }
}

MenuGroup.propTypes = {
  previousActivityId: PropTypes.string,
  currentMenu: PropTypes.object,
  menus: PropTypes.array.isRequired,
  currentActivityGroup: PropTypes.string,
  reports: PropTypes.object,
  teamReports: PropTypes.array,
  solutionMenu: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
  openMenuItem: PropTypes.func.isRequired,
  setMenu: PropTypes.func.isRequired,
};

const mapStateToProps = ({
  flinkPlay: {
    previousActivityId,
    solutionMenu,
    learner: { reports },
    team,
    solutionTranslate,
    currentActivityGroup,
    menus,
    currentMenu,
  },
}) => ({
  previousActivityId,
  teamReports: team && team.map((m) => m.reports),
  reports,
  currentMenu,
  solutionMenu,
  translate: solutionTranslate,
  currentActivityGroup,
  menus,
});

export default connect(mapStateToProps, {
  setMenu,
  openMenuItem,
})(MenuGroup);
