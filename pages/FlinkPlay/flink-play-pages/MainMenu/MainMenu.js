import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import _ from "lodash";

import { MainMenuBoard, SetGoal } from "components/flink-play";
import {
  setMenu,
  openMenuItem,
  setMainMenuScroll
} from "actions/flinkPlayActions";

export class MainMenu extends Component {
  constructor(props) {
    super(props);

    const { menus, setMenu } = props;
    const menu = _.find(menus, { forMainMenu: true });

    if (!menu) {
      return alert("Main Menu Not Found");
    }

    setMenu(menu);
  }

  componentDidMount() {
    this.checkIfOneMenuGroup();
  }

  componentDidUpdate(prevProps) {
    const { solutionMenu, menu } = this.props;
    if (solutionMenu !== prevProps.solutionMenu || menu !== prevProps.menu) {
      this.checkIfOneMenuGroup();
    }
  }

  checkIfOneMenuGroup = () => {
    const { solutionMenu, openMenuItem } = this.props;

    if (!solutionMenu || !solutionMenu.menuGroups) return;

    const { menuGroups } = solutionMenu;

    if (menuGroups.length === 1) {
      openMenuItem(menuGroups[0].groupId, true);
    }
  };

  render() {
    const {
      team,
      menu,
      translate,
      solutionMenu,
      reports,
      openMenuItem,
      mainMenuScroll,
      setMainMenuScroll,
      solutionName,
      solutionId,
      learningGoals
    } = this.props;
    if (!menu || !solutionMenu) return null;

    const teamReports = (team && team.map(m => m.reports)) || undefined;

    const learnerGoal = _.find(learningGoals, { solution: solutionId });

    const props = {
      title: translate(menu.labelString),
      subtitle: solutionName,
      solutionMenu,
      translate,
      reports,
      openMenuItem,
      scrollPosition: mainMenuScroll,
      setScrollPosition: setMainMenuScroll,
      teamReports,
      learnerGoal
    };

    return (
      <MainMenuBoard {...props}>
        <SetGoal />
      </MainMenuBoard>
    );
  }
}

MainMenu.propTypes = {
  menus: PropTypes.array.isRequired,
  setMenu: PropTypes.func.isRequired,
  menu: PropTypes.object,
  solutionMenu: PropTypes.object,
  reports: PropTypes.object,
  translate: PropTypes.func.isRequired,
  mainMenuScroll: PropTypes.object,
  setMainMenuScroll: PropTypes.func.isRequired,
  openMenuItem: PropTypes.func.isRequired,
  learningGoals: PropTypes.array.isRequired,
  solutionId: PropTypes.string.isRequired,
  team: PropTypes.array
};

const mapStateToProps = ({ flinkPlay }) => ({
  reports: flinkPlay.learner.reports,
  learningGoals: flinkPlay.learner.learningGoals,
  team: flinkPlay.team,
  menus: flinkPlay.menus,
  menu: flinkPlay.currentMenu,
  translate: flinkPlay.solutionTranslate,
  mainMenuScroll: flinkPlay.mainMenuScroll,
  solutionMenu: flinkPlay.solutionMenu,
  solutionName: flinkPlay.solution.displayName,
  solutionId: flinkPlay.solution._id
});

export default connect(mapStateToProps, {
  setMenu,
  openMenuItem,
  setMainMenuScroll
})(MainMenu);
