import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Redirect } from 'react-router-dom';
import styled from 'styled-components';
import _ from 'lodash';

import EditActivities from './tabs/EditActivities/EditActivities';
import SearchActivities from './tabs/SearchActivities/SearchActivities';
import ActivityGroups from './tabs/ActivityGroups/ActivityGroups';
import Wordlists from './tabs/Wordlists/Wordlists';
import SearchWordlists from './tabs/SearchWordlists/SearchWordlists';
import TransferActivities from './tabs/TransferActivities/TransferActivities';
import EditActivity from './tabs/EditActivity/EditActivity';
import Preview from './tabs/Preview/Preview';
import NotFoundPage from 'pages/NotFoundPage/NotFoundPage';
import { renderRoutes } from 'utils';
import {
  fetchFlinkMakeCommonData,
  setFlinkMakeCommonData
} from 'actions/flinkMakeActions';
import { imagesURL } from 'config';
import './FlinkMake.scss';
import { SUPER } from 'consts/user-roles';

const routes = [
  [
    '/flink-make/edit-activities',
    true,
    EditActivities,
    { flinkMake: { tabs: { editActivities: true } } }
  ],
  [
    '/flink-make/search-activities',
    true,
    SearchActivities,
    { flinkMake: { tabs: { searchActivities: true } } }
  ],
  [
    '/flink-make/activity-groups',
    true,
    ActivityGroups,
    { flinkMake: { tabs: { activityGroups: true } } }
  ],
  [
    '/flink-make/wordlists',
    true,
    Wordlists,
    { flinkMake: { tabs: { wordlists: true } } }
  ],
  [
    '/flink-make/search-wordlists',
    true,
    SearchWordlists,
    { flinkMake: { tabs: { searchWordlists: true } } }
  ],
  [
    '/flink-make/edit-activities/:activityId',
    false,
    EditActivity,
    {
      flinkMake: {
        tabs: { editActivities: true },
        editActivitiesTab: { editListButtons: { edit: true } }
      }
    }
  ],
  [
    '/flink-make/preview/:activityId',
    true,
    Preview,
    {
      flinkMake: {
        tabs: { editActivities: true },
        editActivitiesTab: { editListButtons: { preview: true } }
      }
    }
  ],
  ['/flink-make/transfer-activities', true, TransferActivities, ['SUPER']],
  // 404 Page
  [null, false, NotFoundPage]
];

const FlinkMake = ({
  auth,
  dataLoaded,
  fetchFlinkMakeCommonData,
  setFlinkMakeCommonData
}) => {
  const redirectTo = useMemo(() => {
    const {
      userRole: { permissions, alias }
    } = auth;

    const isSuper = alias === SUPER;

    const hasAccessToTabs = _.get(permissions, 'flinkMake[tabs]') || {};

    let redirectTo = '/';
    if (isSuper || hasAccessToTabs.editActivities) {
      redirectTo = '/flink-make/edit-activities';
    } else if (hasAccessToTabs.searchActivities) {
      redirectTo = '/flink-make/search-activities';
    } else if (hasAccessToTabs.activityGroups) {
      redirectTo = '/flink-make/activity-groups';
    } else if (hasAccessToTabs.wordlists) {
      redirectTo = '/flink-make/wordlists';
    } else if (hasAccessToTabs.searchWordlists) {
      redirectTo = '/flink-make/search-wordlists';
    }

    return redirectTo;
  }, [auth]);

  useEffect(() => {
    // const redirectTo =
    fetchFlinkMakeCommonData();

    return () => setFlinkMakeCommonData(null);
  }, [fetchFlinkMakeCommonData, setFlinkMakeCommonData]);

  if (!dataLoaded) return null;

  return (
    <StyledFlinkMake className="flink-make">
      <Switch>
        <Redirect from="/flink-make" exact to={redirectTo} />
        <Redirect
          from="/flink-make/preview"
          exact
          to="/flink-make/edit-activities"
        />
        {renderRoutes(routes, auth, redirectTo)}
      </Switch>
    </StyledFlinkMake>
  );
};

FlinkMake.propTypes = {
  auth: PropTypes.object.isRequired,
  fetchFlinkMakeCommonData: PropTypes.func.isRequired,
  setFlinkMakeCommonData: PropTypes.func.isRequired,
  dataLoaded: PropTypes.bool
};

const mapStateToProps = ({ auth, flinkMake }) => ({
  auth,
  dataLoaded: flinkMake.common.loaded
});

export default connect(mapStateToProps, {
  fetchFlinkMakeCommonData,
  setFlinkMakeCommonData
})(FlinkMake);

const StyledFlinkMake = styled.div`
  .content-wrapper {
    background: url(${imagesURL}/FlinkMake/background.jpg) center top/cover
      no-repeat rgb(102, 51, 204);
  }
`;
