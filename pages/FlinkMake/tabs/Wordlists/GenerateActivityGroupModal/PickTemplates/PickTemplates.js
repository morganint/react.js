import React from 'react';
import PropTypes from 'prop-types';

import classes from './PickTemplates.module.scss';

const PickTemplates = ({
  templates,
  selected,
  toggleTemplateHandler,
  label
}) => {
  return (
    <fieldset className={classes.container}>
      <legend>{label}</legend>
      <div className={classes.checkboxesGroup}>
        {templates
          .sort((a, b) => {
            if (a.activityTemplateName < b.activityTemplateName) {
              return -1;
            }
            if (a.activityTemplateName > b.activityTemplateName) {
              return 1;
            }
            return 0;
          })
          .map(
            ({
              _id: id,
              activityTemplateName: name,
              alwaysIncludedInWordlists
            }) => (
              <label key={id} className={classes.label}>
                <input
                  type="checkbox"
                  disabled={alwaysIncludedInWordlists}
                  value={id}
                  checked={selected.includes(id)}
                  onChange={e => toggleTemplateHandler(id, e.target.checked)}
                />
                <span>{name}</span>
              </label>
            )
          )}
      </div>
    </fieldset>
  );
};

PickTemplates.defaultProps = {
  templates: [],
  label: '',
  selected: []
};

PickTemplates.propTypes = {
  label: PropTypes.string,
  templates: PropTypes.array.isRequired,
  selected: PropTypes.array.isRequired,
  toggleTemplateHandler: PropTypes.func.isRequired
};

export default PickTemplates;
