import React, { Component } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { connect } from "react-redux";
import { compose } from "redux";

import PickTemplates from "./PickTemplates/PickTemplates";
import classes from "./GenerateActivityGroupModal.module.scss";
import { withTranslate } from "components/hocs";
import { showInfo } from "actions/statusActions";
import { generateActivityGroup, checkWords } from "actions/flinkMakeActions";
import { formatLocalesToOptions } from "utils";

class GenerateActivityGroupModal extends Component {
  state = {
    localesOptions: [],
    selectedTemplatesIDs: [],
    activityLocale: "",
    helpLocale: [],
    defaultLocale: "",
    activityGroupName: "",
    wordlistName: "wordlistName1",
    wordsWithImages: false,
    includeDefinitionAudio: true
  };

  componentDidMount() {
    const { locales } = this.props;

    const localesOptions = formatLocalesToOptions(locales);

    const firstLocaleCode = localesOptions[0] ? localesOptions[0].value : "";

    this.setState({
      localesOptions,
      activityLocale: firstLocaleCode,
      helpLocale: ["en"]
    });

    this.filterTemplates();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.wordsWithImages !== this.state.wordsWithImages) {
      this.filterTemplates();
    }
  }

  toggleTemplateHandler = (templateId, isChecked) => {
    const { selectedTemplatesIDs } = this.state;

    const newSelectedTemplates = [...selectedTemplatesIDs];

    if (isChecked) {
      newSelectedTemplates.push(templateId);
    } else {
      _.pull(newSelectedTemplates, templateId);
    }

    this.setState({
      selectedTemplatesIDs: _.uniq(newSelectedTemplates)
    });
  };

  checkWordsHandler = async () => {
    const {
      activityLocale,
      helpLocale,
      includeDefinitionAudio,
      selectedTemplatesIDs,
      wordsWithImages
    } = this.state;
    const { checkWords, wordlist, showInfo, translate } = this.props;

    const params = {
      wordlist,
      helpLocale,
      includeDefinitionAudio,
      activityLocale,
      selectedTemplatesIDs,
      wordsWithImages
    };

    const results = await checkWords(params);

    if (!results) return;
    const { missedAssets } = results;

    let message = translate(434, "Words passed");

    if (missedAssets.length) {
      message += `<br><br><b>Missed Assets:</b><br>${missedAssets.join(
        "<br>"
      )}`;
    }

    showInfo({ message, html: true });
  };

  filterTemplates = () => {
    const { activityTemplates } = this.props;
    const {
      wordsWithImages,
      selectedTemplatesIDs,
      filteredTemplates: stateFilteredTemplates
    } = this.state;

    const newSelectedTemplates = [...selectedTemplatesIDs];

    const filteredTemplates = activityTemplates.filter(template => {
      if (!template.availableForWordlists) return false;

      if (template.imageRequired) {
        if (wordsWithImages) {
          newSelectedTemplates.push(template._id);
        } else {
          _.pull(newSelectedTemplates, template._id);
          return false;
        }
      } else if (!stateFilteredTemplates) {
        // template.alias ===
        //   'MOVING_ANSWERS' &&
        newSelectedTemplates.push(template._id);
      }

      return true;
    });

    this.setState({
      filteredTemplates,
      selectedTemplatesIDs: _.uniq(newSelectedTemplates)
    });
  };

  validateParams = () => {
    const {
      activityGroupName,
      activityLocale,
      defaultLocale,
      helpLocale,
      selectedTemplatesIDs
    } = this.state;

    if (!activityGroupName.trim().length) return false;

    if (!activityLocale || !helpLocale.length) return false;

    if (helpLocale.length > 1 && !defaultLocale) return false;

    if (!selectedTemplatesIDs || !selectedTemplatesIDs.length) return false;

    return true;
  };

  generateActivityGroupHandler = () => {
    const {
      activityGroupName,
      activityLocale,
      includeDefinitionAudio,
      helpLocale,
      defaultLocale,
      selectedTemplatesIDs,
      wordlistName,
      wordsWithImages
    } = this.state;
    const { generateActivityGroup, wordlist } = this.props;

    const params = {
      wordlist,
      activityGroupName,
      activityLocale,
      includeDefinitionAudio,
      helpLocale,
      defaultLocale,
      selectedTemplatesIDs,
      wordlistName,
      wordsWithImages
    };

    generateActivityGroup(params);
  };

  changePropHandler = (prop, value) => {
    this.setState({ [prop]: value });
  };

  helpLocaleChangeHandler = e => {
    const { helpLocale, defaultLocale } = this.state;
    const { checked, value } = e.target;
    const newHelpLocales = _.xor(helpLocale, [value]);

    this.changePropHandler("helpLocale", newHelpLocales);

    if ((defaultLocale === value && !checked) || !newHelpLocales.length) {
      this.changePropHandler("defaultLocale", "");
    }

    if (newHelpLocales.length > 1 && !defaultLocale) {
      this.changePropHandler("defaultLocale", newHelpLocales[0]);
    }
  };

  render() {
    const { translate } = this.props;
    const {
      wordsWithImages,
      includeDefinitionAudio,
      filteredTemplates = [],
      selectedTemplatesIDs,
      activityGroupName,
      wordlistName,
      activityLocale,
      helpLocale,
      defaultLocale,
      localesOptions
    } = this.state;

    const isDisabled = !this.validateParams();

    return (
      <div className={classes.container}>
        <div className={classes.column}>
          <div className={classes.inputGroup}>
            <label>{translate(183, "Name:")}</label>
            <input
              type="text"
              onChange={e =>
                this.changePropHandler("activityGroupName", e.target.value)
              }
              value={activityGroupName}
            />
          </div>

          <div className={classes.wordlistNameChoice}>
            <label className={classes.radioGroup}>
              <input
                type="radio"
                checked={wordlistName === "wordlistName1"}
                value="wordlistName1"
                onChange={e =>
                  this.changePropHandler("wordlistName", e.target.value)
                }
              />
              <span>{translate(656, "Wordlist Name 1")}</span>
            </label>

            <label className={classes.radioGroup}>
              <input
                type="radio"
                checked={wordlistName === "wordlistName2"}
                value="wordlistName2"
                onChange={e =>
                  this.changePropHandler("wordlistName", e.target.value)
                }
              />
              <span>{translate(657, "Wordlist Name 2")}</span>
            </label>
          </div>

          <fieldset className={classes.localeSelects}>
            <legend>{translate(236, "Locales:")}</legend>
            <div className={classes.localeGroup}>
              <span>{translate(7, "Activities")}</span>

              <select
                value={activityLocale}
                onChange={e =>
                  this.changePropHandler("activityLocale", e.target.value)
                }
              >
                {localesOptions.map(opt => (
                  <option key={opt.value} value={opt.value}>
                    {opt.label}
                  </option>
                ))}
              </select>
            </div>

            <div className={classes.localeGroup}>
              <span>{translate(66, "Help")}</span>

              <div className={classes.localesCheckbox}>
                {localesOptions.map(opt => (
                  <label key={opt.value}>
                    <input
                      type="checkbox"
                      value={opt.value}
                      onChange={this.helpLocaleChangeHandler}
                      checked={helpLocale.includes(opt.value)}
                    />
                    <span>{opt.value}</span>
                  </label>
                ))}
              </div>

              {/* <select
                value={helpLocale}
                onChange={e =>
                  this.changePropHandler("helpLocale", e.target.value)
                }
              >
                {localesOptions.map(opt => (
                  <option key={opt.value} value={opt.value}>
                    {opt.label}
                  </option>
                ))}
              </select> */}
            </div>

            {helpLocale.length > 1 && (
              <div className={classes.localeGroup}>
                <span>{translate(0, "Default Help")}</span>

                <select
                  value={defaultLocale}
                  onChange={e =>
                    this.changePropHandler("defaultLocale", e.target.value)
                  }
                >
                  <option value=""></option>
                  {localesOptions
                    .filter(opt => helpLocale.includes(opt.value))
                    .map(opt => (
                      <option key={opt.value} value={opt.value}>
                        {opt.label}
                      </option>
                    ))}
                </select>
              </div>
            )}
          </fieldset>
        </div>

        <div className={classes.column}>
          <div className={classes.checkboxGroup}>
            <label className={classes.checkbox}>
              <input
                type="checkbox"
                checked={wordsWithImages}
                onChange={e =>
                  this.changePropHandler("wordsWithImages", e.target.checked)
                }
              />
              <span>{translate(381, "Words With Images")}</span>
            </label>

            <label className={classes.checkbox}>
              <input
                type="checkbox"
                checked={includeDefinitionAudio}
                onChange={e =>
                  this.changePropHandler(
                    "includeDefinitionAudio",
                    e.target.checked
                  )
                }
              />
              <span>{translate(0, "Definition Audio")}</span>
            </label>
          </div>

          <PickTemplates
            label={translate(227, "Pick templates: ")}
            templates={filteredTemplates.filter(templ => !templ.imageRequired)}
            selected={selectedTemplatesIDs}
            toggleTemplateHandler={this.toggleTemplateHandler}
          />

          {wordsWithImages && (
            <PickTemplates
              label={translate(380, "Pick templates that use Images:")}
              templates={filteredTemplates.filter(templ => templ.imageRequired)}
              selected={selectedTemplatesIDs}
              toggleTemplateHandler={this.toggleTemplateHandler}
            />
          )}
        </div>

        <button
          onClick={this.checkWordsHandler}
          className={classes.generateBtn}
          disabled={isDisabled}
        >
          {translate(248, "Check Words")}
        </button>

        <button
          onClick={this.generateActivityGroupHandler}
          className={classes.generateBtn}
          disabled={isDisabled}
        >
          {translate(243, "Generate")}
        </button>
      </div>
    );
  }
}

GenerateActivityGroupModal.propTypes = {
  translate: PropTypes.func.isRequired,
  activityTemplates: PropTypes.array.isRequired,
  locales: PropTypes.array.isRequired,
  generateActivityGroup: PropTypes.func.isRequired
};

const mapStateToProps = ({ common: { locales, activityTemplates } }) => ({
  locales,
  activityTemplates
});

export default compose(
  withTranslate,
  connect(mapStateToProps, { generateActivityGroup, showInfo, checkWords })
)(GenerateActivityGroupModal);
