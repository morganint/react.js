import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import _ from "lodash";
import { SelectEducationalCategory } from "components/flink-components";

import { DraggableModal } from "components/flink-components";
import { showError } from "actions/statusActions";
import { withTranslate } from "components/hocs";
import {
  Help,
  ActionsPanel,
  DataList,
  WordsList
} from "components/flink-components";
import { MainNav, Header } from "../../interface";
import {
  fetchMyWordlists,
  createWordlist,
  editWordlist,
  addWordsToWordlist,
  undoDeleteWordlist,
  removeFromMyWordlists,
  saveWordlistChanges,
  deleteWordlist
} from "actions/flinkMakeActions";
import { showConfirm } from "actions/statusActions";
import { validateWordlist } from "utils/wordlistHelpers";
import { imagesURL } from "config";
import classes from "./Wordlists.module.scss";
import GenerateActivityGroupModal from "./GenerateActivityGroupModal/GenerateActivityGroupModal";
import { desc } from "utils/getSorting";

class Wordlists extends Component {
  state = {
    filteredWordlists: null,
    activeWordlist: null,
    showGenerateModal: false
  };

  componentDidMount() {
    const { fetchMyWordlists } = this.props;
    fetchMyWordlists();
  }

  componentDidUpdate(prevProps) {
    const { wordlists } = this.props;

    if (wordlists !== prevProps.wordlists) {
      this.filterWordlists();
    }
  }

  filterWordlists = () => {
    const { userId, wordlists } = this.props;
    const { activeWordlist } = this.state;
    if (!wordlists) return;

    const filteredWordlists = wordlists
      .filter(w => w.inEditList === userId)
      .sort((a, b) => -desc(a.name, b.name));

    const newActiveWordlist =
      activeWordlist && _.find(filteredWordlists, { _id: activeWordlist._id });

    this.setState({
      filteredWordlists,
      activeWordlist: newActiveWordlist || filteredWordlists[0]
    });
  };

  setActiveWordlist = wordlist => {
    this.setState({
      activeWordlist: wordlist
    });
  };

  componentWillUnmount() {
    this.props.saveWordlistChanges();
  }

  deleteWordlistHandler = async () => {
    const { deleteWordlist, translate } = this.props;
    const { activeWordlist } = this.state;

    const isConfirmed = window.confirm(
      translate(95, "You sure you want to delete wordlist?")
    );

    if (!isConfirmed) return;

    deleteWordlist(activeWordlist);
  };

  openGenerateActivityGroupModal = () => {
    this.setState({ showGenerateModal: true });
  };

  closeGenerateActivityGroupModal = () => {
    this.setState({ showGenerateModal: false });
  };

  addWordlistHandler = async () => {
    const { createWordlist } = this.props;
    const newWordlist = await createWordlist();

    if (newWordlist) {
      this.setState({ activeWordlist: newWordlist });
    }
  };

  onNameChange = name => {
    const { editWordlist } = this.props;
    const { activeWordlist } = this.state;

    editWordlist({ ...activeWordlist, name });
  };

  onEdusChange = educationalCategories => {
    const { editWordlist } = this.props;
    const { activeWordlist } = this.state;

    editWordlist({ ...activeWordlist, educationalCategories });
  };

  removeFromMyWordlists = async () => {
    const { editWordlist } = this.props;
    const { activeWordlist } = this.state;

    editWordlist({ ...activeWordlist, inEditList: "" });
  };

  removeAllFromMyList = () => {
    const { removeFromMyWordlists, showConfirm, translate } = this.props;
    const { filteredWordlists } = this.state;

    const wordlistsToRemoveFromList = filteredWordlists
      .filter(validateWordlist)
      .map(w => w._id);

    showConfirm({
      message: translate(
        539,
        "You sure you want to remove all wordlists from you list?",
        true
      ),
      cb: isConfirmed => {
        isConfirmed && removeFromMyWordlists(wordlistsToRemoveFromList);
      }
    });
  };

  onImportWordsHandler = async words => {
    const { addWordsToWordlist, showError, translate } = this.props;
    const { activeWordlist } = this.state;

    if (words.length > 20) {
      return showError({ message: "Max words count is 20." });
    }

    const result = await addWordsToWordlist({
      words,
      locale: "en",
      wordlist: activeWordlist
    });

    if (!result.success) return;

    const { wordsNotInDB } = result;

    if (wordsNotInDB && wordsNotInDB.length) {
      showError({
        message: `${translate(275, "Next words are not in DB:")}
        ${wordsNotInDB.join(", ")}`
      });
    }
  };

  render() {
    const { filteredWordlists, activeWordlist, showGenerateModal } = this.state;
    const { translate, undoDeleteWordlist, wordlistsToDelete } = this.props;

    return (
      <div className="flink-make__inner">
        <MainNav />

        <div className="content-wrapper">
          <Help>{translate(611)}</Help>

          <Header />

          {filteredWordlists ? (
            <div className="main-row">
              <div className="main-row__col" style={{ width: "30%" }}>
                <h4>{translate(379, "My Word Lists")}</h4>
                <div className="glass-wrapper glass-wrapper--full-height">
                  <ActionsPanel
                    buttons={[
                      {
                        title: translate(11, "Add", true),
                        handler: this.addWordlistHandler,
                        icon: "add.png"
                      },
                      {
                        title: translate(96, "Delete", true),
                        handler: this.deleteWordlistHandler,
                        icon: "delete.png",
                        disabled: !activeWordlist
                      },
                      {
                        title: translate(268, "Undo Delete", true),
                        handler: undoDeleteWordlist,
                        icon: "undodelete.png",
                        disabled: !wordlistsToDelete.length
                      },
                      {
                        title: translate(387, "Remove from My Wordlists", true),
                        handler: this.removeFromMyWordlists,
                        icon: "remove_small.png",
                        disabled:
                          !activeWordlist || !validateWordlist(activeWordlist)
                      },
                      {
                        title: translate(
                          655,
                          "Remove all from My Wordlists",
                          true
                        ),
                        handler: this.removeAllFromMyList,
                        icon: "removeall_small.png",
                        disabled:
                          !filteredWordlists || !filteredWordlists.length
                      }
                    ]}
                  />
                  <DataList
                    data={filteredWordlists}
                    clickHandler={this.setActiveWordlist}
                    active={activeWordlist}
                    showName={w => w.name}
                    validate={validateWordlist}
                    big
                  />
                </div>
              </div>

              <div className="main-row__col" style={{ width: "30%" }}>
                <h4>&nbsp;</h4>
                <div className="glass-wrapper glass-wrapper--full-height">
                  {activeWordlist && (
                    <Fragment>
                      <div>
                        <label className={classes.label}>
                          {translate(183, "Name:")}
                        </label>
                        <input
                          value={activeWordlist.name || ""}
                          onChange={e => this.onNameChange(e.target.value)}
                          type="text"
                          className={classes.nameField}
                        />
                      </div>

                      <fieldset className={classes.form__fieldset}>
                        <legend>
                          {translate(396, "Select the educational categories")}
                        </legend>
                        <SelectEducationalCategory
                          oneSet
                          disableMultiple
                          current={
                            (activeWordlist &&
                              activeWordlist.educationalCategories) ||
                            []
                          }
                          changeHandler={changedEdus => {
                            if (
                              activeWordlist &&
                              changedEdus !==
                                activeWordlist.educationalCategories
                            ) {
                              this.onEdusChange(changedEdus);
                            }
                          }}
                        />
                      </fieldset>

                      {validateWordlist(activeWordlist) && (
                        <Fragment>
                          <button
                            className={classes.generateBtn}
                            onClick={this.openGenerateActivityGroupModal}
                          >
                            <span>
                              {translate(141, "Generate Activity Group")}
                            </span>
                            <img
                              src={`${imagesURL}/FlinkMake/ActivityGroup.png`}
                              alt=""
                            />
                          </button>
                          <DraggableModal
                            show={showGenerateModal}
                            onClose={this.closeGenerateActivityGroupModal}
                            title={translate(141, "Generate Activity Group")}
                            withOverlay
                          >
                            <GenerateActivityGroupModal
                              wordlist={activeWordlist}
                            />
                          </DraggableModal>
                        </Fragment>
                      )}
                    </Fragment>
                  )}
                </div>
              </div>

              <div className="main-row__col" style={{ width: "40%" }}>
                <h4>{translate(405, "Words")}</h4>
                <div className="glass-wrapper glass-wrapper--full-height">
                  {activeWordlist && (
                    <WordsList
                      currentWords={activeWordlist.words || []}
                      onImport={this.onImportWordsHandler}
                    />
                  )}
                </div>
              </div>
            </div>
          ) : (
            <span>Fetching wordlists...</span>
          )}
        </div>
      </div>
    );
  }
}

Wordlists.propTypes = {
  wordlists: PropTypes.array,
  translate: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  wordlistsToDelete: PropTypes.array.isRequired,

  editWordlist: PropTypes.func.isRequired,
  fetchMyWordlists: PropTypes.func.isRequired,
  createWordlist: PropTypes.func.isRequired,
  deleteWordlist: PropTypes.func.isRequired,
  addWordsToWordlist: PropTypes.func.isRequired,
  undoDeleteWordlist: PropTypes.func.isRequired,
  removeFromMyWordlists: PropTypes.func.isRequired,
  saveWordlistChanges: PropTypes.func.isRequired,
  showConfirm: PropTypes.func.isRequired
};

const mapStateToProps = ({ flinkMake: { wordlists }, auth }) => ({
  wordlists: wordlists.list,
  userId: auth.user._id,
  wordlistsToDelete: wordlists.wordlistsToDelete
});

export default compose(
  connect(mapStateToProps, {
    showError,
    editWordlist,
    fetchMyWordlists,
    createWordlist,
    deleteWordlist,
    addWordsToWordlist,
    undoDeleteWordlist,
    saveWordlistChanges,
    showConfirm,
    removeFromMyWordlists
  }),
  withTranslate
)(Wordlists);
