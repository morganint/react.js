import React from 'react';
import { TemplateWrapper } from "components/flink-play";

const Preview = ({ match }) => {
  const { activityId } = match.params;

  return activityId && <TemplateWrapper activity={activityId} preview />;
};

export default Preview;
