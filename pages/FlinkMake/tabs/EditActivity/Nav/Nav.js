import React from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';

import { withTranslate } from 'components/hocs';

const Nav = ({ translate, match, activityValid }) => {
  const { activityId } = match.params;

  return (
    <nav className="main-nav">
      <NavLink
        to={`/flink-make/edit-activities/${activityId}/content`}
        className="main-nav__link"
      >
        {translate(89, 'Content')}
      </NavLink>

      <NavLink
        to={`/flink-make/edit-activities/${activityId}/help`}
        className="main-nav__link"
      >
        {translate(66, 'Help')}
      </NavLink>

      <NavLink
        to={`/flink-make/edit-activities/${activityId}/options`}
        className="main-nav__link"
      >
        {translate(382, 'Options')}
      </NavLink>

      {activityValid ? (
        <NavLink
          to={`/flink-make/edit-activities/${activityId}/preview`}
          className="main-nav__link"
        >
          {translate(202, 'Preview')}
        </NavLink>
      ) : (
        <span className="main-nav__link disabled">
          {translate(202, 'Preview')}
        </span>
      )}

      <Link to="/flink-make/edit-activities" className="main-nav__link">
        {translate(367, 'Done')}
      </Link>
    </nav>
  );
};

export default withRouter(withTranslate(Nav));
