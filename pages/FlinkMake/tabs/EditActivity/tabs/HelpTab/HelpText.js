import React from 'react';

export default () => (
  <dl>
    <dd>
      Use Help to tell children the purpose of the activity and how to use the
      activity.
    </dd>
    <dt>DEFAULT TEXT:</dt>
    <dd>
      The program inserts default text in the text box in the center of the
      screen.
    </dd>
    <dt>WRITE TEXT:</dt>
    <dd>You can edit the default text or write completely new text.</dd>
    <dt>FORMAT TEXT:</dt>
    <dd>
      Use the Format toolbar to change the font, size, style, color, alignment,
      or line spacing of your text.
    </dd>
    <dt>IMPORT/RECORD AUDIO:</dt>
    <dd>
      You can import audio created outside of the program or record your own. If
      you want to associate an audio file the instructions, click the ADD AUDIO
      button. Click Choose File to import an audio file. Click Start to record
      your own.
    </dd>
  </dl>
);
