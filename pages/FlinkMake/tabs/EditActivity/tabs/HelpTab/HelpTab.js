import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";
import _ from "lodash";

import {
  changeActivityData,
  removeContentFiles
} from "actions/flinkMakeActions/activity";
import {
  Help,
  ActivityAddAudio,
  Editor,
  MultiLocaleControls
} from "components/flink-components";
import classes from "./HelpTab.module.scss";

class HelpTab extends Component {
  constructor(props) {
    super(props);

    const { activity } = props;
    const { multiLocaleHelp } = activity.data;

    this.state = {
      selectedLocale: multiLocaleHelp ? multiLocaleHelp.defaultLocale : "",
      showEditor: true
    };
  }

  componentDidUpdate(prevProps) {
    const { activity } = this.props;

    const { multiLocaleHelp } = activity.data;

    if (!!multiLocaleHelp !== !!prevProps.activity.data.multiLocaleHelp) {
      this.reloadEditor();
    }
  }

  componentWillUnmount() {
    this.props.saveHandler();
  }

  makeMultiHandler = currentLocale => {
    const { changeActivityData, activity } = this.props;
    const { helpHtml, helpAudio } = activity.data;

    this.setState({
      selectedLocale: currentLocale
    });

    changeActivityData("helpHtml", "");
    changeActivityData("helpAudio", "");

    this.changeMultiLocaleHelp({
      [currentLocale]: {
        helpAudio,
        helpHtml
      },
      defaultLocale: currentLocale
    });
  };

  makeSingleHandler = locale => {
    const { changeActivityData, activity, removeContentFiles } = this.props;
    const { multiLocaleHelp } = activity.data;

    const audioFilesToDelete = _.chain(multiLocaleHelp)
      .map((val, key) => {
        if (key === "defaultLocale" || key === locale) return null;
        return val && val.helpAudio;
      })
      .compact()
      .value();

    // Add audio files to remove list
    removeContentFiles(audioFilesToDelete);

    const helpHtml =
      multiLocaleHelp[locale] && multiLocaleHelp[locale].helpHtml;
    const helpAudio =
      multiLocaleHelp[locale] && multiLocaleHelp[locale].helpAudio;

    changeActivityData("helpHtml", helpHtml || "");
    changeActivityData("helpAudio", helpAudio || "");
    this.changeMultiLocaleHelp(null);
  };

  reloadEditor = () => {
    this.setState({ showEditor: false }, () => {
      this.setState({ showEditor: true });
    });
  };

  changeHelp = (type, data) => {
    const { selectedLocale } = this.state;

    const { changeActivityData, activity } = this.props;

    const { multiLocaleHelp } = activity.data;

    if (multiLocaleHelp) {
      if (!selectedLocale) {
        return alert("No selected locale");
      }

      const dataToSet = {
        ...multiLocaleHelp,
        [selectedLocale]: { ...multiLocaleHelp[selectedLocale], [type]: data }
      };
      this.changeMultiLocaleHelp(dataToSet);
    } else {
      changeActivityData(type, data);
    }
  };

  changeLocaleHandler = locale => {
    this.setState({ selectedLocale: locale }, this.reloadEditor);
  };

  changeMultiLocaleHelp = data => {
    const { changeActivityData } = this.props;

    changeActivityData("multiLocaleHelp", data);
  };

  setCurrentLocaleAsDefault = () => {
    const { selectedLocale } = this.state;
    const {
      activity: { data }
    } = this.props;

    this.changeMultiLocaleHelp({
      ...data.multiLocaleHelp,
      defaultLocale: selectedLocale
    });
  };

  render() {
    const { showEditor, selectedLocale } = this.state;

    const {
      activity,
      translate
      // currentLang,
    } = this.props;

    const { helpHtml, helpAudio, multiLocaleHelp } = activity.data;

    const currentLocaleHelp =
      multiLocaleHelp && (multiLocaleHelp[selectedLocale] || {});

    const currentHtmlHelp = currentLocaleHelp
      ? currentLocaleHelp.helpHtml
      : helpHtml;
    const currentAudioHelp = currentLocaleHelp
      ? currentLocaleHelp.helpAudio
      : helpAudio;

    return (
      <>
        <Help>{translate(581)}</Help>

        <div className={classes.wrapper}>
          <MultiLocaleControls
            selectedLocale={selectedLocale}
            multiLocaleData={multiLocaleHelp}
            changeLocaleHandler={this.changeLocaleHandler}
            makeDefaultHandler={this.setCurrentLocaleAsDefault}
            makeSingleHandler={this.makeSingleHandler}
            makeMultiHandler={this.makeMultiHandler}
            makeSingleDialogText={translate(
              272,
              "Which help to use as single? Warning all other helps will be deleted including audio files"
            )}
            makeMultiDialogText={translate(
              261,
              "Select language of current help"
            )}
            classes={{
              wrapper: classes.multiLocaleControlsWrapper,
              button: classes.localeButton
            }}
          />

          <div className={classes.editorWrapper}>
            {showEditor && (
              <Editor
                className={classes.editor}
                initialValue={currentHtmlHelp || ""}
                changeHandler={html => this.changeHelp("helpHtml", html)}
              />
            )}
          </div>

          <ActivityAddAudio
            // filename="ActivityHelp"
            current={currentAudioHelp}
            onChangeHandler={filename => {
              this.changeHelp("helpAudio", filename);
            }}
          />
        </div>
      </>
    );
  }
}

HelpTab.propTypes = {
  activity: PropTypes.object.isRequired,
  helpHtml: PropTypes.string,
  currentLang: PropTypes.string.isRequired,
  changeActivityData: PropTypes.func.isRequired,
  removeContentFiles: PropTypes.func.isRequired
};

const mapStateToProps = ({ status, common }) => ({
  translate: status.translate,
  currentLang: status.lang.name
});

export default compose(
  connect(mapStateToProps, {
    changeActivityData,
    removeContentFiles
  })
)(HelpTab);
