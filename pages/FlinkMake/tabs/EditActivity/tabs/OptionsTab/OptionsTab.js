import React, { Fragment, useMemo, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { compose } from 'redux';

import { changeOptions } from 'actions/flinkMakeActions';
import { Help, ActivityOptions } from 'components/flink-components';
import { withTranslate } from 'components/hocs';
import ActivityTemplates from 'activity-templates';

const OptionsTab = ({
  saveHandler,
  editingActivity,
  changeOptions,
  translate
}) => {
  const {
    data: { options },
    template: { alias: templateAlias, helpStrings, defaultOptions }
  } = editingActivity;

  useEffect(() => saveHandler, [saveHandler]);

  const optionsToRender = useMemo(() => {
    const TemplateData = ActivityTemplates[templateAlias];

    if (!TemplateData || !TemplateData.optionsArray) {
      console.error('template not found');
      return null;
    }

    return TemplateData.optionsArray;
  }, [templateAlias]);

  return (
    <Fragment>
      <Help>{translate(helpStrings.optionsTab)}</Help>
      <StyledWrapper>
        <StyledInner>
          <ActivityOptions
            options={options}
            changeHandler={changeOptions}
            optionsToRender={optionsToRender}
            defaultOptions={defaultOptions}
          />
        </StyledInner>
      </StyledWrapper>
    </Fragment>
  );
};

OptionsTab.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeOptions: PropTypes.func.isRequired
};

const mapStateToProps = ({ flinkMake }) => ({
  editingActivity: flinkMake.activity.editingActivity
});

export default compose(
  connect(
    mapStateToProps,
    { changeOptions }
  ),
  withTranslate
)(OptionsTab);

const StyledWrapper = styled.div`
  flex-grow: 1;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  padding-bottom: 20px;
`;

const StyledInner = styled.div`
  margin-top: 30px;
  background-color: white;
  padding: 40px;
  border: 2px solid black;
  border-radius: 10px;
  box-shadow: inset 0 0 20px #000;
  width: 80%;
`;
