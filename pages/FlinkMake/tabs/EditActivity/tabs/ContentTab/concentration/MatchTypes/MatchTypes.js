import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

import classes from './MatchTypes.module.scss';

const matchTypeOptions = ['text', 'audio', 'image'];

const setHandler = (currentMatchTypes, idx, prop, val, changeFn) => {
  const currentMatchType = currentMatchTypes[idx] || {};

  // If true then outer function will trigger saveActivity which
  // will clear deleted matches (problems)
  const isExistingTypeWasChanged = prop === 'type' && !!currentMatchType[prop];

  currentMatchType[prop] = val;

  const copyMatchTypes = [...currentMatchTypes];
  copyMatchTypes[idx] = currentMatchType;

  changeFn(copyMatchTypes, isExistingTypeWasChanged);
};

const MatchTypes = ({ gameData, translate, onChange }) => {
  const { matchTypes = [], problems = [] } = gameData;
  const isCommonFieldAlreadyExist = !!matchTypes.filter(m => m.isCommon).length;

  return (
    <div className={classes.matchTypeWrapper}>
      {_.range(3).map(idx => {
        const currentMatch = matchTypes[idx] || {};
        const prevMatch = matchTypes[idx - 1];
        const nextMatch = matchTypes[idx + 1];
        const notFirst = idx !== 0;
        const last = idx === matchTypes.length - 1;

        return (
          <div className={classes.matchTypeBlock} key={idx}>
            <span>{idx + 1}</span>
            <div className={'glass-wrapper ' + classes.matchTypeFields}>
              <label>{translate(215, 'Title')}</label>
              <input
                value={currentMatch.title || ''}
                disabled={notFirst && (!prevMatch || !prevMatch.type)}
                onChange={e => {
                  const { value } = e.target;
                  setHandler(matchTypes, idx, 'title', value, onChange);
                }}
                type="text"
              />
              <label>{translate(453, 'Type')}</label>
              <select
                disabled={
                  (notFirst && (!prevMatch || !prevMatch.type)) ||
                  (!!problems.length && currentMatch.type)
                }
                value={currentMatch.type || ''}
                onChange={e => {
                  const { value } = e.target;
                  setHandler(matchTypes, idx, 'type', value, onChange);
                }}
              >
                {(last || !nextMatch || !nextMatch.type) && (
                  <option value=""></option>
                )}
                {matchTypeOptions.map(opt => (
                  <option key={opt} value={opt}>
                    {opt}
                  </option>
                ))}
              </select>

              {(currentMatch.isCommon ||
                (currentMatch.type === 'text' &&
                  !isCommonFieldAlreadyExist)) && (
                <label>
                  <input
                    type="checkbox"
                    checked={!!currentMatch.isCommon}
                    onChange={e => {
                      const { checked } = e.target;
                      setHandler(
                        matchTypes,
                        idx,
                        'isCommon',
                        checked,
                        onChange
                      );
                    }}
                    disabled={notFirst && (!prevMatch || !prevMatch.type)}
                  />
                  {translate(87, 'Common')}
                </label>
              )}
            </div>
          </div>
        );
      })}
    </div>
  );
};

MatchTypes.propTypes = {
  onChange: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  gameData: PropTypes.object.isRequired
};

export default MatchTypes;
