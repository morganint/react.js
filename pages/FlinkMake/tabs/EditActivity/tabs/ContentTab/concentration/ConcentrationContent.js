import React, { useState, useMemo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { changeGameData, saveEditingActivity } from 'actions/flinkMakeActions';
import { ProblemList } from 'pages/FlinkMake/interface';
import { SelectItemBlock, ItemConfigurator } from 'components/flink-components';
import {
  validateProblem,
  createProblem,
  getContentFilesFromProblem
} from 'activity-templates/concentration/concentrationHelpers';
import { withTranslate } from 'components/hocs';
import MatchTypes from './MatchTypes/MatchTypes';
import CardsPerLevel from './CardsPerLevel/CardsPerLevel';

const ConcentrationContent = ({
  translate,
  saveHandler,
  deletedProblems,
  changeGameData,
  editingActivity
}) => {
  const [activeProblemId, changeActiveProblemId] = useState();
  const [selectedItemPath, changeSelectedItemPath] = useState('items[0]');

  const changeCardsPerLevel = useMemo(
    () => val => {
      changeGameData('cardsPerLevel', val);
    },
    [changeGameData]
  );

  const {
    data: { gameData }
  } = editingActivity;
  const { problems, matchTypes = [] } = gameData;

  const activeProblemIdx = useMemo(() => {
    return _.findIndex(problems, { id: activeProblemId });
  }, [activeProblemId, problems]);

  const activeProblem = problems[activeProblemIdx];
  const activeItem = _.get(activeProblem, selectedItemPath);

  const matchesTypesCount = matchTypes.filter(m => !!m.type).length;

  const [, selectedItemIdx] = selectedItemPath.match(/\[(\d+)\]/);
  const currentMatch = matchTypes[selectedItemIdx] || {};

  return (
    <>
      <div className="main-row">
        <ProblemList
          notShouldCreate={matchesTypesCount < 2}
          createProblem={createProblem}
          problemsPath="problems"
          validate={validateProblem.bind(null, matchTypes)}
          title={translate(50, 'Match List')}
          dontCreateProblem
          namePrefix={'Match:'}
          getContentFilesFromProblem={getContentFilesFromProblem}
          activeProblemId={activeProblemId}
          onChangeActiveProblem={changeActiveProblemId}
        />

        <div
          className="main-row__col"
          style={{ width: '300px', flexShrink: 0 }}
        >
          <div className="main-row__col-inner">
            <h4>1. {translate(54, 'Match Types')}</h4>
            <MatchTypes
              gameData={gameData}
              translate={translate}
              onChange={(newMatchTypes, isExistingTypeWasChanged) => {
                // Clear deleted problems if existing and if match type was changed
                isExistingTypeWasChanged &&
                  deletedProblems.length &&
                  saveHandler();

                changeGameData('matchTypes', newMatchTypes);
              }}
            />
          </div>
        </div>

        <div className="main-row__col">
          <div className="glass-wrapper" style={{ height: '300px' }}>
            <h4>{'2. ' + translate(88, 'Make Matches')}</h4>

            {activeProblem && (
              <SelectItemBlock
                selectedItem={selectedItemPath}
                activeProblem={activeProblem}
                pathPrefix="items"
                count={matchesTypesCount}
                selectHandler={changeSelectedItemPath}
                activeProblemIdx={activeProblemIdx}
              />
            )}

            {activeProblem && selectedItemPath && (
              <ItemConfigurator
                withoutText={currentMatch.type !== 'text'}
                withoutAudio={currentMatch.type !== 'audio'}
                withoutImage={currentMatch.type !== 'image'}
                activeProblemIdx={activeProblemIdx}
                activeItem={activeItem}
                selectedItemPath={selectedItemPath}
                changeHandler={changeGameData}
              />
            )}
          </div>

          <div className="glass-wrapper" style={{ alignSelf: 'flex-start' }}>
            <h4>{'3. ' + translate(106, 'Cards per Level')}</h4>

            <CardsPerLevel
              gameData={gameData}
              translate={translate}
              onChange={changeCardsPerLevel}
            />
          </div>
        </div>
      </div>
    </>
  );
};

ConcentrationContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired,
  saveHandler: PropTypes.func.isRequired,
  deletedProblems: PropTypes.array.isRequired,
  saveEditingActivity: PropTypes.func.isRequired
};

const mapStateToProps = ({ flinkMake }) => ({
  deletedProblems: flinkMake.activity.deletedProblems
});

export default compose(
  withTranslate,
  connect(
    mapStateToProps,
    { changeGameData, saveEditingActivity }
  )
)(ConcentrationContent);
