import React, { useMemo, useEffect } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { validateProblem } from 'activity-templates/concentration/concentrationHelpers';

const possibleValues = [12, 16, 20, 24];

const setHandler = (currentValues, idx, val, changeFn) => {
  const copyCurrentValues = [...currentValues];
  copyCurrentValues[idx] = val;

  changeFn(copyCurrentValues);
};

const formatValuesToNotExceedMax = (values, max) =>
  values.map((v, idx, arr) => {
    if (v > max) return '';
    if (idx === 0 || !v) return v;

    const prev = arr[idx - 1];
    if (!prev || prev >= v) return '';

    return v;
  });

const CardsPerLevel = ({ gameData, translate, onChange }) => {
  const { problems: matches = [], matchTypes, cardsPerLevel = [] } = gameData;

  const validMatches = useMemo(
    () => matches.filter(validateProblem.bind(null, matchTypes)),
    [matches, matchTypes]
  );

  const cardsNumber = useMemo(() => validMatches.length * 2, [validMatches]);

  useEffect(() => {
    const formattedValues = formatValuesToNotExceedMax(
      cardsPerLevel,
      cardsNumber
    );

    if (!_.isEqual(cardsPerLevel, formattedValues)) {
      onChange(formattedValues);
    }
  }, [onChange, cardsPerLevel, cardsNumber]);

  const filteredValues = useMemo(
    () => possibleValues.filter(v => v <= cardsNumber),
    [cardsNumber]
  );

  return (
    <Wrapper>
      {_.range(3).map(idx => {
        const prevLevel = cardsPerLevel[idx - 1];
        const isFirst = idx === 0;

        const filteredOptions = isFirst
          ? filteredValues
          : prevLevel
          ? filteredValues.filter(v => v > prevLevel)
          : [];

        return (
          <div key={idx}>
            <label>{`${translate(107, 'Level')} ${idx + 1}`}</label>
            <select
              disabled={!isFirst && !prevLevel}
              value={cardsPerLevel[idx] || ''}
              onChange={e => {
                const { value } = e.target;
                setHandler(cardsPerLevel, idx, value && +value, onChange);
              }}
            >
              <option value="">N/A</option>
              {filteredOptions.map(v => (
                <option value={v} key={v}>
                  {v}
                </option>
              ))}
            </select>
          </div>
        );
      })}
    </Wrapper>
  );
};

CardsPerLevel.propTypes = {
  translate: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  gameData: PropTypes.object.isRequired
};

export default CardsPerLevel;

const Wrapper = styled.div`
  min-width: 250px;
  margin-top: 20px;

  label {
    display: block;
    margin-bottom: 5px;
  }

  select {
    margin-bottom: 10px;
    width: 100%;
  }
`;
