import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import { changeGameData } from 'actions/flinkMakeActions';
import { ProblemList, WordConfigurator } from 'pages/FlinkMake/interface';
import {
  validateProblem,
  createProblem,
  getContentFilesFromProblem,
} from 'activity-templates/spelling-keyboard/spellingKeyboardHelpers';
import { withTranslate } from 'components/hocs';

const SpellingKeyboardContent = ({
  translate,
  changeGameData,
  editingActivity,
}) => {
  const [activeProblemId, changeActiveProblemId] = useState();

  return (
    <Fragment>
      <div className="main-row">
        <ProblemList
          createProblem={createProblem}
          problemsPath="problems"
          getName={(problem) => problem.word}
          validate={validateProblem}
          title={translate(317, 'Word List')}
          getContentFilesFromProblem={getContentFilesFromProblem}
          activeProblemId={activeProblemId}
          onChangeActiveProblem={changeActiveProblemId}
        />
        <div className="main-row__col">
          <div className="main-row__col-inner">
            <WordConfigurator
              defaultFixCharacters={[' ', '-']}
              activeWordId={activeProblemId}
              activity={editingActivity}
              changeGameData={changeGameData}
            />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

SpellingKeyboardContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired,
};

export default compose(
  connect(null, { changeGameData }),
  withTranslate
)(SpellingKeyboardContent);
