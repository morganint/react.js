import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";
import _ from "lodash";

import { changeGameData } from "actions/flinkMakeActions";
import { showError, showInfo } from "actions/statusActions";
import {
  MultiLocaleInstructions,
  WordsList
} from "components/flink-components";
import { withTranslate } from "components/hocs";

class WordConnectablesContent extends Component {
  changeHandler = words => {
    const { changeGameData } = this.props;
    changeGameData("problems[0].words", words);
  };

  deleteAllWordsHandler = () => {
    this.changeHandler([]);
  };

  importHandler = words => {
    const { showError, showInfo } = this.props;

    // words in props are unique already but case-sensitive
    const uniqWords = _.uniqWith(
      words,
      (a, b) => a.toLowerCase() === b.toLowerCase()
    );

    // Filter words that are shorter than 3 characters and longer than 12
    const filteredWords = _.filter(
      uniqWords,
      w => w.length >= 3 && w.length <= 12
    );

    const incorrectWords = _.difference(uniqWords, filteredWords);

    let infoMessage = "";

    if (incorrectWords.length) {
      infoMessage += `<div style="text-align: left">
        <p>Min words length is 3 and max is 12.<br>
        <b>Following words are not imported:</b></p>
        <ul>
          ${incorrectWords.map(w => `<li>${w}</li>`).join("")}
        </ul>
      </div>`;
    }

    // Take only 20 words
    const wordsToInclude = _.take(filteredWords, 20);
    const notIncluded = _.slice(filteredWords, 20);

    if (notIncluded.length) {
      infoMessage += `<div style="text-align: left">
        <p>Max words count is 20.<br>
        <b>Following words are not imported:</b></p>
        <ul>
          ${notIncluded.map(w => `<li>${w}</li>`).join("")}
        </ul>
      </div>`;
    }

    if (infoMessage) {
      showInfo({
        html: true,
        message: infoMessage
      });
    }

    if (filteredWords.length < 3) {
      return showError({
        message: "A puzzle must include at least three words"
      });
    }
    this.changeHandler(wordsToInclude);
  };

  changeTitle = title => {
    const { changeGameData } = this.props;
    changeGameData("problems[0].title", title);
  };

  render() {
    const { translate, editingActivity } = this.props;

    const {
      data: {
        gameData: { problems }
      }
    } = editingActivity;

    const activeProblemIdx = 0;
    const problem = problems[activeProblemIdx] || {};

    return (
      <>
        <div className="main-row" style={{ marginTop: "20px" }}>
          <div
            className="glass-wrapper glass-wrapper--full-height"
            style={{ width: "40%" }}
          >
            <div style={{ marginBottom: "20px" }}>
              <label style={{ display: "block", marginBottom: "5px" }}>
                {translate(209, "Puzzle Title")}
              </label>
              <input
                onChange={e => this.changeTitle(e.target.value)}
                value={problem.title || ""}
                style={{ width: "350px", padding: "5px 10px" }}
                type="text"
              />
            </div>

            <MultiLocaleInstructions
              problemPath={`problems[${activeProblemIdx}]`}
            />
          </div>
          <div
            className="glass-wrapper glass-wrapper--full-height"
            style={{ width: "60%" }}
          >
            <WordsList
              onImport={this.importHandler}
              deleteAllHandler={this.deleteAllWordsHandler}
              currentWords={problem.words || []}
            />
          </div>
        </div>
      </>
    );
  }
}

WordConnectablesContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  showInfo: PropTypes.func.isRequired
};

export default compose(
  connect(null, { changeGameData, showError, showInfo }),
  withTranslate
)(WordConnectablesContent);
