import React, { useState, useMemo } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";
import _ from "lodash";

import { changeGameData } from "actions/flinkMakeActions";
import { ProblemList } from "pages/FlinkMake/interface";
import {
  MultiLocaleInstructions,
  SelectItemBlock,
  ItemConfigurator
} from "components/flink-components";
import {
  validateProblem,
  createProblem,
  getContentFilesFromProblem
} from "activity-templates/matching-definitions/matchingDefinitionsHelpers";
import { withTranslate } from "components/hocs";

const MatchingDefinitions = ({
  translate,
  changeGameData,
  editingActivity
}) => {
  const [activeProblemId, changeActiveProblemId] = useState();
  const [selectedItemPath, changeSelectedItemPath] = useState("questions[0]");

  const {
    data: {
      gameData: { problems }
    }
  } = editingActivity;

  const activeProblemIdx = useMemo(() => {
    return _.findIndex(problems, { id: activeProblemId });
  }, [activeProblemId, problems]);

  const activeProblem = problems[activeProblemIdx];
  const activeItem = _.get(activeProblem, selectedItemPath);

  return (
    <>
      <div className="main-row">
        <ProblemList
          createProblem={createProblem}
          problemsPath="problems"
          validate={validateProblem}
          title={translate(205, "Problem List")}
          getContentFilesFromProblem={getContentFilesFromProblem}
          activeProblemId={activeProblemId}
          onChangeActiveProblem={changeActiveProblemId}
        />

        <div className="glass-wrapper glass-wrapper--full-height">
          <MultiLocaleInstructions
            problemPath={`problems[${activeProblemIdx}]`}
          />

          <ItemConfigurator
            activeProblemIdx={activeProblemIdx}
            activeItem={activeItem}
            selectedItemPath={selectedItemPath}
            changeHandler={changeGameData}
            withoutImage
            textAreaWidth="500px"
            maxLetters={_.startsWith(selectedItemPath, "questions") ? 300 : 30}
          />

          <SelectItemBlock
            selectedItem={selectedItemPath}
            activeProblem={activeProblem}
            pathPrefix="questions"
            count={4}
            label={translate(213, "Questions:")}
            selectHandler={changeSelectedItemPath}
            activeProblemIdx={activeProblemIdx}
            width="22%"
            nextSetPathPrefix="correctAnswers"
            verticalTab
          />
          <SelectItemBlock
            selectedItem={selectedItemPath}
            activeProblem={activeProblem}
            pathPrefix="correctAnswers"
            count={4}
            label={translate(518, "Correct Answers:")}
            selectHandler={changeSelectedItemPath}
            activeProblemIdx={activeProblemIdx}
            width="22%"
            nextSetPathPrefix="questions"
            lastBlock
            verticalTab
          />
        </div>
      </div>
    </>
  );
};

MatchingDefinitions.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired
};

export default compose(
  withTranslate,
  connect(null, { changeGameData })
)(MatchingDefinitions);
