import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import _ from "lodash";
import ReactPlayer from "react-player";

import { getSorting, addIdToFilename } from "utils";

import {
  createProblem,
  createText,
  copyProblem,
  validateProblem,
  getContentFilesFromProblem
} from "activity-templates/ebook/ebookHelpers";
import { EBOOK_WIDTH, EBOOK_HEIGHT } from "consts/ebook-consts";
import {
  AddImageBlock,
  AddVideoBlock,
  ActivityAddAudio,
  EbookEditor
} from "components/flink-components";
import { ProblemList } from "pages/FlinkMake/interface";
import { changeGameData, saveEditingActivity } from "actions/flinkMakeActions";
import { showError } from "actions/statusActions";
import { uploadFiles } from "actions/commonActions";
import { imagesURL, s3bucketPublicURL, activitiesKey } from "config";
import classes from "./EbookContent.module.scss";

const MAX_PAGES = 50;
const MAX_IMAGE_SIZE = 200 * 1000; // 200kb
const MAX_VIDEO_SIZE = 20 * 1000 * 1000; // 20mb

export class EbookContent extends Component {
  state = {
    activeProblemId: "",
    activeProblemIdx: ""
  };

  fileInput = React.createRef();

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.editingActivity.data.gameData !==
        this.props.editingActivity.data.gameData ||
      prevState.activeProblemId !== this.state.activeProblemId
    ) {
      this.setActiveProblemIdx();
    }
  }

  setActiveProblemIdx = () => {
    const { editingActivity } = this.props;
    const { activeProblemId } = this.state;
    const { problems } = editingActivity.data.gameData;
    const activeProblemIdx = _.findIndex(problems, { id: activeProblemId });

    this.setState({ activeProblemIdx });
  };

  changeActiveProblemId = activeProblemId => this.setState({ activeProblemId });

  addTextBlock = () => {
    const { changeGameData } = this.props;
    const { activeProblemIdx } = this.state;
    const activeProblem = this.getActiveProblem();

    const activeProblemText = activeProblem.text;
    const newText = createText();

    changeGameData(`problems[${activeProblemIdx}].text`, [
      ...activeProblemText,
      newText
    ]);

    this.setEditorInFocus(newText.id);
  };

  getActiveProblem = () => {
    const { editingActivity } = this.props;
    const { activeProblemIdx } = this.state;
    const { problems } = editingActivity.data.gameData;

    return problems[activeProblemIdx];
  };

  removeTextHandler = id => {
    const { changeGameData } = this.props;
    const { activeProblemIdx } = this.state;
    const activeProblem = this.getActiveProblem();

    const modifiedProblemText = activeProblem.text.filter(t => t.id !== id);

    changeGameData(`problems[${activeProblemIdx}].text`, modifiedProblemText);
  };

  setEditorInFocus = editorInFocusId => {
    this.setState({ editorInFocusId });
  };

  importHandler = () => {
    this.fileInput.current.click();
  };

  onFileInput = e => {
    const {
      translate,
      editingActivity,
      changeGameData,
      saveEditingActivity,
      showError,
      uploadFiles
    } = this.props;
    const { problems } = editingActivity.data.gameData;
    const { files } = e.target;

    if (!files || !files.length) return;

    const pagesLeft = MAX_PAGES - problems.length;

    if (files.length > pagesLeft) {
      this.fileInput.current.value = "";

      return showError({
        message: translate(
          0,
          `<p>
            Max pages count is ${MAX_PAGES}, and you already have ${problems.length}. <br/> 
            Please select less number of images to import
          </p>`
        ),
        html: true
      });
    }

    const bigImages = _.filter(files, f => f.size > MAX_IMAGE_SIZE);

    if (bigImages.length) {
      this.fileInput.current.value = "";

      return showError({
        message: translate(
          0,
          `<p>Max image size is 200kb. Please re-check your images</p>`
        ),
        html: true
      });
    }

    const gameDataFolder = `${activitiesKey}/${editingActivity.activity._id}/gamedata`;

    const data = _.map(files, file => {
      const filename = addIdToFilename(file.name);
      const key = `${gameDataFolder}/${filename}`;

      return { key, file, filename };
    });

    uploadFiles(data).then(results => {
      const newProblems = [];

      results.forEach((isSuccess, idx) => {
        if (isSuccess) {
          const { filename } = data[idx];
          const problem = createProblem();
          problem.image = filename;
          newProblems.push(problem);
        }
      });

      const sortedNewProblems = newProblems.sort(getSorting("asc", "image"));
      let changedProblems;

      if (
        problems &&
        problems.length === 1 &&
        (!problems[0].text || problems[0].text.length === 0) &&
        !problems[0].image &&
        !problems[0].audio
      ) {
        changedProblems = sortedNewProblems;
      } else {
        changedProblems = [...problems, ...sortedNewProblems];
      }

      changeGameData("problems", changedProblems);
      saveEditingActivity();
      this.fileInput.current.value = "";
    });
  };

  render() {
    const { translate, editingActivity, changeGameData } = this.props;
    const { activeProblemId, activeProblemIdx, editorInFocusId } = this.state;
    const {
      problems,
      benchmark = "",
      totalWords = ""
    } = editingActivity.data.gameData;

    const activeProblem = problems[activeProblemIdx] || {};

    const { image, video, audio, text = [] } = activeProblem;
    const currentProblemPath = `problems[${activeProblemIdx}]`;

    const gameDataFolder = `${s3bucketPublicURL}/${editingActivity.activity.contentFolder}/gamedata`;

    return (
      <div className="main-row">
        <input
          type="file"
          ref={this.fileInput}
          hidden
          onChange={this.onFileInput}
          accept=".jpg, .jpeg, .png, .gif"
          multiple
        />

        <ProblemList
          namePrefix="Page:"
          copyProblem={copyProblem}
          notShouldCreate={problems.length === MAX_PAGES}
          createProblem={createProblem}
          problemsPath="problems"
          validate={validateProblem}
          title={translate(428, "Page List")}
          getContentFilesFromProblem={getContentFilesFromProblem}
          activeProblemId={activeProblemId}
          onChangeActiveProblem={this.changeActiveProblemId}
          importButton={{
            handler: this.importHandler,
            disabled: problems.length === MAX_PAGES
          }}
        />

        <div className="glass-wrapper glass-wrapper--full-height">
          <div className={classes.topControls}>
            <div className={classes.addImageBlock}>
              <AddImageBlock
                disabled={!!video}
                maxSize={MAX_IMAGE_SIZE}
                withId
                withOverlay
                withoutPreview
                pathToProp={`gameData.problems[${activeProblemIdx}].image`}
              />
            </div>

            <div className={classes.addImageBlock}>
              <AddVideoBlock
                withId
                disabled={!!image}
                maxSize={MAX_VIDEO_SIZE}
                pathToProp={`gameData.problems[${activeProblemIdx}].video`}
              />
            </div>

            <ActivityAddAudio
              wrapperClassname={classes.addAudioBlock}
              current={audio}
              onChangeHandler={filename => {
                changeGameData(`${currentProblemPath}.audio`, filename);
              }}
            />

            <button
              disabled={!image && !video}
              onClick={this.addTextBlock}
              className={classes.addTextButton}
            >
              <span className={classes.label}>{translate(18, "Add Text")}</span>
              <img src={`${imagesURL}/FlinkMake/AddText.png`} alt="" />
            </button>

            <div className={classes.enterNumberBlock}>
              <span className={classes.label}>
                {translate(290, "Benchmark:")}
              </span>

              <input
                type="number"
                value={benchmark}
                onChange={e => {
                  const { value } = e.target;
                  changeGameData(`benchmark`, value);
                }}
              />
            </div>

            <div className={classes.enterNumberBlock}>
              <span className={classes.label}>
                {translate(292, "Total Words:")}
              </span>

              <input
                type="number"
                value={totalWords}
                onChange={e => {
                  const { value } = e.target;
                  changeGameData(`totalWords`, value);
                }}
              />
            </div>
          </div>

          <div
            className={classes.contentArea}
            style={{ width: EBOOK_WIDTH + "px", height: EBOOK_HEIGHT + "px" }}
            onBlur={this.setEditorInFocus}
            tabIndex="1"
          >
            {image && (
              <img
                className={classes.textImage}
                src={`${gameDataFolder}/${image}`}
                alt=""
              />
            )}

            {video && (
              <ReactPlayer
                playing
                width="100%"
                height="100%"
                url={`${gameDataFolder}/${video}`}
              />
            )}

            {text.map((t, idx) => {
              return (
                <EbookEditor
                  key={t.id}
                  data={t}
                  inFocus={t.id === editorInFocusId}
                  selectHandler={this.setEditorInFocus.bind(null, t.id)}
                  changeHandler={data => {
                    changeGameData(`${currentProblemPath}.text[${idx}]`, data);
                  }}
                  boundsSelector={"." + classes.contentArea}
                  removeHandler={this.removeTextHandler.bind(null, t.id)}
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

EbookContent.propTypes = {
  translate: PropTypes.func.isRequired,
  changeGameData: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  uploadFiles: PropTypes.func.isRequired,
  saveEditingActivity: PropTypes.func.isRequired,
  editingActivity: PropTypes.object.isRequired
};

const mapStateToProps = ({ status: { translate } }) => ({ translate });

export default connect(mapStateToProps, {
  changeGameData,
  showError,
  saveEditingActivity,
  uploadFiles
})(EbookContent);
