import React, { useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import { changeGameData } from 'actions/flinkMakeActions';
import {
  FlinkMakeActivityImage,
  ProblemList,
  QnAForm,
} from 'pages/FlinkMake/interface';
import {
  addMediaToProblems,
  validateQuestion,
  createQuestion,
  getContentFilesFromProblem,
  createProblemFromTextImport,
} from 'activity-templates/moving-answers/movingAnswersHelpers';
import { ImportProblems } from 'components/flink-components';
import { withTranslate } from 'components/hocs';
import GameOptions from './GameOptions/GameOptions';

const MovingAnswers = ({ translate, changeGameData, editingActivity }) => {
  const [isImportOpened, setIsImportOpened] = useState(false);
  const [activeProblemId, changeActiveProblemId] = useState();

  const {
    activity: { _id: activityId },
    data: { gameData },
  } = editingActivity;

  return (
    <>
      <ImportProblems
        show={isImportOpened}
        activityId={activityId}
        addMediaToProblemsFn={addMediaToProblems}
        createProblemFn={createProblemFromTextImport}
        onImport={(newQuestions) => {
          changeGameData('problems', [...gameData.problems, ...newQuestions]);
          setIsImportOpened(false);
        }}
        onClose={() => {
          setIsImportOpened(false);
        }}
      />
      <GameOptions changeHandler={changeGameData} activity={editingActivity} />

      <div className="glass-wrapper">
        <FlinkMakeActivityImage />
      </div>

      <div className="main-row">
        <ProblemList
          createProblem={createQuestion}
          problemsPath="problems"
          getName={(item, idx) =>
            !item.problem && item.audio ? `Problem ${idx + 1}` : item.problem
          }
          validate={validateQuestion}
          title={translate(211, 'Question List')}
          getContentFilesFromProblem={getContentFilesFromProblem}
          activeProblemId={activeProblemId}
          onChangeActiveProblem={changeActiveProblemId}
          importButton={{
            handler: () => setIsImportOpened(true),
            disabled: gameData.useImages,
          }}
        />
        <div className="main-row__col">
          <div className="main-row__col-inner">
            <QnAForm
              problemsPath="problems"
              incorrectNumber={gameData.longAnswers ? 3 : 4}
              activeProblemId={activeProblemId}
            />
          </div>
        </div>
      </div>
    </>
  );
};

MovingAnswers.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired,
};

export default compose(
  connect(null, { changeGameData }),
  withTranslate
)(MovingAnswers);
