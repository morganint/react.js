import React from 'react';
import PropTypes from 'prop-types';
import classes from './GameOptions.module.scss';
import { withTranslate } from 'components/hocs';

const checkIfFilledProblemsExists = problems => {
  let exist = false;

  problems.forEach(problem => {
    if (exist) return;

    if (problem.problem) {
      return (exist = true);
    }

    if (
      problem.correct.text ||
      problem.correct.image ||
      problem.correct.audio
    ) {
      return (exist = true);
    }

    problem.incorrectAnswers.forEach(answer => {
      if (answer && (answer.text || answer.image || answer.audio)) {
        return (exist = true);
      }
    });
  });

  return exist;
};

const checkIfAnswersExists = problems => {
  let exist = false;

  problems.forEach(problem => {
    if (exist) return;

    if (
      problem.correct.text ||
      problem.correct.image ||
      problem.correct.audio
    ) {
      return (exist = true);
    }

    problem.incorrectAnswers.forEach(answer => {
      if (answer && (answer.text || answer.image || answer.audio)) {
        return (exist = true);
      }
    });
  });

  return exist;
};

const GameOptions = ({ changeHandler, activity, translate }) => {
  const {
    data: { gameData }
  } = activity;

  const { problems } = gameData;

  const problemsExists = checkIfFilledProblemsExists(problems);
  const answersExists = checkIfAnswersExists(problems);

  return (
    <div className={classes.gameOptions}>
      <div className={classes.gameOptionsGroup}>
        <input
          type="checkbox"
          disabled={problemsExists || gameData.useImages}
          id="game-options-long"
          checked={gameData.longAnswers || false}
          onChange={e => changeHandler('longAnswers', e.target.checked)}
        />
        <label htmlFor="game-options-long">
          {translate(488, 'Long Answers')}
        </label>
      </div>
      <div className={classes.gameOptionsGroup}>
        <input
          type="checkbox"
          id="game-options-audio"
          checked={gameData.withAudio || false}
          onChange={e => changeHandler('withAudio', e.target.checked)}
        />
        <label htmlFor="game-options-audio">
          {translate(489, 'Answer Audio')}
        </label>
      </div>
      <div className={classes.gameOptionsGroup}>
        <input
          type="checkbox"
          disabled={answersExists || gameData.longAnswers}
          id="game-options-images"
          checked={gameData.useImages || false}
          onChange={e => changeHandler('useImages', e.target.checked)}
        />
        <label htmlFor="game-options-images">
          {translate(490, 'Use Images')}
        </label>
      </div>
    </div>
  );
};

GameOptions.propTypes = {
  changeHandler: PropTypes.func.isRequired,
  activity: PropTypes.object.isRequired
};

export default withTranslate(GameOptions);
