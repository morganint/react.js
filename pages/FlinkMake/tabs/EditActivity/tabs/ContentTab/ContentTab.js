import React, { useEffect, useMemo, Fragment } from 'react';
import PropTypes from 'prop-types';

import { Help } from 'components/flink-components';
import { withTranslate } from 'components/hocs';
import { connect } from 'react-redux';
import { compose } from 'redux';

import getContentTabByAlias from './getContentTabByAlias';

const ContentTab = ({
  templateAlias,
  saveHandler,
  translate,
  editingActivity
}) => {
  useEffect(() => saveHandler, [saveHandler]);
  const ContentTab = useMemo(() => getContentTabByAlias(templateAlias), [
    templateAlias
  ]);

  const {
    template: {
      helpStrings: { contentTab }
    }
  } = editingActivity;

  return (
    <Fragment>
      <Help>{translate(contentTab)}</Help>

      <ContentTab editingActivity={editingActivity} saveHandler={saveHandler} />
    </Fragment>
  );
};

ContentTab.propTypes = {
  templateAlias: PropTypes.string.isRequired,
  saveHandler: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  editingActivity: PropTypes.object.isRequired
};

const mapStateToProps = ({ flinkMake }) => ({
  editingActivity: flinkMake.activity.editingActivity
});

export default compose(
  withTranslate,
  connect(mapStateToProps)
)(ContentTab);
