import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import { changeGameData } from 'actions/flinkMakeActions';
import { ProblemList } from 'pages/FlinkMake/interface';
import {
  validateProblem,
  createProblem,
  getContentFilesFromProblem
} from 'activity-templates/word-connectables/wordConnectablesHelpers';
import { withTranslate } from 'components/hocs';
import SyllablesConfigurator from './SyllablesConfigurator/SyllablesConfigurator';

const WordConnectablesContent = ({
  translate,
  changeGameData,
  editingActivity
}) => {
  const [activeProblemId, changeActiveProblemId] = useState();

  return (
    <Fragment>
      <div className="main-row">
        <ProblemList
          createProblem={createProblem}
          problemsPath="problems"
          // namePath="word"
          getName={problem => problem.syllables.join('')}
          validate={validateProblem}
          title={translate(317, 'Word List')}
          getContentFilesFromProblem={getContentFilesFromProblem}
          activeProblemId={activeProblemId}
          onChangeActiveProblem={changeActiveProblemId}
        />
        <div className="main-row__col">
          <div className="main-row__col-inner">
            <SyllablesConfigurator
              activeProblemId={activeProblemId}
              activity={editingActivity}
              changeGameData={changeGameData}
            />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

WordConnectablesContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired
};

export default compose(
  connect(
    null,
    { changeGameData }
  ),
  withTranslate
)(WordConnectablesContent);
