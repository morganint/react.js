import React, { useMemo, Fragment } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { AddImageBlock, ActivityAddAudio } from 'components/flink-components';
import { withTranslate } from 'components/hocs';
import classes from './SyllablesConfigurator.module.scss';

const SyllablesConfigurator = ({
  activity,
  translate,
  activeProblemId,
  changeGameData
}) => {
  const { data } = activity;
  const { problems } = data.gameData;

  const activeProblemIndex = useMemo(
    () =>
      _.findIndex(problems, {
        id: activeProblemId
      }),
    [activeProblemId, problems]
  );

  const activeProblem = problems[activeProblemIndex];

  if (!activeProblem) return null;

  return (
    <div>
      <AddImageBlock
        withId
        pathToProp={`gameData.problems[${activeProblemIndex}].image`}
      />

      <div className="audioWrapper">
        <ActivityAddAudio
          current={activeProblem.audio}
          onChangeHandler={filename => {
            changeGameData(`problems[${activeProblemIndex}].audio`, filename);
          }}
        />
      </div>

      <label className={classes.label}>{translate(523, 'Word Parts:')}</label>
      <div className={classes.wordConfig}>
        {_.times(6, idx => {
          const { syllables, mask } = activeProblem;

          const syllable = syllables && syllables[idx];
          const isChecked = mask && mask[idx] === '1';

          const isDisabledField = !!(idx !== 0 && !syllables[idx - 1]);

          return (
            <div key={idx} className={classes.syllableBox}>
              <input
                type="text"
                value={syllable || ''}
                onChange={e => {
                  const value = e.target.value.split(' ').join('');
                  let changedSyllables = [...syllables];
                  changedSyllables[idx] = value;

                  if (!value) {
                    changedSyllables = changedSyllables.slice(0, idx);
                  }

                  changeGameData(
                    `problems[${activeProblemIndex}].syllables`,
                    changedSyllables
                  );

                  let newMask = mask;

                  if (newMask.length > changedSyllables.length) {
                    newMask = newMask.substring(0, changedSyllables.length);
                  } else {
                    newMask = _.padEnd(newMask, changedSyllables.length, '0');
                  }

                  if (!value) {
                    newMask =
                      newMask.substring(0, idx) +
                      '0' +
                      newMask.substring(idx + 1);
                  }

                  changeGameData(
                    `problems[${activeProblemIndex}].mask`,
                    newMask
                  );
                }}
                disabled={isDisabledField}
              />
              <input
                tabIndex="-1"
                checked={isChecked}
                onChange={e => {
                  const { checked } = e.target;
                  const newMask =
                    mask.substring(0, idx) +
                    (checked ? '1' : '0') +
                    mask.substring(idx + 1);
                  changeGameData(
                    `problems[${activeProblemIndex}].mask`,
                    newMask
                  );
                }}
                type="checkbox"
                disabled={!syllable}
              />
            </div>
          );
        })}
      </div>

      <div className={classes.options}>
        <label className={classes.label}>{translate(382, 'Options:')}</label>
        {_.times(4, idx => (
          <Fragment key={idx}>
            <span className={classes.label}>{idx + 1}</span>
            <input
              tabIndex={3 + idx}
              type="text"
              value={activeProblem.options[idx] || ''}
              onChange={e => {
                const { value } = e.target;
                const newOptions = [...activeProblem.options];
                newOptions[idx] = value.trim() || null;
                changeGameData(
                  `problems[${activeProblemIndex}].options`,
                  newOptions
                );
              }}
            />
          </Fragment>
        ))}
      </div>

      {/* <div className={classes.definition}>
        <label className={classes.label}>
          {translate(null, 'Definition:')}
        </label>
        <textarea
          tabIndex="2"
          rows="3"
          value={activeProblem.definition || ''}
          onChange={e =>
            changeGameData(
              `problems[${activeProblemIndex}].definition`,
              e.target.value
            )
          }
        />
      </div> */}
    </div>
  );
};

SyllablesConfigurator.propTypes = {
  activeProblemId: PropTypes.string,
  activity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired
};

export default withTranslate(SyllablesConfigurator);
