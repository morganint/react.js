import React, { useState, useMemo } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";
import _ from "lodash";

import { changeGameData } from "actions/flinkMakeActions";
import { ProblemList } from "pages/FlinkMake/interface";
import {
  MultiLocaleInstructions,
  SelectItemBlock,
  ItemConfigurator,
  ProblemOptions
} from "components/flink-components";
import {
  validateProblem,
  createProblem,
  getContentFilesFromProblem
} from "activity-templates/all-together-now/allTogetherNowHelpers";
import { withTranslate } from "components/hocs";

const AllTogetherNowContent = ({
  translate,
  changeGameData,
  editingActivity
}) => {
  const [activeProblemId, changeActiveProblemId] = useState();
  const [selectedItemPath, changeSelectedItemPath] = useState("questions[0]");

  const {
    data: {
      gameData: { problems }
    }
  } = editingActivity;

  const activeProblemIdx = useMemo(() => {
    return _.findIndex(problems, { id: activeProblemId });
  }, [activeProblemId, problems]);

  const activeProblem = problems[activeProblemIdx];
  const activeItem = _.get(activeProblem, selectedItemPath);

  return (
    <div className="main-row">
      <ProblemList
        createProblem={createProblem}
        problemsPath="problems"
        validate={validateProblem}
        title={translate(205, "Problem List")}
        getContentFilesFromProblem={getContentFilesFromProblem}
        activeProblemId={activeProblemId}
        onChangeActiveProblem={changeActiveProblemId}
      />
      <div className="glass-wrapper glass-wrapper--full-height">
        <MultiLocaleInstructions
          problemPath={`problems[${activeProblemIdx}]`}
        />

        <ProblemOptions
          activeProblemIdx={activeProblemIdx}
          activeProblem={activeProblem}
          changeHandler={changeGameData}
          options={["answerBelongs", "fontFamily", "fontColor"]}
          correctItems={{
            min: 1,
            max: 5
          }}
          incorrectItems={{
            min: 1,
            max: 5
          }}
        />

        <ItemConfigurator
          activeProblemIdx={activeProblemIdx}
          activeItem={activeItem}
          selectedItemPath={selectedItemPath}
          changeHandler={changeGameData}
        />

        <SelectItemBlock
          selectedItem={selectedItemPath}
          activeProblem={activeProblem}
          pathPrefix="questions"
          count={1}
          nextSetPathPrefix="correctAnswers"
          label={translate(212, "Question")}
          selectHandler={changeSelectedItemPath}
          activeProblemIdx={activeProblemIdx}
        />

        <SelectItemBlock
          selectedItem={selectedItemPath}
          activeProblem={activeProblem}
          pathPrefix="correctAnswers"
          count={6}
          nextSetPathPrefix="incorrectAnswers"
          label={
            activeProblem && activeProblem.answerBelongs
              ? translate(584, "Correct (Belongs)")
              : translate(586, "Correct (Does Not Belong)")
          }
          selectHandler={changeSelectedItemPath}
          activeProblemIdx={activeProblemIdx}
        />
        
        <SelectItemBlock
          selectedItem={selectedItemPath}
          activeProblem={activeProblem}
          pathPrefix="incorrectAnswers"
          count={6}
          label={
            activeProblem && !activeProblem.answerBelongs
              ? translate(587, "Incorrect (Belongs)")
              : translate(585, "Incorrect (Does Not Belong)")
          }
          selectHandler={changeSelectedItemPath}
          activeProblemIdx={activeProblemIdx}
        />
      </div>
    </div>
  );
};

AllTogetherNowContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired
};

export default compose(
  withTranslate,
  connect(null, { changeGameData })
)(AllTogetherNowContent);
