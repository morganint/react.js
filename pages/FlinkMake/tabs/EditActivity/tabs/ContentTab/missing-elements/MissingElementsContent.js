import React, { Fragment, useState, useMemo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { changeGameData, saveEditingActivity } from 'actions/flinkMakeActions';
import { ProblemList } from 'pages/FlinkMake/interface';
import {
  validateProblem,
  createProblem,
  getContentFilesFromProblem
} from 'activity-templates/missing-elements/missingElementsHelpers';
import MissingElementsConfigurator from './MissingElementsConfigurator/MissingElementsConfigurator';
import { withTranslate } from 'components/hocs';

const MatchingDefinitions = ({
  translate,
  changeGameData,
  editingActivity
  // saveEditingActivity
}) => {
  const [activeProblemId, changeActiveProblemId] = useState();

  const {
    data: {
      gameData: { problems }
    }
  } = editingActivity;

  const activeProblemIdx = useMemo(() => {
    return _.findIndex(problems, { id: activeProblemId });
  }, [activeProblemId, problems]);

  const activeProblem = useMemo(() => problems[activeProblemIdx], [
    activeProblemIdx,
    problems
  ]);

  return (
    <Fragment>
      <div className="main-row">
        <ProblemList
          createProblem={createProblem}
          problemsPath="problems"
          validate={validateProblem}
          title={translate(205, 'Problem List')}
          getContentFilesFromProblem={getContentFilesFromProblem}
          activeProblemId={activeProblemId}
          onChangeActiveProblem={changeActiveProblemId}
        />

        <div className="main-row__col">
          <div className="main-row__col-inner">
            {activeProblem && (
              <MissingElementsConfigurator
                changeGameData={changeGameData}
                activeProblem={activeProblem}
                activeProblemIdx={activeProblemIdx}
              />
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

MatchingDefinitions.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired,
  saveEditingActivity: PropTypes.func.isRequired
};

export default compose(
  withTranslate,
  connect(
    null,
    { changeGameData, saveEditingActivity }
  )
)(MatchingDefinitions);
