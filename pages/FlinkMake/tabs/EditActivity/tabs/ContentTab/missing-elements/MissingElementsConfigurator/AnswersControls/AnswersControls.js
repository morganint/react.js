import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import shortId from 'shortid';
import classnames from 'classnames';

import classes from './AnswersControls.module.scss';
import { getCorrectAnswersFromHtml } from 'activity-templates/missing-elements/missingElementsHelpers';
import { imagesURL } from 'config';
import { withTranslate } from 'components/hocs';

class AnswersControls extends Component {
  state = {};

  componentDidUpdate(prevProps) {
    if (prevProps.problem.id !== this.props.problem.id) {
      this.setState({
        selectedCorrectAnswer: null,
        selectedIncorrectAnswer: null
      });
    }
  }

  addIncorrentAnswerHandler = () => {
    const { problem, changeHandler } = this.props;

    const currentIncorrectAnswers = [...(problem.incorrectAnswers || [])];
    currentIncorrectAnswers.push({ id: shortId.generate(), text: '' });

    changeHandler('incorrectAnswers', currentIncorrectAnswers);
  };

  deleteIncorrectAnswerHandler = () => {
    const { selectedIncorrectAnswer } = this.state;
    const { problem, changeHandler } = this.props;
    const { incorrectAnswers } = problem;

    const modifiedIncorrectAnswers = incorrectAnswers.filter(
      ans => ans.id !== selectedIncorrectAnswer
    );

    changeHandler('incorrectAnswers', modifiedIncorrectAnswers);
    this.setState({ selectedIncorrectAnswer: null });
  };

  selectCorrectAnswerHandler = id => {
    this.setState({ selectedCorrectAnswer: id });
  };

  selectIncorrectAnswerHandler = id => {
    this.setState({ selectedIncorrectAnswer: id });
  };

  onIncorrectAnswerInput = (id, text) => {
    const { problem, changeHandler } = this.props;
    const { incorrectAnswers } = problem;

    const modifiedIncorrectAnswers = incorrectAnswers.map(ans =>
      ans.id === id ? { id, text } : ans
    );

    changeHandler('incorrectAnswers', modifiedIncorrectAnswers);
  };

  render() {
    const {
      translate,
      problem,
      addCorrectAnswerHandler,
      allowAddCorrectAnswer,
      deleteCorrectAnswerHandler
    } = this.props;
    const { selectedCorrectAnswer, selectedIncorrectAnswer } = this.state;
    const { incorrectAnswers = [] } = problem;

    const { html } = problem;

    const correctAnswers = getCorrectAnswersFromHtml(html);

    return (
      <div className={classes.wrapper}>
        <TopLine
          label={translate(294, 'Correct Answers:')}
          addHandler={addCorrectAnswerHandler}
          hideAddBtn={correctAnswers.length === 10 || !allowAddCorrectAnswer}
          hideDeleteBtn={!selectedCorrectAnswer}
          deleteHandler={() => {
            deleteCorrectAnswerHandler(selectedCorrectAnswer);
            this.setState({ selectedCorrectAnswer: null });
          }}
        />
        <Blocks
          selected={selectedCorrectAnswer}
          blocks={correctAnswers}
          onSelect={this.selectCorrectAnswerHandler}
          useSize
        />

        <TopLine
          label={translate(430, 'Incorrect Answers:')}
          addHandler={this.addIncorrentAnswerHandler}
          hideAddBtn={incorrectAnswers.length === 4}
          hideDeleteBtn={!selectedIncorrectAnswer}
          deleteHandler={this.deleteIncorrectAnswerHandler}
        />
        <Blocks
          selected={selectedIncorrectAnswer}
          blocks={incorrectAnswers}
          onChange={this.onIncorrectAnswerInput}
          onSelect={this.selectIncorrectAnswerHandler}
        />
      </div>
    );
  }
}

const TopLine = ({
  label,
  addHandler,
  deleteHandler,
  hideAddBtn,
  hideDeleteBtn
}) => (
  <div className={classes.topLine}>
    <span>{label}</span>
    <div className={classes.controls}>
      <button onClick={addHandler} disabled={hideAddBtn}>
        <img src={`${imagesURL}/FlinkMake/add.png`} alt="Add" />
      </button>
      <button onClick={deleteHandler} disabled={hideDeleteBtn}>
        <img src={`${imagesURL}/FlinkMake/delete.png`} alt="Delete" />
      </button>
    </div>
  </div>
);

const Blocks = ({ blocks = [], onSelect, onChange, selected, useSize }) => (
  <div className={classes.blocksWrapper}>
    {blocks.map(block => (
      <div
        key={block.id}
        className={classnames(classes.singleBlock, {
          [classes.selectedBlock]: selected === block.id
        })}
        onClick={() => onSelect(block.id)}
      >
        <input
          size={useSize ? block.text.length : 20}
          type="text"
          value={block.text || ''}
          disabled={!onChange}
          onChange={e =>
            onChange ? onChange(block.id, e.target.value) : e.preventDefault()
          }
        />
      </div>
    ))}
  </div>
);

AnswersControls.propTypes = {
  translate: PropTypes.func.isRequired,
  addCorrectAnswerHandler: PropTypes.func.isRequired,
  deleteCorrectAnswerHandler: PropTypes.func.isRequired,
  problem: PropTypes.object.isRequired,
  changeHandler: PropTypes.func.isRequired
};

export default compose(withTranslate)(AnswersControls);
