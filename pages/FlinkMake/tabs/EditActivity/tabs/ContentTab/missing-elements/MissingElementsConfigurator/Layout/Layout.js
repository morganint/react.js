import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import ReactQuill from 'react-quill';
import shortId from 'shortid';
import AnswersControls from '../AnswersControls/AnswersControls';

import { showError } from 'actions/statusActions';
import { activitiesURL } from 'config';
import classes from './Layout.module.scss';

let Inline = ReactQuill.Quill.import('blots/inline');

class CorrectWordBlot extends Inline {
  static create(id) {
    let node = super.create();
    node.setAttribute('data-word-id', id);
    node.contentEditable = 'false';
    return node;
  }

  static formats(node) {
    return node.getAttribute('data-word-id');
  }

  static value(node) {
    return node.getAttribute('data-word-id');
  }
}

CorrectWordBlot.blotName = 'correctWord';
CorrectWordBlot.tagName = 'u';
ReactQuill.Quill.register('formats/correctWord', CorrectWordBlot);

const modules = {
  toolbar: false,
  clipboard: {
    matchVisual: false
  }
};

const formats = ['correctWord'];

class Layout extends Component {
  state = {
    editorContent: null
  };

  editor = React.createRef();

  componentDidMount() {
    this.setContent();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.problem.id !== this.props.problem.id) {
      this.setContent();
    }
  }

  setContent = () => {
    const { problem } = this.props;
    this.setState({ editorContent: problem.html });
  };

  addCorrectAnswerHandler = () => {
    const { showError } = this.props;
    const editor = this.editor.current.getEditor();

    const selection = editor.getSelection();

    if (!selection) {
      return showError({ message: 'You did not select any word' });
    }

    const selected = editor.getText(selection.index, selection.length);

    const word = selected.replace(/\n/g, '');

    if (word !== selected) {
      return showError({ message: 'You should select text in one line' });
    }

    const format = editor.getFormat(selection.index, selection.length);

    if (format.correctWord) {
      return showError({ message: 'Word already in correct words' });
    }

    // Create correct word
    const id = shortId.generate();

    try {
      editor.format('correctWord', id);
      this.setState({ allowAddCorrectAnswer: false });
    } catch (err) {
      console.log(err);
    }
  };

  deleteCorrectAnswerHandler = id => {
    const { problem, changeHandler } = this.props;

    const { html } = problem;

    const correctTagRegExp = new RegExp(`<u data-word-id="${id}">([\\s\\S]+?)</u>`);

    const [, word] = html.match(correctTagRegExp);

    const newHtml = html.replace(correctTagRegExp, word);

    changeHandler('html', newHtml);
    this.setState({ editorContent: newHtml });
  };

  onChangeSelection = (range, source) => {
    if (!this.editor.current) return;

    const { allowAddCorrectAnswer } = this.state;

    const exit = () => {
      if (allowAddCorrectAnswer) {
        this.setState({ allowAddCorrectAnswer: false });
      }
    };

    const editor = this.editor.current.getEditor();

    if (source !== 'user' || !range || !range.length) {
      return;
    }

    const selected = editor.getText(range.index, range.length);

    const word = selected.replace(/\n/g, '');

    if (word !== selected) {
      return exit();
    }

    const format = editor.getFormat(range.index, range.length);

    if (format.correctWord) {
      return exit();
    }

    this.setState({ allowAddCorrectAnswer: true });
  };

  editorOnChange = (content, delta, source, editor) => {
    const formatted = content.replace(/\scontenteditable="false"/g, '');

    this.setState({ editorContent: content });
    this.props.changeHandler('html', formatted);
  };

  render() {
    const { changeHandler, problem, activityId } = this.props;
    const { allowAddCorrectAnswer, editorContent } = this.state;

    const imgSrc = problem.image
      ? `${activitiesURL}/${activityId}/gamedata/${problem.image}`
      : null;

    return (
      <>
        <div className={classes.wrapper}>
          <div className={classes[problem.layoutType]}>
            <div className={classes.textOuterWrapper}>
              <div className={classes.textInnerWrapper}>
                <ReactQuill
                  ref={this.editor}
                  preserveWhitespace
                  className={classes.editor}
                  formats={formats}
                  modules={modules}
                  value={editorContent || ''}
                  style={{
                    fontFamily: problem.fontFamily,
                    fontSize: problem.fontSize + 'px'
                  }}
                  onChangeSelection={this.onChangeSelection}
                  onChange={this.editorOnChange}
                />
              </div>
            </div>

            <div className={classes.imageWrapper}>
              {imgSrc && <img  src={imgSrc} alt="" />}
            </div>
          </div>
        </div>

        <AnswersControls
          allowAddCorrectAnswer={allowAddCorrectAnswer}
          addCorrectAnswerHandler={this.addCorrectAnswerHandler}
          deleteCorrectAnswerHandler={this.deleteCorrectAnswerHandler}
          problem={problem}
          changeHandler={changeHandler}
        />
      </>
    );
  }
}

Layout.propTypes = {
  showError: PropTypes.func.isRequired,
  changeHandler: PropTypes.func.isRequired,
  problem: PropTypes.object.isRequired,
  activityId: PropTypes.string.isRequired
};

const mapStateToProps = ({ flinkMake }) => ({
  activityId: flinkMake.activity.editingActivity.activity._id
});

export default compose(
  connect(
    mapStateToProps,
    { showError }
  )
)(Layout);
