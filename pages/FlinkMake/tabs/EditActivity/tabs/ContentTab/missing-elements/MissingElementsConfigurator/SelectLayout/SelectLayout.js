import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { imagesURL } from 'config';

import {
  IMAGE_IN_RIGHT,
  IMAGE_IN_TOP,
  BIG_TEXT,
  SMALL_TEXT
} from 'activity-templates/missing-elements/layoutTypes';
import { withTranslate } from 'components/hocs';
import classes from './SelectLayout.module.scss';

const imgPrefix = `${imagesURL}/FlinkMake/Templates/MissingElements`;

const SelectLayout = ({ onChange, selected = '', translate }) => {
  return (
    <div className={classes.selectLayoutButtons}>
      <span className={classes.layoutLabel}>{translate(199, 'Layouts')}</span>

      <ul className={classes.layoutsList}>
        <li>
          <button
            className={classnames({
              [classes.active]: selected === IMAGE_IN_RIGHT
            })}
            title="Image in right"
            onClick={() => onChange(IMAGE_IN_RIGHT)}
          >
            <img  src={`${imgPrefix}/missingelements_layout1.png`} alt="" />
          </button>
        </li>
        <li>
          <button
            className={classnames({
              [classes.active]: selected === IMAGE_IN_TOP
            })}
            title="Image in top"
            onClick={() => onChange(IMAGE_IN_TOP)}
          >
            <img  src={`${imgPrefix}/missingelements_layout2.png`} alt="" />
          </button>
        </li>
        <li>
          <button
            className={classnames({ [classes.active]: selected === BIG_TEXT })}
            title="Big Text"
            onClick={() => onChange(BIG_TEXT)}
          >
            <img  src={`${imgPrefix}/missingelements_layout3.png`} alt="" />
          </button>
        </li>
        <li>
          <button
            className={classnames({
              [classes.active]: selected === SMALL_TEXT
            })}
            title="Small Text"
            onClick={() => onChange(SMALL_TEXT)}
          >
            <img  src={`${imgPrefix}/missingelements_layout4.png`} alt="" />
          </button>
        </li>
      </ul>
    </div>
  );
};

SelectLayout.propTypes = {
  selected: PropTypes.string,
  translate: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

export default withTranslate(SelectLayout);
