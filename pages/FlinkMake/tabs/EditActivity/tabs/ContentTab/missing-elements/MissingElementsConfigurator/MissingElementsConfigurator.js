import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { AddImageBlock, ProblemOptions } from 'components/flink-components';
import SelectLayout from './SelectLayout/SelectLayout';
import Layout from './Layout/Layout';
import classes from './MissingElementsConfigurator.module.scss';

export class MissingElementsConfigurator extends Component {
  changeHandler = (prop, val) => {
    const { activeProblemIdx, changeGameData } = this.props;

    changeGameData(`problems[${activeProblemIdx}].${prop}`, val);
  };

  render() {
    const { activeProblemIdx, activeProblem, changeGameData } = this.props;
    if (!activeProblem || activeProblemIdx === -1) return null;

    const { layoutType } = activeProblem;

    return (
      <div className={classes.wrapper}>
        <div className={classes.header}>
          <div className={classes.addImageWrapper}>
            <AddImageBlock
              withId
              withOverlay
              withoutPreview
              pathToProp={`gameData.problems[${activeProblemIdx}].image`}
            />
          </div>

          <SelectLayout
            selected={layoutType}
            onChange={this.changeHandler.bind(null, 'layoutType')}
          />

          <div className={classes.optionsWrapper}>
            <ProblemOptions
              activeProblemIdx={activeProblemIdx}
              activeProblem={activeProblem}
              changeHandler={changeGameData}
              onSelect={val => console.log(val)}
              options={['fontFamily', 'fontSize']}
            />
          </div>
        </div>

        <Layout
          problem={activeProblem}
          changeHandler={this.changeHandler}
        />
      </div>
    );
  }
}

MissingElementsConfigurator.propTypes = {
  activeProblemIdx: PropTypes.number,
  activeProblem: PropTypes.object,
  changeGameData: PropTypes.func.isRequired
};

export default MissingElementsConfigurator;
