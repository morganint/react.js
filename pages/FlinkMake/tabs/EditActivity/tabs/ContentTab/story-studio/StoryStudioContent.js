import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import _ from "lodash";

import {
  changeGameData,
  saveEditingActivity,
  changeActivityData,
  removeContentFiles
} from "actions/flinkMakeActions";
import { showError, showInfo, showConfirmPromise } from "actions/statusActions";
import {
  Slots,
  MultiLocaleInstructions,
  WordsList,
  AddImageBlock
} from "components/flink-components";
import { uploadFiles } from "actions/commonActions";
import { getSorting, addIdToFilename } from "utils";
import { imagesURL, s3bucketPublicURL, activitiesKey } from "config";

import { defaultData } from "activity-templates/story-studio/storyStudioHelpers";
import classes from "./StoryStudioContent.module.scss";

const MAX_IMAGE_SIZE = 200 * 1000; // 200kb
const WORDS_COUNT = 10;
const pagesOptions = [4, 6, 8, 10, 12];

class StoryStudioContent extends Component {
  state = { activeImageBlockIdx: 0 };

  componentDidMount() {
    const { editingActivity, changeActivityData } = this.props;

    const {
      data: { gameData }
    } = editingActivity;

    changeActivityData("gameData", { ...defaultData, ...gameData });
  }

  importWordsHandler = words => {
    const { showInfo } = this.props;

    // words in props are unique already but case-sensitive
    // This will remove all duplicates, case-insensitive
    const uniqWords = _.uniqWith(
      words,
      (a, b) => a.toLowerCase() === b.toLowerCase()
    );

    // Take only 10 words
    const wordsToInclude = _.take(uniqWords, WORDS_COUNT);
    const notIncluded = _.slice(uniqWords, WORDS_COUNT);

    if (notIncluded.length) {
      showInfo({
        html: true,
        message: `<div style="text-align: left">
        <p>Max words count is ${WORDS_COUNT}.<br>
        <b>Following words are not imported:</b></p>
        <ul>
          ${notIncluded.map(w => `<li>${w}</li>`).join("")}
        </ul>
      </div>`
      });
    }

    this.onChangeWords(wordsToInclude);
  };

  onChangeWords = words => {
    const { changeGameData } = this.props;
    changeGameData("words", words);
  };

  deleteAllWordsHandler = () => {
    this.onChangeWords([]);
  };

  changeActiveImageBlockIdx = idx => {
    this.setState({ activeImageBlockIdx: idx });
  };

  onChangeImportClipart = e => {
    const { checked } = e.target;
    const { changeGameData } = this.props;

    changeGameData("importClipart", checked);
  };

  onChangeAutoInsertArt = e => {
    const { activeImageBlockIdx } = this.state;
    const { checked } = e.target;
    const {
      changeGameData,
      showConfirmPromise,
      editingActivity,
      removeContentFiles
    } = this.props;
    const {
      data: {
        gameData: { images = [] }
      }
    } = editingActivity;

    if (checked) {
      return changeGameData("autoInsertArt", checked);
    }

    // Check if activity has images in last four slots, cause if auto
    // insert art option is off, then max images that author can add is 8 instead of 12
    const imagesInLastFourSlots = _.chain(images)
      .slice(8)
      .compact()
      .value();

    if (imagesInLastFourSlots.length) {
      showConfirmPromise({
        message: `
        <p>
          If auto insert art is OFF, you can add just 8 images. <br/>
          There is image(s) in last four image slots that will be removed?<br/>
          Do you want to continue?
        </p>`,
        html: true
      }).then(isConfirmed => {
        if (!isConfirmed) return;

        removeContentFiles(imagesInLastFourSlots);
        changeGameData("autoInsertArt", checked);
        changeGameData("images", _.take(images, 8));
        if (activeImageBlockIdx > 7) {
          this.changeActiveImageBlockIdx(0);
        }
      });
    } else {
      changeGameData("autoInsertArt", checked);
      if (activeImageBlockIdx > 7) {
        this.changeActiveImageBlockIdx(0);
      }
    }
  };

  fileInput = React.createRef();

  resetFileInput = () => {
    this.fileInput.current.value = "";
  };

  onFileInput = async e => {
    const {
      translate,
      editingActivity,
      changeGameData,
      saveEditingActivity,
      showError,
      showConfirmPromise,
      removeContentFiles,
      uploadFiles
    } = this.props;
    const { files } = e.target;

    if (!files || !files.length) return;

    const {
      activity: { _id: activityId },
      data: {
        gameData: { images, autoInsertArt }
      }
    } = editingActivity;

    // Check if activity already has images
    const oldImages = _.compact(images);

    if (oldImages.length) {
      const isConfirmed = await showConfirmPromise({
        message: `
        <p>
          This will remove all your current images. <br/>
          Do you want to continue?
        </p>`,
        html: true
      });

      if (!isConfirmed) {
        return this.resetFileInput();
      }
    }

    // Check if files are less than max possible
    const maxImagesCount = autoInsertArt ? 12 : 8;

    if (files.length > maxImagesCount) {
      this.resetFileInput();

      return showError({
        message: translate(
          0,
          `<p>
            Max images count is ${maxImagesCount}. <br/>
            Please select less number of images to import
          </p>`
        ),
        html: true
      });
    }

    const bigImages = _.filter(files, f => f.size > MAX_IMAGE_SIZE);

    if (bigImages.length) {
      this.resetFileInput();

      return showError({
        message: translate(
          0,
          `<p>Max image size is 200kb. Please re-check your images</p>`
        ),
        html: true
      });
    }

    const gameDataFolder = `${activitiesKey}/${activityId}/gamedata`;

    const data = _.map(files, file => {
      const filename = addIdToFilename(file.name);
      const key = `${gameDataFolder}/${filename}`;

      return { key, file, filename };
    });

    uploadFiles(data).then(results => {
      const newImages = _.chain(results)
        .map((isSuccess, idx) => isSuccess && data[idx].filename)
        .compact()
        .sort(getSorting("asc"))
        .value();

      changeGameData("images", newImages);
      // remove old images
      oldImages.length && removeContentFiles(oldImages);
      saveEditingActivity();
      this.resetFileInput();
    });
  };

  render() {
    const { translate, editingActivity, changeGameData } = this.props;
    const { activeImageBlockIdx } = this.state;
    const {
      activity: { contentFolder },
      data: {
        gameData: {
          pagesCount = 8,
          images = [],
          words = [],
          autoInsertArt = false,
          importClipart = false
        }
      }
    } = editingActivity;

    const imagesCount = autoInsertArt ? 12 : 8;
    const gameDataFolder = `${s3bucketPublicURL}/${contentFolder}/gamedata/`;

    return (
      <>
        <div className="main-row" style={{ marginTop: "20px" }}>
          <div className="main-row__col" style={{ width: "60%" }}>
            <div className="glass-wrapper glass-wrapper--full-height">
              <MultiLocaleInstructions />

              <div className={classes.options}>
                <fieldset className={classes.features}>
                  <legend>{translate(388, "Features:")}</legend>

                  <ul className={classes.checkboxesList}>
                    <li>
                      <label>
                        <input
                          type="checkbox"
                          onChange={this.onChangeAutoInsertArt}
                          disabled={importClipart}
                          checked={autoInsertArt}
                        />
                        <span>{translate(390, "Auto Insert Art")}</span>
                      </label>
                    </li>
                    <li>
                      <label>
                        <input
                          type="checkbox"
                          onChange={this.onChangeImportClipart}
                          disabled={autoInsertArt}
                          checked={importClipart}
                        />
                        <span>{translate(400, "Import Clipart")}</span>
                      </label>
                    </li>
                  </ul>
                </fieldset>

                {!autoInsertArt && (
                  <div className={classes.selectWrapper}>
                    <label>{translate(407, "Pages:")}</label>
                    <select
                      value={pagesCount}
                      onChange={e => {
                        const { value } = e.target;
                        changeGameData("pagesCount", value);
                      }}
                    >
                      {pagesOptions.map(opt => (
                        <option value={opt} key={opt}>
                          {opt}
                        </option>
                      ))}
                    </select>
                  </div>
                )}
              </div>

              <div className={classes.imagesAuthoring}>
                <div className={classes.imagesControlsWrapper}>
                  <AddImageBlock
                    maxSize={MAX_IMAGE_SIZE}
                    withId
                    withOverlay
                    withoutPreview
                    pathToProp={`gameData.images[${activeImageBlockIdx}]`}
                  />

                  <label className={classes.importButton}>
                    <span>{translate(331, "Import")}</span>
                    <img
                      src={`${imagesURL}/FlinkMake/import_large.png`}
                      alt=""
                    />
                    <input
                      type="file"
                      ref={this.fileInput}
                      hidden
                      onChange={this.onFileInput}
                      accept=".jpg, .jpeg, .png, .gif"
                      multiple
                    />
                  </label>
                </div>

                <Slots
                  number={imagesCount}
                  selectedIdx={activeImageBlockIdx}
                  onSelect={(item, idx) => this.changeActiveImageBlockIdx(idx)}
                  list={images}
                  render={(item, idx) =>
                    item && <img src={gameDataFolder + item} alt="" />
                  }
                />
              </div>
            </div>

            {/* <div className="glass-wrapper glass-wrapper--full-height"></div> */}
          </div>

          <div
            className="glass-wrapper glass-wrapper--full-height"
            style={{ width: "40%" }}
          >
            <WordsList
              editable
              wordsCount={WORDS_COUNT}
              onChange={this.onChangeWords}
              onImport={this.importWordsHandler}
              deleteAllHandler={this.deleteAllWordsHandler}
              currentWords={words}
            />
          </div>
        </div>
      </>
    );
  }
}

StoryStudioContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
  changeGameData: PropTypes.func.isRequired,
  changeActivityData: PropTypes.func.isRequired,
  saveEditingActivity: PropTypes.func.isRequired,
  removeContentFiles: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  showConfirmPromise: PropTypes.func.isRequired,
  showInfo: PropTypes.func.isRequired
};

const mapStateToProps = ({ status: { translate } }) => ({ translate });

export default connect(mapStateToProps, {
  changeGameData,
  changeActivityData,
  removeContentFiles,
  saveEditingActivity,
  uploadFiles,
  showError,
  showConfirmPromise,
  showInfo
})(StoryStudioContent);
