import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import { changeGameData } from 'actions/flinkMakeActions';
import { ProblemList } from 'pages/FlinkMake/interface';
import {
  validateProblem,
  createProblem,
  getContentFilesFromProblem
} from 'activity-templates/sentence-connectables/sentenceConnectablesHelpers';
import { withTranslate } from 'components/hocs';
import SentenceConfigurator from './SentenceConfigurator/SentenceConfigurator';

const SentenceConnectablesContent = ({
  translate,
  changeGameData,
  editingActivity
}) => {
  const [activeProblemId, changeActiveProblemId] = useState();

  return (
    <Fragment>
      <div className="main-row">
        <ProblemList
          activeProblemId={activeProblemId}
          createProblem={createProblem}
          problemsPath="problems"
          namePath="sentence"
          validate={validateProblem}
          title={translate(423, 'Sentence List')}
          getContentFilesFromProblem={getContentFilesFromProblem}
          onChangeActiveProblem={changeActiveProblemId}
        />
        <div className="main-row__col">
          <div className="main-row__col-inner">
            <SentenceConfigurator
              activeProblemId={activeProblemId}
              activity={editingActivity}
              changeGameData={changeGameData}
            />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

SentenceConnectablesContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired
};

export default compose(
  connect(
    null,
    { changeGameData }
  ),
  withTranslate
)(SentenceConnectablesContent);
