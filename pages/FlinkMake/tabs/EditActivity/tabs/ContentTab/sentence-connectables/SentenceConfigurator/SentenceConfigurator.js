import React, { useMemo, Fragment } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { ActivityAddAudio } from 'components/flink-components';
import { withTranslate } from 'components/hocs';
import classes from './SentenceConfigurator.module.scss';

const SentenceConfigurator = ({
  activity,
  translate,
  activeProblemId,
  changeGameData
}) => {
  const { data } = activity;
  const { problems } = data.gameData;

  const activeProblemIndex = useMemo(
    () =>
      _.findIndex(problems, {
        id: activeProblemId
      }),
    [activeProblemId, problems]
  );

  const activeProblem = problems[activeProblemIndex];

  if (!activeProblem) return null;

  return (
    <div>
      <div className="audioWrapper">
        <ActivityAddAudio
          current={activeProblem.audio}
          onChangeHandler={filename => {
            changeGameData(`problems[${activeProblemIndex}].audio`, filename);
          }}
        />
      </div>

      <div className={classes.sentenceFieldWrapper}>
        <label className={classes.label}>{translate(222, 'Sentence:')}</label>
        <textarea
          autoFocus
          tabIndex="1"
          className={classes.sentenceInput}
          value={activeProblem.sentence || ''}
          onChange={e => {
            const { value } = e.target;
            let { wordsMask } = activeProblem;

            let changedSentence = _.trimStart(value.replace(/\s+/g, ' '));

            if (changedSentence.split(' ').length > 8) {
              const array = changedSentence.split(' ');

              changedSentence = [
                ...array.slice(0, 7),
                array.slice(7).join('')
              ].join(' ');
            }

            const changedSentenceArray = changedSentence
              ? changedSentence.trim().split(' ')
              : [];

            if (wordsMask.length > changedSentenceArray.length) {
              wordsMask = wordsMask.substring(0, changedSentenceArray.length);
            } else {
              wordsMask = _.padEnd(wordsMask, changedSentenceArray.length, '0');
            }

            changeGameData(
              `problems[${activeProblemIndex}].sentence`,
              changedSentence
            );
            changeGameData(
              `problems[${activeProblemIndex}].wordsMask`,
              wordsMask
            );
          }}
        />
      </div>

      <div className={classes.sentenceConfig}>
        {_.times(8, idx => {
          const { sentence, wordsMask } = activeProblem;
          const word = sentence && sentence.split(' ')[idx];
          const isChecked = wordsMask && wordsMask[idx] === '1';

          return (
            <div key={idx} className={classes.wordBox}>
              <input type="text" value={word || ''} disabled />
              <input
                tabIndex="-1"
                checked={isChecked}
                onChange={e => {
                  const { checked } = e.target;
                  const newWordsMask =
                    wordsMask.substring(0, idx) +
                    (checked ? '1' : '0') +
                    wordsMask.substring(idx + 1);
                  changeGameData(
                    `problems[${activeProblemIndex}].wordsMask`,
                    newWordsMask
                  );
                }}
                type="checkbox"
                disabled={!word}
              />
            </div>
          );
        })}
      </div>

      <div className={classes.options}>
        <label className={classes.label}>{translate(382, 'Options:')}</label>
        {_.times(4, idx => (
          <Fragment key={idx}>
            <span className={classes.label}>{idx + 1}</span>
            <input
              tabIndex={3 + idx}
              type="text"
              value={activeProblem.options[idx] || ''}
              onChange={e => {
                const { value } = e.target;
                const newOptions = [...activeProblem.options];
                newOptions[idx] = value.trim() || null;
                changeGameData(
                  `problems[${activeProblemIndex}].options`,
                  newOptions
                );
              }}
            />
          </Fragment>
        ))}
      </div>
    </div>
  );
};

SentenceConfigurator.propTypes = {
  activeProblemId: PropTypes.string,
  activity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired
};

export default withTranslate(SentenceConfigurator);
