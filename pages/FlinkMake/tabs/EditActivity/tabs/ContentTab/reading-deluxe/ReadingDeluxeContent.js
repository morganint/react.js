import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import _ from "lodash";
import classnames from "classnames";

import {
  createProblem,
  createText,
  copyProblem,
  validateProblem,
  createFullsizeText,
  getContentFilesFromProblem
} from "activity-templates/reading-deluxe/readingDeluxeHelpers";
import { EBOOK_WIDTH, EBOOK_HEIGHT } from "consts/ebook-consts";
import {
  AddImageBlock,
  ActivityAddAudio,
  EbookEditor,
  MultiLocaleInstructions
} from "components/flink-components";
import { ProblemList } from "pages/FlinkMake/interface";
import { changeGameData, saveEditingActivity } from "actions/flinkMakeActions";
import { showError, showConfirm } from "actions/statusActions";
import { uploadFile, deleteFiles } from "actions/commonActions";
import { imagesURL, s3bucketPublicURL, activitiesKey } from "config";

import classes from "./ReadingDeluxeContent.module.scss";

const MAX_IMAGE_SIZE = 200 * 1000; // 200kb
const MAX_PDF_SIZE = 5 * 1000 * 1000; // 5mb

export class EbookContent extends Component {
  state = {
    activeProblemId: "",
    activeProblemIdx: ""
  };

  componentDidUpdate(prevProps, prevState) {
    const {
      editingActivity: {
        data: { gameData: currentGameData }
      }
    } = this.props;
    const { activeProblemId } = this.state;

    const {
      editingActivity: {
        data: { gameData: prevGameData }
      }
    } = prevProps;

    if (
      prevGameData !== currentGameData ||
      prevState.activeProblemId !== activeProblemId
    ) {
      this.setActiveProblemIdx();
    }
  }

  beforeDelete = (pageToDelete, cb) => {
    const {
      translate,
      showConfirm,
      changeGameData,
      editingActivity: {
        data: {
          gameData: { problems }
        }
      }
    } = this.props;

    // Check if other pages has prop "textFrom" to this page
    let flag = false;
    const changedProblems = problems.map(p => {
      if (p.textFrom === pageToDelete.id) {
        flag = true;
        return { ...p, textFrom: "" };
      }
      return p;
    });

    if (flag) {
      showConfirm({
        message: translate(
          0,
          "Some of the pages refs to current page's text, you sure you want to delete the page?"
        ),
        cb: isConfirmed => {
          if (!isConfirmed) return;
          changeGameData("problems", changedProblems);
          cb();
        }
      });
    } else {
      cb();
    }
  };

  setActiveProblemIdx = () => {
    const { editingActivity } = this.props;
    const { activeProblemId } = this.state;
    const { problems } = editingActivity.data.gameData;
    const activeProblemIdx = _.findIndex(problems, { id: activeProblemId });

    this.setState({ activeProblemIdx });
  };

  changeActiveProblemId = activeProblemId => this.setState({ activeProblemId });

  addTextBlock = () => {
    const { changeGameData } = this.props;
    const { activeProblemIdx } = this.state;
    const activeProblem = this.getActiveProblem();

    const activeProblemText = activeProblem.text;
    const newText = createText();

    changeGameData(`problems[${activeProblemIdx}].text`, [
      ...activeProblemText,
      newText
    ]);

    this.setEditorInFocus(newText.id);
  };

  getActiveProblem = () => {
    const { editingActivity } = this.props;
    const { activeProblemIdx } = this.state;
    const { problems } = editingActivity.data.gameData;

    return problems[activeProblemIdx];
  };

  removeTextHandler = id => {
    const { changeGameData } = this.props;
    const { activeProblemIdx } = this.state;
    const activeProblem = this.getActiveProblem();

    const { text, image } = activeProblem;

    const modifiedProblemText = text.filter(t => t.id !== id);

    if (!image && modifiedProblemText.length === 0) {
      changeGameData(`problems[${activeProblemIdx}].textOnly`, true);
      changeGameData(`problems[${activeProblemIdx}].text`, [
        createFullsizeText()
      ]);
    } else {
      changeGameData(`problems[${activeProblemIdx}].text`, modifiedProblemText);
    }
  };

  setEditorInFocus = editorInFocusId => {
    this.setState({ editorInFocusId });
  };

  importPdfHandler = e => {
    const {
      translate,
      editingActivity,
      changeGameData,
      saveEditingActivity,
      showError,
      uploadFile
    } = this.props;
    const { files } = e.target;

    if (!files || !files.length) return;

    const pdfFile = files[0];

    if (pdfFile.size > MAX_PDF_SIZE) {
      e.target.value = "";

      return showError({
        message: translate(0, `<p>Max PDF size is 5mb</p>`),
        html: true
      });
    }

    const gameDataFolder = `${activitiesKey}/${editingActivity.activity._id}/gamedata`;
    const key = `${gameDataFolder}/${pdfFile.name}`;

    e.persist();

    uploadFile(key, pdfFile).then(success => {
      if (!success) {
        return showError({
          message: translate(0, `<p>Error uploading file</p>`),
          html: true
        });
      }

      changeGameData("pdf", pdfFile.name);
      saveEditingActivity();
      e.target.value = "";
    });
  };

  toggleTimer = e => {
    const { activeProblemIdx } = this.state;
    const { checked } = e.target;
    const { changeGameData } = this.props;

    changeGameData(`problems[${activeProblemIdx}].hasTimer`, checked);
  };

  removePdf = e => {
    const {
      translate,
      editingActivity,
      changeGameData,
      saveEditingActivity,
      showError,
      deleteFiles
    } = this.props;

    e.preventDefault();

    const {
      activity: { _id },
      data: { gameData }
    } = editingActivity;

    const gameDataFolder = `${activitiesKey}/${_id}/gamedata`;
    const key = `${gameDataFolder}/${gameData.pdf}`;

    deleteFiles([key]).then(success => {
      if (!success) {
        return showError({
          message: translate(0, `<p>Error deleting pdf file</p>`),
          html: true
        });
      }

      changeGameData("pdf", "");
      saveEditingActivity();
    });
  };

  changeTextFrom = e => {
    const { activeProblemIdx } = this.state;
    const { changeGameData, editingActivity } = this.props;
    const { value } = e.target;

    if (!value) {
      return changeGameData(`problems[${activeProblemIdx}].textFrom`, "");
    }

    const activeProblem = this.getActiveProblem();

    const { problems } = editingActivity.data.gameData;

    const changedProblems = problems.map(p => {
      if (p.textFrom === activeProblem.id) {
        return { ...p, textFrom: "" };
      }

      if (p.id === activeProblem.id) {
        return { ...p, textFrom: value };
      }

      return p;
    });

    changeGameData("problems", changedProblems);
  };

  render() {
    const { translate, editingActivity, changeGameData } = this.props;
    const { activeProblemId, activeProblemIdx, editorInFocusId } = this.state;
    const { problems, pdf } = editingActivity.data.gameData;

    const activeProblem = problems[activeProblemIdx] || {};

    const {
      image,
      audio,
      text = [],
      hasTimer = false,
      benchmark = "",
      totalWords = "",
      textFrom,
      textOnly
    } = activeProblem;
    const currentProblemPath = `problems[${activeProblemIdx}]`;
    const gameDataFolder = `${s3bucketPublicURL}/${editingActivity.activity.contentFolder}/gamedata`;

    const textFromOptions = _.chain(problems)
      .map((p, idx) => {
        if (p.textFrom || idx === activeProblemIdx) return null;

        // const emptyTags = /<[^>\/]+><\/[^>]+>/gm;

        // const hasText = !!p.text.filter(
        //   t => !!(t.text && t.text.replace(emptyTags, "").length)
        // ).length;

        // if (!hasText) return null;

        return { label: idx + 1, value: p.id };
      })
      .compact()
      .value();

    const fromPage = textFrom && _.find(problems, { id: textFrom });

    return (
      <div className="main-row">
        <ProblemList
          copyProblem={copyProblem}
          namePrefix="Page:"
          beforeDelete={this.beforeDelete}
          createProblem={createProblem}
          problemsPath="problems"
          validate={validateProblem}
          title={translate(428, "Page List")}
          getContentFilesFromProblem={getContentFilesFromProblem}
          activeProblemId={activeProblemId}
          onChangeActiveProblem={this.changeActiveProblemId}
        />

        <div className="glass-wrapper glass-wrapper--full-height">
          <div className={classes.topSection}>
            <MultiLocaleInstructions
              maxCharacters={140}
              withoutAudio
              problemPath={`problems[${activeProblemIdx}]`}
            />

            <div className={classes.pdfBlock}>
              {pdf ? (
                <button className={classes.button} onClick={this.removePdf}>
                  <span className={classes.label}>
                    {translate(0, "Delete PDF")}
                  </span>
                  <img src={`${imagesURL}/FlinkMake/PDF.png`} alt="" />
                  <small className={classes.filename}>{pdf}</small>
                </button>
              ) : (
                <label className={classes.button}>
                  <input
                    hidden
                    type="file"
                    onChange={this.importPdfHandler}
                    accept=".pdf"
                  />
                  <span className={classes.label}>
                    {translate(0, "Add PDF")}
                  </span>
                  <img src={`${imagesURL}/FlinkMake/PDF.png`} alt="" />
                </label>
              )}
            </div>
          </div>

          <div className={classes.topControls}>
            <div className={classes.addImageBlock}>
              <AddImageBlock
                maxSize={MAX_IMAGE_SIZE}
                withId
                withOverlay
                withoutPreview
                onChangeHandler={filename => {
                  if (filename && textOnly) {
                    changeGameData(`${currentProblemPath}.textOnly`, false);
                    changeGameData(`${currentProblemPath}.text`, []);
                  } else if (!filename && !text.length) {
                    changeGameData(`${currentProblemPath}.textOnly`, true);
                    changeGameData(`${currentProblemPath}.text`, [
                      createFullsizeText()
                    ]);
                  }
                }}
                pathToProp={`gameData.problems[${activeProblemIdx}].image`}
              />
            </div>

            <ActivityAddAudio
              wrapperClassname={classes.addAudioBlock}
              current={audio}
              onChangeHandler={filename => {
                changeGameData(`${currentProblemPath}.audio`, filename);
              }}
            />

            <div className={classes.textFromBlock}>
              <label>{translate(0, "Text from:")}</label>
              <select
                // disabled={text.length}
                value={textFrom || ""}
                onChange={this.changeTextFrom}
              >
                <option value=""></option>
                {textFromOptions.map(({ value, label }) => (
                  <option value={value} key={value}>
                    {label}
                  </option>
                ))}
              </select>
            </div>

            <button
              disabled={textFrom || textOnly}
              onClick={this.addTextBlock}
              className={classes.button}
            >
              <span className={classes.label}>{translate(18, "Add Text")}</span>
              <img src={`${imagesURL}/FlinkMake/AddText.png`} alt="" />
            </button>

            <div className={classes.timerBlock}>
              <label>
                <input
                  type="checkbox"
                  checked={hasTimer}
                  onChange={this.toggleTimer}
                />
                <span>{translate(259, "Timer:")}</span>
              </label>

              <div
                className={classnames(classes.enterNumberBlock, {
                  [classes.disabled]: !hasTimer
                })}
              >
                <span className={classes.label}>
                  {translate(290, "Benchmark:")}
                </span>

                <input
                  disabled={!hasTimer}
                  type="number"
                  value={benchmark}
                  onChange={e => {
                    const { value } = e.target;
                    changeGameData(`${currentProblemPath}.benchmark`, value);
                  }}
                />
              </div>

              <div
                className={classnames(classes.enterNumberBlock, {
                  [classes.disabled]: !hasTimer
                })}
              >
                <span className={classes.label}>
                  {translate(292, "Total Words:")}
                </span>

                <input
                  disabled={!hasTimer}
                  type="number"
                  value={totalWords}
                  onChange={e => {
                    const { value } = e.target;
                    changeGameData(`${currentProblemPath}.totalWords`, value);
                  }}
                />
              </div>
            </div>
          </div>

          <div
            className={classes.contentArea}
            style={{ width: EBOOK_WIDTH + "px", height: EBOOK_HEIGHT + "px" }}
            onBlur={this.setEditorInFocus}
            tabIndex="1"
          >
            {image && (
              <img
                className={classes.textImage}
                src={`${gameDataFolder}/${image}`}
                alt=""
              />
            )}

            {(!textFrom ? text : (fromPage && fromPage.text) || []).map(
              (t, idx) => {
                return (
                  <EbookEditor
                    disableEditing={!!textFrom}
                    disableDelete={textOnly}
                    disableDrag={textOnly}
                    disableResize={textOnly}
                    key={t.id}
                    data={t}
                    inFocus={t.id === editorInFocusId}
                    selectHandler={this.setEditorInFocus.bind(null, t.id)}
                    changeHandler={data => {
                      if (textFrom) return;
                      changeGameData(
                        `${currentProblemPath}.text[${idx}]`,
                        data
                      );
                    }}
                    boundsSelector={"." + classes.contentArea}
                    removeHandler={this.removeTextHandler.bind(null, t.id)}
                  />
                );
              }
            )}
          </div>
        </div>
      </div>
    );
  }
}

EbookContent.propTypes = {
  translate: PropTypes.func.isRequired,
  changeGameData: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  showConfirm: PropTypes.func.isRequired,
  uploadFile: PropTypes.func.isRequired,
  deleteFiles: PropTypes.func.isRequired,
  saveEditingActivity: PropTypes.func.isRequired,
  editingActivity: PropTypes.object.isRequired
};

const mapStateToProps = ({ status: { translate } }) => ({ translate });

export default connect(mapStateToProps, {
  changeGameData,
  showConfirm,
  showError,
  saveEditingActivity,
  uploadFile,
  deleteFiles
})(EbookContent);
