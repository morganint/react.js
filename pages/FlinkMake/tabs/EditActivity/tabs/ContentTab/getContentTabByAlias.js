import AllTogetherNow from './all-together-now';
import Concentration from './concentration';
import DirectionsGrid from './directions-grid';
import Ebook from './ebook';
import FindTheOne from './find-the-one';
import MatchingDefinitions from './matching-definitions';
import MissingElements from './missing-elements';
import MatchIt from './match-it';
import MovingAnswers from './moving-answers';
import OrderIt from './order-it';
import StoryStudio from './story-studio';
import ReadingDeluxe from './reading-deluxe';
import SpellingConnectables from './spelling-connectables';
import SpellingKeyboard from './spelling-keyboard';
import SentenceConnectables from './sentence-connectables';
import WritingAssistant from './writing-assistant';
import WordConnectables from './word-connectables';
import WordSearch from './word-search';

import {
  ALL_TOGETHER_NOW,
  CONCENTRATION,
  DIRECTIONS_GRID,
  EBOOK,
  FIND_THE_ONE,
  MATCHING_DEFINITIONS,
  MISSING_ELEMENTS,
  MATCH_IT,
  ORDER_IT,
  MOVING_ANSWERS,
  STORY_STUDIO,
  SPELLING_CONNECTABLES,
  SPELLING_KEYBOARD,
  SENTENCE_CONNECTABLES,
  READING_DELUXE,
  WORD_CONNECTABLES,
  WORD_SEARCH,
  WRITING_ASSISTANT,
} from 'consts/activity-templates';

export default (templateAlias) => {
  let Component;

  switch (templateAlias) {
    case ALL_TOGETHER_NOW:
      Component = AllTogetherNow;
      break;
    case CONCENTRATION:
      Component = Concentration;
      break;
    case DIRECTIONS_GRID:
      Component = DirectionsGrid;
      break;
    case EBOOK:
      Component = Ebook;
      break;
    case FIND_THE_ONE:
      Component = FindTheOne;
      break;
    case MATCHING_DEFINITIONS:
      Component = MatchingDefinitions;
      break;
    case MISSING_ELEMENTS:
      Component = MissingElements;
      break;
    case MATCH_IT:
      Component = MatchIt;
      break;
    case MOVING_ANSWERS:
      Component = MovingAnswers;
      break;
    case ORDER_IT:
      Component = OrderIt;
      break;
    case STORY_STUDIO:
      Component = StoryStudio;
      break;
    case READING_DELUXE:
      Component = ReadingDeluxe;
      break;
    case SPELLING_CONNECTABLES:
      Component = SpellingConnectables;
      break;
    case SPELLING_KEYBOARD:
      Component = SpellingKeyboard;
      break;
    case SENTENCE_CONNECTABLES:
      Component = SentenceConnectables;
      break;
    case WORD_CONNECTABLES:
      Component = WordConnectables;
      break;
    case WORD_SEARCH:
      Component = WordSearch;
      break;
    case WRITING_ASSISTANT:
      Component = WritingAssistant;
      break;
    default:
      Component = null;
  }

  return Component;
};
