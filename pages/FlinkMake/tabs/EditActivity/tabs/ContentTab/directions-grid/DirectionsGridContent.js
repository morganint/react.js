import React, { useState, useMemo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import styled from 'styled-components';

import { changeGameData } from 'actions/flinkMakeActions';
import { ProblemList } from 'pages/FlinkMake/interface';
import {
  ASSETS,
  MIN_QUESTION_NUMBER,
  MAX_QUESTION_NUMBER,
  DIRECTIONS,
  GRID_OPTIONS,
  validateProblem,
  createProblem,
  getContentFilesFromProblem,
  REQUIRED_ANSWERS,
} from 'activity-templates/directions-grid/directionsGridHelpers';
import { withTranslate } from 'components/hocs';

const DIRECTION_TRANSLATES_STRING_NUMBERS = {
  [DIRECTIONS.ABOVE]: 798,
  [DIRECTIONS.BELOW]: 799,
  [DIRECTIONS.LEFT]: 800,
  [DIRECTIONS.RIGHT]: 801,
  [DIRECTIONS.BETWEEN_HORIZONTAL]: 802,
  [DIRECTIONS.BETWEEN_VERTICAL]: 803,
};

const ASSETS_TRANSLATES_STRING_NUMBERS = {
  FARM: 817,
  FOOD: 818,
  LETTERS: 819,
  SHAPES: 820,
};

const DirectionsGridContent = ({
  translate,
  changeGameData,
  editingActivity,
}) => {
  const [activeProblemId, changeActiveProblemId] = useState();

  const {
    data: {
      gameData: { problems },
    },
  } = editingActivity;

  const activeProblemIdx = useMemo(() => {
    return _.findIndex(problems, { id: activeProblemId });
  }, [activeProblemId, problems]);

  const activeProblem = problems[activeProblemIdx];

  const getRadioProps = (name, value) => {
    return {
      name,
      value,
      checked: !!(activeProblem && activeProblem[name] === value),
      onChange: (e) => {
        changeGameData(`problems[${activeProblemIdx}].${name}`, value);
      },
    };
  };

  const getCheckboxProps = (name) => {
    return {
      name,
      checked: !!(activeProblem && activeProblem[name]),
      onChange: (e) => {
        changeGameData(
          `problems[${activeProblemIdx}].${name}`,
          e.target.checked
        );
      },
    };
  };

  const getSelectProps = (name) => {
    return {
      name,
      value: (activeProblem && activeProblem[name]) || '',
      onChange: (e) => {
        changeGameData(`problems[${activeProblemIdx}].${name}`, e.target.value);
      },
    };
  };

  return (
    <div className="main-row">
      <ProblemList
        createProblem={createProblem}
        problemsPath="problems"
        namePrefix={translate(0, 'Grid:')}
        validate={validateProblem}
        title={translate(791, 'Grid List')}
        getContentFilesFromProblem={getContentFilesFromProblem}
        activeProblemId={activeProblemId}
        onChangeActiveProblem={changeActiveProblemId}
      />
      <div className="glass-wrapper glass-wrapper--full-height">
        <div>
          <StyledFieldset>
            <legend>{translate(792, 'Set the size of the grid:')}</legend>

            {GRID_OPTIONS.map((grid, idx) => (
              <label key={idx}>
                <input type="radio" {...getRadioProps('gridSize', grid)} />
                {grid.split(',').join('x')}
              </label>
            ))}
          </StyledFieldset>

          <StyledFieldset>
            <legend>{translate(797, 'Select one or more directions:')}</legend>

            {Object.keys(DIRECTIONS).map((key) => (
              <label key={key}>
                <input type="radio" {...getRadioProps('direction', key)} />
                {translate(DIRECTION_TRANSLATES_STRING_NUMBERS[key], key)}
              </label>
            ))}
          </StyledFieldset>

          <StyledFieldset>
            <legend>{translate(804, 'Set the number of questions:')}</legend>

            {_.range(MIN_QUESTION_NUMBER, MAX_QUESTION_NUMBER + 1).map(
              (number) => (
                <label key={number}>
                  <input
                    type="radio"
                    {...getRadioProps('questionsNumber', number)}
                  />
                  {number}
                </label>
              )
            )}
          </StyledFieldset>

          <StyledFieldset>
            <legend>{translate(810, 'Set Question Features:')}</legend>

            <label>
              <input type="checkbox" {...getCheckboxProps('highlightItems')} />
              {translate(811, 'Highlight Items')}
            </label>
            <br />
            <label>
              <input type="checkbox" {...getCheckboxProps('showGrid')} />
              {translate(812, 'Show Grid')}
            </label>
          </StyledFieldset>

          <StyledFieldset>
            <legend>{translate(813, 'Set answer features:')}</legend>

            <label>
              <input
                type="radio"
                {...getRadioProps('requiredAnswers', REQUIRED_ANSWERS.SINGLE)}
              />
              {translate(814, 'Single Answer')}
            </label>
            <label>
              <input
                type="radio"
                {...getRadioProps('requiredAnswers', REQUIRED_ANSWERS.ALL)}
              />
              {translate(815, 'All Possible Answers')}
            </label>
          </StyledFieldset>

          <StyledFieldset>
            <legend>{translate(816, 'Set assets:')}</legend>

            <select name="assetsType" {...getSelectProps('assetsType')}>
              {Object.keys(ASSETS).map((key) => (
                <option value={key} key={key}>
                  {translate(ASSETS_TRANSLATES_STRING_NUMBERS[key], key)}
                </option>
              ))}
            </select>
          </StyledFieldset>
        </div>

        {/*
        <ProblemOptions
          activeProblemIdx={activeProblemIdx}
          activeProblem={activeProblem}
          changeHandler={changeGameData}
          options={["answerBelongs", "fontFamily", "fontColor"]}
          correctItems={{
            min: 1,
            max: 5
          }}
          incorrectItems={{
            min: 1,
            max: 5
          }}
        />

        <ItemConfigurator
          activeProblemIdx={activeProblemIdx}
          activeItem={activeItem}
          selectedItemPath={selectedItemPath}
          changeHandler={changeGameData}
        />

        <SelectItemBlock
          selectedItem={selectedItemPath}
          activeProblem={activeProblem}
          pathPrefix="questions"
          count={1}
          nextSetPathPrefix="correctAnswers"
          label={translate(212, "Question")}
          selectHandler={changeSelectedItemPath}
          activeProblemIdx={activeProblemIdx}
        />

        <SelectItemBlock
          selectedItem={selectedItemPath}
          activeProblem={activeProblem}
          pathPrefix="correctAnswers"
          count={6}
          nextSetPathPrefix="incorrectAnswers"
          label={
            activeProblem && activeProblem.answerBelongs
              ? translate(584, "Correct (Belongs)")
              : translate(586, "Correct (Does Not Belong)")
          }
          selectHandler={changeSelectedItemPath}
          activeProblemIdx={activeProblemIdx}
        />
        
        <SelectItemBlock
          selectedItem={selectedItemPath}
          activeProblem={activeProblem}
          pathPrefix="incorrectAnswers"
          count={6}
          label={
            activeProblem && !activeProblem.answerBelongs
              ? translate(587, "Incorrect (Belongs)")
              : translate(585, "Incorrect (Does Not Belong)")
          }
          selectHandler={changeSelectedItemPath}
          activeProblemIdx={activeProblemIdx}
        /> */}
      </div>
    </div>
  );
};

DirectionsGridContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired,
};

export default compose(
  withTranslate,
  connect(null, { changeGameData })
)(DirectionsGridContent);

const StyledFieldset = styled.fieldset`
  margin-bottom: 20px;
  border: solid 1px rgba(0, 0, 0, 0.4);

  legend {
    font-weight: 500;
  }

  label {
    cursor: pointer;
    display: inline-block;
    margin-right: 20px;
    margin-bottom: 5px;
    font-weight: 400 !important;
  }

  input {
    margin-right: 5px;
  }
`;
