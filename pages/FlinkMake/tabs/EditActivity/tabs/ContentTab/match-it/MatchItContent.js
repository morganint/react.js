import React, { useState, useMemo } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";
import _ from "lodash";

import { changeGameData } from "actions/flinkMakeActions";
import { ProblemList } from "pages/FlinkMake/interface";
import {
  ProblemOptions,
  MultiLocaleInstructions,
  SelectItemBlock,
  ItemConfigurator
} from "components/flink-components";
import {
  validateProblem,
  createProblem,
  getContentFilesFromProblem
} from "activity-templates/match-it/matchItHelpers";
import { withTranslate } from "components/hocs";

const MatchItContent = ({ translate, changeGameData, editingActivity }) => {
  const [activeProblemId, changeActiveProblemId] = useState();
  const [selectedItemPath, changeSelectedItemPath] = useState("questions[0]");

  const {
    data: {
      gameData: { problems }
    }
  } = editingActivity;

  const activeProblemIdx = useMemo(() => {
    return _.findIndex(problems, { id: activeProblemId });
  }, [activeProblemId, problems]);

  const activeProblem = problems[activeProblemIdx];
  const activeItem = _.get(activeProblem, selectedItemPath);

  return (
    <>
      <div className="main-row">
        <ProblemList
          createProblem={createProblem}
          problemsPath="problems"
          validate={validateProblem}
          title={translate(205, "Problem List")}
          getContentFilesFromProblem={getContentFilesFromProblem}
          activeProblemId={activeProblemId}
          onChangeActiveProblem={changeActiveProblemId}
        />

        <div className="glass-wrapper glass-wrapper--full-height">
          <MultiLocaleInstructions
            problemPath={`problems[${activeProblemIdx}]`}
          />

          <ProblemOptions
            activeProblemIdx={activeProblemIdx}
            activeProblem={activeProblem}
            changeHandler={changeGameData}
            options={["fontFamily", "fontColor"]}
            questionItems={{
              min: 2,
              max: 8
            }}
          />

          <ItemConfigurator
            activeProblemIdx={activeProblemIdx}
            activeItem={activeItem}
            selectedItemPath={selectedItemPath}
            changeHandler={changeGameData}
          />

          <SelectItemBlock
            selectedItem={selectedItemPath}
            activeProblem={activeProblem}
            pathPrefix="questions"
            count={8}
            nextSetPathPrefix="correctAnswers"
            label={translate(213, "Questions:")}
            selectHandler={changeSelectedItemPath}
            activeProblemIdx={activeProblemIdx}
          />
          <SelectItemBlock
            selectedItem={selectedItemPath}
            activeProblem={activeProblem}
            pathPrefix="correctAnswers"
            count={8}
            nextSetPathPrefix="incorrectAnswers"
            label={translate(518, "Correct Answers:")}
            selectHandler={changeSelectedItemPath}
            activeProblemIdx={activeProblemIdx}
          />
          <SelectItemBlock
            selectedItem={selectedItemPath}
            activeProblem={activeProblem}
            pathPrefix="incorrectAnswers"
            count={2}
            label={translate(430, "Incorrect Answers:")}
            selectHandler={changeSelectedItemPath}
            activeProblemIdx={activeProblemIdx}
          />
        </div>
      </div>
    </>
  );
};

MatchItContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired
};

export default compose(
  withTranslate,
  connect(null, { changeGameData })
)(MatchItContent);
