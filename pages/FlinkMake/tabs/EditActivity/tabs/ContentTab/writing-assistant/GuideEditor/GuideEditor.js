import React from 'react';
import PropTypes from 'prop-types';

import {
  GUIDE_HEIGHT,
  GUIDE_WIDTH
} from 'activity-templates/writing-assistant/writingAssistantHelpers';
import { Editor, DraggableModal } from 'components/flink-components';

const GuideEditor = props => {
  const { show, onClose, value = '', onChange, editorClassname } = props;

  if (!show) return null;

  return (
    <DraggableModal
      // title={translate(0, "Guide")}
      size={{ width: GUIDE_WIDTH + 6 + 'px' }}
      show
      zIndex={101}
      // withOverlay
      onClose={onClose}
    >
      <Editor
        style={{
          height: GUIDE_HEIGHT + 'px',
          width: GUIDE_WIDTH + 'px'
        }}
        overflowHidden
        className={editorClassname}
        changeHandler={onChange}
        initialValue={value}
      />
    </DraggableModal>
  );
};

GuideEditor.propTypes = {
  editorClassname: PropTypes.string,
  show: PropTypes.bool.isRequired,
  value: PropTypes.string,
  onClose: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

export default GuideEditor;
