import React from "react";
import PropTypes from "prop-types";

import {
  EDITOR_HEIGHT,
  EDITOR_WIDTH
} from "activity-templates/writing-assistant/writingAssistantHelpers";
import { DraggableModal } from "components/flink-components";
import WritingAssistantEditor from "activity-templates/writing-assistant/WritingAssistantEditor/WritingAssistantEditor";

const StarterEditor = props => {
  const { show, onClose, value = "", onChange } = props;

  if (!show) return null;

  return (
    <DraggableModal
      // title={translate(0, "Starter")}
      size={{ width: EDITOR_WIDTH + 6 + "px", height: "800px" }}
      show
      withOverlay
      onClose={onClose}
    >
      <WritingAssistantEditor
        initialValue={value}
        style={{
          height: EDITOR_HEIGHT + "px",
          width: EDITOR_WIDTH + "px"
        }}
        changeHandler={onChange}
      />
    </DraggableModal>
  );
};

StarterEditor.propTypes = {
  show: PropTypes.bool.isRequired,
  value: PropTypes.string,
  onClose: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

export default StarterEditor;
