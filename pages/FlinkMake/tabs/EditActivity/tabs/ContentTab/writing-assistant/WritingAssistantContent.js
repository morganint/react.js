import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import _ from "lodash";

import {
  changeGameData,
  saveEditingActivity,
  changeActivityData,
  removeContentFiles
} from "actions/flinkMakeActions";
import { showError, showInfo, showConfirmPromise } from "actions/statusActions";
import {
  Slots,
  WordsList,
  AddImageBlock,
  ActivityAddAudio,
  MultiLocaleControls
} from "components/flink-components";
import { uploadFiles } from "actions/commonActions";
import { getSorting, addIdToFilename } from "utils";
import { imagesURL, s3bucketPublicURL, activitiesKey } from "config";
import { defaultData } from "activity-templates/story-studio/storyStudioHelpers";
import GuideEditor from "./GuideEditor/GuideEditor";
import StarterEditor from "./StarterEditor/StarterEditor";
import TextControl from "./TextControl/TextControl";
import classes from "./WritingAssistantContent.module.scss";

const MAX_IMAGE_SIZE = 200 * 1000; // 200kb
const WORDS_NUMBER = 12;
const GUIDES_NUMBER = 10;
const IMAGES_NUMBER = 8;

const ImportIcon = () => (
  <img src={`${imagesURL}/FlinkMake/import_large.png`} alt="" />
);

class WritingAssistantContent extends Component {
  state = {
    activeImageBlockIdx: 0,
    activeGuideIdx: 0,
    isGuideOpened: false,
    isStarterOpened: false,
    isReadingOpened: false,
    selectedLocale: "en"
  };

  componentDidMount() {
    const { editingActivity, changeActivityData } = this.props;

    const {
      data: { gameData }
    } = editingActivity;

    const { multiLocaleGuides } = gameData;

    changeActivityData("gameData", { ...defaultData, ...gameData });

    this.setState({
      selectedLocale: multiLocaleGuides ? multiLocaleGuides.defaultLocale : ""
    });
  }

  changeLocaleHandler = locale => {
    this.setState({ selectedLocale: locale });
  };

  setCurrentLocaleAsDefault = () => {
    const { selectedLocale } = this.state;
    const { multiLocaleGuides } = this.props.editingActivity.data.gameData;

    this.changeMultiLocaleGuides({
      ...multiLocaleGuides,
      defaultLocale: selectedLocale
    });
  };

  changeMultiLocaleGuides = data => {
    const { changeGameData } = this.props;
    changeGameData("multiLocaleGuides", data);
  };

  makeGuidesSingleLocale = locale => {
    const { changeGameData, editingActivity, removeContentFiles } = this.props;
    const { multiLocaleGuides } = editingActivity.data.gameData;

    const audioFilesToDelete = _.chain(multiLocaleGuides)
      .map((guides, key) => {
        if (key === "defaultLocale" || key === locale || !guides) return null;

        return guides.map(guide => guide && guide.audio).filter(a => !!a);
      })
      .compact()
      .flatten()
      .value();

    console.log("audioFilesToDelete", audioFilesToDelete);

    // Add audio files to remove list
    removeContentFiles(audioFilesToDelete);

    const guides = multiLocaleGuides[locale];

    changeGameData("guides", guides || []);

    this.changeMultiLocaleGuides(null);
  };

  makeGuidesMultiLocale = currentLocale => {
    const { changeGameData, editingActivity } = this.props;
    const { guides } = editingActivity.data.gameData;

    this.setState({
      selectedLocale: currentLocale
    });

    changeGameData("guides", []);

    this.changeMultiLocaleGuides({
      [currentLocale]: guides,
      defaultLocale: currentLocale
    });
  };

  importWordsHandler = words => {
    const { showInfo } = this.props;

    // words in props are unique already but case-sensitive
    // This will remove all duplicates, case-insensitive
    const uniqWords = _.uniqWith(
      words,
      (a, b) => a.toLowerCase() === b.toLowerCase()
    );

    // Take only 10 words
    const wordsToInclude = _.take(uniqWords, WORDS_NUMBER);
    const notIncluded = _.slice(uniqWords, WORDS_NUMBER);

    if (notIncluded.length) {
      showInfo({
        html: true,
        message: `<div style="text-align: left">
        <p>Max words count is ${WORDS_NUMBER}.<br>
        <b>Following words are not imported:</b></p>
        <ul>
          ${notIncluded.map(w => `<li>${w}</li>`).join("")}
        </ul>
      </div>`
      });
    }

    this.onChangeWords(wordsToInclude);
  };

  onChangeWords = words => {
    const { changeGameData } = this.props;
    changeGameData("words", words);
  };

  deleteAllWordsHandler = () => {
    this.onChangeWords([]);
  };

  changeActiveGuideIdx = idx => {
    this.setState({ activeGuideIdx: idx });
  };

  changeActiveImageBlockIdx = idx => {
    this.setState({ activeImageBlockIdx: idx });
  };

  fileInput = React.createRef();

  resetFileInput = () => {
    this.fileInput.current.value = "";
  };

  showGuideEditor = state => {
    this.setState({ isGuideOpened: state });
  };

  showStarterEditor = state => {
    this.setState({ isStarterOpened: state, isGuideOpened: false });
  };

  showReadingEditor = state => {
    this.setState({ isReadingOpened: state, isGuideOpened: false });
  };

  changeGuideHandler = (path, data) => {
    const { changeGameData, editingActivity } = this.props;
    const { activeGuideIdx, selectedLocale } = this.state;

    const { multiLocaleGuides } = editingActivity.data.gameData;

    if (multiLocaleGuides) {
      if (!selectedLocale) {
        return alert("No selected locale");
      }

      const guides = multiLocaleGuides[selectedLocale]
        ? [...multiLocaleGuides[selectedLocale]]
        : [];

      guides[activeGuideIdx] = {
        ...(guides[activeGuideIdx] || {}),
        [path]: data
      };

      const dataToSet = {
        ...multiLocaleGuides,
        [selectedLocale]: guides
      };
      this.changeMultiLocaleGuides(dataToSet);
    } else {
      changeGameData(`guides[${activeGuideIdx}].${path}`, data);
    }
  };

  changeStarterHandler = html => {
    const { changeGameData } = this.props;
    const { activeGuideIdx } = this.state;

    changeGameData(`starters[${activeGuideIdx}]`, html);
  };

  changeReadingHandler = html => {
    this.props.changeGameData("reading", html);
  };

  onFileInput = async e => {
    const {
      translate,
      editingActivity,
      changeGameData,
      saveEditingActivity,
      showError,
      showConfirmPromise,
      removeContentFiles,
      uploadFiles
    } = this.props;
    const { files } = e.target;

    if (!files || !files.length) return;

    const {
      activity: { _id: activityId },
      data: {
        gameData: { images, autoInsertArt }
      }
    } = editingActivity;

    // Check if activity already has images
    const oldImages = _.compact(images);

    if (oldImages.length) {
      const isConfirmed = await showConfirmPromise({
        message: `
        <p>
          This will remove all your current images. <br/>
          Do you want to continue?
        </p>`,
        html: true
      });

      if (!isConfirmed) {
        return this.resetFileInput();
      }
    }

    // Check if files are less than max possible
    const maxImagesCount = autoInsertArt ? 12 : 8;

    if (files.length > maxImagesCount) {
      this.resetFileInput();

      return showError({
        message: translate(
          0,
          `<p>
            Max images count is ${maxImagesCount}. <br/>
            Please select less number of images to import
          </p>`
        ),
        html: true
      });
    }

    const bigImages = _.filter(files, f => f.size > MAX_IMAGE_SIZE);

    if (bigImages.length) {
      this.resetFileInput();

      return showError({
        message: translate(
          0,
          `<p>Max image size is 200kb. Please re-check your images</p>`
        ),
        html: true
      });
    }

    const gameDataFolder = `${activitiesKey}/${activityId}/gamedata`;

    const data = _.map(files, file => {
      const filename = addIdToFilename(file.name);
      const key = `${gameDataFolder}/${filename}`;

      return { key, file, filename };
    });

    uploadFiles(data).then(results => {
      const newImages = _.chain(results)
        .map((isSuccess, idx) => isSuccess && data[idx].filename)
        .compact()
        .sort(getSorting("asc"))
        .value();

      changeGameData("images", newImages);
      // remove old images
      oldImages.length && removeContentFiles(oldImages);
      saveEditingActivity();
      this.resetFileInput();
    });
  };

  render() {
    const { translate, editingActivity } = this.props;
    const {
      activeImageBlockIdx,
      activeGuideIdx,
      isGuideOpened,
      isStarterOpened,
      isReadingOpened,
      selectedLocale
    } = this.state;
    const {
      activity: { contentFolder },
      data: {
        gameData: {
          images = [],
          words = [],
          guides = [],
          starters = [],
          reading = "",
          multiLocaleGuides
        }
      }
    } = editingActivity;
    const gameDataFolder = `${s3bucketPublicURL}/${contentFolder}/gamedata/`;

    const currentGuides = multiLocaleGuides
      ? multiLocaleGuides[selectedLocale] || []
      : guides;

    const currentGuide = currentGuides[activeGuideIdx] || {};

    const { audio = "", text } = currentGuide;

    const starter = starters[activeGuideIdx] || "";

    return (
      <>
        <GuideEditor
          show={isGuideOpened}
          value={text}
          onClose={this.showGuideEditor.bind(null, false)}
          onChange={text => this.changeGuideHandler("text", text)}
        />

        <StarterEditor
          show={isStarterOpened}
          value={starter}
          onClose={this.showStarterEditor.bind(null, false)}
          onChange={this.changeStarterHandler}
        />

        <StarterEditor
          show={isReadingOpened}
          value={reading}
          onClose={this.showReadingEditor.bind(null, false)}
          onChange={this.changeReadingHandler}
        />

        <div className="glass-wrapper" style={{ marginTop: "20px" }}>
          <div className={classes.controls}>
            <TextControl
              onAdd={this.showGuideEditor.bind(null, true)}
              onDelete={() => this.changeGuideHandler("text", "")}
              label={translate(160, "Guide")}
              deleteButtonProps={{
                style: { visibility: text ? "visible" : "hidden" },
                disabled: isGuideOpened
              }}
            />

            <TextControl
              onAdd={this.showStarterEditor.bind(null, true)}
              onDelete={() => this.changeStarterHandler("")}
              label={translate(306, "Starter")}
              deleteButtonProps={{
                style: { visibility: starter ? "visible" : "hidden" },
                disabled: isStarterOpened
              }}
            />

            <ActivityAddAudio
              wrapperClassname={classes.addAudioBlock}
              current={audio}
              onChangeHandler={filename => {
                this.changeGuideHandler("audio", filename);
              }}
            />

            <MultiLocaleControls
              selectedLocale={selectedLocale}
              multiLocaleData={multiLocaleGuides}
              changeLocaleHandler={this.changeLocaleHandler}
              makeDefaultHandler={this.setCurrentLocaleAsDefault}
              makeSingleHandler={this.makeGuidesSingleLocale}
              makeMultiHandler={this.makeGuidesMultiLocale}
              makeSingleDialogText={translate(
                0,
                "Which guide to use as single? Warning all other guides will be deleted including audio files"
              )}
              makeMultiDialogText={translate(
                0,
                "Select language of current guide"
              )}
              // classes={{
              //   wrapper: classes.multiLocaleControls,
              //   select: classes.localeSelectWrapper,
              //   button: classes.localeButton,
              //   checkbox: classes.checkbox
              // }}
            />

            <TextControl
              onAdd={this.showReadingEditor.bind(null, true)}
              onDelete={() => this.changeReadingHandler("")}
              label={translate(214, "Reading")}
              deleteButtonProps={{
                style: { visibility: reading ? "visible" : "hidden" },
                disabled: isReadingOpened
              }}
            />
          </div>

          <div className={classes.row}>
            <Slots
              number={GUIDES_NUMBER}
              selectedIdx={activeGuideIdx}
              onSelect={(item, idx) => this.changeActiveGuideIdx(idx)}
              list={currentGuides}
              render={(item, idx) => {
                const { audio, text } = item || {};
                const starter = starters[idx];

                return (
                  <div className={classes.guideSlot}>
                    {text && (
                      <span className={classes.guideTopText}>
                        {translate(160, "Guide")}
                      </span>
                    )}

                    {starter && (
                      <span className={classes.guideBottomText}>
                        {translate(306, "Starter")}
                      </span>
                    )}

                    {audio && (
                      <img
                        className={classes.guideAudioIcon}
                        src={`${imagesURL}/Images/Audio/audio_make.png`}
                        alt="audio-icon"
                      />
                    )}
                  </div>
                );
              }}
            />
          </div>
        </div>

        <div className="main-row">
          <div className="main-row__col" style={{ width: "60%" }}>
            <div className="glass-wrapper glass-wrapper--full-height">
              <div className={classes.row}>
                <div className={classes.rowControls}>
                  <AddImageBlock
                    maxSize={MAX_IMAGE_SIZE}
                    withId
                    withOverlay
                    withoutPreview
                    pathToProp={`gameData.images[${activeImageBlockIdx}]`}
                  />

                  <label className={classes.rowControlButton}>
                    <span>{translate(331, "Import")}</span>
                    <ImportIcon />
                    <input
                      type="file"
                      ref={this.fileInput}
                      hidden
                      onChange={this.onFileInput}
                      accept=".jpg, .jpeg, .png, .gif"
                      multiple
                    />
                  </label>
                </div>

                <Slots
                  number={IMAGES_NUMBER}
                  selectedIdx={activeImageBlockIdx}
                  onSelect={(item, idx) => this.changeActiveImageBlockIdx(idx)}
                  list={images}
                  render={(item, idx) =>
                    item && <img src={gameDataFolder + item} alt="" />
                  }
                />
              </div>
            </div>
          </div>

          <div
            className="glass-wrapper glass-wrapper--full-height"
            style={{ width: "40%" }}
          >
            <WordsList
              editable
              wordsCount={WORDS_NUMBER}
              onChange={this.onChangeWords}
              onImport={this.importWordsHandler}
              deleteAllHandler={this.deleteAllWordsHandler}
              currentWords={words}
            />
          </div>
        </div>
      </>
    );
  }
}

WritingAssistantContent.propTypes = {
  editingActivity: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
  changeGameData: PropTypes.func.isRequired,
  changeActivityData: PropTypes.func.isRequired,
  saveEditingActivity: PropTypes.func.isRequired,
  removeContentFiles: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  showConfirmPromise: PropTypes.func.isRequired,
  showInfo: PropTypes.func.isRequired
};

const mapStateToProps = ({ status: { translate } }) => ({ translate });

export default connect(mapStateToProps, {
  changeGameData,
  changeActivityData,
  removeContentFiles,
  saveEditingActivity,
  uploadFiles,
  showError,
  showConfirmPromise,
  showInfo
})(WritingAssistantContent);
