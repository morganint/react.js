import React from "react";
import PropTypes from "prop-types";

import { imagesURL } from "config";
import classes from "./TextControl.module.scss";

const DeleteIcon = () => (
  <img src={`${imagesURL}/FlinkMake/delete.png`} alt="" />
);

const AddTextIcon = () => (
  <img src={`${imagesURL}/FlinkMake/AddText.png`} alt="" />
);

const TextControl = ({ onAdd, onDelete, label, deleteButtonProps = {} }) => {
  return (
    <div className={classes.textControls}>
      <button onClick={onAdd} className={classes.rowControlButton}>
        <span>{label}</span>
        <AddTextIcon />
      </button>

      <button onClick={onDelete} {...deleteButtonProps}>
        <DeleteIcon />
      </button>
    </div>
  );
};

TextControl.propTypes = {
  onAdd: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  deleteButtonProps: PropTypes.object
};

export default TextControl;
