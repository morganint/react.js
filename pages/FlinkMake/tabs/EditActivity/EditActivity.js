import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Switch, Route, Redirect } from "react-router-dom";

import {
  setEditingActivity,
  saveEditingActivity
  // removeContentFilesInBucket
} from "actions/flinkMakeActions";
import Nav from "./Nav/Nav";
import HelpTab from "./tabs/HelpTab/HelpTab";
import OptionsTab from "./tabs/OptionsTab/OptionsTab";
import ContentTab from "./tabs/ContentTab/ContentTab";
import EditActivityHeader from "./EditActivityHeader/EditActivityHeader";
import ActivityTemplates from "activity-templates";
import { LoadMessage } from "components/common";
import { TemplateWrapper } from "components/flink-play";
import { WithPageTitle } from "components/hocs";

export class EditActivity extends Component {
  state = {};

  componentDidMount() {
    const { history, activitiesToDelete, match } = this.props;
    const { activityId } = match.params;

    const isActivityInDeleteList = !!activitiesToDelete.filter(
      ({ _id }) => _id === activityId
    )[0];

    if (isActivityInDeleteList) {
      console.log("activity was deleted");
      return history.goBack();
    }
    this.fetchActivityData();
  }

  componentDidUpdate() {
    this.fetchActivityData();
  }

  fetchActivityData = async () => {
    const { history, activityTemplates, hasUnsavedChanges, match } = this.props;

    const { activityLoading, activityLoaded } = this.state;
    const { activityId } = match.params;

    if (hasUnsavedChanges || activityLoading || activityLoaded) {
      return;
    }

    this.setState({ activityLoading: true });

    const result = await this.props.setEditingActivity(
      activityId,
      activityTemplates
    );

    if (!result) return history.push("/flink-make/edit-activities");

    this.setState({ activityLoaded: true, activityLoading: false });
  };

  componentWillUnmount() {
    this.saveChanges();
    this.props.setEditingActivity(null);
  }

  saveChanges = async () => {
    const {
      // editingActivity,
      saveEditingActivity
      //  filesToDelete
    } = this.props;
    // const success =
    await saveEditingActivity();
    // if (!success) return;

    // filesToDelete.length &&
    //   removeContentFilesInBucket({
    //     filesToDelete,
    //     path: editingActivity.activity.contentFolder + '/gamedata'
    //   });
  };

  render() {
    const { editingActivity, match } = this.props;
    const { activityId } = match.params;

    if (!editingActivity) return <LoadMessage>Loading activity...</LoadMessage>;

    const templateAlias = editingActivity.template.alias;

    const isActivityValid = ActivityTemplates[templateAlias].validateActivity(
      editingActivity.data
    );

    return (
      <Switch>
        <Route path="/flink-make/edit-activities/:activityId/preview">
          <WithPageTitle>
            <TemplateWrapper activity={editingActivity} preview />
          </WithPageTitle>
        </Route>

        <Route>
          <div className="flink-make__inner">
            <Nav activityValid={isActivityValid} />

            <div className="content-wrapper">
              <EditActivityHeader activity={editingActivity} />

              <Switch>
                <Redirect
                  exact
                  from="/flink-make/edit-activities/:activityId"
                  to={`/flink-make/edit-activities/${activityId}/content`}
                />
                <Route path="/flink-make/edit-activities/:activityId/content">
                  <WithPageTitle>
                    <ContentTab
                      saveHandler={this.saveChanges}
                      templateAlias={editingActivity.template.alias}
                    />
                  </WithPageTitle>
                </Route>
                <Route path="/flink-make/edit-activities/:activityId/options">
                  <WithPageTitle>
                    <OptionsTab saveHandler={this.saveChanges} />
                  </WithPageTitle>
                </Route>
                <Route path="/flink-make/edit-activities/:activityId/help">
                  <WithPageTitle>
                    <HelpTab
                      saveHandler={this.saveChanges}
                      activity={editingActivity}
                    />
                  </WithPageTitle>
                </Route>
              </Switch>
            </div>
          </div>
        </Route>
      </Switch>
    );
  }
}

EditActivity.propTypes = {
  setEditingActivity: PropTypes.func.isRequired,
  activityWasChanged: PropTypes.bool.isRequired,
  hasUnsavedChanges: PropTypes.bool,
  saveEditingActivity: PropTypes.func.isRequired,
  activitiesToDelete: PropTypes.array.isRequired,
  filesToDelete: PropTypes.array.isRequired,
  activityTemplates: PropTypes.array.isRequired
};

const mapStateToProps = ({ flinkMake, common }) => ({
  activitiesToDelete: flinkMake.editList.activitiesToDelete,
  editingActivity: flinkMake.activity.editingActivity,
  hasUnsavedChanges: flinkMake.editList.hasUnsavedChanges,
  filesToDelete: flinkMake.activity.filesToDelete,
  activityWasChanged: flinkMake.activity.activityWasChanged,
  activityTemplates: common.activityTemplates
});

export default connect(mapStateToProps, {
  setEditingActivity,
  saveEditingActivity
})(EditActivity);
