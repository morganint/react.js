import React from 'react';
import { Header } from 'pages/FlinkMake/interface';
import styled from 'styled-components';

import { imagesURL } from 'config';

const EditActivityHeader = ({ activity }) => {
  return (
    <Header>
      <StyledWrapper>
        <img
          src={`${imagesURL}/FlinkMake/TemplateLogos/${
            activity.template.logoFilename
          }`}
          alt=""
        />
        <h1 className="activity-name">
          {activity.activity.activityName}
        </h1>
      </StyledWrapper>
    </Header>
  );
};

export default EditActivityHeader;

const StyledWrapper = styled.div`
  align-self: flex-end;
`;
