import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
// import _ from 'lodash';

import { axiosAPI } from 'utils';
import { getActivityData } from 'actions/gameActions';
import { MainNav, Header } from '../../interface';
import classes from './TransferActivities.module.scss';
// import API from 'api';

class TransferActivities extends Component {
  state = {
    data: null,
  };

  startTransfer = async () => {
    this.setState({ started: true, error: false, data: null });

    try {
      const res = await axiosAPI.get('api/utils/transfer-activities');
      this.setState({ data: res.data, started: false });
    } catch (err) {
      let error;

      if (err && err.response && err.response.data && err.response.data) {
        error = err.response.data;
      } else {
        error = 'Something goes wrong';
      }

      this.setState({ error, started: false });
    }
  };

  // convertOldActivities = async (activitiesIds) => {
  //   const { activityTemplates } = this.props;
  //   // console.log(activities);

  //   console.log(activities.length);

  //   const portion = activities.slice(4000);
  //   console.log(portion);

  //   const result = await Promise.all(
  //     portion.map(async (activity) => {
  //       const template = _.find(activityTemplates, {
  //         _id: activity.templateId,
  //       });

  //       try {
  //         const activityData = await API.activity.getActivityData(
  //           activity._id,
  //           template
  //         );

  //         if (!activityData) {
  //           throw new Error('Cannot get/convert activity data');
  //         }

  //         return { success: true, activity, activityData };
  //       } catch (err) {
  //         console.log(err);
  //         return { success: false, activity, template };
  //       }
  //     })
  //   );

  //   console.log(result);
  //   console.log(
  //     'Success',
  //     result.filter((r) => r.success)
  //   );
  //   console.log(
  //     'Errors',
  //     result.filter((r) => !r.success)
  //   );

  //   // const result = await Promise.all(activitiesIds.map(id) => {

  //   // })
  // };

  showData = (dataType) => {
    this.setState({ selectedType: dataType });
  };

  render() {
    const { data, selectedType, error, started } = this.state;

    return (
      <div className="flink-make__inner">
        <MainNav />

        <div className="content-wrapper">
          <Header />

          <div className="glass-wrapper grow-1">
            <div>
              <button
                disabled={started}
                className={classes.startBtn}
                onClick={this.startTransfer}
              >
                {started ? 'Processing...' : data ? 'Restart' : 'Start'}
              </button>

              {error && (
                <>
                  <p>Error</p>
                  <div dangerouslySetInnerHTML={{ __html: error + '' }}></div>
                </>
              )}

              {data && !started && !error && (
                <div>
                  <ul className={classes.statList}>
                    <li
                      className={classnames({
                        [classes.active]: selectedType === 'activitiesInDB',
                      })}
                      onClick={() => this.showData('activitiesInDB')}
                    >
                      Activities in DB - {data.activitiesInDB.length}
                    </li>
                    <li
                      className={classnames({
                        [classes.active]: selectedType === 'activitiesInS3',
                      })}
                      onClick={() => this.showData('activitiesInS3')}
                    >
                      Activities in S3 - {data.activitiesInS3.length}
                    </li>
                    <li
                      className={classnames({
                        [classes.active]: selectedType === 'notInDB',
                      })}
                      onClick={() => this.showData('notInDB')}
                    >
                      Not in DB - {data.notInDB.length}
                    </li>
                    <li
                      className={classnames({
                        [classes.active]: selectedType === 'relatedData',
                      })}
                      onClick={() => this.showData('relatedData')}
                    >
                      Related data to new activities - {data.relatedData.length}
                    </li>
                    <li
                      className={classnames({
                        [classes.active]: selectedType === 'formattedDataToAdd',
                      })}
                      onClick={() => this.showData('formattedDataToAdd')}
                    >
                      Formatted data - {data.formattedDataToAdd.length}
                    </li>
                    <li
                      className={classnames({
                        [classes.active]: selectedType === 'saveResults',
                      })}
                      onClick={() => this.showData('saveResults')}
                    >
                      Transfered - {data.saveResults.length}
                    </li>
                  </ul>
                  {this.renderData()}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderData() {
    const { selectedType, data } = this.state;

    if (
      !data ||
      !selectedType ||
      !data[selectedType] ||
      !data[selectedType].length
    )
      return null;

    const d = data[selectedType];

    if (typeof d[0] === 'string') {
      return (
        <ul className={classes.list}>
          {d.map((data) => (
            <li key={data}>{data}</li>
          ))}
        </ul>
      );
    }

    const keys = Object.keys(d[0]);

    return (
      <div className={classes.tableWrapper}>
        <table className={classes.table}>
          <thead>
            <tr>
              {keys.map((key) => (
                <th key={key}>{key}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {d.map((data, idx) => (
              <tr key={idx}>
                {keys.map((key) => (
                  <td key={key}>{JSON.stringify(data[key], null, 1)}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  activityTemplates: state.common.activityTemplates,
});

export default connect(mapStateToProps, { getActivityData })(
  TransferActivities
);
