import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";

import { SearchResultsList } from "components/flink-components";
import { Scope } from "utils";
import { imagesURL } from "config";
import { withTranslate } from "components/hocs";
import { moveWordlistsToEditList } from "actions/flinkMakeActions";
import { showError } from "actions/statusActions";

import classes from "./ResultsArea.module.scss";

class ResultsArea extends Component {
  state = { active: null };

  setFilter = eduFilter => {
    const { scopes } = this.props;

    let scopeDoc =
      eduFilter && scopes.filter(scope => scope._id === eduFilter.scopeId)[0];

    this.setState({
      eduFilter,
      scope: scopeDoc && new Scope(scopeDoc)
    });
  };

  componentDidUpdate(prevProps) {
    const { items, eduFilter } = this.props;
    const { active, eduFilter: eduFilterState } = this.state;

    if (!eduFilterState || items !== prevProps.items) {
      this.setFilter(eduFilter);
    }

    if ((!active && items && items.length) || items !== prevProps.items) {
      this.setState({ active: items[0] });
    }
  }

  changeActive = item => {
    this.setState({ active: item });
  };

  moveOneHandler = async () => {
    const {
      items,
      showError,
      translate,
      changeList,
      moveWordlistsToEditList
    } = this.props;
    const { active } = this.state;

    if (active.inEditList) {
      showError({
        message: translate(
          0,
          'Selected wordlist already in "My Wordlists" of another user.',
          true
        )
      });

      const isConfirmed = this.getSuperConfirm();

      if (!isConfirmed) return;
    }

    const success = await moveWordlistsToEditList([active._id]);

    success && changeList(items.filter(w => w._id !== active._id));
  };

  getSuperConfirm = () => {
    const { isSuperAdmin } = this.props;
    return isSuperAdmin
      ? window.confirm(
          'You are a super admin and can force move wordlist(s) to your "My Wordlists", but this will remove wordlist(s) from "My Wordlists" of current user. Do you want this? '
        )
      : false;
  };

  moveAllHandler = async () => {
    const {
      items,
      showError,
      translate,
      changeList,
      moveWordlistsToEditList
    } = this.props;

    let itemsNotInUsersList = items.filter(i => !i.inEditList);

    if (itemsNotInUsersList.length !== items.length) {
      showError({
        message: translate(
          0,
          'Some or all of the wordlists are in "My Wrodlists" of other users.',
          true
        )
      });

      const isConfirmed = this.getSuperConfirm();

      if (isConfirmed) {
        itemsNotInUsersList = items;
      }
    }

    const itemsNotInUsersListIDs = itemsNotInUsersList.map(i => i._id);

    const success = await moveWordlistsToEditList(itemsNotInUsersListIDs);
    success &&
      changeList(items.filter(w => !itemsNotInUsersListIDs.includes(w._id)));
  };

  render() {
    const { items, translate } = this.props;
    const { active } = this.state;

    return (
      <div className="main-row">
        <div className="main-row__col" style={{ width: "40%" }}>
          <SearchResultsList
            items={items}
            changeActive={this.changeActive}
            active={active}
            showNameFn={item => item.name}
            validateFn={item => item.isValid}
          />
        </div>

        <div className="main-row__col">
          <ul className={classes.actionsList}>
            <li>
              <button onClick={this.moveOneHandler} disabled={!active}>
                <img src={`${imagesURL}/FlinkMake/movefile.png`} alt="" />
                {translate(377, "Move to My Wordlist")}
              </button>
            </li>
            <li>
              <button
                onClick={this.moveAllHandler}
                disabled={!items || !items.length}
              >
                <img src={`${imagesURL}/FlinkMake/movefiles.png`} alt="" />
                {translate(375, "Move All to My Wordlist")}
              </button>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

ResultsArea.propTypes = {
  items: PropTypes.array,
  eduFilter: PropTypes.object,
  scopes: PropTypes.array.isRequired,
  changeList: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  moveWordlistsToEditList: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired
};

const mapStateToProps = ({ flinkMake, auth: { userRole } }) => ({
  scopes: flinkMake.common.scopes,
  isSuperAdmin: userRole.alias === "SUPER"
});

export default compose(
  withTranslate,
  connect(mapStateToProps, {
    showError,
    moveWordlistsToEditList
  })
)(ResultsArea);
