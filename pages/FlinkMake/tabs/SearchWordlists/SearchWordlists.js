import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";

import { Help, ActivitiesFilter } from "components/flink-components";
import { MainNav, Header } from "../../interface";
import { fetchWordlists } from "actions/flinkMakeActions";
import ResultsArea from "./ResultsArea/ResultsArea";
import { withTranslate } from "components/hocs";

export class SearchWordlists extends Component {
  state = {
    filteredItems: null,
    eduFilter: {},
    filter: {
      keyword: ""
    }
  };

  setFilteredWordlists = (filteredItems, filter, eduFilter) => {
    this.setState({ filteredItems, filter, eduFilter });
  };

  setWordlists = wordlists => {
    this.setState({ filteredItems: wordlists });
  };

  searchHandler = async () => {
    const { userId, fetchWordlists } = this.props;
    const { filter, eduFilter } = this.state;

    const params = {
      educationalCategories: { elemMatch: eduFilter },
      inEditList: { ne: userId }
    };

    if (filter.keyword)
      params.name = { regExp: filter.keyword, flags: 'i' };

    const result = await fetchWordlists(params);

    this.setState({ filteredItems: result.data });
  };

  onChangeCategories = eduFilter => this.setState({ eduFilter });
  onChangeFilter = filter => this.setState({ filter });

  render() {
    const { translate } = this.props;
    const { filteredItems, eduFilter, filter } = this.state;

    return (
      <div className="flink-make__inner">
        <MainNav />

        <div className="content-wrapper">
          <Help>{translate(612)}</Help>
          <Header />

          <div className="glass-wrapper">
            <ActivitiesFilter
              filter={filter}
              eduFilter={eduFilter}
              onChangeCategories={this.onChangeCategories}
              onChangeFilter={this.onChangeFilter}
              withoutTemplate
            >
              <button
                onClick={this.searchHandler}
                // className={classes.searchBtn}
                type="button"
                disabled={!eduFilter.scopeId}
              >
                {translate(394, "Search Wordlists")}
              </button>
            </ActivitiesFilter>

            <ResultsArea
              eduFilter={eduFilter}
              items={filteredItems}
              changeList={this.setWordlists}
            />
          </div>
        </div>
      </div>
    );
  }
}

SearchWordlists.propTypes = {
  wordlists: PropTypes.array,
  userId: PropTypes.string.isRequired,
  scopes: PropTypes.array.isRequired,
  translate: PropTypes.func.isRequired,
  fetchWordlists: PropTypes.func.isRequired
};

const mapStateToProps = ({ flinkMake, auth }) => ({
  scopes: flinkMake.common.scopes,
  userId: auth.user._id,
  wordlists: flinkMake.wordlists.list
});

const mapDispatchToProps = {
  fetchWordlists
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withTranslate
)(SearchWordlists);
