import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

import { MainNav, Header } from '../../interface';
import {
  fetchActivities,
  editActivityGroup,
  fetchActivityGroups,
  getGroupElementsById,
  deleteActivityGroup,
} from 'actions/flinkMakeActions';
import { getSolutions } from 'actions/solutionsActions';
import { showConfirmPromise, showError, showInfo } from 'actions/statusActions';
import {
  Help,
  ActivitiesFilter,
  SearchResultsList,
} from 'components/flink-components';
import { ActivityThumbnail } from '../../interface';
import ActivityGroupsBox from './ActivityGroupsBox/ActivityGroupsBox';
import classes from './ActivityGroups.module.scss';
import filterActivityGroups from './filterActivityGroups';

const initialState = {
  eduFilter: {},
  filter: { keyword: '' },
  activityGroups: [],
  activeGroupId: '',
  activeInSearchResults: null,
  activeGroupElements: null,
  currentGroupFilter: null,
  currentSearchFilter: null,
  searchResults: null,
  activeSearchItem: null,
  filteredSearchResults: null,
};

export class ActivityGroups extends Component {
  state = initialState;

  componentDidUpdate(prevProps, prevState) {
    const {
      activeInSearchResults,
      filteredSearchResults,
      searchResults,
      activeGroupElements,
      activeGroupId,
    } = this.state;

    if (
      activeInSearchResults &&
      prevState.filteredSearchResults !== filteredSearchResults
    ) {
      if (
        !filteredSearchResults ||
        !filteredSearchResults.length ||
        !_.find(filteredSearchResults, { _id: activeInSearchResults._id })
      ) {
        this.setState({ activeInSearchResults: null });
      }
    }

    if (
      searchResults !== prevState.searchResults ||
      activeGroupElements !== prevState.activeGroupElements ||
      activeGroupId !== prevState.activeGroupId
    ) {
      this.filterSearchResults();
    }
  }

  componentWillUnmount() {
    this.saveGroupChanges();
  }

  searchActivities = async () => {
    const { fetchActivities } = this.props;
    const { filter, eduFilter } = this.state;

    const activeGroup = this.getActiveGroup();

    const correctEdu =
      (activeGroup &&
        activeGroup.educationalCategories &&
        activeGroup.educationalCategories[0]) ||
      eduFilter;

    const params = {
      educationalCategories: { elemMatch: correctEdu },
      generatedFromWordlist: { exists: false },
      isValid: true,
    };

    if (filter.templateId) params.templateId = filter.templateId;
    if (filter.keyword)
      params.activityName = { regExp: filter.keyword, flags: 'i' };

    const result = await fetchActivities(params);

    const currentSearchFilter = {
      ...filter,
      isGroups: false,
      eduFilter: { ...correctEdu },
    };

    if (result.success) {
      this.setState({
        searchResults: result.data,
        currentSearchFilter,
      });
    }
  };

  searchActivityGroups = async () => {
    const { fetchActivityGroups } = this.props;
    const { filter, eduFilter } = this.state;

    const activeGroup = this.getActiveGroup();

    const correctEdu =
      (activeGroup &&
        activeGroup.educationalCategories &&
        activeGroup.educationalCategories[0]) ||
      eduFilter;

    const params = {
      educationalCategories: { elemMatch: correctEdu },
    };

    if (filter.keyword) {
      params.name = { regExp: filter.keyword, flags: 'i' };
    }

    const result = await fetchActivityGroups(params);

    const currentSearchFilter = {
      isGroups: true,
      filter: { keyword: filter.keyword },
      eduFilter: { ...correctEdu },
    };

    if (result.success) {
      this.setState({
        searchResults: result.data,
        currentSearchFilter,
      });
    }
  };

  filterSearchResults = () => {
    const { searchResults, activeGroupElements } = this.state;

    const activeGroup = this.getActiveGroup();

    if (!activeGroup) {
      return this.setState({ filteredSearchResults: searchResults });
    }

    const filteredSearchResults = _.chain(searchResults)
      .filter((item) => {
        if (item._id === activeGroup._id) {
          return false;
        }

        if (!activeGroup.forMenu) {
          // Check that item has the same educational category as activeGroup
          const activeGroupEduCategories = activeGroup.educationalCategories[0];

          const hasEdu = _.filter(
            item.educationalCategories,
            activeGroupEduCategories
          ).length;

          if (!hasEdu) return false;
        }

        if (activeGroupElements) {
          let isInGroupAlready = false;

          activeGroupElements.forEach((ele) => {
            if (isInGroupAlready) return;
            if (ele._id === item._id) {
              isInGroupAlready = true;
            }
          });

          if (isInGroupAlready) {
            return false;
          }
        }

        return true;
      })
      .value();

    this.setState({ filteredSearchResults });
  };

  displayGroups = async (forMenu = false) => {
    const { fetchActivityGroups, showInfo } = this.props;
    const { filter, eduFilter, searchResults } = this.state;

    const params = {
      select: '-elements',
      forMenu,
    };

    if (filter.keyword) {
      params.name = { regExp: filter.keyword, flags: 'i' };
    }

    if (!forMenu) {
      params.educationalCategories = { elemMatch: eduFilter };
    }

    const result = await fetchActivityGroups(params);

    if (!result.data.length) {
      showInfo({ message: 'No groups found by current filter' });
    }

    this.setState(
      {
        searchResults: forMenu ? searchResults : null,
        activityGroups: result.data,
        activeGroupElements: null,
        currentGroupFilter: {
          filter: { keyword: filter.keyword },
          forMenu,
          eduFilter: forMenu ? null : { ...eduFilter },
        },
      },
      () => this.setActiveGroupId(result.data[0] && result.data[0]._id)
    );
  };

  onChangeCategories = (eduFilter) => this.setState({ eduFilter });

  onChangeFilter = (filter) => this.setState({ filter });

  clearFilters = () => {
    this.saveGroupChanges();
    this.setState(initialState);
  };

  getActiveGroup = () => {
    const { activeGroupId, activityGroups } = this.state;

    return activeGroupId && _.find(activityGroups, { _id: activeGroupId });
  };

  saveGroupChanges = async () => {
    const { editActivityGroup } = this.props;

    const {
      elementsWasChanged,
      activeGroupId,
      activeGroupElements,
    } = this.state;

    if (!elementsWasChanged) return;

    const elements = activeGroupElements.map((ele) => ({
      _id: ele._id,
      isGroup: !ele.activityName,
    }));

    await editActivityGroup({ _id: activeGroupId, elements });
    this.setState({ elementsWasChanged: false });
  };

  setActiveGroupId = async (id) => {
    if (!id) {
      return this.setState({ activeGroupId: '', activeGroupElements: null });
    }

    if (id === this.state.activeGroupId && this.state.activeGroupElements) {
      // If ID not changed and elements already fetched, then return;
      return;
    }

    this.saveGroupChanges();

    const { getGroupElementsById } = this.props;
    const result = await getGroupElementsById(id);

    console.log('New active group ID', id);

    this.setState({ activeGroupId: id, activeGroupElements: result.data });
  };

  changeGroupElements = (elements) => {
    this.setState({ activeGroupElements: elements, elementsWasChanged: true });
  };

  changeActiveInSearchResults = (id) => {
    this.setState({ activeInSearchResults: id });
  };

  deleteActiveGroupHandler = async () => {
    const {
      deleteActivityGroup,
      translate,
      showConfirmPromise,
      showError,
      getSolutions,
    } = this.props;
    const { activeGroupId, activityGroups } = this.state;

    console.log('delete', activeGroupId);

    const activeGroup = this.getActiveGroup();
    const isGenerated = !!activeGroup.generatedFromWordlist;

    const message = isGenerated
      ? translate(
          521,
          'Are you sure you want to delete selected Activity Group? This activity group was generated from wordlist, so all activities inside will be deleted too. Continue?'
        )
      : translate(
          663,
          'Are you sure you want to delete selected Activity Group?'
        );

    const isConfirmed = await showConfirmPromise({ message });

    if (!isConfirmed) return;

    // Check if there is solutions that uses this group
    const getSolutionsResult = await getSolutions({
      'solutionMenu.menuGroups': { elemMatch: { groupId: activeGroupId } },
    });

    if (!getSolutionsResult.success) {
      console.log(
        'Error while trying to check solutions that uses current group'
      );
      return showError({ message: 'Something goes wrong, please try again' });
    }

    if (getSolutionsResult.count) {
      // There is solutions that are using current group
      const message = `
        <div>
          <p>
            Following solutions are using current group. <br>
            You sure you want to delete it anyway?
          </p>

          <ul>
            ${getSolutionsResult.data.map((s) => `<li>${s.name}</li>`).join('')}
          </ul>
        </div>
      `;
      const isConfirmed = await showConfirmPromise({ html: true, message });

      if (!isConfirmed) return;
    }

    const deleteResult = await deleteActivityGroup(activeGroupId);

    if (deleteResult.success) {
      const filteredElements = activityGroups.filter(
        (gr) => gr._id !== activeGroupId
      );

      this.setState(
        {
          activityGroups: filteredElements,
          elementsWasChanged: false,
        },
        () => {
          this.setActiveGroupId(
            filteredElements[0] ? filteredElements[0]._id : ''
          );
        }
      );
    } else {
      showError({ message: 'Something goes wrong, please try again' });
    }
  };

  addHandler = (newGroup) => {
    const activityGroups = [newGroup, ...(this.state.activityGroups || [])];

    const isMatchFilter = this.checkIfGroupMatchesFilter(newGroup);

    this.setState(
      isMatchFilter
        ? { activityGroups }
        : {
            activityGroups: [newGroup],
            currentGroupFilter: {
              filter: {},
              eduFilter:
                newGroup.educationalCategories &&
                newGroup.educationalCategories[0],
              forMenu: newGroup.forMenu,
            },
          },
      () => this.setActiveGroupId(newGroup._id)
    );
  };

  editHandler = (editedGroup) => {
    const activityGroups = (this.state.activityGroups || []).map((gr) =>
      gr._id === editedGroup._id ? editedGroup : gr
    );

    const isMatchFilter = this.checkIfGroupMatchesFilter(editedGroup);

    this.setState(
      isMatchFilter
        ? { activityGroups }
        : {
            activityGroups: [editedGroup],
            currentGroupFilter: {
              filter: {},
              eduFilter:
                editedGroup.educationalCategories &&
                editedGroup.educationalCategories[0],
              forMenu: editedGroup.forMenu,
            },
          }
    );
  };

  checkIfGroupMatchesFilter = (group) => {
    const { currentGroupFilter } = this.state;

    let isMatch = true;

    if (currentGroupFilter) {
      isMatch = !!filterActivityGroups([group], currentGroupFilter).length;
    }

    return isMatch;
  };

  render() {
    const { translate } = this.props;
    const {
      activityGroups,
      ///
      filter,
      eduFilter,
      filteredSearchResults,
      currentSearchFilter,
      currentGroupFilter,
      activeGroupElements,
      activeInSearchResults,
    } = this.state;

    const activeGroup = this.getActiveGroup();

    return (
      <div className="flink-make__inner">
        <MainNav />

        <div className="content-wrapper">
          <Help>{translate(610)}</Help>
          <Header />

          <div className="glass-wrapper">
            <ActivitiesFilter
              filter={filter}
              eduFilter={eduFilter}
              onChangeCategories={this.onChangeCategories}
              onChangeFilter={this.onChangeFilter}
              hideEduFilters={!!activeGroup && activeGroup.forMenu === false}
            >
              <div className={classes.buttonsWrapper}>
                <button
                  onClick={this.searchActivities}
                  className={classes.button}
                  type="button"
                  disabled={
                    !activeGroup ||
                    (activeGroup.forMenu && (!eduFilter || !eduFilter.scopeId))
                  }
                >
                  {translate(343, 'Search Activities')}
                </button>

                <button
                  onClick={this.displayGroups.bind(null, false)}
                  className={classes.button}
                  type="button"
                  disabled={!eduFilter.scopeId}
                >
                  {translate(666, 'Display Activity Groups')}
                </button>

                <div style={{ width: '100%', height: '10px' }} />

                <button
                  onClick={this.searchActivityGroups}
                  className={classes.button}
                  type="button"
                  disabled={
                    !activeGroup ||
                    (activeGroup.forMenu && (!eduFilter || !eduFilter.scopeId))
                  }
                >
                  {translate(391, 'Search Activity Groups')}
                </button>

                <button
                  onClick={this.clearFilters}
                  disabled={!currentGroupFilter && !currentSearchFilter}
                  className={classes.button}
                  style={{ width: '100px', minWidth: '100px' }}
                  type="button"
                >
                  {translate(664, 'Clear')}
                </button>

                <button
                  onClick={this.displayGroups.bind(null, true)}
                  className={classes.button}
                  type="button"
                >
                  {translate(667, 'Display Activity Groups for Menu')}
                </button>
              </div>
            </ActivitiesFilter>

            <div className="main-row">
              <div className="main-row__col" style={{ width: '33%' }}>
                <SearchResultsList
                  postTitle={
                    currentSearchFilter &&
                    ` (${
                      currentSearchFilter.isGroups
                        ? translate(361, 'Activity Groups')
                        : translate(7, 'Activities')
                    } - ${
                      filteredSearchResults ? filteredSearchResults.length : 0
                    })`
                  }
                  items={filteredSearchResults}
                  changeActive={this.changeActiveInSearchResults}
                  active={activeInSearchResults}
                  showNameFn={(item) =>
                    currentSearchFilter && currentSearchFilter.isGroups
                      ? item.name
                      : item.activityName
                  }
                  validateFn={(item) =>
                    (currentSearchFilter && currentSearchFilter.isGroups) ||
                    item.isValid
                  }
                />
              </div>

              <div>
                {activeInSearchResults &&
                  activeInSearchResults.activityName && (
                    <ActivityThumbnail
                      activity={activeInSearchResults}
                      marginTop="25px"
                    />
                  )}
              </div>

              <div className="main-row__col" style={{ width: '38%' }}>
                <ActivityGroupsBox
                  addHandler={this.addHandler}
                  editHandler={this.editHandler}
                  activeGroup={activeGroup}
                  activeGroupElements={activeGroupElements}
                  setActiveGroupId={this.setActiveGroupId}
                  changeGroupElements={this.changeGroupElements}
                  activityGroups={activityGroups}
                  currentGroupFilter={currentGroupFilter}
                  deleteHandler={this.deleteActiveGroupHandler}
                  searchResults={filteredSearchResults}
                  searchResultsActiveItem={activeInSearchResults}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ActivityGroups.propTypes = {
  activeGroupId: PropTypes.string,
  activityGroups: PropTypes.array,
  translate: PropTypes.func.isRequired,
  fetchActivities: PropTypes.func.isRequired,
  getGroupElementsById: PropTypes.func.isRequired,
  fetchActivityGroups: PropTypes.func.isRequired,
  editActivityGroup: PropTypes.func.isRequired,
  deleteActivityGroup: PropTypes.func.isRequired,
  showConfirmPromise: PropTypes.func.isRequired,
  getSolutions: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  showInfo: PropTypes.func.isRequired,
};

const mapStateToProps = ({ status: { translate } }) => ({
  translate,
});

const mapDispatchToProps = {
  fetchActivities,
  getGroupElementsById,
  fetchActivityGroups,
  editActivityGroup,
  deleteActivityGroup,
  getSolutions,
  showConfirmPromise,
  showError,
  showInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(ActivityGroups);
