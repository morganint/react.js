import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import _ from 'lodash';

import {
  DraggableModal,
  SelectEducationalCategory
} from 'components/flink-components';
import { withTranslate } from 'components/hocs';
import classes from './AddActivityGroupModal.module.scss';

const validateValues = values => {
  const { name, forMenu, educationalCategories, menuId } = values;
  if (!name) return false;

  if (!forMenu && (!educationalCategories || !educationalCategories.length)) {
    return false;
  }

  if (!menuId) {
    return false;
  }

  return true;
};

const startValues = {
  name: '',
  forMenu: false,
  educationalCategories: []
};

const AddActivityGroupModal = ({
  menus,
  show,
  onClose,
  submitHandler,
  translate,
  initialValues
}) => {
  const [values, setValues] = useState(startValues);

  useEffect(() => {
    setValues(initialValues || startValues);
  }, [initialValues]);

  const wasChanged =
    !initialValues ||
    (initialValues && JSON.stringify(initialValues) !== JSON.stringify(values));

  const isValidValues = validateValues(values);

  const menuOptions = useMemo(() => {
    return menus.map(m => ({ label: m.menuName, value: m._id }));
  }, [menus]);

  const selectedMenuOption = useMemo(() => {
    const { menuId } = values;
    return _.find(menuOptions, { value: menuId });
  }, [menuOptions, values]);

  const hasElements = !!(values.elements && values.elements.length);

  return (
    <DraggableModal
      show={show}
      onClose={onClose}
      title={
        initialValues
          ? translate(675, 'Edit Activity Group')
          : translate(669, 'New Activity Group')
      }
      withOverlay
    >
      <div className={classes.wrapper}>
        <div className={classes.inputGroup}>
          <label>{translate(240, 'Enter name of new activity group:')}</label>
          <input
            type="text"
            value={values.name || ''}
            onChange={e => setValues({ ...values, name: e.target.value })}
          />
        </div>

        <label className={classes.checkboxLabel}>
          <input
            checked={values.forMenu || false}
            disabled={hasElements}
            onChange={e => {
              const isChecked = e.target.checked;

              setValues({
                ...values,
                forMenu: isChecked,
                // menuId: '',
                educationalCategories: []
              });
            }}
            type="checkbox"
          />
          <span>{translate(668, 'For Menu')}</span>
        </label>
        
        <div>
          <label>{translate(453, 'Type:')}</label>
          <Select
            isClearable
            maxMenuHeight={200}
            options={menuOptions}
            value={selectedMenuOption || ''}
            onChange={opt =>
              setValues({
                ...values,
                menuId: opt && opt.value
              })
            }
          />
        </div>

        {!values.forMenu && (
          <div style={{ flexShrink: 0 }}>
            <SelectEducationalCategory
              disabled={hasElements}
              oneSet
              disableMultiple
              current={values.educationalCategories}
              changeHandler={changedEdus => {
                if (changedEdus !== values.educationalCategories) {
                  setValues({ ...values, educationalCategories: changedEdus });
                }
              }}
            />
          </div>
        )}

        <div className={classes.buttonWrapper}>
          <button
            onClick={() => submitHandler(values)}
            disabled={!isValidValues || !wasChanged}
          >
            {initialValues ? translate(672, 'Edit') : translate(91, 'Create')}
          </button>
        </div>
      </div>
    </DraggableModal>
  );
};

AddActivityGroupModal.propTypes = {
  menus: PropTypes.array.isRequired,
  initialValues: PropTypes.object,
  show: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  submitHandler: PropTypes.func.isRequired
};

export default withTranslate(AddActivityGroupModal);
