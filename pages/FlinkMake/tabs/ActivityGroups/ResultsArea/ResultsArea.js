import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import _ from "lodash";

import { ActivityThumbnail } from "../../../interface";
import { SearchResultsList } from "components/flink-components";
import ActivityGroupsBox from "../ActivityGroupsBox/ActivityGroupsBox";
import { Scope } from "utils";

class ResultsArea extends Component {
  state = { active: null };

  setFilter = eduFilter => {
    const { scopes } = this.props;

    let scopeDoc =
      eduFilter && scopes.filter(scope => scope._id === eduFilter.scopeId)[0];

    this.setState({
      eduFilter,
      scope: scopeDoc && new Scope(scopeDoc)
    });
  };

  componentDidUpdate(prevProps, prevState) {
    const { items, eduFilter } = this.props;
    const { active, eduFilter: eduFilterState, filteredItems } = this.state;

    if (!eduFilterState || items !== prevProps.items) {
      this.filterItems();
      this.setFilter(eduFilter);
    }

    if (prevProps.activeActivityGroupId !== this.props.activeActivityGroupId) {
      this.filterItems();
    }

    if (!filteredItems) return;

    if (
      (!active && filteredItems && filteredItems.length) ||
      filteredItems !== prevState.filteredItems
    ) {
      this.setState({ active: filteredItems[0] });
    }
  }

  changeActive = item => {
    this.setState({ active: item });
  };

  filterItems = () => {
    const {
      items,
      activityGroups,
      activeActivityGroupId,
      isGroups
    } = this.props;

    if (!activeActivityGroupId) {
      return this.setState({
        filteredItems: items,
        groupsThatHasActiveGroup: null
      });
    }

    const activeActivityGroup = _.find(activityGroups, {
      _id: activeActivityGroupId
    });

    if (isGroups) {
      const groupsInActiveGroup = activeActivityGroup.elements
        .filter(ele => ele.isGroup)
        .map(ele => ele._id);

      const groupsThatHasActiveGroup = activityGroups.filter(group => {
        let hasActiveGroup = false;

        group.elements.forEach(ele => {
          if (hasActiveGroup) return;

          if (ele.isGroup && ele._id === activeActivityGroupId) {
            hasActiveGroup = true;
          }
        });

        return hasActiveGroup;
      });

      if (groupsThatHasActiveGroup.length) {
        // if Group already related to another group, then don't show
        // any activity groups in resutls list to prevent adding to active group
        return this.setState({ filteredItems: [], groupsThatHasActiveGroup });
      }

      const filteredItems = _.filter(items, item => {
        if (item._id === activeActivityGroupId) {
          return false;
        }

        if (groupsInActiveGroup.includes(item._id)) {
          return false;
        }

        let shouldBeIncluded = true;

        item.elements.forEach(element => {
          if (!shouldBeIncluded) return;

          if (element.isGroup) {
            shouldBeIncluded = false;
          }
        });

        return shouldBeIncluded;
      });

      return this.setState({ filteredItems, groupsThatHasActiveGroup: null });
    }

    // If activities in search results
    const activityGroup = _.find(activityGroups, {
      _id: activeActivityGroupId
    });
    const activitiesInThatGroup = _.chain(activityGroup.elements)
      .filter(ele => !ele.isGroup)
      .map(ele => ele._id)
      .value();

    const filteredItems = _.filter(
      items,
      item => !activitiesInThatGroup.includes(item._id)
    );
    return this.setState({ filteredItems, groupsThatHasActiveGroup: null });
  };

  render() {
    const { isGroups, changeList, items } = this.props;
    const {
      filteredItems,
      active,
      scope,
      groupsThatHasActiveGroup
    } = this.state;

    return (
      <div className="main-row">
        <div className="main-row__col">
          <SearchResultsList
            items={filteredItems}
            changeActive={this.changeActive}
            active={active}
            showNameFn={item => (isGroups ? item.title : item.activityName)}
            validateFn={item => isGroups || item.isValid}
          />
        </div>

        <div>
          {isGroups ? (
            <p style={{ width: "280px", flexShrink: "0", padding: "20px" }}>
              {groupsThatHasActiveGroup && (
                <Fragment>
                  <span style={{ fontWeight: "500" }}>Already in Groups:</span>
                  {groupsThatHasActiveGroup
                    .map(group => group.title)
                    .join(", ")}
                </Fragment>
              )}
            </p>
          ) : (
            <ActivityThumbnail activity={active} marginTop="25px" />
          )}
        </div>

        <div className="main-row__col">
          <ActivityGroupsBox
            scope={scope}
            changeQueryItems={changeList}
            queryItems={items}
            isGroups={isGroups}
            resultsActiveItem={active}
            filteredItems={filteredItems}
          />
        </div>
      </div>
    );
  }
}

ResultsArea.propTypes = {
  items: PropTypes.array,
  eduFilter: PropTypes.object,
  scopes: PropTypes.array.isRequired,
  activeActivityGroupId: PropTypes.string,
  activityGroups: PropTypes.array,
  changeList: PropTypes.func.isRequired
};

const mapStateToProps = ({ flinkMake }) => ({
  scopes: flinkMake.common.scopes,
  activeActivityGroupId: flinkMake.activityGroups.active,
  activityGroups: flinkMake.activityGroups.list
});

export default connect(mapStateToProps)(ResultsArea);
