import _ from "lodash";

export default (groups, filters) => {
  const { filter, eduFilter, forMenu = false } = filters;

  return _.chain(groups)
    .filter(group => {
      if (forMenu !== group.forMenu) {
        return false;
      }

      if (
        !forMenu &&
        (!group.educationalCategories || !group.educationalCategories.length)
      ) {
        return false;
      }

      if (
        eduFilter &&
        !_.filter(group.educationalCategories, eduFilter).length
      ) {
        return false;
      }

      if (
        filter &&
        filter.keyword &&
        group.name.toLowerCase().indexOf(filter.keyword.toLowerCase()) === -1
      ) {
        return false;
      }

      return true;
    })
    .value();
};
