import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Select from 'react-select';
import _ from 'lodash';
import classnames from 'classnames';

import { withTranslate } from 'components/hocs';
import classes from './ActivityGroupsBox.module.scss';
import { ActionsPanel, OrderButtons } from 'components/flink-components';
import { imagesURL } from 'config';

import { showError, showInfo } from 'actions/statusActions';
import {
  checkIfGroupsCanBeAdded,
  fetchMyEditActivitiesList,
  moveToEditList,
  addActivityGroup,
  editActivityGroup
} from 'actions/flinkMakeActions';
import AddActivityGroupModal from '../AddActivityGroupModal/AddActivityGroupModal';
import { desc } from 'utils/getSorting';

const findOptionByValue = (value, options) => {
  return options.filter(option => option.value === value)[0] || {};
};

class ActivityGroupsBox extends Component {
  state = {
    addGroupModalOpened: false,
    editGroupModalOpened: false,
    groupsOptions: [],
    activeElement: null
  };

  componentDidMount() {
    this.setGroups();
  }

  componentDidUpdate(prevProps) {
    const { activityGroups, activeGroupElements, activeGroup } = this.props;
    const { activeElement } = this.state;

    if (prevProps.activityGroups !== activityGroups) {
      this.setGroups();
    }

    if (activeElement) {
      if (
        !activeGroupElements ||
        !activeGroupElements.length ||
        (activeGroup && activeGroup._id) !==
          (prevProps.activeGroup && prevProps.activeGroup._id) ||
        (activeGroupElements &&
          !_.find(activeGroupElements, { _id: activeElement._id }))
      ) {
        this.setState({ activeElement: null });
      }
    }
  }

  setActiveItemInContent = item => {
    this.setState({ activeElement: item });
  };

  setGroups = () => {
    const { activityGroups, activeGroup, setActiveGroupId } = this.props;

    if (!activityGroups || !activityGroups.length) {
      this.setState({ groupsOptions: [] });
      setActiveGroupId(null);
      return;
    }

    const groupsOptions = _.chain(activityGroups)
      .sort((a, b) => -desc(a.name, b.name))
      .map(group => ({
        label: group.name,
        value: group._id
      }))
      .value();

    const isActiveGroupInList =
      activeGroup &&
      _.find(activityGroups, {
        _id: activeGroup._id
      });

    if (!isActiveGroupInList) {
      const newActiveGroup = activityGroups.length
        ? activityGroups[0]._id
        : null;

      setActiveGroupId(newActiveGroup && newActiveGroup._id);
    }

    this.setState({
      groupsOptions
    });
  };

  showAddGroupModal = () => {
    this.setState({ addGroupModalOpened: true });
  };

  showEditGroupModal = () => {
    this.setState({ editGroupModalOpened: true });
  };

  closeModal = () => {
    this.setState({ addGroupModalOpened: false, editGroupModalOpened: false });
  };

  submitGroupFormHandler = async activityGroupValues => {
    const {
      showError,
      translate,
      editActivityGroup,
      addActivityGroup,
      editHandler,
      addHandler
    } = this.props;

    const { addGroupModalOpened, editGroupModalOpened } = this.state;

    let result;

    if (editGroupModalOpened) {
      result = await editActivityGroup(activityGroupValues);
    } else if (addGroupModalOpened) {
      result = await addActivityGroup(activityGroupValues);
    } else {
      return;
    }

    console.log(result);

    if (!result.success) {
      if (result.error === 'duplicate name') {
        showError({
          message: translate(
            609,
            'The activity Group name is already taken. Use a different name.',
            true
          )
        });
      } else {
        showError({
          message: 'Something goes wrong. Try again please'
        });
      }
      return;
    }

    this.closeModal();

    if (result.success) {
      editGroupModalOpened ? editHandler(result.data) : addHandler(result.data);
    }
  };

  addSelectedItemToGroup = async () => {
    const {
      translate,
      activeGroup,
      showError,
      searchResultsActiveItem,
      activeGroupElements,
      changeGroupElements,
      checkIfGroupsCanBeAdded
    } = this.props;

    if (!activeGroup || !searchResultsActiveItem) return;

    const isGroup = !searchResultsActiveItem.activityName;

    if (isGroup) {
      // Check if group can be added
      const result = await checkIfGroupsCanBeAdded(activeGroup._id, [
        searchResultsActiveItem._id
      ]);

      if (!result.success) {
        return showError({ message: 'Something goes wrong, please try again' });
      }

      const canBeAdded = result.data[searchResultsActiveItem._id];

      if (!canBeAdded) {
        showError({
          message: translate(
            671,
            "This group can't be added to prevent the loop."
          )
        });

        return;
      }
    }

    changeGroupElements([
      ...(activeGroupElements || []),
      { ...searchResultsActiveItem }
    ]);
  };

  addAllItemsToGroup = async () => {
    const {
      translate,
      searchResults,
      changeGroupElements,
      activeGroupElements,
      showError,
      activeGroup,
      checkIfGroupsCanBeAdded
    } = this.props;

    if (!activeGroup) return;

    const groupsThatCanMakeLoop = [];

    let activitiesToAdd = [];
    let activityGroupsToAdd = [];

    searchResults.forEach(ele => {
      const isGroup = !ele.activityName;

      (isGroup ? activityGroupsToAdd : activitiesToAdd).push(ele);
    });

    if (activityGroupsToAdd.length) {
      // Check if groups can be added
      const result = await checkIfGroupsCanBeAdded(
        activeGroup._id,
        activityGroupsToAdd.map(ele => ele._id)
      );

      if (!result.success) {
        return showError({ message: 'Something goes wrong, please try again' });
      }

      activityGroupsToAdd = activityGroupsToAdd.filter(group => {
        const canBeAdded = result.data[group._id];

        if (!canBeAdded) {
          groupsThatCanMakeLoop.push(group);
        }

        return canBeAdded;
      });
    }

    const modifiedElements = _.compact(
      _.concat(activeGroupElements, [
        ...activitiesToAdd,
        ...activityGroupsToAdd
      ])
    );

    changeGroupElements(modifiedElements);

    if (groupsThatCanMakeLoop.length) {
      showError({
        html: true,
        message: `
          <p>
            ${translate(
              671,
              'The following groups cannot be added to prevent loop: '
            )}
          </p>
          <ul>
            ${groupsThatCanMakeLoop
              .map(group => `<li>${group.name}</li>`)
              .join('')}
          </ul>
        `
      });
    }
  };

  deleteOneFromGroup = () => {
    const {
      changeGroupElements,
      activeGroup,
      activeGroupElements
    } = this.props;
    const { activeElement } = this.state;

    if (!activeGroup) return;

    const filteredElements = activeGroupElements.filter(
      ele => ele._id !== activeElement._id
    );

    changeGroupElements(filteredElements);
  };

  deleteAllFromGroup = async () => {
    const { changeGroupElements, activeGroup } = this.props;

    if (!activeGroup) return;

    changeGroupElements([]);
  };

  copyHandler = async () => {
    const {
      activeGroup,
      translate,
      moveToEditList,
      userId,
      showError,
      showInfo,
      isSuperAdmin,
      activeGroupElements,
      fetchMyEditActivitiesList
    } = this.props;

    if (!activeGroup) return;

    // Get user edit list
    const activitiesInUserEditList = await fetchMyEditActivitiesList();

    if (activitiesInUserEditList.length) {
      return showError({
        message: translate(
          670,
          'You should clear you Edit List of Activities firstly'
        )
      });
    }

    const activitiesInOtherEditLists = [];

    const activitiesInGroup = activeGroupElements
      .filter(ele => {
        if (ele.isGroup) return false;

        // ele is activity
        if (ele.inEditList && ele.inEditList !== userId) {
          activitiesInOtherEditLists.push(ele);
          return false;
        }

        return true;
      })
      .map(ele => ele._id);

    if (activitiesInOtherEditLists.length) {
      showError({
        message: `Following activities already in edit lists of other users: ${activitiesInOtherEditLists
          .map(act => act.activityName)
          .join(', ')}`
      });
    }

    let activitiesToMove;

    if (isSuperAdmin && activitiesInOtherEditLists.length) {
      const confirmed = window.confirm(
        'You are a super admin and can force move activity(ies) to your "My Edit List", but this will remove activity(ies) from "My Edit List" of other users. Do you want this? '
      );

      if (confirmed) {
        activitiesToMove = [
          ...activitiesInGroup,
          ...activitiesInOtherEditLists.map(act => act._id)
        ];
      } else {
        activitiesToMove = activitiesInGroup;
      }
    } else {
      activitiesToMove = activitiesInGroup;
    }

    if (!activitiesToMove.length) {
      return;
    }

    const success = await moveToEditList(activitiesToMove);

    if (success) {
      showInfo({
        message: translate(271, 'Success')
      });
    }
  };

  changeOrderHandler = async elements => {
    const { activeGroup, changeGroupElements } = this.props;

    if (!activeGroup) return;

    changeGroupElements(elements);
  };

  render() {
    const {
      menus,
      translate,
      searchResults,
      setActiveGroupId,
      searchResultsActiveItem,
      currentGroupFilter,
      activeGroupElements,
      activeGroup,
      deleteHandler
    } = this.props;
    const {
      addGroupModalOpened,
      editGroupModalOpened,
      groupsOptions,

      activeElement
    } = this.state;

    const isGenerated = activeGroup && !!activeGroup.generatedFromWordlist;

    return (
      <Fragment>
        {(addGroupModalOpened || editGroupModalOpened) && (
          <AddActivityGroupModal
            menus={menus}
            show={addGroupModalOpened || editGroupModalOpened}
            submitHandler={this.submitGroupFormHandler}
            onClose={this.closeModal}
            initialValues={editGroupModalOpened ? activeGroup : undefined}
          />
        )}

        <div className={classes.wrapper}>
          <div className={classes.actions}>
            <ActionsPanel
              vertical
              buttons={[
                {
                  title: translate(363, 'Add Group', true),
                  handler: this.showAddGroupModal,
                  icon: 'add.png'
                },
                {
                  title: translate(607, 'Copy Group', true),
                  handler: this.copyHandler,
                  icon: 'copy.png',
                  dontShow:
                    !activeGroup ||
                    !activeGroupElements ||
                    !activeGroupElements.length
                },
                {
                  title: translate(673, 'Edit Group', true),
                  handler: this.showEditGroupModal,
                  icon: 'authoredit.png',
                  dontShow: !activeGroup
                },
                {
                  title: translate(326, 'Delete Group', true),
                  handler: deleteHandler,
                  icon: 'delete.png',
                  dontShow: !activeGroup
                },
                {
                  title: translate(604, 'Add all to Activity Group', true),
                  handler: this.addAllItemsToGroup,
                  icon: 'doublerightarrow.png',
                  disabled: isGenerated,
                  dontShow:
                    !activeGroup || !searchResults || !searchResults.length
                },
                {
                  title: translate(603, 'Add to Activity Group', true),
                  handler: this.addSelectedItemToGroup,
                  icon: 'arrowright.png',
                  disabled: isGenerated,
                  dontShow: !activeGroup || !searchResultsActiveItem
                },
                {
                  title: translate(605, 'Delete from Activity Group', true),
                  handler: this.deleteOneFromGroup,
                  icon: 'arrowleft.png',
                  disabled: isGenerated,
                  dontShow: !activeElement
                },
                {
                  title: translate(606, 'Delete all from Activity Group', true),
                  handler: this.deleteAllFromGroup,
                  icon: 'doubleleftarrow.png',
                  disabled: isGenerated,
                  dontShow:
                    !activeGroup ||
                    !activeGroupElements ||
                    !activeGroupElements.length
                }
              ]}
            />
          </div>

          <div className={classes.content}>
            <h4>
              {translate(361, 'Activity Groups')}{' '}
              {currentGroupFilter &&
                currentGroupFilter.forMenu &&
                translate(668, 'For Menu')}
            </h4>
            <Select
              className={classes.groupSelect}
              value={
                groupsOptions &&
                findOptionByValue(activeGroup && activeGroup._id, groupsOptions)
              }
              onChange={option => setActiveGroupId(option.value)}
              options={groupsOptions || []}
            />

            <h4>{translate(608, 'Activity Group Content')}</h4>

            <ul className={classes.activityGroupContentList}>
              {activeGroupElements &&
                activeGroupElements.map(
                  ele =>
                    !!ele && (
                      <li
                        key={ele._id}
                        onClick={() => this.setActiveItemInContent(ele)}
                        style={{
                          backgroundImage: `url(${imagesURL}/FlinkMake/${
                            ele.activityName
                              ? 'activitysmall.png'
                              : 'activitygroupsmall.png'
                          })`
                        }}
                        className={classnames({
                          [classes.selected]:
                            activeElement && activeElement._id === ele._id
                        })}
                      >
                        {_.truncate(ele.activityName || ele.name, {
                          length: 45,
                          omission: '...'
                        })}
                      </li>
                    )
                )}
            </ul>

            <OrderButtons
              list={activeGroupElements}
              active={activeElement}
              changeOrderHandler={this.changeOrderHandler}
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

ActivityGroupsBox.propTypes = {
  menus: PropTypes.array.isRequired,
  translate: PropTypes.func.isRequired,
  searchResults: PropTypes.array,
  activityGroups: PropTypes.array,
  currentGroupFilter: PropTypes.object,
  searchResultsActiveItem: PropTypes.object,
  setActiveGroupId: PropTypes.func.isRequired,
  moveToEditList: PropTypes.func.isRequired,
  fetchMyEditActivitiesList: PropTypes.func.isRequired,
  changeGroupElements: PropTypes.func.isRequired,
  deleteHandler: PropTypes.func.isRequired,
  addHandler: PropTypes.func.isRequired,
  editHandler: PropTypes.func.isRequired,
  editActivityGroup: PropTypes.func.isRequired,
  addActivityGroup: PropTypes.func.isRequired,
  checkIfGroupsCanBeAdded: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  showInfo: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

const mapStateToProps = ({ flinkMake: { common }, auth }) => ({
  menus: common.menus,
  userId: auth.user._id,
  isSuperAdmin: auth.userRole.alias === 'SUPER'
});

export default compose(
  withTranslate,
  connect(mapStateToProps, {
    showError,
    showInfo,
    moveToEditList,
    addActivityGroup,
    editActivityGroup,
    fetchMyEditActivitiesList,
    checkIfGroupsCanBeAdded
  })
)(ActivityGroupsBox);
