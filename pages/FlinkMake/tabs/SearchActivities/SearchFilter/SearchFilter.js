import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { withTranslate } from 'components/hocs';
import classes from './SearchFilter.module.scss';
import { CategoriesFilter } from 'components/flink-components';
import { filterActivities } from 'utils';
import { fetchActivities } from 'actions/flinkMakeActions';

class SearchFilter extends Component {
  state = {
    eduFilter: {},
    filter: {
      templateId: '',
      keyword: '',
    },
    activityTemplateOptions: [],
  };

  componentDidMount() {
    const { activityTemplates } = this.props;

    const activityTemplateOptions = activityTemplates
      .map((templ) => ({
        value: templ._id,
        label: templ.activityTemplateName,
      }))
      .sort((a, b) => (a.label > b.label ? 1 : -1));

    this.setState({ activityTemplateOptions });
  }

  searchHandler = async () => {
    const { callback, userId, fetchActivities } = this.props;
    const { filter, eduFilter } = this.state;

    const params = {
      generatedFromWordlist: { exists: false },
      educationalCategories: { elemMatch: eduFilter },
      isValid: true,
    };

    if (filter.templateId) params.templateId = filter.templateId;
    if (filter.keyword)
      params.activityName = { regExp: filter.keyword, flags: 'i' };

    const result = await fetchActivities(params);

    const filteredActivities = filterActivities({
      activities: result.data,
      userId,
      filter,
      eduFilter,
    });

    callback(filteredActivities, filter, eduFilter);
  };

  changeFilter = (filter) => {
    this.setState({ filter: { ...this.state.filter, ...filter } });
  };

  changeEduFilter = (eduFilter) => {
    this.setState({ eduFilter });
  };

  render() {
    const { activityTemplateOptions, filter, eduFilter } = this.state;
    const { translate, disabled } = this.props;

    return (
      <div>
        <CategoriesFilter
          filter={eduFilter}
          changeHandler={this.changeEduFilter}
        />

        <div className={classes.bottomSection}>
          <div className={classes.inputGroup}>
            <label>{translate(399, 'Template')}</label>
            <select
              value={filter.templateId}
              onChange={(e) =>
                this.changeFilter({ templateId: e.target.value })
              }
            >
              <option value="">{translate(590, 'Select Template')}</option>

              {activityTemplateOptions.map((opt) => (
                <option key={opt.value} value={opt.value}>
                  {opt.label}
                </option>
              ))}
            </select>
          </div>

          <br />

          <div className={classes.inputGroup}>
            <label>{translate(372, 'Keyword')}</label>
            <input
              type="text"
              value={filter.keyword}
              onChange={(e) => this.changeFilter({ keyword: e.target.value })}
            />
          </div>

          <button
            onClick={this.searchHandler}
            className={classes.searchBtn}
            type="button"
            disabled={!eduFilter.scopeId || disabled}
          >
            {translate(343, 'Search Activities')}
          </button>
        </div>
      </div>
    );
  }
}

SearchFilter.propTypes = {
  disabled: PropTypes.bool,
  userId: PropTypes.string,
  scopes: PropTypes.array.isRequired,
  translate: PropTypes.func.isRequired,
  activityTemplates: PropTypes.array.isRequired,
  fetchActivities: PropTypes.func.isRequired,
};

const mapStateToProps = ({ flinkMake, common, auth }) => ({
  activityTemplates: common.activityTemplates,
  scopes: flinkMake.common.scopes,
  userId: auth.user._id,
});

export default compose(
  connect(mapStateToProps, { fetchActivities }),
  withTranslate
)(SearchFilter);
