import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { ActivityThumbnail } from "../../../interface";
import { SearchResultsList } from "components/flink-components";
import ActionsList from "./ActionsList/ActionsList";
import ActivityAttributes from "./ActivityAttributes/ActivityAttributes";
import { Scope } from "utils";

class ResultsArea extends Component {
  state = { active: null };

  setFilter = eduFilter => {
    const { scopes } = this.props;

    let scopeDoc =
      eduFilter && scopes.filter(scope => scope._id === eduFilter.scopeId)[0];

    this.setState({
      eduFilter,
      scope: scopeDoc && new Scope(scopeDoc)
    });
  };

  componentDidUpdate(prevProps) {
    const { activities, eduFilter } = this.props;
    const { active, eduFilter: eduFilterState } = this.state;

    if (!eduFilterState || activities !== prevProps.activities) {
      this.setFilter(eduFilter);
    }

    if (
      (!active && activities && activities.length) ||
      activities !== prevProps.activities
    ) {
      this.setState({ active: activities[0] });
    }
  }

  changeActive = activity => {
    this.setState({ active: activity });
  };

  moveFromResultsList = movedActivitiesIds => {
    const { activities } = this.props;

    this.props.changeList(
      activities.filter(act => !movedActivitiesIds.includes(act._id))
    );
    this.changeActive(null);
  };

  render() {
    const { activities, activityTemplates } = this.props;
    const { active, eduFilter, scope } = this.state;

    return (
      <div className="main-row">
        <div className="main-row__col">
          <SearchResultsList
            items={activities}
            changeActive={this.changeActive}
            active={active}
            showNameFn={item => item.activityName}
            validateFn={item => item.isValid}
          />
        </div>

        <ActivityThumbnail activity={active} marginTop="20px" />

        <div className="main-row__col">
          <ActivityAttributes
            activityTemplates={activityTemplates}
            activity={active}
            scope={scope}
            eduFilter={eduFilter}
          />

          <ActionsList
            active={active}
            searchResults={activities}
            moveFromResultsList={this.moveFromResultsList}
          />
        </div>
      </div>
    );
  }
}

ResultsArea.propTypes = {
  activities: PropTypes.array,
  eduFilter: PropTypes.object,
  scopes: PropTypes.array.isRequired,
  activityTemplates: PropTypes.array.isRequired,
  changeList: PropTypes.func.isRequired
};

const mapStateToProps = ({ flinkMake, common }) => ({
  scopes: flinkMake.common.scopes,
  activityTemplates: common.activityTemplates
});

export default connect(mapStateToProps)(ResultsArea);
