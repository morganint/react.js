import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import styled from 'styled-components';

import { withTranslate } from 'components/hocs';

const ActivityAttributes = ({
  translate,
  activity,
  eduFilter,
  scope,
  activityTemplates
}) => {
  const [currentEdu, setCurrentEdu] = useState(null);
  const [currentTemplate, setCurrentTemplate] = useState(null);

  const activityId = activity && activity._id;
  const templateId = activity && activity.templateId;
  const educationalCategories = activity && activity.educationalCategories;

  useEffect(() => {
    if (!templateId || !educationalCategories) return;
    const currentTemplate = _.find(activityTemplates, {
      _id: templateId
    });
    const currentEdu = _.find(educationalCategories, eduFilter);
    setCurrentEdu(currentEdu);
    setCurrentTemplate(currentTemplate);
  }, [activityId, eduFilter, activityTemplates, educationalCategories, templateId]);

  if (!activity || !scope) return null;

  const singleFirstMenu = scope.getSingleFirstMenuTitle();

  return (
    <StyledWrapper>
      <h4>{translate(359, 'Activity Attributes')}</h4>

      <table>
        <tbody>
          <tr>
            <th>Template:</th>
            <td>{currentTemplate && currentTemplate.activityTemplateName}</td>
          </tr>
          <tr>
            <th>{_.capitalize(singleFirstMenu)}:</th>
            <td>{currentEdu && currentEdu[singleFirstMenu]}</td>
          </tr>
          <tr>
            <th>{scope.getFirstCategoryTitle()}:</th>
            <td>
              {currentEdu &&
                currentEdu.firstCategory &&
                scope.getFirstCategoryTitleById(currentEdu.firstCategory)}
            </td>
          </tr>
          <tr>
            <th>{scope.getSecondCategoryTitle()}:</th>
            <td>
              {currentEdu &&
                currentEdu.secondCategory &&
                scope.getSecondCategoryTitleById(currentEdu.secondCategory)}
            </td>
          </tr>
          <tr>
            <th>{scope.getThirdCategoryTitle()}:</th>
            <td>
              {currentEdu &&
                currentEdu.thirdCategory &&
                scope.getThirdCategoryTitleById(currentEdu.thirdCategory)}
            </td>
          </tr>
        </tbody>
      </table>
    </StyledWrapper>
  );
};

ActivityAttributes.propTypes = {
  activity: PropTypes.object,
  translate: PropTypes.func.isRequired,
  activityTemplates: PropTypes.array.isRequired,
  scope: PropTypes.object,
  eduFilter: PropTypes.object
};

export default withTranslate(ActivityAttributes);

const StyledWrapper = styled.div`
  width: 350px;

  table {
    width: 100%;
    font-size: 14px;
  }

  th {
    text-align: left;
  }

  td {
    text-align: right;
  }
`;
