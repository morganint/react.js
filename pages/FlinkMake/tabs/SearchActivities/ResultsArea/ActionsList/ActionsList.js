import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import {
  moveToEditList,
  copyActivityInEditList
} from 'actions/flinkMakeActions';
import { showInfo } from 'actions/statusActions';
import { withTranslate } from 'components/hocs';
import { imagesURL } from 'config';
import classes from './ActionsList.module.scss';
import { TemplateWrapper } from "components/flink-play";
import { SUPER } from 'consts/user-roles';

class ActionsList extends Component {
  state = { preview: false, userPermissions: {} };

  componentDidMount() {
    const { userRole } = this.props;

    const userPermissions =
      _.get(userRole, 'permissions[flinkMake][searchActivitiesTab][actions]') ||
      {};

    this.setState({ userPermissions });
  }

  openPreview = () => {
    this.setState({ preview: true });
  };

  closePreview = () => {
    this.setState({ preview: false });
  };

  getSuperConfirm = () => {
    const { isSuper } = this.props;
    return isSuper
      ? window.confirm(
          'You are a super admin and can force move activity(ies) to your "My Edit List", but this will remove activity(ies) from "My Edit List" of current user. Do you want this? '
        )
      : false;
  };

  moveHandler = async () => {
    const {
      showInfo,
      active,
      moveToEditList,
      moveFromResultsList
    } = this.props;

    if (active.inEditList) {
      const isConfirmed = this.getSuperConfirm();

      if (!isConfirmed) {
        return showInfo({
          message: 'Activity already in edit list of other user'
        });
      }
    }

    await moveToEditList([active._id]);
    moveFromResultsList([active._id]);
  };

  moveAllHandler = async () => {
    const {
      showInfo,
      moveToEditList,
      moveFromResultsList,
      searchResults
    } = this.props;

    const filteredActivities = searchResults
      .filter(act => !act.inEditList)
      .map(act => act._id);

    if (!filteredActivities.length) {
      const isConfirmed = this.getSuperConfirm();

      if (!isConfirmed) {
        return showInfo({
          message: 'All activities already in edit list of other user'
        });
      }
    }

    await moveToEditList(filteredActivities);
    moveFromResultsList(filteredActivities);
  };

  render() {
    const { active, translate, copyActivityInEditList, isSuper } = this.props;
    const { preview, userPermissions } = this.state;

    if (!active) {
      return null;
    }

    if (preview) {
      return (
        <TemplateWrapper
          preview
          activity={active._id}
          closeHandler={this.closePreview}
        />
      );
    }

    return (
      <ul className={classes.actionsList}>
        {(isSuper || userPermissions.move) && (
          <li>
            <button onClick={this.moveHandler}>
              <img src={`${imagesURL}/FlinkMake/movefile.png`} alt="" />
              {translate(376, 'Move to My Edit List')}
            </button>
          </li>
        )}

        {(isSuper || userPermissions.moveAll) && (
          <li>
            <button onClick={this.moveAllHandler}>
              <img src={`${imagesURL}/FlinkMake/movefiles.png`} alt="" />
              {translate(374, 'Move All to My Edit List')}
            </button>
          </li>
        )}

        {(isSuper || userPermissions.copy) && (
          <li>
            <button
              onClick={() => {
                copyActivityInEditList(active._id);
              }}
            >
              <img src={`${imagesURL}/FlinkMake/copyfile.png`} alt="" />
              {translate(365, 'Copy to My Edit List')}
            </button>
          </li>
        )}

        {(isSuper || userPermissions.preview) && (
          <li>
            <button
              onClick={() => {
                this.openPreview();
              }}
            >
              <img src={`${imagesURL}/FlinkMake/preview_large.png`} alt="" />
              {translate(202, 'Preview')}
            </button>
            {/* <Link to={`/flink-make/preview/${active._id}`}>
            <img  src={`${imagesURL}/FlinkMake/preview_large.png`} alt="" />
            {translate(202, 'Preview')}
          </Link> */}
          </li>
        )}
      </ul>
    );
  }
}

ActionsList.propTypes = {
  userRole: PropTypes.object,
  isSuper: PropTypes.bool,
  active: PropTypes.object,
  searchResults: PropTypes.array,
  showInfo: PropTypes.func.isRequired,
  moveFromResultsList: PropTypes.func.isRequired,
  copyActivityInEditList: PropTypes.func.isRequired,
  moveToEditList: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth: { userRole } }) => ({
  isSuper: userRole.alias === SUPER,
  userRole
});

export default compose(
  connect(mapStateToProps, {
    moveToEditList,
    copyActivityInEditList,
    showInfo
  }),
  withTranslate
)(ActionsList);
