import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Help } from 'components/flink-components';
import { MainNav, Header } from '../../interface';
import ResultsArea from './ResultsArea/ResultsArea';
import SearchFilter from './SearchFilter/SearchFilter';

export class SearchActivities extends Component {
  state = { filteredActivities: null, eduFilter: {}, filter: null };

  setFilteredActivities = (filteredActivities, filter, eduFilter) => {
    this.setState({ filteredActivities, filter, eduFilter });
  };

  setActivities = activities => {
    this.setState({ filteredActivities: activities });
  };

  render() {
    const { translate } = this.props;
    const { filteredActivities, eduFilter } = this.state;

    return (
      <div className="flink-make__inner">
        <MainNav />

        <div className="content-wrapper">
          <Help>{translate(190)}</Help>
          <Header />

          <div className="glass-wrapper">
            <SearchFilter callback={this.setFilteredActivities} />

            <ResultsArea
              eduFilter={eduFilter}
              activities={filteredActivities}
              changeList={this.setActivities}
            />
          </div>
        </div>
      </div>
    );
  }
}

SearchActivities.propTypes = {
  translate: PropTypes.func.isRequired
};

const mapStateToProps = ({ status: { translate } }) => ({ translate });

export default connect(mapStateToProps)(SearchActivities);
