import React, { useEffect, useState, useMemo } from "react";
import _ from "lodash";
import PropTypes from "prop-types";

import { withTranslate } from "components/hocs";
import { axiosAPI } from "utils";
import { useFormData } from "utils/hooks";
import classes from "./ActivitySettings.module.scss";
import { SelectEducationalCategory } from "components/flink-components";
import { SUPER } from "consts/user-roles";

const ActivitySettings = ({
  auth,
  activity,
  translate,
  changeHandler,
  activityTemplates,
  changeEdusInAllEditList
}) => {
  const [data, handleDataChange] = useFormData(activity, changeHandler);
  const [currentTemplate, setTemplate] = useState();
  const { educationalCategories } = data;
  const [selectedScopeId, setSelectedScopeId] = useState();

  const isSuper = auth.user.roleAlias === SUPER;

  const userPermissions = useMemo(
    () =>
      _.get(
        auth.userRole,
        "permissions[flinkMake][editActivitiesTab][editActivityControls]"
      ) || {},
    [auth]
  );

  const onHelpActivityToggle = useMemo(
    () => async (id, isChecked) => {
      // If checked
      if (isChecked) {
        return handleDataChange({ isHelpActivity: true });
      }

      const res = await axiosAPI.get(
        `/api/activities/check-if-activity-used-as-help/${id}`
      );

      if (res.data.isUsed) {
        alert(
          "Activity is used as help activity in solutions: " +
            res.data.solutions.map(s => s.name).join(", ")
        );
      } else {
        handleDataChange({ isHelpActivity: isChecked });
      }
    },
    [handleDataChange]
  );

  useEffect(() => {
    const currentTemplate = _.find(activityTemplates, {
      _id: data.templateId
    });

    setTemplate(currentTemplate);
  }, [data._id, activityTemplates, data.templateId]);

  return (
    <div className={classes.form}>
      <div className={classes.form__group}>
        <label className={classes.form__label}>{translate(183, "Name:")}</label>
        <input
          type="text"
          disabled={!isSuper && !userPermissions.editName}
          value={data.activityName}
          onChange={e => handleDataChange({ activityName: e.target.value })}
        />
      </div>

      {currentTemplate && (
        <div className={classes.form__group}>
          <label className={classes.form__label}>
            {translate(399, "Template:")}
          </label>
          <span>{currentTemplate.activityTemplateName}</span>
        </div>
      )}

      {(isSuper || userPermissions.helpActivity) && (
        <div className={classes.form__group}>
          <label className={classes["form__checkbox-label"]}>
            <input
              // disabled={!isSuper && !userPermissions.helpActivity}
              type="checkbox"
              checked={data.isHelpActivity}
              onChange={e => onHelpActivityToggle(data._id, e.target.checked)}
            />
            {translate(0, "Help Activity")}
          </label>
        </div>
      )}

      {(isSuper ||
        userPermissions.eduCategories ||
        userPermissions.setEditList) && (
        <fieldset className={classes.form__fieldset}>
          <legend>{translate(396, "Select the educational categories")}</legend>

          {(isSuper || userPermissions.eduCategories) && (
            <SelectEducationalCategory
              // disabled={!isSuper && !userPermissions.eduCategories}
              current={educationalCategories}
              changeHandler={changedEdus => {
                if (changedEdus !== educationalCategories) {
                  handleDataChange({ educationalCategories: changedEdus });
                }
              }}
              onScopeChange={setSelectedScopeId}
            />
          )}

          {(isSuper || userPermissions.setEditList) && (
            <button
              disabled={!selectedScopeId}
              className={classes.btn}
              type="button"
              onClick={() =>
                changeEdusInAllEditList(educationalCategories, selectedScopeId)
              }
            >
              {translate(398, "Set Edit List Scope")}
            </button>
          )}
        </fieldset>
      )}
    </div>
  );
};

ActivitySettings.propTypes = {
  auth: PropTypes.object.isRequired,
  activityTemplates: PropTypes.array.isRequired,
  activity: PropTypes.object.isRequired,
  changeHandler: PropTypes.func.isRequired,
  changeEdusInAllEditList: PropTypes.func.isRequired
};

export default withTranslate(ActivitySettings);
