import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';

import { withTranslate } from 'components/hocs';
import {
  Help,
  AddActivityModal,
  ActionsPanel,
  DataList
} from 'components/flink-components';
import { MainNav, Header, ActivityThumbnail } from '../../interface';
import {
  deleteActivity,
  editActivity,
  createActivity,
  changeSettingsForReal,
  fetchMyEditActivitiesList,
  undoDeleteActivity,
  removeFromEditList,
  setActiveActivity,
  deleteActivitiesForReal,
  copyActivityInEditList
} from 'actions/flinkMakeActions';
import ActivitySettings from './ActivitySettings/ActivitySettings';
import { SUPER } from 'consts/user-roles';

class EditActivities extends Component {
  state = {
    showAddModal: false,
    userPermissions: {},
    isSuper: false
  };

  componentDidMount() {
    const { fetchMyEditActivitiesList, hasUnsavedChanges, auth } = this.props;
    const isSuper = auth.user.roleAlias === SUPER;

    const userPermissions =
      _.get(
        auth.userRole,
        'permissions[flinkMake][editActivitiesTab][editListButtons]'
      ) || {};

    if (!hasUnsavedChanges) {
      fetchMyEditActivitiesList();
      this.setState({ isSuper, userPermissions, editListLoaded: true });
    } else {
      this.setState({ isSuper, userPermissions });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      editActivitiesList,
      fetchMyEditActivitiesList,
      setActiveActivity,
      activeActivity,
      hasUnsavedChanges
    } = this.props;

    const { editListLoaded } = this.state;

    if (!hasUnsavedChanges && !editListLoaded) {
      console.log('fetching');

      fetchMyEditActivitiesList();
      this.setState({ editListLoaded: true });
    }

    if (!activeActivity && editActivitiesList && editActivitiesList.length) {
      return setActiveActivity(editActivitiesList[0]);
    }
  }

  componentWillUnmount() {
    this.saveChanges();
  }

  saveChanges = () => {
    const {
      activitiesToDelete,
      deleteActivitiesForReal,
      editActivitiesList,
      changeSettingsForReal
    } = this.props;

    // change settings in activities that was changed for real
    editActivitiesList &&
      changeSettingsForReal(editActivitiesList.filter(act => act.wasChanged));

    // delete activities for real
    console.log('Activities to delete', activitiesToDelete);
    deleteActivitiesForReal(activitiesToDelete);
  };

  deleteHandler = async () => {
    const {
      deleteActivity,
      translate,
      activeActivity,
      setActiveActivity
    } = this.props;

    // TODO add confirmation popup
    const isConfirmed = window.confirm(
      translate(28, 'You sure you want to delete activity?')
    );

    if (!isConfirmed) return;

    deleteActivity(activeActivity);
    setActiveActivity(null);
  };

  undoDeleteHandler = () => {
    const { undoDeleteActivity } = this.props;
    undoDeleteActivity();
  };

  openAddModal = () => {
    this.setState({ showAddModal: true });
  };

  closeAddModal = () => {
    this.setState({ showAddModal: false });
  };

  createActivityHandler = async data => {
    console.log(data);
    const {
      createActivity,
      setActiveActivity,
      translate,
      currentLocale
    } = this.props;
    const { activityName, template } = data;

    const activityData = {
      templateAlias: template.alias,
      templateId: template._id,
      // useDefaultAudioHelp: true,
      helpHtml: translate(template.helpStrings.activity, '', false, true),
      helpAudio: template.activityTemplateName.split(' ').join('') + '.mp3'
    };

    const newActivity = await createActivity({
      activityName,
      templateId: template._id,
      activityData,
      language: currentLocale.name
    });

    if (!newActivity) return;

    setActiveActivity(newActivity);
    this.editActivityContent();
  };

  editActivityContent = () => {
    const { activeActivity } = this.props;

    if (!activeActivity || !activeActivity._id) return;

    this.props.history.push(
      `/flink-make/edit-activities/${activeActivity._id}`
    );
  };

  previewHandler = () => {
    const { history, activeActivity } = this.props;

    history.push(`/flink-make/preview/${activeActivity._id}`);
  };

  removeFromEditList = async () => {
    const {
      activeActivity,
      changeSettingsForReal,
      removeFromEditList,
      editActivitiesList,
      setActiveActivity
    } = this.props;

    const activityIdx = _.findIndex(editActivitiesList, {
      _id: activeActivity._id
    });
    const activity = editActivitiesList[activityIdx];

    if (
      !activity.educationalCategories ||
      !activity.educationalCategories.length
    ) {
      alert('You should add educational category firstly');
      return;
    }

    if (activity.wasChanged) {
      await changeSettingsForReal([activity]);
    }
    await removeFromEditList([activity._id]);

    setActiveActivity(editActivitiesList[activityIdx + 1]);
  };

  removeAllFromEditList = async () => {
    const { translate } = this.props;

    const confirmed = window.confirm(
      translate(
        401,
        'You sure you want to remove all activities (valid) from you edit list?'
      )
    );

    if (!confirmed) return;

    const {
      changeSettingsForReal,
      removeFromEditList,
      editActivitiesList,
      setActiveActivity
    } = this.props;

    const activitiesToRemoveFromList = [];

    const promises = [];

    editActivitiesList.forEach(activity => {
      if (
        !activity ||
        !activity.isValid ||
        !activity.educationalCategories ||
        !activity.educationalCategories.length
      ) {
        return;
      }

      if (activity.wasChanged) {
        promises.push(changeSettingsForReal([activity]));
      }

      activitiesToRemoveFromList.push(activity._id);
    });

    activitiesToRemoveFromList &&
      (await removeFromEditList(activitiesToRemoveFromList));

    setActiveActivity(null);
  };

  copyHandler = async () => {
    const {
      activeActivity,
      copyActivityInEditList,
      setActiveActivity
    } = this.props;

    // const activityFromList = _.find(
    //   editActivitiesList,
    //   a => a._id === activeActivity._id
    // );

    const { activity, error } = await copyActivityInEditList(
      activeActivity._id,
      activeActivity
    );

    if (error) {
      alert(error);
    }

    if (activity) {
      setActiveActivity(activity);
    }
  };

  changeSettingsHandler = editedActivity => {
    const { editActivity } = this.props;
    editActivity(editedActivity);
  };

  changeEdusInAllEditList = (edus, scopeId) => {
    const { translate } = this.props;
    const userConfirm = window.confirm(
      translate(
        530,
        `This may replace category values in the other activities in My Edit List with category values from this activity`
      )
    );

    if (!userConfirm) return;

    const filteredEdus = _.filter(edus, { scopeId });

    const { editActivitiesList } = this.props;

    _.forEach(editActivitiesList, activity => {
      const activityCopy = JSON.parse(JSON.stringify(activity));

      activityCopy.educationalCategories = _.chain(
        activityCopy.educationalCategories || []
      )
        .reject({ scopeId })
        .concat(filteredEdus)
        .value();

      this.changeSettingsHandler(activityCopy);
    });
  };

  render() {
    const { showAddModal, userPermissions, isSuper } = this.state;
    const {
      auth,
      activeActivity,
      editActivitiesList,
      activitiesToDelete,
      activityTemplates,
      setActiveActivity,
      translate
    } = this.props;

    return (
      <div className="flink-make__inner">
        <MainNav />

        <AddActivityModal
          activityTemplates={activityTemplates}
          show={showAddModal}
          onClose={this.closeAddModal}
          onCreate={this.createActivityHandler}
        />

        <div className="content-wrapper">
          <Help>
            {editActivitiesList && editActivitiesList.length
              ? translate(466)
              : translate(465)}
          </Help>

          <Header />

          <div className="main-row">
            <div className="main-row__col">
              <h4>{translate(181, 'My Edit List')}</h4>
              <div className="glass-wrapper glass-wrapper--full-height">
                <ActionsPanel
                  buttons={[
                    {
                      title: translate(12, 'Add Activity', true),
                      handler: this.openAddModal,
                      icon: 'add.png',
                      dontShow: !isSuper && !userPermissions.add
                    },
                    {
                      title: translate(90, 'Copy Activity', true),
                      handler: this.copyHandler,
                      icon: 'copy.png',
                      disabled:
                        !activeActivity || activeActivity.generatedFromWordlist,
                      dontShow:
                        (!isSuper && !userPermissions.copy) ||
                        !editActivitiesList ||
                        !editActivitiesList.length
                    },
                    {
                      title: translate(43, 'Author/Edit Activity', true),
                      handler: this.editActivityContent,
                      icon: 'authoredit.png',
                      disabled: !activeActivity || !activeActivity.templateId,
                      dontShow:
                        (!isSuper && !userPermissions.edit) ||
                        !editActivitiesList ||
                        !editActivitiesList.length
                    },
                    {
                      title: translate(97, 'Delete Activity', true),
                      handler: this.deleteHandler,
                      icon: 'delete.png',
                      disabled: !activeActivity,
                      dontShow:
                        (!isSuper && !userPermissions.delete) ||
                        !editActivitiesList ||
                        !editActivitiesList.length
                    },
                    {
                      title: translate(269, 'Undo Delete Activity', true),
                      handler: this.undoDeleteHandler,
                      icon: 'undodelete.png',
                      disabled: !activitiesToDelete.length,
                      dontShow: !isSuper && !userPermissions.undoDelete
                    },
                    {
                      title: translate(203, 'Preview Activity', true),
                      handler: this.previewHandler,
                      icon: 'preview_small.png',
                      disabled: !activeActivity || !activeActivity.isValid,
                      dontShow: !isSuper && !userPermissions.preview
                    },
                    {
                      title: translate(386, 'Remove from Edit List', true),
                      handler: this.removeFromEditList,
                      icon: 'remove_small.png',
                      disabled: !activeActivity || !activeActivity.isValid,
                      dontShow: !isSuper && !userPermissions.removeFromList
                    },
                    {
                      title: translate(674, 'Remove all from Edit List', true),
                      handler: this.removeAllFromEditList,
                      icon: 'removeall_small.png',
                      disabled:
                        !editActivitiesList || !editActivitiesList.length,
                      dontShow: !isSuper && !userPermissions.removeAllFromList
                    }
                  ]}
                />
                <DataList
                  data={editActivitiesList || []}
                  clickHandler={setActiveActivity}
                  active={activeActivity}
                  showName={activity => activity.activityName}
                  validate={activity => activity.isValid}
                  big
                  sort
                />
              </div>
            </div>

            <ActivityThumbnail activity={activeActivity} marginTop="100px" />

            <div className="main-row__col">
              <h4>{translate(94, 'Define the Activity')}</h4>
              <div className="glass-wrapper glass-wrapper--full-height">
                {activeActivity ? (
                  <ActivitySettings
                    auth={auth}
                    activityTemplates={activityTemplates}
                    activity={activeActivity}
                    changeHandler={this.changeSettingsHandler}
                    changeEdusInAllEditList={this.changeEdusInAllEditList}
                  />
                ) : (
                  <p>You should add activity first</p>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ flinkMake, common, status, auth }) => ({
  auth,
  editActivitiesList: flinkMake.editList.editActivitiesList,
  activitiesToDelete: flinkMake.editList.activitiesToDelete,
  activeActivity: flinkMake.editList.activeActivity,
  hasUnsavedChanges: flinkMake.editList.hasUnsavedChanges,
  activityTemplates: common.activityTemplates,
  currentLocale: status.lang
});

export default compose(
  connect(mapStateToProps, {
    deleteActivity,
    editActivity,
    createActivity,
    fetchMyEditActivitiesList,
    deleteActivitiesForReal,
    changeSettingsForReal,
    undoDeleteActivity,
    setActiveActivity,
    removeFromEditList,
    copyActivityInEditList
  }),
  withTranslate
)(EditActivities);
