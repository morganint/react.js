import React from 'react';
import { Link } from 'react-router-dom';

import {imagesURL} from 'config'

const Logo = () => {
  return (
    <Link className="logo-wrapper" to="/">
      <img  src={`${imagesURL}/Images/Login/FLCLogo.png`} alt="Flink Logo" />
    </Link>
  );
};

export default Logo;
