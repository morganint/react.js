import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { compose } from 'redux';
import ContentEditable from 'react-contenteditable';

import { changeGameData } from 'actions/flinkMakeActions';
import { withTranslate } from 'components/hocs';
import classes from './QnAForm.module.scss';
import { AddImageBlock, ActivityAddAudio } from 'components/flink-components';
import { validateQuestion } from 'activity-templates/moving-answers/movingAnswersHelpers';

const isFillnInReady = question => {
  return (
    validateQuestion(question) &&
    question.correct.text.trim().length &&
    question.problem.indexOf(question.correct.text) !== -1
  );
};

const getMarkedProblemHtml = ({ problem, correct }) =>
  problem.replace(correct.text, `<em>${correct.text}</em>`);

const QnAForm = ({
  incorrectNumber,
  editingActivity,
  changeGameData,
  problemsPath,
  activeProblemId,
  translate
}) => {
  const problems = _.get(editingActivity.data.gameData, problemsPath);

  const activeProblemIndex = useMemo(
    () =>
      _.findIndex(problems, {
        id: activeProblemId
      }),
    [activeProblemId, problems]
  );

  const activeProblem = problems[activeProblemIndex];
  if (!activeProblem) return null;

  const { withAudio, longAnswers, useImages } = editingActivity.data.gameData;

  const isFillIn = activeProblem.fillIn;

  return (
    <div>
      <div className={classes.header}>
        <ActivityAddAudio
          current={activeProblem.audio}
          onChangeHandler={filename => {
            changeGameData(`problems[${activeProblemIndex}].audio`, filename);
          }}
        />

        {!useImages && !longAnswers && (
          <button
            className={classes.questionTypeBtn}
            disabled={!isFillIn && !isFillnInReady(activeProblem)}
            onClick={e =>
              changeGameData(
                `problems[${activeProblemIndex}].fillIn`,
                !isFillIn
              )
            }
          >
            {isFillIn
              ? translate(520, 'Make Multiple Choice')
              : translate(491, 'Make Fill-In')}
          </button>
        )}
      </div>

      <label className={classes.label}> {translate(212, 'Question')} </label>

      <ContentEditable
        className={classes.question}
        html={
          isFillIn
            ? getMarkedProblemHtml(activeProblem)
            : activeProblem.problem || ''
        }
        onChange={e => {
          const { target } = e.nativeEvent;
          const newVal = target.innerText;

          if (
            newVal.length > 150 &&
            newVal.length > activeProblem.problem.length
          ) {
            target.innerHTML = isFillIn
              ? getMarkedProblemHtml(activeProblem)
              : activeProblem.problem;
            return;
          }

          if (isFillIn && newVal.indexOf(activeProblem.correct.text) === -1) {
            target.innerHTML = getMarkedProblemHtml(activeProblem);
            return;
          }

          changeGameData(
            `problems[${activeProblemIndex}].problem`,
            target.innerText
          );
        }}
      />

      <label className={classes.label}>
        {translate(518, 'Correct Answer:')}
      </label>

      <div
        className={
          longAnswers ? classes.longAnswerWrapper : classes.answerWrapper
        }
      >
        {useImages ? (
          <AddImageBlock
            withId
            pathToProp={`gameData.problems[${activeProblemIndex}].correct.image`}
          />
        ) : (
          <div className={classes.fieldWrapper}>
            <textarea
              disabled={isFillIn}
              className={classes.textareaAnswer}
              maxLength={longAnswers ? '120' : '25'}
              autoComplete="off"
              value={
                (activeProblem.correct && activeProblem.correct.text) || ''
              }
              onChange={e =>
                changeGameData(
                  `problems[${activeProblemIndex}].correct.text`,
                  e.target.value
                )
              }
              name="correct"
            />
          </div>
        )}

        {withAudio && (
          <ActivityAddAudio
            current={activeProblem.correct.audio}
            onChangeHandler={filename => {
              changeGameData(
                `problems[${activeProblemIndex}].correct.audio`,
                filename
              );
            }}
          />
        )}
      </div>

      <label className={classes.label}>
        {translate(430, 'Incorrect Answers:')}
      </label>

      {_.range(incorrectNumber).map(index => {
        return (
          <div
            className={
              longAnswers ? classes.longAnswerWrapper : classes.answerWrapper
            }
            key={index}
          >
            {useImages ? (
              <AddImageBlock
                withId
                pathToProp={`gameData.problems[${activeProblemIndex}].incorrectAnswers[${index}].image`}
              />
            ) : (
              <div className={classes.fieldWrapper} key={index}>
                <textarea
                  className={classes.textareaAnswer}
                  maxLength={longAnswers ? '120' : '25'}
                  autoComplete="off"
                  value={
                    (activeProblem.incorrectAnswers[index] &&
                      activeProblem.incorrectAnswers[index].text) ||
                    ''
                  }
                  onChange={e =>
                    changeGameData(
                      `problems[${activeProblemIndex}].incorrectAnswers[${index}].text`,
                      e.target.value
                    )
                  }
                  name={`incorrectAnswers[${index}]`}
                />
              </div>
            )}

            {withAudio && (
              <ActivityAddAudio
                current={
                  activeProblem.incorrectAnswers[index] &&
                  activeProblem.incorrectAnswers[index].audio
                }
                onChangeHandler={filename => {
                  changeGameData(
                    `problems[${activeProblemIndex}].incorrectAnswers[${index}].audio`,
                    filename
                  );
                }}
              />
            )}
          </div>
        );
      })}
    </div>
  );
};

QnAForm.propTypes = {
  incorrectNumber: PropTypes.number.isRequired,
  editingActivity: PropTypes.object.isRequired,
  problemsPath: PropTypes.string.isRequired,
  activeProblemId: PropTypes.string,
  translate: PropTypes.func.isRequired
};

const mapStateToProps = ({ flinkMake }) => ({
  editingActivity: flinkMake.activity.editingActivity
});

export default compose(
  connect(
    mapStateToProps,
    {
      changeGameData
    }
  ),
  withTranslate
)(QnAForm);
