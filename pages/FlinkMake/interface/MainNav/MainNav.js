import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { NeedPermissions } from 'utils';
import { withTranslate } from 'components/hocs';

const MainNav = ({ translate }) => {
  return (
    <nav className="main-nav">
      <NeedPermissions
        permissions={{ flinkMake: { tabs: { editActivities: true } } }}
      >
        <NavLink className="main-nav__link" to="/flink-make/edit-activities">
          {translate(109, 'Edit Activities')}
        </NavLink>
      </NeedPermissions>

      <NeedPermissions
        permissions={{ flinkMake: { tabs: { searchActivities: true } } }}
      >
        <NavLink className="main-nav__link" to="/flink-make/search-activities">
          {translate(343, 'Search Activities')}
        </NavLink>
      </NeedPermissions>

      <NeedPermissions
        permissions={{ flinkMake: { tabs: { activityGroups: true } } }}
      >
        <NavLink className="main-nav__link" to="/flink-make/activity-groups">
          {translate(361, 'Activity Groups')}
        </NavLink>
      </NeedPermissions>

      <NeedPermissions
        permissions={{ flinkMake: { tabs: { wordlists: true } } }}
      >
        <NavLink className="main-nav__link" to="/flink-make/wordlists">
          {translate(404, 'Wordlists')}
        </NavLink>
      </NeedPermissions>

      <NeedPermissions
        permissions={{ flinkMake: { tabs: { searchWordlists: true } } }}
      >
        <NavLink className="main-nav__link" to="/flink-make/search-wordlists">
          {translate(394, 'Search Wordlists')}
        </NavLink>
      </NeedPermissions>

      <Link to="/admin/my-account" className="main-nav__my-account">
        {translate(336, 'My Account')}
      </Link>
    </nav>
  );
};

export default withTranslate(MainNav);
