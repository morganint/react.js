import React from 'react';

import Logo from '../Logo/Logo';

const Header = ({ children }) => {
  return (
    <header className="header">
      <Logo />
      {children}
    </header>
  );
};

export default Header;
