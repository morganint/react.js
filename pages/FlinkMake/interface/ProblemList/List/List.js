import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { imagesURL } from 'config';
import classes from './List.module.scss';

const redStarUrl = `${imagesURL}/FlinkMake/starred.png`;
const greenStarUrl = `${imagesURL}/FlinkMake/stargreen.png`;

const List = ({
  listRef,
  data,
  clickHandler,
  active,
  showName,
  validate = () => false
}) => {
  return (
    <ul
      id="problem-list"
      className={classes['list']}
      ref={listRef}
      tabIndex="1"
    >
      {data.map((item, index) => {
        const styles = {
          backgroundImage: `url(${validate(item) ? greenStarUrl : redStarUrl})`
        };

        return (
          <li
            key={index}
            style={styles}
            className={classNames({
              [classes['active']]: active && active.id === item.id
            })}
            onClick={clickHandler.bind(null, item.id)}
          >
            {showName(item, index)}
          </li>
        );
      })}
    </ul>
  );
};

List.propTypes = {
  data: PropTypes.array.isRequired,
  clickHandler: PropTypes.func.isRequired,
  showName: PropTypes.func.isRequired,
  active: PropTypes.object,
  validate: PropTypes.func
};

export default List;
