import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";
import _ from "lodash";

import API from "api";
import {
  changeGameData,
  removeContentFiles,
  restoreContentFiles,
  saveEditingActivity,
  setDeletedProblems
} from "actions/flinkMakeActions";
import { ActionsPanel, OrderButtons } from "components/flink-components";
import List from "./List/List";
import { withTranslate } from "components/hocs";
import { activitiesKey } from "config";

class ProblemList extends Component {
  componentDidMount() {
    this.setActiveProblem();
    document.addEventListener("keydown", this.keyPressListener);
  }

  componentDidUpdate() {
    this.setActiveProblem();
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.keyPressListener);
  }

  changeActiveProblemId = problemId => {
    this.props.onChangeActiveProblem(problemId);
  };

  changeOrderHandler = reorderedProblems => {
    if (!reorderedProblems) return;
    const { changeGameData, problemsPath } = this.props;
    changeGameData(problemsPath, reorderedProblems);
  };

  list = React.createRef();

  keyPressListener = e => {
    if (e.target !== this.list.current) return;
    if (e.keyCode !== 40 && e.keyCode !== 38) return;
    e.preventDefault();

    const { editingActivity, problemsPath, activeProblemId } = this.props;

    const problems = _.get(editingActivity.data.gameData, problemsPath);
    const activeProblemIndex = _.findIndex(problems, { id: activeProblemId });
    const problemsLength = problems.length;

    let nextIndex;
    if (e.keyCode === 40) {
      // Arrow down
      nextIndex = activeProblemIndex + 1;
      nextIndex = nextIndex === problemsLength ? 0 : nextIndex;
    } else if (e.keyCode === 38) {
      // Arrow Up
      nextIndex = activeProblemIndex - 1;
      nextIndex = nextIndex < 0 ? problemsLength - 1 : nextIndex;
    }

    this.changeActiveProblemId(problems[nextIndex].id);
  };

  setActiveProblem = () => {
    // const { activeProblemId } = this.state;
    const {
      activeProblemId,
      editingActivity,
      changeGameData,
      problemsPath,
      dontCreateProblem,
      createProblem
    } = this.props;

    const activeProblem =
      activeProblemId &&
      _.find(editingActivity.data.gameData[problemsPath], {
        id: activeProblemId
      });

    if (!activeProblem) {
      const firstProblem = editingActivity.data.gameData[problemsPath][0];

      if (firstProblem) {
        this.changeActiveProblemId(firstProblem.id);
      } else {
        !dontCreateProblem &&
          changeGameData(`${problemsPath}[0]`, createProblem());
      }
    }
  };

  addProblemHandler = () => {
    const {
      editingActivity,
      changeGameData,
      createProblem,
      problemsPath
    } = this.props;
    const newProblem = createProblem();

    changeGameData(problemsPath, [
      ...editingActivity.data.gameData[problemsPath],
      newProblem
    ]);

    this.changeActiveProblemId(newProblem.id);
  };

  copyProblemHandler = () => {
    const {
      copyProblem,
      editingActivity: { activity, data },
      changeGameData,
      saveEditingActivity,
      problemsPath,
      activeProblemId
    } = this.props;
    const problems = [...data.gameData[problemsPath]];

    const index = _.findIndex(problems, {
      id: activeProblemId
    });

    const [copy, files] = copyProblem(problems[index]);

    const changedProblems = [
      ...problems.slice(0, index + 1),
      copy,
      ...problems.slice(index + 1)
    ];

    console.log('origin', problems[index])
    console.log('copy', copy)

    changeGameData(problemsPath, changedProblems);

    if (files.length) {
      const path = `${activitiesKey}/${activity._id}/gamedata/`;

      const params = files.map(set => ({
        src: { key: path + set.src, uploadBucket: true },
        dest: { key: path + set.dest, uploadBucket: true }
      }));

      API.storage.copyFiles(params).then(results => {
        saveEditingActivity();
        this.changeActiveProblemId(copy.id);
      });
    } else {
      this.changeActiveProblemId(copy.id);
    }
  };

  deleteProblemHandler = () => {
    const {
      editingActivity,
      changeGameData,
      removeContentFiles,
      problemsPath,
      setDeletedProblems,
      deletedProblems,
      activeProblemId,
      getContentFilesFromProblem
    } = this.props;
    const problems = [...editingActivity.data.gameData[problemsPath]];

    const index = _.findIndex(problems, {
      id: activeProblemId
    });

    const problemToDelete = problems.splice(index, 1)[0];

    const files = getContentFilesFromProblem(problemToDelete);
    removeContentFiles(files);

    changeGameData(problemsPath, problems);

    const prevId =
      index > 0
        ? editingActivity.data.gameData[problemsPath][index - 1].id
        : null;

    this.changeActiveProblemId(prevId);

    setDeletedProblems([...deletedProblems, problemToDelete]);
  };

  undeDeleteHandler = () => {
    const {
      problemsPath,
      deletedProblems,
      setDeletedProblems,
      changeGameData,
      editingActivity,
      restoreContentFiles,
      getContentFilesFromProblem
    } = this.props;

    const lastDeleted = _.last(deletedProblems);

    if (_.isArray(lastDeleted)) {
      const problems = [
        ...editingActivity.data.gameData[problemsPath],
        ...lastDeleted
      ];

      const filesToRestore = [];

      lastDeleted.forEach(problem => {
        const files = getContentFilesFromProblem(problem);
        filesToRestore.push(...files);
      });

      restoreContentFiles(filesToRestore);

      changeGameData(problemsPath, problems);

      setDeletedProblems(_.slice(deletedProblems, 0, -1));
    } else {
      const problems = [
        ...editingActivity.data.gameData[problemsPath],
        lastDeleted
      ];

      const files = getContentFilesFromProblem(lastDeleted);
      restoreContentFiles(files);

      changeGameData(problemsPath, problems);

      this.changeActiveProblemId(lastDeleted.id);

      setDeletedProblems(_.slice(deletedProblems, 0, -1));
    }
  };

  deleteAllHandler = () => {
    const {
      changeGameData,
      setDeletedProblems,
      deletedProblems,
      editingActivity,
      removeContentFiles,
      problemsPath,
      getContentFilesFromProblem
    } = this.props;

    const problems = [...editingActivity.data.gameData[problemsPath]];

    let filesToDelete = [];

    problems.forEach(problems => {
      const files = getContentFilesFromProblem(problems);
      filesToDelete.push(...files);
    });

    removeContentFiles(filesToDelete);

    changeGameData(problemsPath, []);

    this.changeActiveProblemId(null);

    setDeletedProblems([...deletedProblems, problems]);
  };

  getButtons = () => {
    const {
      beforeDelete,
      translate,
      notShouldCreate,
      deletedProblems,
      importButton,
      copyProblem
    } = this.props;
    const problems = this.getActivityProblems();
    const activeProblem = this.getActiveProblem();

    let buttons = [
      {
        title: translate(11, "Add", true),
        handler: this.addProblemHandler,
        disabled: notShouldCreate,
        icon: "add.png"
      },
      {
        title: translate(96, "Delete", true),
        handler: beforeDelete
          ? beforeDelete.bind(null, activeProblem, this.deleteProblemHandler)
          : this.deleteProblemHandler,
        disabled: !activeProblem,
        icon: "delete.png"
      },
      {
        title: translate(268, "Undo Delete", true),
        disabled: !deletedProblems.length,
        handler: this.undeDeleteHandler,
        icon: "undodelete.png"
      },
      {
        title: translate(98, "Delete All", true),
        handler: this.deleteAllHandler,
        disabled: !problems || !problems.length,
        icon: "deleteall.png"
      }
    ];

    if (importButton) {
      buttons.push({
        title: translate(331, "Import", true),
        handler: importButton.handler,
        disabled: importButton.disabled,
        icon: "Import.png"
      });
    }

    if (copyProblem) {
      buttons = [
        ...buttons.slice(0, 1),
        {
          title: translate(414, "Copy", true),
          handler: this.copyProblemHandler,
          disabled: !activeProblem,
          icon: "copy.png"
        },
        ...buttons.slice(1)
      ];
    }

    return buttons;
  };

  showProblemName = (item, idx) => {
    const { namePath, getName, namePrefix, translate } = this.props;

    const title = namePath
      ? _.get(item, namePath)
      : getName
      ? getName(item, idx)
      : namePrefix
      ? `${namePrefix} ${idx + 1}`
      : `${translate(206, "Problem:")} ${idx + 1}`;
    return _.truncate(title, { length: 40, separator: "" });
  };

  getActiveProblem = () => {
    const { activeProblemId } = this.props;

    const problems = this.getActivityProblems();
    const activeProblem = _.find(problems, { id: activeProblemId });
    return activeProblem;
  };

  getActivityProblems = () => {
    const { problemsPath, editingActivity } = this.props;

    return editingActivity.data.gameData[problemsPath];
  };

  render() {
    const { title, validate } = this.props;

    const problems = this.getActivityProblems();
    const activeProblem = this.getActiveProblem();

    // if (!activeProblem) return null;

    return (
      <div className="glass-wrapper glass-wrapper--full-height problem-list">
        <h4>{title}</h4>
        <ActionsPanel buttons={this.getButtons()} />
        <List
          listRef={this.list}
          data={problems || []}
          showName={this.showProblemName}
          clickHandler={this.changeActiveProblemId}
          active={activeProblem}
          validate={validate}
        />
        <OrderButtons
          list={problems}
          active={activeProblem}
          changeOrderHandler={this.changeOrderHandler}
        />
      </div>
    );
  }
}

ProblemList.propTypes = {
  dontCreateProblem: PropTypes.bool,
  createProblem: PropTypes.func.isRequired,
  getContentFilesFromProblem: PropTypes.func.isRequired,
  onChangeActiveProblem: PropTypes.func.isRequired,
  changeGameData: PropTypes.func.isRequired,
  saveEditingActivity: PropTypes.func.isRequired,
  removeContentFiles: PropTypes.func.isRequired,
  restoreContentFiles: PropTypes.func.isRequired,
  validate: PropTypes.func.isRequired,
  setDeletedProblems: PropTypes.func.isRequired,
  deletedProblems: PropTypes.array.isRequired,
  problemsPath: PropTypes.string.isRequired,
  namePath: PropTypes.string,
  getName: PropTypes.func,
  title: PropTypes.string,
  editingActivity: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
  beforeDelete: PropTypes.func,
  activeProblemId: PropTypes.string
};

const mapStateToProps = ({ flinkMake }) => ({
  editingActivity: flinkMake.activity.editingActivity,
  deletedProblems: flinkMake.activity.deletedProblems
});

export default compose(
  withTranslate,
  connect(mapStateToProps, {
    changeGameData,
    saveEditingActivity,
    removeContentFiles,
    restoreContentFiles,
    setDeletedProblems
  })
)(ProblemList);
