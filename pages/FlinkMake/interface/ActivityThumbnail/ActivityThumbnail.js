import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { find } from 'lodash';

import { imagesURL } from 'config';

const ActivityThumbnail = ({ activityTemplates, activity, marginTop }) => {
  if (!activity || !activity.templateId) {
    return <div className="activity-thumbnail" />;
  }

  const currentTemplate = find(activityTemplates, {
    _id: activity.templateId
  });

  return (
    <div className="activity-thumbnail">
      {currentTemplate && (
        <img
          style={{ marginTop }}
          src={`${imagesURL}/TemplateThumbnails/${
            currentTemplate.thumbFilename
          }`}
          alt=""
        />
      )}
    </div>
  );
};

ActivityThumbnail.propTypes = {
  activity: PropTypes.object,
  marginTop: PropTypes.string,
  activityTemplates: PropTypes.array.isRequired
};

const mapStateToProps = ({ common }) => ({
  activityTemplates: common.activityTemplates
});

export default connect(mapStateToProps)(ActivityThumbnail);
