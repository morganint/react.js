import React from 'react';
import PropTypes from 'prop-types';
import { AddImageBlock } from 'components/flink-components';
import { withTranslate } from 'components/hocs';

import classes from './FlinkMakeActivityImage.module.scss';

const FlinkMakeActivityImage = ({ translate }) => {
  return (
    <div className={classes.wrapper}>
      <span className={classes.text}>{translate(71, 'Activity Image')}</span>

      <AddImageBlock
        pathToProp="activityImage"
        fileName="activity-image"
        withoutTitle
        withId
      />
    </div>
  );
};

FlinkMakeActivityImage.propTypes = {
  translate: PropTypes.func.isRequired
};

export default withTranslate(FlinkMakeActivityImage);