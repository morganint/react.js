import React, { useMemo, Fragment } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';

import { AddImageBlock, ActivityAddAudio } from 'components/flink-components';
import { withTranslate } from 'components/hocs';
import useNameModes from 'consts/use-name-modes';
import classes from './WordConfigurator.module.scss';

const WordConfigurator = ({
  withUseName = false,
  withOptions = false,
  withImage = false,
  defaultFixCharacters = [],
  activity,
  translate,
  activeWordId,
  changeGameData,
}) => {
  const { data } = activity;
  const { problems } = data.gameData;

  const activeWordIndex = useMemo(
    () =>
      _.findIndex(problems, {
        id: activeWordId,
      }),
    [activeWordId, problems]
  );

  const activeWord = problems[activeWordIndex];

  const modesOptions = useMemo(
    () => [
      {
        label: translate(782, 'FIRST NAME only'),
        value: useNameModes.FIRST_NAME,
      },
      {
        label: translate(783, 'LAST NAME only'),
        value: useNameModes.LAST_NAME,
      },
      {
        label: translate(784, 'FIRST and LAST NAME'),
        value: useNameModes.FIRST_AND_LAST_NAME,
      },
    ],
    [translate]
  );

  if (!activeWord) return null;

  const useNameBlock = (
    <div className={classes.useNameBlock}>
      <label>
        <input
          type="checkbox"
          checked={!!activeWord.useName}
          onChange={(e) => {
            const { checked } = e.target;
            changeGameData(`problems[${activeWordIndex}].useName`, checked);
            if (checked && !activeWord.useNameMode) {
              changeGameData(
                `problems[${activeWordIndex}].useNameMode`,
                modesOptions[0].value
              );
            }

            if (checked && activeWord.word) {
              changeGameData(`problems[${activeWordIndex}].word`, '');
              changeGameData(`problems[${activeWordIndex}].letters`, '');
            }

            if (!checked && activeWord.useNameMode) {
              changeGameData(`problems[${activeWordIndex}].useNameMode`, '');
            }
          }}
        />
        <span>{translate(785, 'Use name')}</span>
      </label>

      {activeWord.useName &&
        modesOptions.map((option) => (
          <label key={option.value}>
            <input
              type="radio"
              checked={activeWord.useNameMode === option.value}
              onChange={(e) => {
                changeGameData(
                  `problems[${activeWordIndex}].useNameMode`,
                  option.value
                );
              }}
            />
            <span>{option.label}</span>
          </label>
        ))}
    </div>
  );

  const optionsBlock = withOptions && (
    <div className={classes.options}>
      <label className={classes.label}>{translate(382, 'Options:')}</label>
      {_.times(4, (idx) => (
        <Fragment key={idx}>
          <span className={classes.label}>{idx + 1}</span>
          <input
            tabIndex={3 + idx}
            type="text"
            maxLength="1"
            value={(activeWord.options && activeWord.options[idx]) || ''}
            onChange={(e) => {
              const { value } = e.target;
              const newOptions = [...activeWord.options];
              newOptions[idx] = value || null;
              changeGameData(
                `problems[${activeWordIndex}].options`,
                newOptions
              );
            }}
          />
        </Fragment>
      ))}
    </div>
  );

  return (
    <div>
      {withImage && (
        <AddImageBlock
          withId
          pathToProp={`gameData.problems[${activeWordIndex}].image`}
        />
      )}

      <div className="audioWrapper">
        <ActivityAddAudio
          current={activeWord.audio}
          onChangeHandler={(filename) => {
            changeGameData(`problems[${activeWordIndex}].audio`, filename);
          }}
        />
      </div>

      {withUseName && useNameBlock}

      {!activeWord.useName && (
        <>
          <div className={classes.wordFieldWrapper}>
            <label className={classes.label}>{translate(279, 'Word:')}</label>
            <input
              disabled={!!activeWord.useName}
              autoFocus
              tabIndex="1"
              type="text"
              maxLength="12"
              className={classes.wordInput}
              value={activeWord.word || ''}
              onChange={(e) => {
                const { value } = e.target;
                let { letters } = activeWord;

                if (letters.length > value.length) {
                  letters = letters.substring(0, value.length);
                } else {
                  for (let i = letters.length; i < value.length; i++) {
                    letters += defaultFixCharacters.includes(value[i])
                      ? '1'
                      : '0';
                  }
                }
                changeGameData(`problems[${activeWordIndex}].word`, value);
                changeGameData(`problems[${activeWordIndex}].letters`, letters);
              }}
            />
          </div>

          <div className={classes.wordConfig}>
            {_.times(12, (idx) => {
              const { word, letters } = activeWord;
              const letter = word && word[idx];
              const isChecked = letters[idx] === '1';

              return (
                <div key={idx} className={classes.letterBox}>
                  <label
                    className={classnames({ [classes.disabledLabel]: !letter })}
                    htmlFor={`letter-${idx}`}
                  >
                    {letter}
                  </label>
                  <input
                    tabIndex="-1"
                    checked={isChecked}
                    onChange={(e) => {
                      const { checked } = e.target;
                      const newLetters =
                        letters.substring(0, idx) +
                        (checked ? '1' : '0') +
                        letters.substring(idx + 1);
                      changeGameData(
                        `problems[${activeWordIndex}].letters`,
                        newLetters
                      );
                    }}
                    id={`letter-${idx}`}
                    type="checkbox"
                    disabled={!letter}
                  />
                </div>
              );
            })}
          </div>
        </>
      )}

      {withOptions && optionsBlock}

      <div className={classes.definition}>
        <label className={classes.label}>{translate(522, 'Definition:')}</label>
        <textarea
          tabIndex="2"
          rows="3"
          value={activeWord.definition || ''}
          onChange={(e) =>
            changeGameData(
              `problems[${activeWordIndex}].definition`,
              e.target.value
            )
          }
        />
      </div>
    </div>
  );
};

WordConfigurator.propTypes = {
  withUseName: PropTypes.bool,
  withOptions: PropTypes.bool,
  withImage: PropTypes.bool,
  activeWordId: PropTypes.string,
  defaultFixCharacters: PropTypes.array,
  activity: PropTypes.object.isRequired,
  changeGameData: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
};

export default withTranslate(WordConfigurator);
