import React, { useCallback, useState, useRef } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

import { getLoginScreenByEmail } from 'actions/authActions';
import { imagesURL } from 'config';
import { redirect } from 'utils';
import classes from './Home.module.scss';

const Home = ({ auth, getLoginScreenByEmail, history }) => {
  const [error, setError] = useState('');

  const submitHandler = useCallback(
    async (e) => {
      e.preventDefault();
      setError('');

      const emailValue = emailRef.current.value;

      const result = await getLoginScreenByEmail(emailValue);

      if (result.success) {
        history.push(result.data);
      } else {
        setError(result.error || 'Something goes wrong');
      }
    },
    [history, getLoginScreenByEmail]
  );

  const emailRef = useRef();

  if (auth.isAuthenticated) {
    return <Redirect to={redirect(auth)} />;
  }

  return (
    <div className={classes.wrapper}>
      <header className={classes.header}>
        <div className={classes.logoWrapper}>
          <img src={`${imagesURL}/Images/Login/FLCLogo.png`} alt="Flink logo" />
        </div>

        <h1 className={classes.pageTitle}>Login/Iniciar sesión</h1>
      </header>

      <main className={classes.main}>
        <p>
          Enter your email then click OK. You will see your login screen.
          Bookmark this login screen to make it easier for you and other family
          members to log in.
        </p>
        <p>
          Ingrese su nombre de correo electrónico luego haga clic en Aceptar.
          Verá su pantalla de inicio de sesión. Marque esta pantalla de inicio
          de sesión para que usted y otros miembros de la familia puedan iniciar
          sesión más fácilmente.
        </p>

        <form className={classes.form} onSubmit={submitHandler}>
          <input
            placeholder="Email/Correo electrónico"
            type="email"
            required
            ref={emailRef}
          />
          <button type="submit">OK</button>
          {error && <p className={classes.formError}>{error}</p>}
        </form>
      </main>
    </div>
  );
};

Home.propTypes = {
  auth: PropTypes.object,
  getLoginScreenByEmail: PropTypes.func.isRequired,
  // learningCenters: PropTypes.array.isRequired,
};

const mapStateToProps = ({ auth, common }) => ({
  auth,
  // learningCenters: common.learningCenters,
});

export default connect(mapStateToProps, { getLoginScreenByEmail })(Home);
