import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import styled from "styled-components";

import { NeedPermissions } from "utils";
import { imagesURL } from "config";
import { logout } from "actions/authActions";
import { allRoles } from "consts/user-roles";

import "./Home.scss";

class Home extends Component {
  render() {
    const { auth, logout } = this.props;

    return (
      <div>
        <div className="home">
          <StyledLogo>
            <img
              src={`${imagesURL}/Images/Login/FLCLogo.png`}
              alt="Flink logo"
            />
          </StyledLogo>

          <NeedPermissions
            auth={auth}
            permissions={{ apps: { flinkMake: true } }}
          >
            <Link to="/flink-make" className="home__cta">
              Make
            </Link>
          </NeedPermissions>
          <NeedPermissions
            auth={auth}
            permissions={{ apps: { flinkAdmin: true } }}
          >
            <Link to="/flink-admin" className="home__cta">
              Reports
            </Link>
          </NeedPermissions>
          <NeedPermissions auth={auth} permissions={allRoles}>
            <Link to="admin/my-account" className="home__cta">
              My-account
            </Link>
          </NeedPermissions>

          {!auth.isAuthenticated ? (
            <Fragment>
              <Link to="/login" className="home__login-button">
                Login
              </Link>
              {this.renderLearningCentersList()}
            </Fragment>
          ) : (
            <button onClick={logout} className="home__cta">
              Logout
            </button>
          )}
        </div>
      </div>
    );
  }

  renderLearningCentersList = () => {
    const { common } = this.props;

    if (!common || !common.learningCenters || !common.learningCenters.length) {
      return;
    }

    return (
      <StyledListWrapper>
        <StyledListTitle>Learning Centers List:</StyledListTitle>
        <StyledUrlsList>
          {common.learningCenters.map((learningCenter, index) => {
            if (
              learningCenter.expirationDate &&
              new Date(learningCenter.expirationDate) < new Date()
            ) {
              return null;
            }

            return (
              <li key={index}>
                <StyledUrl to={`/${learningCenter.url}`}>
                  {learningCenter.name}
                </StyledUrl>
              </li>
            );
          })}
        </StyledUrlsList>
      </StyledListWrapper>
    );
  };
}

Home.propTypes = {
  logout: PropTypes.func.isRequired,
  common: PropTypes.object.isRequired
};

const mapStateToProps = ({ auth, common }) => ({ auth, common });

export default connect(mapStateToProps, { logout })(Home);

const StyledLogo = styled.div`
  height: 220px;
  margin-bottom: 50px;
  text-align: center;

  img {
    height: 100%;
  }
`;

const StyledListWrapper = styled.div`
  text-align: center;
`;

const StyledListTitle = styled.p`
  font-size: 2rem;
  font-weight: bold;
  color: #000;
`;

const StyledUrlsList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`;

const StyledUrl = styled(Link)`
  font-size: 1.8rem;
  text-decoration: none;
  color: #e8e8e8;
  margin: 5px 0;
  display: block;

  &:hover {
    text-decoration: underline;
  }
`;
