import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';

import { MainMenuBoard, MenuGroupView } from 'components/flink-play';
import { showError, showInfo } from 'actions/statusActions';
import { getGroupElements } from 'utils';
import classes from './IndividualReport.module.scss';

class IndividualReport extends Component {
  state = {
    breadcrumbs: [],
    groupElements: [],
    currentGroup: null,
  };

  openMenuItem = (itemId) => {
    const { solutionMenu, showError, showInfo } = this.props;

    const activityGroup = _.find(solutionMenu.activityGroups, { _id: itemId });

    if (!activityGroup) {
      return showError({ message: 'Erorr: Menu Group not found' });
    }

    if (!activityGroup.elements || !activityGroup.elements.length) {
      return showInfo({ message: 'Group is empty' });
    }

    this.setState({ currentGroup: activityGroup }, this.setGroupElements);
  };

  goBackBtn = () => {
    const { breadcrumbs } = this.state;

    const breadcrumbsCopy = [...breadcrumbs];

    const previousGroup = breadcrumbsCopy.pop();

    if (previousGroup) {
      this.setState(
        {
          currentGroup: previousGroup,
          breadcrumbs: breadcrumbsCopy,
        },
        this.setGroupElements
      );
    } else {
      this.setState({
        currentGroup: null,
      });
    }
  };

  setGroupElements = () => {
    const { solutionMenu } = this.props;
    const { currentGroup } = this.state;

    if (!currentGroup) {
      return this.setState({ groupElements: [] });
    }

    const groupElements = getGroupElements(
      currentGroup,
      solutionMenu.activityGroups,
      solutionMenu.activities
    );

    this.setState({ groupElements });
  };

  render() {
    const { translate, solutionMenu, reports, closeHandler } = this.props;
    const { currentGroup, groupElements } = this.state;

    return (
      <div className={classes.individualReportWrapper}>
        {currentGroup ? (
          <MenuGroupView
            // title={title}
            items={groupElements}
            reports={reports}
            groupClickHandler={this.openMenuItem}
          />
        ) : (
          <MainMenuBoard
            translate={translate}
            solutionMenu={solutionMenu}
            reports={reports}
            openMenuItem={this.openMenuItem}
          />
        )}

        <div className={classes.buttonsWrapper}>
          {currentGroup && (
            <button onClick={this.goBackBtn} className={classes.goBackBtn}>
              {translate(58, 'Go Back')}
            </button>
          )}
          <button
            onClick={closeHandler}
            className={classes.closeIndividualReportBtn}
          >
            {translate(80, 'Close')}
          </button>
        </div>
      </div>
    );
  }
}

IndividualReport.propTypes = {
  translate: PropTypes.func.isRequired,
  solutionMenu: PropTypes.object.isRequired,
  reports: PropTypes.object.isRequired,
  showInfo: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
};

export default connect(null, { showError, showInfo })(IndividualReport);
