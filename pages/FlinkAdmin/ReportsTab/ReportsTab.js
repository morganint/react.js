import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Select from 'react-select';
import _ from 'lodash';
import classnames from 'classnames';

import API from 'api';
import { getSolutionMenu } from 'actions/flinkPlayActions';
import { showInfo } from 'actions/statusActions';
import DataList from 'components/flink-components/DataList/DataList';
import classes from './ReportsTab.module.scss';
import { FromToPicker, Help } from 'components/flink-components';
import { msToDuration } from 'utils';
import IndividualReport from './IndividualReport/IndividualReport';

export class ReportsTab extends Component {
  state = {
    process: false,
    learners: [],
    individual: true,
    activeLearner: null,
    allSolutions: true,
    allPeriod: true,
    startDate: '',
    endDate: '',
    reports: null,
    selectedSolutions: [],
    individualReport: null,
    solutionsMenus: {},
  };

  componentDidMount() {
    this.setReportGroupsOptions();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.selectedReportGroup !== this.state.selectedReportGroup) {
      this.getProductSolutions();
      this.filterLearners();
    }
  }

  getProductSolutions = async () => {
    const { selectedReportGroup } = this.state;

    const result = await API.products.getProductSolutionsList(
      selectedReportGroup.productId
    );

    if (!result.success) {
      return this.setState({ solutions: [], selectedSolutions: [] });
    }

    const solutions = result.data;

    const solutionsOptions = solutions.map((s) => ({
      value: s._id,
      label: s.displayName,
    }));

    this.setState({ solutions, solutionsOptions });
  };

  setReportGroupsOptions = () => {
    const { reportGroups } = this.props;

    if (!reportGroups) return;

    const reportGroupsOptions = reportGroups
      .map((rp) => ({
        ...rp,
        label: rp.name,
        value: rp._id,
      }))
      .sort();

    this.setState({
      reportGroupsOptions,
      selectedReportGroup: reportGroupsOptions[0],
    });
  };

  changeReportGroupHandler = (selectedReportGroup) => {
    this.setState({ selectedReportGroup, activeLearner: null });
  };

  filterLearners = () => {
    const { reportGroups, familyMembers } = this.props;
    const { selectedReportGroup } = this.state;

    if (!familyMembers || !reportGroups || !selectedReportGroup) {
      return this.setState({ learners: [] });
    }

    const learners =
      selectedReportGroup.learners &&
      selectedReportGroup.learners
        .map((_id) => _.find(familyMembers, { _id }))
        .filter((l) => !!l);

    this.setState({ learners: learners || [] });
  };

  setActiveLearner = (activeLearner) => {
    this.setState({ activeLearner });
  };

  generateReport = async () => {
    this.setState({ proceed: true, reports: null });

    if (this.state.individual) {
      await this.getIndividualReport();
    } else {
      await this.getReports();
    }

    this.setState({ proceed: false });
  };

  getIndividualReport = async () => {
    const { getSolutionMenu, showInfo } = this.props;
    const { activeLearner, solutionsMenus } = this.state;

    // Get actual info about learner
    const result = await API.familyMembers.getById(activeLearner._id);

    if (!result.success) {
      return alert('Error fetching family member');
    }

    let { reports, reportSolution, firstname, lastname } = result.data;

    const name = `${firstname} ${lastname}`;

    if (!reportSolution) {
      return showInfo({ message: `${name} hasn't product for report` });
    }

    // if (!reports) {
    //   return showInfo({ message: `${name} hasn't any reports yet` });
    // }

    let solutionMenu = solutionsMenus[reportSolution];

    try {
      // get solution menu
      if (!solutionMenu) {
        solutionMenu = await getSolutionMenu(reportSolution, {
          forMember: activeLearner._id,
        });
      }

      this.setState((state) => ({
        solutionsMenus: {
          ...state.solutionsMenus,
          [reportSolution]: solutionMenu,
        },
        individualReport: { solutionMenu, learnerReports: reports || {} },
      }));
    } catch (err) {
      alert("Can't fetch solution menu");
      this.setState({
        individualReport: null,
      });
    }
  };

  getReports = async () => {
    const {
      allPeriod,
      startDate,
      endDate,
      selectedSolutions,
      solutionsOptions,
      allSolutions,
      selectedReportGroup,
    } = this.state;

    const data = {
      solutions: (allSolutions ? solutionsOptions : selectedSolutions).map(
        (s) => s.value
      ),
    };

    if (!allPeriod) {
      data.startDate = startDate;
      data.endDate = endDate;
    }

    const result = await API.reports.byReportGroup(
      selectedReportGroup._id,
      data
    );

    if (result.success) {
      this.setState({ reports: result.data });
    }
  };

  closeIndividualReport = () => {
    this.setState({ individualReport: null });
  };

  render() {
    const { translate } = this.props;
    const {
      selectedReportGroup,
      reportGroupsOptions,
      learners,
      allSolutions,
      solutionsOptions,
      activeLearner,
      individual,
      startDate,
      endDate,
      allPeriod,
      reports,
      proceed,
      selectedSolutions,
      individualReport,
    } = this.state;

    let disabledBtn =
      proceed ||
      !(
        selectedReportGroup &&
        (individual ? !!activeLearner : !!learners.length) &&
        (individual || allSolutions || !!selectedSolutions.length) &&
        (allPeriod || (startDate && endDate))
      );

    return (
      <>
        <Help>{translate(729)}</Help>

        <div className={classes.filters}>
          <div className={classes.filterColumnWithSelect}>
            <label>{translate(726, 'Select Report Group:')}</label>

            <Select
              value={selectedReportGroup}
              onChange={this.changeReportGroupHandler}
              options={reportGroupsOptions}
            />
          </div>

          <div className={classes.filterColumn}>
            <label className={classes.radioControl}>
              <input
                type="radio"
                checked={individual}
                onChange={() =>
                  this.setState({
                    individual: true,
                    reports: null,
                  })
                }
              />
              {translate(725, 'Individual')}
            </label>

            <label className={classes.radioControl}>
              <input
                type="radio"
                checked={!individual}
                onChange={() =>
                  this.setState({ individual: false, individualReport: null })
                }
              />
              {translate(528, 'Report Group')}
            </label>
          </div>

          {!individual && (
            <>
              <div className={classes.filterColumnWithSelect}>
                <label className={classes.radioControl}>
                  <input
                    disabled={individual}
                    type="radio"
                    checked={allSolutions}
                    onChange={() =>
                      this.setState({
                        allSolutions: true,
                      })
                    }
                  />
                  {translate(711, 'All Solutions')}
                </label>

                <label className={classes.radioControl}>
                  <input
                    disabled={individual}
                    type="radio"
                    checked={!allSolutions}
                    onChange={() => this.setState({ allSolutions: false })}
                  />
                  {translate(712, 'By Solution')}
                </label>

                <Select
                  placeholder={translate(698, 'Select')}
                  isDisabled={allSolutions || individual}
                  value={selectedSolutions}
                  onChange={(opt) =>
                    this.setState({ selectedSolutions: [opt] })
                  }
                  options={solutionsOptions}
                />
              </div>

              <div className={classes.filterColumn}>
                <label className={classes.radioControl}>
                  <input
                    type="radio"
                    checked={allPeriod}
                    onChange={() =>
                      this.setState({
                        allPeriod: true,
                        startDate: '',
                        endDate: '',
                      })
                    }
                  />
                  {translate(730, 'All Dates')}
                </label>

                <label className={classes.radioControl}>
                  <input
                    type="radio"
                    checked={!allPeriod}
                    onChange={() => this.setState({ allPeriod: false })}
                  />
                  {translate(701, 'Specific Dates')}
                </label>

                <FromToPicker
                  disabled={allPeriod}
                  changeHandler={(dates) => this.setState(dates)}
                  startDate={startDate}
                  endDate={endDate}
                />
              </div>
            </>
          )}
        </div>

        <div className="flink-admin__main-row">
          {individual && (
            <div className="flink-admin__main-row-col" style={{ width: '30%' }}>
              <h4>{translate(201, 'Learners List')}</h4>

              <div className="glass-wrapper glass-wrapper--full-height">
                <DataList
                  data={learners}
                  clickHandler={this.setActiveLearner}
                  active={activeLearner}
                  showName={(l) => `${l.firstname} ${l.lastname}`}
                  withoutStars
                  big
                  sort
                />
              </div>
            </div>
          )}

          <div className="flink-admin__main-row-col" style={{ flexGrow: 1 }}>
            <h4>{translate(341, 'Reports')}</h4>

            <button
              onClick={this.generateReport}
              className={classnames('btn', classes.generateBtn)}
              disabled={disabledBtn}
            >
              {translate(704, 'Generate Report')}
            </button>

            {reports && (
              <div className="glass-wrapper glass-wrapper--full-height">
                <table className={classes.table}>
                  <thead>
                    <tr>
                      <th align="left">{translate(183, 'Name')}</th>
                      <th align="right">{translate(718, 'Total Time')}</th>
                      <th>
                        {translate(allSolutions ? 727 : 728, 'Skill Levels')}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {reports.map((learner) => (
                      <tr key={learner.id}>
                        <td>{learner.name}</td>
                        <td align="right">
                          {msToDuration(learner.totalTime || 0, {
                            translate,
                            withoutDays: true,
                          })}
                        </td>
                        <td align="center">{learner.skillsMastered}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            )}

            {individualReport && (
              <IndividualReport
                translate={translate}
                closeHandler={this.closeIndividualReport}
                reports={individualReport.learnerReports}
                solutionMenu={individualReport.solutionMenu}
              />
            )}
          </div>
        </div>
      </>
    );
  }
}

ReportsTab.propTypes = {
  reportGroups: PropTypes.array.isRequired,
  translate: PropTypes.func.isRequired,
  getSolutionMenu: PropTypes.func.isRequired,
  showInfo: PropTypes.func.isRequired,
  familyMembers: PropTypes.array,
};

const mapStateToProps = ({ flinkAdmin, status, families }) => ({
  reportGroups: flinkAdmin.reportGroups,
  translate: status.translate,
  familyMembers: families.familyMembers,
});

export default connect(mapStateToProps, { getSolutionMenu, showInfo })(
  ReportsTab
);
