import React from 'react';
import { NavLink } from 'react-router-dom';

import { withTranslate } from 'components/hocs';
import Logo from '../Logo/Logo';

const Header = ({ translate }) => {
  return (
    <header className="flink-admin__header">
      <Logo />
      <div className="flink-admin__nav">
        <NavLink
          className="flink-admin__nav-btn"
          to="/flink-admin/report-groups"
        >
          {translate(340, 'Report Groups')}
        </NavLink>
        <NavLink className="flink-admin__nav-btn" to="/flink-admin/reports">
          {translate(341, 'Reports')}
        </NavLink>
      </div>
    </header>
  );
};

export default withTranslate(Header);
