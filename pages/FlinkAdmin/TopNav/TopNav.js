import React from "react";

import { Link } from "react-router-dom";

const TopNav = (props) => {
  return (
    <nav className="flink-admin__top-nav">
      <Link to="/admin/my-account" className="flink-admin__my-account">
        My Account
      </Link>
    </nav>
  );
};

export default TopNav;
