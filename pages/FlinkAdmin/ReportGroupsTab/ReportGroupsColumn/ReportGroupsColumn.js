import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

import { editReportGroup } from 'actions/flinkAdminActions';
import { ActionsPanel, DataList } from 'components/flink-components';

class ReportGroupsColumn extends Component {
  state = {
    learners: [],
  };

  componentDidMount() {
    this.setLearners();
  }

  componentDidUpdate(prevProps) {
    const { reportGroups, selectedReportGroupId, familyMembers } = this.props;

    if (
      prevProps.selectedReportGroupId !== selectedReportGroupId ||
      prevProps.reportGroups !== reportGroups ||
      prevProps.familyMembers !== familyMembers
    ) {
      this.setLearners();
    }

    if (prevProps.selectedReportGroupId !== selectedReportGroupId) {
      this.setState({ activeLearner: null });
    }
  }

  setLearners = () => {
    const { selectedReportGroupId, reportGroups, familyMembers } = this.props;

    if (!familyMembers || !reportGroups) return;

    const selectedReportGroup = _.find(reportGroups, {
      _id: selectedReportGroupId,
    });

    const learners =
      selectedReportGroup &&
      selectedReportGroup.learners &&
      selectedReportGroup.learners
        .map((_id) => _.find(familyMembers, { _id }))
        .filter((l) => !!l);

    this.setState({
      selectedReportGroup,
      learners: learners || [],
    });
  };

  setActiveLearner = (activeLearner) => {
    this.setState({ activeLearner });
  };

  deleteMemberFromReportGroup = () => {
    const { editReportGroup } = this.props;
    const { activeLearner, selectedReportGroup } = this.state;

    if (!activeLearner || !selectedReportGroup) return;

    const filteredLearners = selectedReportGroup.learners.filter(
      (id) => id !== activeLearner._id
    );

    editReportGroup({ ...selectedReportGroup, learners: filteredLearners });
    this.setState({ activeLearner: null });
  };

  render() {
    const { translate } = this.props;
    const { activeLearner, learners } = this.state;

    return (
      <div className="glass-wrapper glass-wrapper--full-height">
        <ActionsPanel
          buttons={[
            {
              title: translate(533, 'Delete From Report Group'),
              handler: this.deleteMemberFromReportGroup,
              icon: 'delete.png',
              disabled: !activeLearner,
            },
          ]}
        />
        <DataList
          data={learners}
          clickHandler={this.setActiveLearner}
          active={activeLearner}
          showName={(l) => `${l.firstname} ${l.lastname}`}
          withoutStars
          big
          sort
        />
      </div>
    );
  }
}

ReportGroupsColumn.propTypes = {
  selectedReportGroupId: PropTypes.string,
  translate: PropTypes.func.isRequired,
  reportGroups: PropTypes.array,
  familyMembers: PropTypes.array,
};

const mapStateToProps = ({ flinkAdmin, common, families }) => ({
  reportGroups: flinkAdmin.reportGroups,
  familyMembers: families.familyMembers,
  selectedReportGroupId: flinkAdmin.selectedReportGroupId,
});

export default connect(mapStateToProps, {
  editReportGroup,
})(ReportGroupsColumn);
