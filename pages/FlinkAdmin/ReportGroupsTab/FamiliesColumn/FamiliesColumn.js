import React, { useMemo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import _ from 'lodash';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { changeSelectedFamilyId } from 'actions/flinkAdminActions';
import { DataList } from 'components/flink-components';

import classes from './FamiliesColumn.module.scss';

const FamiliesColumn = ({
  translate,
  families,
  changeSelectedFamilyId,
  selectedFamilyId,
}) => {
  const yearsOptions = useMemo(() => {
    if (!families) return [];
    return _.chain(families)
      .uniqBy('year')
      .sortBy('year')
      .map((f) => ({ label: f.year, value: f.year }))
      .value();
  }, [families]);

  const [selectedYear, setSelectedYear] = useState();
  const [selectedFamily, setSelectedFamily] = useState(null);
  const [filteredFamilies, setFilteredFamilies] = useState([]);

  useEffect(() => {
    if (!filteredFamilies) return;

    const activeFamily =
      (selectedFamilyId &&
        _.find(filteredFamilies, { _id: selectedFamilyId })) ||
      filteredFamilies[0];

    // New active family
    if (activeFamily && activeFamily._id !== selectedFamilyId) {
      changeSelectedFamilyId(activeFamily && activeFamily._id);
    }

    if (!activeFamily && selectedFamilyId) {
      changeSelectedFamilyId(null);
    }

    setSelectedFamily(activeFamily);
  }, [filteredFamilies, selectedFamilyId, changeSelectedFamilyId]);

  useEffect(() => {
    if (!families) return;

    if (!selectedYear) {
      return setFilteredFamilies([]);
    }

    const filteredFamilies = _.chain(families)
      .filter({ year: selectedYear })
      .sortBy('name')
      .value();
    setFilteredFamilies(filteredFamilies);
  }, [selectedYear, families]);

  return (
    <>
      <div className={classes.header}>
        <h4>{translate(524, 'Families List')}</h4>

        <div className={classes.yearControls}>
          <label>{translate(525, 'Year:')}</label>
          <Select
            value={_.find(yearsOptions, { value: selectedYear })}
            onChange={(opt) => setSelectedYear(opt && opt.value)}
            placeholder={translate(698, 'Select')}
            className={classes.select}
            options={yearsOptions}
            isClearable
          />
        </div>
      </div>
      <div className="glass-wrapper glass-wrapper--full-height">
        <DataList
          data={filteredFamilies || []}
          clickHandler={(f) => changeSelectedFamilyId(f._id)}
          active={selectedFamily}
          showName={(i) => i.name}
          withoutStars
          big
          sort
        />
      </div>
    </>
  );
};

FamiliesColumn.propTypes = {
  families: PropTypes.array,
  changeSelectedFamilyId: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  selectedFamilyId: state.flinkAdmin.selectedFamilyId,
  families: state.families.families,
});

export default compose(connect(mapStateToProps, { changeSelectedFamilyId }))(
  FamiliesColumn
);
