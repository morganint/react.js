import React, { useMemo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  Field,
  reduxForm,
  SubmissionError,
  formValueSelector,
} from 'redux-form';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { withTranslate } from 'components/hocs';
import { reduxFormValidator } from 'validation';
import {
  StyledError,
  MaterialSelect,
  renderMaterialTextField,
} from 'components/form-components';
import { createReportGroup, editReportGroup } from 'actions/flinkAdminActions';

const requiredFields = ['name', 'productId'];

const validate = reduxFormValidator(requiredFields);

const ReportGroupForm = ({
  translate,
  teacherId,
  initialValues,
  closeHandler,
  isOpen,
  error,
  handleSubmit,
  pristine,
  submitting,
  products,
  onSuccess,
  editReportGroup,
  createReportGroup,
  learningCenter,
  learners,
  change,
}) => {
  const submit = async (values, dispatch) => {
    console.log(values);
    const result = initialValues
      ? await editReportGroup(values)
      : await createReportGroup({ ...values, teacherId });

    if (!result.success) {
      throw new SubmissionError(result.err);
    } else {
      onSuccess && onSuccess(result.reportGroup);
    }
  };

  const productOptions = useMemo(() => {
    if (!learningCenter || !learningCenter.assignedProducts) return [];

    const productOptions = _.chain(learningCenter.assignedProducts)
      .map((prodInfo) => {
        const prod = _.find(products, { _id: prodInfo.productId });
        return prod ? { label: prod.name, value: prod._id } : null;
      })
      .compact()
      .value();

    if (productOptions.length === 1) {
      change('productId', productOptions[0].value);
    }

    return productOptions;
  }, [learningCenter, products, change]);

  return (
    <Dialog open={isOpen} onClose={closeHandler}>
      <form onSubmit={handleSubmit(submit)} style={{ width: '350px' }}>
        <DialogTitle>
          {initialValues
            ? translate(531, 'Edit Report Group')
            : translate(325, 'Create Report Group')}
        </DialogTitle>
        <DialogContent>
          <Field
            autoFocus
            name="name"
            label={translate(320, 'Name *')}
            component={renderMaterialTextField}
            fullWidth
          />
          <Field
            label={translate(338, 'Product *')}
            component={MaterialSelect}
            name="productId"
            options={productOptions}
            disabled={!!(learners && learners.length)}
          />

          {!pristine && error && <StyledError>{error}</StyledError>}
        </DialogContent>
        <DialogActions>
          <Button onClick={closeHandler} color="primary">
            {translate(52, 'Cancel')}
          </Button>
          <Button
            type="submit"
            color="primary"
            disabled={pristine || submitting}
          >
            {initialValues ? translate(672, 'Edit') : translate(91, 'Create')}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

ReportGroupForm.propTypes = {
  closeHandler: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired,
  learningCenter: PropTypes.object,
  createReportGroup: PropTypes.func.isRequired,
  editReportGroup: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  teacherId: PropTypes.string.isRequired,
  learners: PropTypes.array,
};

const selector = formValueSelector('ReportGroupForm');

const mapStateToProps = (state) => ({
  teacherId: state.auth.user._id,
  learningCenter: state.flinkAdmin.learningCenter,
  products: state.common.products,
  learners: selector(state, 'learners'),
});

export default compose(
  withTranslate,
  reduxForm({
    form: 'ReportGroupForm',
    validate,
  }),
  connect(mapStateToProps, { createReportGroup, editReportGroup })
)(ReportGroupForm);
