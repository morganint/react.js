import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Select from 'react-select';
import _ from 'lodash';

import { showConfirmPromise } from 'actions/statusActions';
import {
  editReportGroup,
  deleteReportGroup,
  changeSelectedReportGroupId,
} from 'actions/flinkAdminActions';
import { ActionsPanel } from 'components/flink-components';
import ReportGroupForm from './ReportGroupForm/ReportGroupForm';

import classes from './ReportGroupSelect.module.scss';

class ReportGroupsColumn extends Component {
  state = {
    isReportGroupFormOpened: false,
    reportGroupsOptions: [],
    reportGroupIdToEdit: null,
  };

  componentDidMount() {
    this.formatReportGroupOptions();
    this.setSelectedReportGroup();
  }

  componentDidUpdate(prevProps) {
    const { reportGroups, selectedReportGroupId } = this.props;
    if (prevProps.reportGroups !== reportGroups) {
      this.formatReportGroupOptions();
    }

    if (prevProps.selectedReportGroupId !== selectedReportGroupId) {
      this.setSelectedReportGroup();
    }
  }

  formatReportGroupOptions = () => {
    const {
      reportGroups,
      changeSelectedReportGroupId,
      selectedReportGroupId,
    } = this.props;

    if (!reportGroups) return;

    const reportGroupsOptions = reportGroups
      .map((rp) => ({
        label: rp.name,
        value: rp._id,
      }))
      .sort();

    this.setState({
      reportGroupsOptions,
    });

    if (!selectedReportGroupId) {
      changeSelectedReportGroupId(
        reportGroupsOptions[0] && reportGroupsOptions[0].value
      );
    }
  };

  addReportGroupHandler = () => {
    this.setState({ isReportGroupFormOpened: true });
  };

  editReportGroupHandler = (id) => {
    this.setState({
      isReportGroupFormOpened: true,
      reportGroupIdToEdit: this.props.selectedReportGroupId,
    });
  };

  closeReportGroupForm = () => {
    this.setState({
      isReportGroupFormOpened: false,
      reportGroupIdToEdit: null,
    });
  };

  changeReportGroupHandler = (reportGroupId) => {
    this.props.changeSelectedReportGroupId(reportGroupId);
  };

  onFormSuccess = (rp) => {
    this.closeReportGroupForm();
    this.changeReportGroupHandler(rp._id);
  };

  deleteGroupHandler = async () => {
    const {
      translate,
      deleteReportGroup,
      showConfirmPromise,
      selectedReportGroupId,
    } = this.props;

    const isConfirmed = await showConfirmPromise({
      message: translate(
        485,
        'Are you sure you want to delete this report group?'
      ),
    });

    if (!isConfirmed) return;

    deleteReportGroup(selectedReportGroupId);
  };

  setSelectedReportGroup = () => {
    const { selectedReportGroupId, reportGroups } = this.props;

    if (!reportGroups) return;

    const selectedReportGroup = _.find(reportGroups, {
      _id: selectedReportGroupId,
    });

    this.setState({
      selectedReportGroup,
    });
  };

  render() {
    const {
      reportGroups,
      selectedReportGroupId,
      products,
      translate,
    } = this.props;
    const {
      selectedReportGroup,
      isReportGroupFormOpened,
      reportGroupsOptions,
      reportGroupIdToEdit,
    } = this.state;

    const productOfSelectedGroup =
      selectedReportGroup &&
      _.find(products, { _id: selectedReportGroup.productId });

    return (
      <Fragment>
        <div className={classes.reportGroupsControls}>
          <ActionsPanel
            buttons={[
              {
                title: translate(529, 'Add Report Group', true),
                handler: this.addReportGroupHandler,
                icon: 'add.png',
              },
            ]}
          />

          <Select
            placeholder={translate(698, 'Select')}
            value={
              _.find(reportGroupsOptions, {
                value: selectedReportGroupId,
              }) || ''
            }
            onChange={(opt) => this.changeReportGroupHandler(opt.value)}
            options={reportGroupsOptions}
            className={classes.reportGroupSelect}
          />

          <ActionsPanel
            buttons={[
              {
                title: translate(531, 'Edit Report Group', true),
                handler: this.editReportGroupHandler,
                icon: 'authoredit.png',
                disabled: !selectedReportGroupId,
              },
              {
                title: translate(532, 'Delete Report Group', true),
                handler: this.deleteGroupHandler,
                icon: 'delete.png',
                disabled: !selectedReportGroupId,
              },
            ]}
          />
          <p className={classes.reportGroupProductName}>
            &nbsp;
            {productOfSelectedGroup && (
              <Fragment>
                {translate(527, 'Product:')}{' '}
                <span>{productOfSelectedGroup.name}</span>
              </Fragment>
            )}
          </p>
        </div>

        {isReportGroupFormOpened && (
          <ReportGroupForm
            onSuccess={this.onFormSuccess}
            initialValues={
              reportGroupIdToEdit &&
              _.find(reportGroups, { _id: reportGroupIdToEdit })
            }
            closeHandler={this.closeReportGroupForm}
            isOpen={isReportGroupFormOpened}
          />
        )}
      </Fragment>
    );
  }
}

ReportGroupsColumn.propTypes = {
  showConfirmPromise: PropTypes.func.isRequired,
  selectedReportGroupId: PropTypes.string,
  changeSelectedReportGroupId: PropTypes.func.isRequired,
  deleteReportGroup: PropTypes.func.isRequired,
  editReportGroup: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  reportGroups: PropTypes.array,
  products: PropTypes.array,
};

const mapStateToProps = ({ flinkAdmin, common }) => ({
  products: common.products,
  reportGroups: flinkAdmin.reportGroups,
  selectedReportGroupId: flinkAdmin.selectedReportGroupId,
});

export default compose(
  connect(mapStateToProps, {
    showConfirmPromise,
    deleteReportGroup,
    changeSelectedReportGroupId,
    editReportGroup,
  })
)(ReportGroupsColumn);
