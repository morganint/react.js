import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  Field,
  reduxForm,
  SubmissionError,
  formValueSelector,
} from 'redux-form';
import _ from 'lodash';

import { FAMILY_DEFAULT_MEMBERS_NUMBER } from 'consts/settings';
import { imagesURL } from 'config';

import {
  renderTextField,
  StyledError,
  FormButtons,
} from 'components/form-components';
import { editReportGroup } from 'actions/flinkAdminActions';
import { editFamily } from 'actions/familiesActions';
import { showError } from 'actions/statusActions';
import classes from './FamilyForm.module.scss';

const CustomField = ({ ...props }) => (
  <Field margin="dense" component={renderTextField} fullWidth {...props} />
);

class FamilyForm extends Component {
  state = {};

  componentDidMount() {
    this.setSelectedReportGroup();
  }

  componentDidUpdate(prevProps) {
    const { selectedReportGroupId, reportGroups } = this.props;

    if (
      prevProps.selectedReportGroupId !== selectedReportGroupId ||
      prevProps.reportGroups !== reportGroups
    ) {
      this.setSelectedReportGroup();
    }
  }

  submit = async (values) => {
    const { onSuccess, editFamily, translate } = this.props;

    const familyMembersErrors = {};
    const memberRequiredFields = ['username', 'firstname', 'lastname'];

    const requiredTranslate = translate(695, 'Required');

    if (!values.name) {
      throw new SubmissionError({ name: requiredTranslate });
    }

    const validateMember = (member, idx) => {
      if (_.isEmpty(member)) return;

      memberRequiredFields.forEach((field) => {
        !member[field] &&
          (familyMembersErrors[idx] = {
            ...familyMembersErrors[idx],
            [field]: requiredTranslate,
          });
      });
    };

    _.forEach(values.familyMembers, validateMember);

    if (!_.isEmpty(familyMembersErrors)) {
      throw new SubmissionError({ familyMembers: familyMembersErrors });
    }

    const result = await editFamily(values);

    if (result.success) {
      onSuccess && onSuccess();
    } else {
      throw new SubmissionError(result.errors);
    }
  };

  setSelectedReportGroup = () => {
    const { selectedReportGroupId, reportGroups } = this.props;
    const selectedGroup = _.find(reportGroups, { _id: selectedReportGroupId });
    this.setState({ selectedGroup });
  };

  addMemberToReportGroupHandler = (id) => {
    const { editReportGroup, initialValues, showError } = this.props;
    const { selectedGroup } = this.state;

    console.log(initialValues);

    if (initialValues.productId !== selectedGroup.productId) {
      return showError({
        message: "Family's product and report group's product should match",
      });
    }

    const newLearners = [...(selectedGroup.learners || []), id];

    editReportGroup({ ...selectedGroup, learners: newLearners });
  };

  render() {
    const {
      translate,
      familyMembers,
      addons,
      error,
      handleSubmit,
      pristine,
      productId,
      products,
      submitting,
      reset,
    } = this.props;

    const { selectedGroup } = this.state;

    const product = _.find(products, { _id: productId });

    const familyAdminTranslate = translate(535, 'Family Admin');
    const familyMemberTranslate = translate(537, 'Family Member');
    const firstNameTranslate = translate(538, 'First Name');
    const lastNameTranslate = translate(654, 'Last Name');

    return (
      <form onSubmit={handleSubmit(this.submit)} className={classes.form}>
        <CustomField
          name="name"
          label={translate(534, 'Family Name *')}
          autoFocus
        />

        <p className={classes.productName}>
          {translate(527, 'Product:')}{' '}
          <b>{product ? product.name : 'No Product'}</b>
        </p>

        <div className={classes.membersWrapper}>
          {_.times(FAMILY_DEFAULT_MEMBERS_NUMBER + (addons || 0)).map((idx) => {
            const shouldAddBtnRender =
              familyMembers[idx] &&
              familyMembers[idx]._id &&
              selectedGroup &&
              selectedGroup.productId &&
              (!selectedGroup.learners ||
                !selectedGroup.learners.includes(familyMembers[idx]._id));

            const memberTranslate =
              idx === 0
                ? familyAdminTranslate
                : `${familyMemberTranslate} ${idx + 1}`;

            return (
              <div key={idx} className={classes.memberFieldsWrapper}>
                <div className={classes.addButtonWrapper}>
                  {shouldAddBtnRender && (
                    <button
                      type="button"
                      className={classes.addBtn}
                      onClick={() =>
                        this.addMemberToReportGroupHandler(
                          familyMembers[idx]._id
                        )
                      }
                    >
                      <img
                        src={`${imagesURL}/FlinkMake/add.png`}
                        alt="Add To Report Group"
                      />
                    </button>
                  )}
                </div>
                <div className={classes.memberFields}>
                  <CustomField
                    name={`familyMembers[${idx}].firstname`}
                    label={`${memberTranslate} ${firstNameTranslate}`}
                  />
                  <CustomField
                    name={`familyMembers[${idx}].lastname`}
                    label={`${memberTranslate} ${lastNameTranslate}`}
                  />
                  <CustomField
                    name={`familyMembers[${idx}].username`}
                    label={translate(102, 'Username')}
                    normalize={(val) => val && val.replace(/[^\w.-]/g, '')}
                  />
                  <CustomField
                    name={`familyMembers[${idx}].password`}
                    label={translate(498, 'Password')}
                  />
                </div>
              </div>
            );
          })}
        </div>

        {!pristine && error && <StyledError>{error}</StyledError>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={true}
          handleReset={reset}
          alignRight
        />
      </form>
    );
  }
}

FamilyForm.propTypes = {
  initialValues: PropTypes.object,
  showError: PropTypes.func.isRequired,
  editFamily: PropTypes.func.isRequired,
  editReportGroup: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  productId: PropTypes.string,
  products: PropTypes.array,
  reportGroups: PropTypes.array,
  selectedReportGroupId: PropTypes.string,
};

const selector = formValueSelector('FamilyForm');

const mapStateToProps = (state) => ({
  familyMembers: selector(state, 'familyMembers'),
  addons: selector(state, 'addons'),
  productId: selector(state, 'productId'),
  products: state.common.products,
  selectedReportGroupId: state.flinkAdmin.selectedReportGroupId,
  reportGroups: state.flinkAdmin.reportGroups,
});

export default compose(
  reduxForm({
    form: 'FamilyForm',
    enableReinitialize: true,
  }),
  connect(mapStateToProps, {
    showError,
    editFamily,
    editReportGroup,
  })
)(FamilyForm);
