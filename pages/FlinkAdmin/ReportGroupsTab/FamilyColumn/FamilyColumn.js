import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import FamilyForm from './FamilyForm/FamilyForm';

export class FamilyColumn extends Component {
  state = {};

  componentDidMount() {
    this.formatFamily();
  }

  componentDidUpdate(prevProps) {
    const { families, familyMembers, selectedFamilyId } = this.props;

    if (
      prevProps.families !== families ||
      prevProps.familyMembers !== familyMembers ||
      prevProps.selectedFamilyId !== selectedFamilyId
    ) {
      this.formatFamily();
    }
  }

  formatFamily = () => {
    const {
      families,
      familyMembers: allMembers,
      selectedFamilyId,
    } = this.props;

    if (!selectedFamilyId) {
      return this.setState({ family: null });
    }

    const familyMembers = _.filter(allMembers, { familyId: selectedFamilyId });

    const familyAdmin = _.find(familyMembers, { isAdmin: true });
    const usualFamilyMembers = _.filter(familyMembers, { isAdmin: false });

    const selectedFamily = _.find(families, { _id: selectedFamilyId });

    const family = {
      ...selectedFamily,
      // Family admin should be first one
      familyMembers: [familyAdmin, ...usualFamilyMembers],
    };

    this.setState({ family });
  };

  render() {
    const { translate } = this.props;
    const { family } = this.state;

    return (
      <>
        <h4>{translate(526, 'Family')}</h4>
        <div className="glass-wrapper glass-wrapper--full-height">
          {family && (
            <FamilyForm initialValues={family} translate={translate} />
          )}
        </div>
      </>
    );
  }
}

FamilyColumn.propTypes = {
  translate: PropTypes.func.isRequired,
  selectedFamilyId: PropTypes.string,
  families: PropTypes.array,
  familyMembers: PropTypes.array,
};

const mapStateToProps = (state) => ({
  selectedFamilyId: state.flinkAdmin.selectedFamilyId,
  families: state.families.families,
  familyMembers: state.families.familyMembers,
});

export default compose(connect(mapStateToProps, {}))(FamilyColumn);
