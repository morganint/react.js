import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { Help } from 'components/flink-components';
import FamiliesColumn from './FamiliesColumn/FamiliesColumn';
import FamilyColumn from './FamilyColumn/FamilyColumn';
import ReportGroupSelect from './ReportGroupSelect/ReportGroupSelect';
import ReportGroupsColumn from './ReportGroupsColumn/ReportGroupsColumn';

export class ReportsTab extends Component {
  render() {
    const { translate } = this.props;

    return (
      <>
        <Help>{translate(61)}</Help>

        <div className="flink-admin__main-row">
          <div className="flink-admin__main-row-col" style={{ width: '30%' }}>
            <h4>{translate(494, 'Report Groups')}</h4>
            <ReportGroupSelect translate={translate} />

            <FamiliesColumn translate={translate} />
          </div>

          <div className="flink-admin__main-row-col" style={{ width: '40%' }}>
            <FamilyColumn translate={translate} />
          </div>
          <div className="flink-admin__main-row-col" style={{ width: '30%' }}>
            <h4>{translate(495, 'Learners List')}</h4>
            <ReportGroupsColumn translate={translate} />
          </div>
        </div>
      </>
    );
  }
}

ReportsTab.propTypes = {
  translate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ status }) => ({
  translate: status.translate,
});

export default connect(mapStateToProps)(ReportsTab);
