import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import ReportsTab from './ReportsTab/ReportsTab';
import ReportGroupsTab from './ReportGroupsTab/ReportGroupsTab';
import NotFoundPage from 'pages/NotFoundPage/NotFoundPage';

import {
  clearFlinkAdminData,
  setLearningCenter,
  getReportGroup,
} from 'actions/flinkAdminActions';
import { getFamiliesAndMembersByTeacherId } from 'actions/familiesActions';

import { renderRoutes } from 'utils';
import { imagesURL } from 'config';
import TopNav from './TopNav/TopNav';
import Header from './Header/Header';
import './FlinkAdmin.scss';

const routes = [
  ['/flink-admin/reports', true, ReportsTab],
  ['/flink-admin/report-groups', true, ReportGroupsTab],
  // 404 Page
  [null, false, NotFoundPage],
];

class FlinkAdmin extends Component {
  state = {};

  componentDidMount() {
    this.init();
  }

  componentWillUnmount() {
    this.props.clearFlinkAdminData();
  }

  init = async () => {
    const {
      auth,
      setLearningCenter,
      getReportGroup,
      getFamiliesAndMembersByTeacherId,
    } = this.props;

    await getReportGroup();
    await setLearningCenter();
    await getFamiliesAndMembersByTeacherId(auth.user._id);

    this.setState({ dataFetched: true });
  };

  render() {
    const { auth } = this.props;
    const { dataFetched } = this.state;

    return (
      <div className="flink-admin">
        <div className="flink-admin__inner">
          <TopNav />

          <div
            className="flink-admin__content-wrapper"
            style={{
              backgroundImage: `url(${imagesURL}/FlinkAdmin/background.png)`,
            }}
          >
            <Header />

            <Switch>
              <Redirect
                from="/flink-admin"
                exact
                to="/flink-admin/report-groups"
              />
              {dataFetched && renderRoutes(routes, auth)}
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

FlinkAdmin.propTypes = {
  auth: PropTypes.object.isRequired,
  clearFlinkAdminData: PropTypes.func.isRequired,
  setLearningCenter: PropTypes.func.isRequired,
  getReportGroup: PropTypes.func.isRequired,
  getFamiliesAndMembersByTeacherId: PropTypes.func.isRequired,
};

const mapStateToProps = ({ auth }) => ({
  auth,
});

export default connect(mapStateToProps, {
  clearFlinkAdminData,
  setLearningCenter,
  getReportGroup,
  getFamiliesAndMembersByTeacherId,
})(FlinkAdmin);
