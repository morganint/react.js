import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import ReactSelect from 'react-select';

import FormLabel from '@material-ui/core/FormLabel';
import { withData } from 'components/hocs';
import API from 'api';
import { ReportsView } from 'components/admin';
import { ProgressButton } from 'components/common';
import { FromToPicker } from 'components/flink-components';
import { formatDate } from 'utils';
import classes from './RevenueReports.module.scss';

class RevenueReports extends Component {
  state = {
    filter: {},
    selectedFilter: {},
    landingPageOptions: [],
  };

  componentDidMount() {
    const { purchaseConfigs } = this.props;

    // Set landing pages options
    const landingPageOptions = purchaseConfigs.map((p) => ({
      label: p.name,
      value: p._id,
    }));

    this.setState({
      landingPageOptions,
      selectedLandingPage: landingPageOptions[0],
    });
  }

  changeLandingPageHandler = (selectedLandingPage) => {
    this.setState({ selectedLandingPage });
  };

  generateReport = async () => {
    const { selectedLandingPage, filter } = this.state;

    this.setState({ loading: true });

    const result = await API.reports.getRevenueReport({
      purchaseConfig: selectedLandingPage.value,
      ...filter,
    });

    console.log(result);

    this.setState({ loading: false });

    // Number of first purchases
    // $ amount for first purchase
    // Number of other purchases
    // $ amount for other purchases
    // Total revenue

    const {
      firstPurchasesNumber,
      firstPurchasesAmount,
      otherPurchasesNumber,
      otherPurchasesAmount,
      totalAmount,
    } = result.data;

    const numbersRows = [
      [
        'Number of first purchases',
        {
          content: firstPurchasesNumber,
          align: 'right',
        },
      ],
      [
        '$ amount for first purchase',
        {
          content: '$' + firstPurchasesAmount,
          align: 'right',
        },
      ],
      [
        'Number of other purchases',
        {
          content: otherPurchasesNumber,
          align: 'right',
        },
      ],
      [
        '$ amount for other purchases',
        {
          content: '$' + otherPurchasesAmount,
          align: 'right',
        },
      ],
      [
        'Total revenue',
        {
          content: '$' + totalAmount,
          align: 'right',
        },
      ],
    ];

    const reports = [
      {
        header: 'Numbers',
        rows: numbersRows,
      },
    ];

    this.setState({
      reports,
      reportsLandingPage: selectedLandingPage,
      selectedFilter: filter,
    });
  };

  changeFilter = (filterUpdate) => {
    this.setState({
      filter: { ...this.state.filter, ...filterUpdate },
    });
  };

  render() {
    const { translate } = this.props;
    const {
      landingPageOptions,
      reports,
      reportsLandingPage,
      loading,
      filter,
      selectedFilter,
      selectedLandingPage,
    } = this.state;

    return (
      <>
        <div className={classes.filters}>
          <div>
            <FormLabel>Landing Page:</FormLabel>

            <ReactSelect
              styles={{
                control: (provided, state) => ({
                  ...provided,
                  height: 40,
                }),
              }}
              onChange={this.changeLandingPageHandler}
              value={selectedLandingPage}
              // getOptionValue={(o) => o.value}
              options={landingPageOptions || []}
              placeholder={translate(698, 'Select')}
            />
          </div>

          <FromToPicker
            changeHandler={this.changeFilter}
            startDate={filter.startDate}
            endDate={filter.endDate}
          />

          <div>
            <ProgressButton
              loading={loading}
              disabled={
                !selectedLandingPage || !filter.startDate || !filter.endDate
              }
              variant="contained"
              color="primary"
              onClick={this.generateReport}
            >
              <label>{translate(704, 'Generate Report')}</label>
            </ProgressButton>
          </div>
        </div>

        <ReportsView
          data={reports}
          title={'Revenue Report'}
          subtitle={reportsLandingPage && reportsLandingPage.label}
          afterSubtitle={`${formatDate(
            selectedFilter.startDate
          )} - ${formatDate(selectedFilter.endDate)}`}
        />
      </>
    );
  }
}

RevenueReports.propTypes = {
  translate: PropTypes.func.isRequired,
  purchaseConfigs: PropTypes.array.isRequired,
};

const mapStateToProps = ({ status }) => ({
  translate: status.translate,
});

export default compose(
  withData(['purchaseConfigs']),
  connect(mapStateToProps)
)(RevenueReports);
