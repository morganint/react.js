import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import ReactSelect from 'react-select';

import FormLabel from '@material-ui/core/FormLabel';
import { withData } from 'components/hocs';
import API from 'api';
import { ReportsView } from 'components/admin';
import { ProgressButton } from 'components/common';
import classes from './ContinuityReports.module.scss';

class ContinuityReports extends Component {
  state = {
    landingPageOptions: [],
  };

  componentDidMount() {
    const { purchaseConfigs } = this.props;

    // Set landing pages options
    const landingPageOptions = purchaseConfigs.map((p) => ({
      label: p.name,
      value: p._id,
    }));

    this.setState({
      landingPageOptions,
      selectedLandingPage: landingPageOptions[0],
    });
  }

  changeLandingPageHandler = (selectedLandingPage) => {
    this.setState({ selectedLandingPage });
  };

  generateReport = async () => {
    const { selectedLandingPage } = this.state;

    this.setState({ loading: true });

    const result = await API.reports.geByPurchaseConfig(
      selectedLandingPage.value
    );

    console.log(result);

    this.setState({ loading: false });

    const { numbers } = result.data;

    const getPercent = (val1, val2) =>
      (val2 ? ((val1 / val2) * 100).toFixed(2) : '0.00') + '%';

    const numbersRows = [
      [
        {
          content: 'The number of customers associated with the landing page',
          colSpan: 4,
        },
        numbers.customersNumber,
      ],
      [
        {
          content:
            'The percentage of customers having purchased a second level',
          colSpan: 4,
        },
        getPercent(numbers.purchasedSecondLevel, numbers.customersNumber),
      ],
      [
        {
          content: 'The percentage of customers having purchased a third level',
          colSpan: 4,
        },
        getPercent(numbers.purchasedThirdLevel, numbers.customersNumber),
      ],
      [
        {
          content:
            'The percentage of customers having purchased a fourth level',
          colSpan: 4,
        },
        getPercent(numbers.purchasedFourthLevel, numbers.customersNumber),
      ],
      [
        {
          content: 'The percentage of customers having purchased a fifth level',
          colSpan: 4,
        },
        getPercent(numbers.purchasedFifthLevel, numbers.customersNumber),
      ],
      [
        {
          content:
            'The average number of levels purchased by the customers associated with the landing page',
          colSpan: 4,
        },
        numbers.customersNumber
          ? (numbers.purchasedLevels / numbers.customersNumber).toFixed(2)
          : '0.00',
      ],
    ];

    const reports = [
      {
        header: 'Numbers',
        rows: numbersRows,
      },
    ];

    console.log(reports);

    this.setState({ reports, reportsLandingPage: selectedLandingPage });
  };

  render() {
    const { translate } = this.props;
    const {
      landingPageOptions,
      selectedLandingPage,
      reports,
      loading,
      reportsLandingPage,
    } = this.state;

    return (
      <>
        <div className={classes.filters}>
          <div>
            <FormLabel>Landing Page:</FormLabel>

            <ReactSelect
              styles={{
                control: (provided, state) => ({
                  ...provided,
                  height: 40,
                }),
              }}
              onChange={this.changeLandingPageHandler}
              value={selectedLandingPage}
              // getOptionValue={(o) => o.value}
              options={landingPageOptions || []}
              placeholder={translate(698, 'Select')}
            />
          </div>

          <div>
            <ProgressButton
              loading={loading}
              disabled={!selectedLandingPage}
              variant="contained"
              color="primary"
              onClick={this.generateReport}
            >
              <label>{translate(704, 'Generate Report')}</label>
            </ProgressButton>
          </div>
        </div>

        <ReportsView
          data={reports}
          title="Continuity Report"
          subtitle={reportsLandingPage && reportsLandingPage.label}
        />
      </>
    );
  }
}

ContinuityReports.propTypes = {
  translate: PropTypes.func.isRequired,
  purchaseConfigs: PropTypes.array.isRequired,
};

const mapStateToProps = ({ status }) => ({
  translate: status.translate,
});

export default compose(
  withData(['purchaseConfigs']),
  connect(mapStateToProps)
)(ContinuityReports);
