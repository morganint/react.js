import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Select from 'react-select';

import API from 'api';

import FormLabel from '@material-ui/core/FormLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import { ProgressButton } from 'components/common';

import { fetchData, getMasterOrganizationById } from 'actions/adminActions';
import {
  SUPER,
  MASTER_ADMIN,
  MASTER_ORGANIZATION_ADMIN,
  LEARNING_CENTER_ADMIN,
} from 'consts/user-roles';
import { ReportsView } from 'components/admin';
import { FromToPicker } from 'components/flink-components';
import { msToDuration, formatDate } from 'utils';
import classes from './ByOrganizationReports.module.scss';

const USAGE_REPORT = 'USAGE_REPORT';
const MASTERY_REPORT = 'MASTERY_REPORT';

const reportTypesOptions = [
  { value: USAGE_REPORT, label: 'Usage' },
  { value: MASTERY_REPORT, label: 'Mastery' },
];

class Reports extends Component {
  state = {
    reportsType: USAGE_REPORT,
    learningCentersOptions: [],
    productsOptions: [],
    solutionsOptions: [],
    masterOrganizationsOptions: [],
    filter: {
      centers: 'all',
      withTeachers: false,
      allPeriod: true,
      startDate: '',
      endDate: '',
      // productId: '',
      // solutionId: '',
      learningCenters: [],
      masterOrganizations: [],
      reportType: reportTypesOptions[0].value,
    },
  };

  componentDidMount() {
    const { user, learningCenters } = this.props;

    const { roleAlias } = user;

    const isMasterOrganizationAdmin = roleAlias === MASTER_ORGANIZATION_ADMIN;
    const isLearningCenterAdmin = roleAlias === LEARNING_CENTER_ADMIN;
    const isSuperOrMasterAdmin =
      roleAlias === SUPER || roleAlias === MASTER_ADMIN;

    const learningCentersOptions = learningCenters
      .filter((l) => {
        if (isLearningCenterAdmin) {
          return l._id === user.learningCenterId;
        } else if (isMasterOrganizationAdmin) {
          return l.masterOrganizationId === user.masterOrganizationId;
        } else {
          return isSuperOrMasterAdmin;
        }
      })
      .map((l) => ({
        label: l.name,
        value: l._id,
        masterOrganizationId: l.masterOrganizationId,
      }));

    if (isSuperOrMasterAdmin) {
      this.fetchMasterOrganizations();
    } else {
      this.fetchMasterOrganization();
    }

    // Fetch user available products
    // this.fetchUserProducts();

    this.changeFilter({ learningCenters: learningCentersOptions });

    this.setState((state) => ({
      isSuperOrMasterAdmin,
      learningCentersOptions,
    }));
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (prevState.filter.productId !== this.state.filter.productId) {
  //     this.fetchSolutions();
  //   }
  // }

  fetchMasterOrganization = async () => {
    const { getMasterOrganizationById, user } = this.props;

    if (!user.masterOrganizationId) return;

    const result = await getMasterOrganizationById(user.masterOrganizationId);

    if (!result) {
      return console.log('error fetching user master organization');
    }

    this.setState({
      userMasterOrganization: result,
    });
  };

  fetchMasterOrganizations = async () => {
    const { fetchData } = this.props;
    const result = await fetchData('masterOrganizations');

    if (!result.success) {
      return console.log('error fetching master organizations');
    }

    const masterOrganizationsOptions = result.data.map((p) => ({
      label: p.name,
      value: p._id,
    }));

    const firstOption = masterOrganizationsOptions[0];

    this.setState((state) => ({
      masterOrganizationsOptions,
      filter: { ...state.filter, productId: firstOption && firstOption.value },
    }));
  };

  // fetchUserProducts = async () => {
  //   const result = await API.products.getUserAvailableProducts();

  //   if (!result.success) {
  //     return console.log('error fetching user products');
  //   }

  //   const productsOptions = result.data.map((p) => ({
  //     label: p.name,
  //     value: p._id,
  //   }));

  //   const firstOption = productsOptions[0];

  //   this.setState((state) => ({
  //     productsOptions,
  //     filter: { ...state.filter, productId: firstOption && firstOption.value },
  //   }));
  // };

  // fetchSolutions = async () => {
  //   const { productId } = this.state.filter;
  //
  //   this.setState((state) => ({
  //     solutionOptions: [],
  //     filter: { ...this.state.filter, solutionId: '' },
  //   }));
  //
  //   if (!productId) {
  //     return;
  //   }
  //
  //   const result = await API.products.getProductSolutionsList(productId);
  //
  //   if (!result.success) {
  //     return console.log('error fetching user solutions');
  //   }
  //
  //   const solutionsOptions = result.data.map((p) => ({
  //     label: p.displayName,
  //     value: p._id,
  //   }));
  //
  //   const firstOption = solutionsOptions[0];
  //
  //   this.setState((state) => ({
  //     solutionsOptions,
  //     filter: { ...state.filter, solutionId: firstOption && firstOption.value },
  //   }));
  // };

  changeFilter = (filterUpdate) => {
    this.setState({
      filter: { ...this.state.filter, ...filterUpdate },
    });
  };

  generateReport = async () => {
    const { filter } = this.state;

    // remove current reports
    this.setState({ reports: null, loading: true });

    // map learning centers to send just ids
    const learningCentersIds = filter.learningCenters.map((opt) => opt.value);

    const formattedFilter = {
      ...filter,
      learningCenters: learningCentersIds,
    };

    const result = await API.reports.getReports(formattedFilter);

    this.setState({ loading: false });

    if (!result.success) {
      return alert('Error getting reports');
    }

    this.setState({ reports: result.data, reportsFilter: { ...filter } });
  };

  onMasterOrganizationsPick = (opts) => {
    opts = opts || [];

    const ids = opts.map((o) => o.value);
    const filteredCenters = this.state.learningCentersOptions.filter((o) =>
      ids.includes(o.masterOrganizationId)
    );

    this.changeFilter({
      masterOrganizations: opts,
      learningCenters: filteredCenters,
    });
  };

  reportsRef = React.createRef();

  render() {
    const {
      filter,
      learningCentersOptions,
      isSuperOrMasterAdmin,
      masterOrganizationsOptions,
      reports,
      loading,
      reportsFilter,
      userMasterOrganization,
      // productsOptions,
      // solutionsOptions,
    } = this.state;

    const { translate } = this.props;

    const buttonDisabled =
      // !filter.productId ||
      // !filter.solutionId ||
      loading ||
      !filter.learningCenters.length ||
      (!filter.allPeriod && (!filter.endDate || !filter.startDate));

    let tableTitle = '';
    let tableSubtitle = '';

    if (reports) {
      if (isSuperOrMasterAdmin) {
        if (reportsFilter.centers === 'all') {
          tableTitle = 'All Master Organizations';
        } else if (reportsFilter.centers === 'byMasterOrganization') {
          tableTitle = reportsFilter.masterOrganizations
            .map((m) => m.label)
            .join(', ');
        } else if (reportsFilter.centers === 'byLearningCenter') {
          tableSubtitle = reportsFilter.learningCenters
            .map((m) => m.label)
            .join(', ');
        }
      } else if (userMasterOrganization) {
        tableTitle = userMasterOrganization.name;

        if (reportsFilter.centers === 'byLearningCenter') {
          tableSubtitle = reportsFilter.learningCenters
            .map((m) => m.label)
            .join(', ');
        }
      } else if (learningCentersOptions.length === 1) {
        tableTitle = learningCentersOptions[0].label;
      }
    }

    let reportsData;

    if (reports) {
      reportsData = [
        {
          header: translate(706, 'Numbers:'),
          rows: [
            !reportsFilter.withTeachers
              ? [
                  translate(708, 'Number of Family Members:'),
                  reports.familyMembersNumber,
                ]
              : null,
            [
              translate(709, 'Number of Family Members in Report Groups:'),
              reports.familyMembersInRpsNumber,
            ],
            [translate(707, 'Number of Families:'), reports.familiesNumber],
          ],
        },
        {
          header: translate(713, 'Mastery:'),
          rows: [
            [
              translate(
                714,
                'Average Skill levels mastered per Family Member:'
              ),
              `${
                reports.membersWithSomeTime
                  ? (
                      reports.totalSkillsMastered / reports.membersWithSomeTime
                    ).toFixed(2)
                  : 0
              } ${translate(721, 'skill levels')}`,
            ],
            [
              translate(715, 'Total Skill levels mastered:'),
              `${reports.totalSkillsMastered} ${translate(
                721,
                'skill levels'
              )}`,
            ],
          ],
        },
        {
          header: translate(716, ' Usage:'),
          rows: [
            [
              translate(717, 'Average Time per Family Member:'),
              `${msToDuration(
                reports.membersWithSomeTime
                  ? reports.totalTime / reports.membersWithSomeTime
                  : 0,
                { translate, withoutDays: true }
              )}`,
            ],
            [
              translate(718, 'Total Time:'),
              `${msToDuration(reports.totalTime, {
                translate,
                withoutDays: true,
              })}`,
            ],
            [
              translate(719, 'Family Members with some time:'),
              reports.membersWithSomeTime,
            ],
            [
              translate(720, 'Percentage of Family Members with some time:'),
              `${
                reports.familyMembersNumber
                  ? (
                      (reports.membersWithSomeTime /
                        reports.familyMembersNumber) *
                      100
                    ).toFixed(2)
                  : 0
              }
          %`,
            ],
          ],
        },
      ];
    }

    const membersString = filter.withTeachers
      ? translate(700, 'Only family members in report groups')
      : translate(699, 'All Members');

    const periodString = filter.allPeriod
      ? translate(730, 'All Dates')
      : `${formatDate(filter.startDate)} - ${formatDate(filter.endDate)}`;

    return (
      <div>
        <h1 className={classes.title}>{translate(341, 'Reports')}</h1>

        {/*<div className={classes.solutionSelect}>*/}
        {/*  <div className={classes.selectWrapper}>*/}
        {/*    <span>Product:</span>*/}
        {/*    <Select*/}
        {/*      getOptionValue={(o) => o.value}*/}
        {/*      options={productsOptions}*/}
        {/*      onChange={(opt) => this.changeFilter({ productId: opt.value })}*/}
        {/*      value={productsOptions.filter(*/}
        {/*        (o) => o.value === filter.productId*/}
        {/*      )}*/}
        {/*    />*/}
        {/*  </div>*/}

        {/*  <div className={classes.selectWrapper}>*/}
        {/*    <span>Solution:</span>*/}
        {/*    <Select*/}
        {/*      getOptionValue={(o) => o.value}*/}
        {/*      options={solutionsOptions}*/}
        {/*      onChange={(opt) => this.changeFilter({ solutionId: opt.value })}*/}
        {/*      value={solutionsOptions.filter(*/}
        {/*        (o) => o.value === filter.solutionId*/}
        {/*      )}*/}
        {/*    />*/}
        {/*  </div>*/}
        {/*</div>*/}

        <div className={classes.filters}>
          {/*<RadioGroup*/}
          {/*  name="reportType"*/}
          {/*  value={filter.reportType}*/}
          {/*  onChange={(e) =>*/}
          {/*    this.changeFilter({ [e.target.name]: e.target.value })*/}
          {/*  }*/}
          {/*  options={[]}*/}
          {/*>*/}
          {/*  {reportTypesOptions.map((opt) => (*/}
          {/*    <FormControlLabel*/}
          {/*      key={opt.value}*/}
          {/*      value={opt.value}*/}
          {/*      control={<Radio size="small" />}*/}
          {/*      label={opt.label}*/}
          {/*    />*/}
          {/*  ))}*/}
          {/*</RadioGroup>*/}

          {learningCentersOptions.length > 1 && (
            <div className={classes.centersColumn}>
              <FormLabel>{translate(468, 'Centers')}</FormLabel>

              <FormControlLabel
                checked={filter.centers === 'all'}
                onClick={(e) =>
                  this.changeFilter({
                    centers: 'all',
                    masterOrganizations: [],
                    learningCenters: learningCentersOptions,
                  })
                }
                control={<Radio size="small" />}
                label={translate(696, 'All')}
              />

              {isSuperOrMasterAdmin && (
                <>
                  <FormControlLabel
                    checked={filter.centers === 'byMasterOrganization'}
                    onClick={(e) =>
                      this.changeFilter({
                        centers: 'byMasterOrganization',
                        masterOrganizations: [],
                        learningCenters: [],
                      })
                    }
                    control={<Radio size="small" />}
                    label={translate(710, 'By Master Organization')}
                  />

                  {filter.centers === 'byMasterOrganization' && (
                    <Select
                      placeholder={translate(698, 'Select')}
                      isMulti
                      getOptionValue={(o) => o.value}
                      options={masterOrganizationsOptions}
                      onChange={this.onMasterOrganizationsPick}
                      value={filter.masterOrganizations}
                    />
                  )}
                </>
              )}

              <FormControlLabel
                checked={filter.centers === 'byLearningCenter'}
                onClick={(e) =>
                  this.changeFilter({
                    centers: 'byLearningCenter',
                    masterOrganizations: [],
                    learningCenters: [],
                  })
                }
                control={<Radio size="small" />}
                label={translate(697, 'By Learning Center')}
              />

              {filter.centers === 'byLearningCenter' && (
                <Select
                  placeholder={translate(698, 'Select')}
                  isMulti
                  getOptionValue={(o) => o.value}
                  options={learningCentersOptions}
                  onChange={(opts) =>
                    this.changeFilter({
                      learningCenters: opts || [],
                    })
                  }
                  value={filter.learningCenters}
                />
              )}
            </div>
          )}

          {/* {learningCentersOptions.length > 1 && (
            <div className={classes.centersColumn}>
            <FormLabel>Centers</FormLabel>
              <RadioGroup name="allCenters" options={[]}>
                <FormControlLabel
                  checked={filter.allCenters}
                  onClick={(e) =>
                    this.changeFilter({
                      allCenters: true,
                      learningCenters: learningCentersOptions,
                    })
                  }
                  control={<Radio size="small" />}
                  label={translate(696, 'All')}
                />
                <FormControlLabel
                  checked={!filter.allCenters}
                  onClick={(e) =>
                    this.changeFilter({
                      allCenters: false,
                      learningCenters: [],
                    })
                  }
                  control={<Radio size="small" />}
                  label={translate(697, 'By Learning Center')}
                />
              </RadioGroup>

              {!filter.allCenters && (
                <Select
                  placeholder={translate(698, 'Select')}
                  isMulti
                  getOptionValue={(o) => o.value}
                  options={learningCentersOptions}
                  onChange={(opts) =>
                    this.changeFilter({
                      learningCenters: opts || [],
                    })
                  }
                  value={filter.learningCenters}
                />
              )}
            </div>
          )} */}

          <div>
            <FormLabel>{translate(481, 'Members')}</FormLabel>
            <RadioGroup name="withTeachers" options={[]}>
              <FormControlLabel
                checked={!filter.withTeachers}
                onClick={(e) => this.changeFilter({ withTeachers: false })}
                control={<Radio size="small" />}
                label={translate(699, 'All Members')}
              />
              <FormControlLabel
                checked={filter.withTeachers}
                onClick={(e) => this.changeFilter({ withTeachers: true })}
                control={<Radio size="small" />}
                label={translate(700, 'Only family members in report groups')}
              />
            </RadioGroup>
          </div>

          <div>
            <FormLabel>{translate(483, 'Period')}</FormLabel>
            <RadioGroup name="allPeriod" options={[]}>
              <FormControlLabel
                checked={filter.allPeriod}
                onClick={(e) => this.changeFilter({ allPeriod: true })}
                control={<Radio size="small" />}
                label={translate(730, 'All Dates')}
              />
              <FormControlLabel
                checked={!filter.allPeriod}
                onClick={(e) => this.changeFilter({ allPeriod: false })}
                control={<Radio size="small" />}
                label={translate(701, 'Specific Dates')}
              />
            </RadioGroup>

            {!filter.allPeriod && (
              <FromToPicker
                changeHandler={this.changeFilter}
                startDate={filter.startDate}
                endDate={filter.endDate}
              />
            )}
          </div>
        </div>

        <ProgressButton
          loading={loading}
          variant="contained"
          color="primary"
          disabled={buttonDisabled}
          onClick={this.generateReport}
        >
          <label>{translate(704, 'Generate Report')}</label>
        </ProgressButton>

        <ReportsView
          data={reportsData}
          afterSubtitle={
            <>
              <p>
                {translate(481, 'Members')}: <b>{membersString}</b>
              </p>
              <p>
                {translate(483, 'Period')}: <b>{periodString}</b>
              </p>
            </>
          }
          title={tableTitle}
          subtitle={tableSubtitle}
        />
      </div>
    );
  }
}

Reports.propTypes = {
  user: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
  getMasterOrganizationById: PropTypes.func.isRequired,
  learningCenters: PropTypes.array.isRequired,
};

const mapStateToProps = ({ auth, common, status }) => ({
  translate: status.translate,
  user: auth.user,
  learningCenters: common.learningCenters,
});

export default connect(mapStateToProps, {
  fetchData,
  getMasterOrganizationById,
})(Reports);
