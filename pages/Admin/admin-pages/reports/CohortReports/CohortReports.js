import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import ReactSelect from 'react-select';

import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';

import { msToDuration } from 'utils';
import { withData } from 'components/hocs';
import API from 'api';
import { ReportsView, DatePicker } from 'components/admin';
import { ProgressButton } from 'components/common';
import classes from './CohortReports.module.scss';

class Cohort extends Component {
  state = {
    familyReportOpened: false,
    landingPageOptions: [],
    selectedDate: new Date(),
  };

  componentDidMount() {
    const { purchaseConfigs } = this.props;

    // Set landing pages options
    const landingPageOptions = purchaseConfigs.map((p) => ({
      label: p.name,
      value: p._id,
    }));

    this.setState({
      landingPageOptions,
      selectedLandingPage: landingPageOptions[0],
    });
  }

  changeLandingPageHandler = (selectedLandingPage) => {
    this.setState({ selectedLandingPage });
  };

  changeMonthHandler = (selectedDate) => {
    console.log(selectedDate);
    this.setState({ selectedDate });
  };

  generateReport = async () => {
    const { selectedDate, selectedLandingPage } = this.state;

    const month = selectedDate.getMonth() + 1;
    const year = selectedDate.getFullYear();

    this.setState({ loading: true });

    const result = await API.reports.geByPurchaseConfig(
      selectedLandingPage.value,
      {
        withCustomers: true,
        month,
        year,
      }
    );

    console.log(result);

    this.setState({ loading: false });

    const { customers, numbers } = result.data;

    const customerRows = customers.map((customer) => {
      if (!customer) return null;

      const {
        family,
        data: {
          purchaseInfo: { successfulTransactionsNumber },
          purchase: { test, fromUrl, isLevelSubscription, subscriptionPlan },
        },
      } = customer;

      return [
        family ? (
          <span
            className={classes.familyBtn}
            onClick={this.showFamilyReport.bind(null, family._id)}
          >
            {test && 'TEST - '}
            {family.email}
          </span>
        ) : (
          `${test ? 'TEST - ' : ''}Family Deleted`
        ),
        fromUrl,
        isLevelSubscription ? 'Subscr.' : 'Entire',
        isLevelSubscription ? subscriptionPlan.label : '',
        isLevelSubscription ? successfulTransactionsNumber : 1,
      ];
    });

    const getPercent = (val1, val2) =>
      (val2 ? ((val1 / val2) * 100).toFixed(2) : '0.00') + '%';

    const numbersRows = [
      [
        { content: 'The number of customers in the cohort', colSpan: 4 },
        numbers.customersNumber,
      ],
      [
        {
          content:
            'The percentage of customers having purchased a second level',
          colSpan: 4,
        },
        getPercent(numbers.purchasedSecondLevel, numbers.customersNumber),
      ],
      [
        {
          content: 'The percentage of customers having purchased a third level',
          colSpan: 4,
        },
        getPercent(numbers.purchasedThirdLevel, numbers.customersNumber),
      ],
      [
        {
          content:
            'The percentage of customers having purchased a fourth level',
          colSpan: 4,
        },
        getPercent(numbers.purchasedFourthLevel, numbers.customersNumber),
      ],
      [
        {
          content: 'The percentage of customers having purchased a fifth level',
          colSpan: 4,
        },
        getPercent(numbers.purchasedFifthLevel, numbers.customersNumber),
      ],
      [
        {
          content: 'The average number of levels purchased by the cohort',
          colSpan: 4,
        },
        numbers.customersNumber
          ? (numbers.purchasedLevels / numbers.customersNumber).toFixed(2)
          : '0.00',
      ],
    ];

    const reports = [
      {
        header: 'Numbers',
        rows: numbersRows,
      },
      {
        header: 'Customers',
        headers: ['Family email', 'URL', 'Type', 'Subscr. Plan', 'Purchases'],
        rows: customerRows,
      },
    ];

    console.log(reports);

    this.setState({ reports });
  };

  showFamilyReport = async (familyId) => {
    // Get reports
    this.setState({ familyReportsLoading: true, familyReportOpened: true });

    let result;

    try {
      result = await API.reports.byFamily(familyId);
    } catch (err) {
      result = { data: null };
    }

    this.setState({ familyReportsLoading: false, familyReports: result.data });
  };

  closeFamilyReport = () => {
    this.setState({ familyReportOpened: false, familyReports: null });
  };

  render() {
    const { translate } = this.props;
    const {
      landingPageOptions,
      selectedLandingPage,
      selectedDate,
      reports,
      loading,
      familyReportOpened,
      familyReportsLoading,
      familyReports,
    } = this.state;

    return (
      <>
        <div className={classes.filters}>
          <div>
            <FormLabel>Landing Page:</FormLabel>

            <ReactSelect
              styles={{
                control: (provided, state) => ({
                  ...provided,
                  height: 40,
                }),
              }}
              onChange={this.changeLandingPageHandler}
              value={selectedLandingPage}
              // getOptionValue={(o) => o.value}
              options={landingPageOptions || []}
              placeholder={translate(698, 'Select')}
            />
          </div>

          <div>
            <FormLabel>Choose Month:</FormLabel>

            <DatePicker
              selected={selectedDate}
              onChange={this.changeMonthHandler}
              dateFormat="MM/yyyy"
              showMonthYearPicker
            />
          </div>

          <div>
            <ProgressButton
              loading={loading}
              disabled={!selectedDate || !selectedLandingPage}
              variant="contained"
              color="primary"
              onClick={this.generateReport}
            >
              <label>{translate(704, 'Generate Report')}</label>
            </ProgressButton>
          </div>
        </div>

        <ReportsView
          data={reports}
          title="Cohort Reports"
          subtitle={`${
            selectedDate.getMonth() + 1
          }/${selectedDate.getFullYear()}`}
        />

        <Dialog open={familyReportOpened} onClose={this.closeFamilyReport}>
          <DialogTitle>Family Report</DialogTitle>
          <DialogContent>
            {familyReportsLoading && (
              <div className={classes.modalProgress}>
                <CircularProgress />
              </div>
            )}

            {!familyReportsLoading && !familyReports && (
              <p>Error loading reports</p>
            )}

            {familyReports && (
              <table className={classes.familyReportsTable}>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Is Admin</th>
                    <th>Time In App</th>
                    <th>Skills Mastered</th>
                  </tr>
                </thead>
                <tbody>
                  {familyReports
                    .sort((a, b) => (a.name > b.name ? 1 : -1))
                    .map((member) => (
                      <tr key={member.id}>
                        <td>{member.name}</td>
                        <td>{member.username}</td>
                        <td align="center">{member.isAdmin ? 'Yes' : 'No'}</td>
                        <td>
                          {msToDuration(member.totalTime, {
                            translate,
                            withoutDays: true,
                          })}
                        </td>
                        <td align="center">{member.skillsMastered}</td>
                      </tr>
                    ))}
                </tbody>
              </table>
            )}
          </DialogContent>

          <DialogActions>
            <Button onClick={this.closeFamilyReport} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

Cohort.propTypes = {
  translate: PropTypes.func.isRequired,
  purchaseConfigs: PropTypes.array.isRequired,
};

const mapStateToProps = ({ status }) => ({
  translate: status.translate,
});

export default compose(
  withData(['purchaseConfigs']),
  connect(mapStateToProps)
)(Cohort);
