import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'displayName',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'parentAdvicesEnabled',
    disablePadding: false,
    label: 'Parent Advices',
    type: 'bool'
  },
  {
    id: 'activityBuilder',
    disablePadding: false,
    label: 'Activity Builder',
    type: 'bool'
  }
];

const Solutions = ({ deleteData, solutions }) => {
  const deleteHandler = (id, e) => {
    deleteData('solutions', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/solutions/create"
      >
        Create solution
      </Button>

      <MaterialTable
        data={solutions}
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/solutions/edit"
        type="solution"
        additionalButtons={[
          {
            title: 'Solution Menu',
            icon: { type: 'fa', name: 'bars' },
            url: '/admin/solutions/solution-menu'
          }
        ]}
        rowWithName="displayName"
      />
    </div>
  );
};

Solutions.propTypes = {
  solutions: PropTypes.array,
  deleteData: PropTypes.func.isRequired
};

export default withData(['solutions'])(Solutions);
