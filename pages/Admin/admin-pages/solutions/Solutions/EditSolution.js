import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import SolutionForm from './SolutionForm/SolutionForm';
import { formatCheckboxesFromArray } from 'components/form-components';
import { Redirect } from 'react-router-dom';
import { isEmpty } from 'validation';

class EditSolution extends Component {
  render() {
    const { match, solutions = [] } = this.props;

    const solutionToEdit = solutions.filter(
      (solution) => solution._id === match.params.id
    )[0];

    const initialValues = solutionToEdit && {
      ...solutionToEdit,
      themes: isEmpty(solutionToEdit.themes)
        ? undefined
        : formatCheckboxesFromArray(solutionToEdit.themes),
      characters: isEmpty(solutionToEdit.characters)
        ? undefined
        : formatCheckboxesFromArray(solutionToEdit.characters),
      parentAdvicesLocales: isEmpty(solutionToEdit.parentAdvicesLocales)
        ? undefined
        : formatCheckboxesFromArray(solutionToEdit.parentAdvicesLocales),
    };

    console.log('Initial Values', initialValues);

    return (
      <Fragment>
        {solutionToEdit ? (
          <SolutionForm isEdit initialValues={initialValues} />
        ) : (
          <Redirect to="/admin/solutions" />
        )}
      </Fragment>
    );
  }
}

EditSolution.propTypes = {
  solutions: PropTypes.array,
};

const mapStateToProps = ({ admin }) => ({
  solutions: admin.solutions,
});

export default compose(connect(mapStateToProps))(EditSolution);
