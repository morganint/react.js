import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
  SubmissionError,
  reset,
  change,
  formValueSelector
} from 'redux-form';
import { compose } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';

import Button from '@material-ui/core/Button';

import API from 'api';
import { withData } from 'components/hocs';
import { editData } from 'actions/adminActions';
import { showConfirmPromise } from 'actions/statusActions';
import { validateScope } from 'validation';
import ScopeColumns from './ScopeColumns/ScopeColumns';
import SelectActivityGroups from './SelectActivityGroups/SelectActivityGroups';
import ReportsConfig from './ReportsConfig/ReportsConfig';
import classes from './SolutionMenu.module.scss';

import {
  renderSelect,
  FormButtons,
  StyledError,
  StyledSuccess
} from 'components/form-components';

const requiredFields = [];

const validate = values => {
  const errors = {};

  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });

  return errors;
};

export class SolutionMenuForm extends Component {
  state = { success: false, selectedScopeCategories: {}, filteredGroups: [] };

  preventSubmitOnEnter = event => {
    if (event.keyCode === 13) {
      event.preventDefault();
      return false;
    }
  };

  componentDidMount() {
    const { scopes, scopeId } = this.props;

    API.activityGroups.getForMenu().then(activityGroups => {
      this.setState({
        activityGroups: activityGroups && _.sortBy(activityGroups, 'name')
      });
    });

    const scopesOptions = scopes
      .filter(validateScope)
      .map(s => ({ value: s._id, label: s.name }));

    if (scopeId) {
      this.setScope();
    }

    window.addEventListener('keydown', this.preventSubmitOnEnter);

    this.setState({ scopesOptions });
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.preventSubmitOnEnter);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.scopeId !== this.props.scopeId) {
      this.setScope();
    }
  }

  setScope = () => {
    const { scopes, scopeId } = this.props;

    if (!scopeId) {
      return this.setState({ selectedScope: null });
    }

    const selectedScope = _.find(scopes, { _id: scopeId });

    this.setState({ selectedScope });
  };

  submit = async (values, dispatch) => {
    const { editData, onSuccess } = this.props;

    const result = await editData('solutions', values);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('SolutionMenuForm'));
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  unassignScopeHandler = () => {
    const { showConfirmPromise, change } = this.props;

    showConfirmPromise({
      message:
        'Are you sure you want to unassign scope from the Solution Menu? This will remove all activity groups assigns and reports.'
    }).then(isConfirmed => {
      if (!isConfirmed) return;

      change('SolutionMenuForm', 'solutionMenu', null);

      this.setState({ selectedScopeCategories: {} });
    });
  };

  changeSelectedScopeCategories = selectedScopeCategories => {
    this.setState({ selectedScopeCategories, filteredGroups: [] });
  };

  // changeMenuType = menuId => {
  //   const { selectedScopeCategories } = this.state;
  //   const { menuGroups, change } = this.props;

  //   const groupsWithoutCurrent =
  //     _.reject(menuGroups, selectedScopeCategories) || [];

  //   const current = _.find(menuGroups, selectedScopeCategories);

  //   const changedGroup = {
  //     ...current,
  //     menuId
  //   };
  //   const changedGroups = [...groupsWithoutCurrent, changedGroup];

  //   change('SolutionMenuForm', 'solutionMenu[menuGroups]', changedGroups);
  // };

  changeGroupHandler = groupId => {
    const { selectedScopeCategories } = this.state;
    const { menuGroups, change } = this.props;

    const groupsWithoutCurrent =
      _.reject(menuGroups, selectedScopeCategories) || [];

    if (!groupId) {
      return change(
        'SolutionMenuForm',
        'solutionMenu[menuGroups]',
        groupsWithoutCurrent
      );
    }

    const newGroup = {
      groupId,
      ...selectedScopeCategories
    };

    const changedGroups = [...groupsWithoutCurrent, newGroup];

    change('SolutionMenuForm', 'solutionMenu[menuGroups]', changedGroups);
  };

  // addGroupHandler = groupId => {
  //   const { selectedScopeCategories } = this.state;
  //   const { menuGroups, change } = this.props;

  //   const groupsWithoutCurrent =
  //     _.reject(menuGroups, selectedScopeCategories) || [];

  //   const current = _.find(menuGroups, selectedScopeCategories) || {};

  //   const newGroup = {
  //     groupId,
  //     ...selectedScopeCategories,
  //     menuId: current.menuId
  //   };
  //   const changedGroups = [...groupsWithoutCurrent, newGroup];

  //   change('SolutionMenuForm', 'solutionMenu[menuGroups]', changedGroups);
  // };

  // removeGroupHandler = () => {
  //   const { selectedScopeCategories } = this.state;
  //   const { menuGroups, change } = this.props;

  //   const groupsWithoutCurrent =
  //     _.reject(menuGroups, selectedScopeCategories) || [];

  //   change(
  //     'SolutionMenuForm',
  //     'solutionMenu[menuGroups]',
  //     groupsWithoutCurrent
  //   );
  // };

  render() {
    const { error, handleSubmit, pristine, submitting, reset } = this.props;
    const { success, scopesOptions, selectedScope } = this.state;

    const successMessage = 'Solution Menu updated successfully';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <div className={classes.selectScopeWrapper}>
          <div style={{ width: '280px' }}>
            <Field
              name="solutionMenu[scopeId]"
              label="Educational Category"
              component={renderSelect}
              options={scopesOptions || []}
              isClearable
              disabled={selectedScope}
            />
          </div>
          <Button
            variant="contained"
            disabled={!selectedScope}
            onClick={this.unassignScopeHandler}
          >
            Unassign
          </Button>
        </div>

        {selectedScope && this.renderMenuConfig()}

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          itemName="Solution Menu"
          handleReset={reset}
        />
      </form>
    );
  }

  renderMenuConfig = () => {
    const { reports, change, menuGroups } = this.props;
    const {
      selectedScope,
      selectedScopeCategories,
      activityGroups
    } = this.state;
    const { firstCategory } = selectedScope;

    if (!selectedScope || !firstCategory) return null;

    const isCategoriesSelected =
      selectedScopeCategories &&
      selectedScopeCategories.menuLevel &&
      selectedScopeCategories.firstCategory;

    return (
      <>
        <ReportsConfig
          items={firstCategory.items}
          reports={reports}
          change={change.bind(null, 'SolutionMenuForm')}
        />
        {/* 
        <Button
          variant="contained"
          disabled={
            !selectedScopeCategories.menuLevel ||
            !selectedScopeCategories.firstCategory
          }
          onClick={this.searchActivityGroups}
        >
          Search Activity Groups
        </Button> */}

        <div className={classes.assignGroupsWrapper}>
          <ScopeColumns
            changeSelected={this.changeSelectedScopeCategories}
            selected={selectedScopeCategories}
            data={selectedScope}
            menuGroups={menuGroups}
          />
          <SelectActivityGroups
            groups={isCategoriesSelected ? activityGroups : []}
            selected={
              isCategoriesSelected &&
              _.find(menuGroups, selectedScopeCategories)
            }
            // addHandler={this.addGroupHandler}
            // removeHandler={this.removeGroupHandler}
            changeGroupHandler={this.changeGroupHandler}
            // changeMenuType={this.changeMenuType}
          />
        </div>
      </>
    );
  };
}

SolutionMenuForm.propTypes = {
  onSuccess: PropTypes.func,
  initialValues: PropTypes.object,
  scopeId: PropTypes.string,
  reports: PropTypes.object,
  menuGroups: PropTypes.array,
  editData: PropTypes.func.isRequired
};

const selector = formValueSelector('SolutionMenuForm');

const mapStateToProps = state => ({
  scopeId: selector(state, 'solutionMenu[scopeId]'),
  reports: selector(state, 'solutionMenu[reports]'),
  menuGroups: selector(state, 'solutionMenu[menuGroups]')
});

export default compose(
  withData(['scopes']),
  reduxForm({
    form: 'SolutionMenuForm',
    initialValues: {},
    enableReinitialize: true,
    validate
  }),
  connect(
    mapStateToProps,
    { editData, change, showConfirmPromise }
  )
)(SolutionMenuForm);
