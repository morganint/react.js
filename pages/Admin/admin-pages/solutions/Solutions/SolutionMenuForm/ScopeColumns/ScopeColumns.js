import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import classes from './ScopeColumns.module.scss';

const ScopeColumns = ({ data, selected, changeSelected, menuGroups }) => {
  const {
    firstCategory,
    // secondCategory,
    // thirdCategory,
    grades,
    levels,
    firstMenu
  } = data;
  const menuLevels = firstMenu === 'levels' ? levels : grades;

  // const secondCategoryList = useMemo(() => {
  //   const list = _.find(secondCategory.lists, {
  //     firstCategoryId: selected.firstCategory
  //   });

  //   return list || { items: [] };
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [selected, data]);

  // const thirdCategoryList = useMemo(() => {
  //   const list = _.find(thirdCategory.lists, {
  //     firstCategoryId: selected.firstCategory,
  //     secondCategoryId: selected.secondCategory
  //   });

  //   return list || { items: [] };
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [selected, data]);

  return (
    <div className={classes.columnsWrapper}>
      <div className={classes.firstColumn}>
        <span className={classes.colTitle}>{_.capitalize(firstMenu)}:</span>
        <List className={classes.list}>
          {menuLevels.map(level => (
            <ListItem
              key={level}
              classes={{ selected: classes.selectedItem }}
              dense
              button
              selected={level === selected.menuLevel}
              onClick={() => changeSelected({ ...selected, menuLevel: level })}
            >
              {level}
            </ListItem>
          ))}
        </List>
      </div>

      <div className={classes.column}>
        <span className={classes.colTitle}>{firstCategory.title}:</span>
        <List className={classes.list}>
          {firstCategory.items.map(item => {
            const { menuLevel } = selected;

            const menuItem =
              !!menuLevel &&
              _.find(menuGroups, {
                menuLevel,
                firstCategory: item.id
              });

            const isCompleted = !!(menuItem && menuItem.groupId);

            return (
              <ListItem
                dense
                button
                key={item.id}
                classes={{
                  root:
                    classes.listItem +
                    ' ' +
                    (isCompleted ? classes.completed : ''),
                  selected: classes.selectedItem
                }}
                selected={item.id === selected.firstCategory}
                onClick={() =>
                  changeSelected({ ...selected, firstCategory: item.id })
                }
              >
                {item.title}
              </ListItem>
            );
          })}
        </List>
      </div>
      {/* <div className={classes.column}>
        <span className={classes.colTitle}>{secondCategory.title}:</span>
        <List className={classes.list}>
          {secondCategoryList.items.map(item => (
            <ListItem
              key={item.id}
              classes={{ selected: classes.selectedItem }}
              dense
              button
              selected={item.id === selected.secondCategory}
              onClick={() =>
                changeSelected({ ...selected, secondCategory: item.id })
              }
            ></ListItem>
          ))}
        </List>
      </div>
      <div className={classes.column}>
        <span className={classes.colTitle}>{thirdCategory.title}:</span>
        <List className={classes.list}>
          {thirdCategoryList.items.map(item => (
            <ListItem dense key={item.id}>
              {item.title}
            </ListItem>
          ))}
        </List>
      </div> */}
    </div>
  );
};

ScopeColumns.propTypes = {
  data: PropTypes.object,
  classes: PropTypes.object
};

export default ScopeColumns;
