import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Select from 'react-select';

// import { withData } from 'components/hocs';
import classes from './SelectActivityGroups.module.scss';

const SelectActivityGroups = ({
  // menus,
  groups = [],
  selected,
  // addHandler,
  // removeHandler,
  changeGroupHandler
  // changeMenuType
}) => {
  // const menuOptions = useMemo(() => {
  //   return menus.map(m => ({ label: m.menuName, value: m._id }));
  // }, [menus]);

  const groupsOptions = useMemo(() => {
    return groups.map(group => ({ label: group.name, value: group._id }));
  }, [groups]);

  const selectedGroup = useMemo(() => {
    const { groupId } = selected || {};
    return _.find(groupsOptions, { value: groupId });
  }, [groupsOptions, selected]);

  // const selectedMenuOption = useMemo(() => {
  //   const { menuId } = selected || {};
  //   return _.find(menuOptions, { value: menuId });
  // }, [menuOptions, selected]);

  return (
    <div className={classes.column}>
      {/* <span className={classes.colTitle}>Activity Groups:</span> */}

      <div className={classes.selectGroupWrapper}>
        <span className={classes.colTitle}>Activity Group:</span>
        <Select
          isClearable
          options={groupsOptions}
          value={selectedGroup || ''}
          onChange={opt => changeGroupHandler(opt && opt.value)}
        />
      </div>

      {/* <List className={classes.list}>
        {groups.map(group => {
          const isChecked = !!selected && selected.groupId === group._id;

          return (
            <ListItem
              key={group._id}
              role={undefined}
              dense
              button
              onClick={() => {
                if (isChecked) {
                  removeHandler(group._id);
                } else {
                  addHandler(group._id);
                }
              }}
            >
              <ListItemIcon>
                <Checkbox
                  edge="start"
                  classes={{ root: classes.checkbox }}
                  checked={isChecked}
                  tabIndex={-1}
                  disableRipple
                  size="small"
                />
              </ListItemIcon>
              <ListItemText primary={group.name} />
            </ListItem>
          );
        })}
      </List> */}

      {/* {selected && (
        <div className={classes.selectMenuTypeWrapper}>
          <span className={classes.colTitle}>Menu Type:</span>
          <Select
            isClearable
            options={menuOptions}
            value={selectedMenuOption || ''}
            onChange={opt => changeMenuType(opt && opt.value)}
          />
        </div>
      )} */}
    </div>
  );
};

SelectActivityGroups.propTypes = {
  groups: PropTypes.array.isRequired,
  // menus: PropTypes.array.isRequired,
  selected: PropTypes.object,
  // addHandler: PropTypes.func.isRequired,
  // changeMenuType: PropTypes.func.isRequired,
  // removeHandler: PropTypes.func.isRequired,
  changeGroupHandler: PropTypes.func.isRequired
};

export default SelectActivityGroups;
// export default withData(['menus'])(SelectActivityGroups);
