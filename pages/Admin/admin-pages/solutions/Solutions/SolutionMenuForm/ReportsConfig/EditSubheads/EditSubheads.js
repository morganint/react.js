import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import SelectTemplates from './SelectTemplates/SelectTemplates';
import classes from './EditSubheads.module.scss';

const EditSubheads = ({ closeHandler, initialState, changeHandler }) => {
  const [templatesModalOpened, setTemplatesModalOpened] = useState(false);
  const [selectedIdx, setSelectedIndex] = useState(null);
  const [values, setValues] = useState(initialState || []);

  return (
    <>
      <Dialog open onClose={closeHandler}>
        <DialogTitle>Edit Subheads</DialogTitle>

        <DialogContent>
          <div>
            {[0, 1, 2, 3].map(idx => {
              return (
                <div className={classes.subheadsControls} key={idx}>
                  <input
                    type="text"
                    value={(values[idx] && values[idx].title) || ''}
                    onChange={e => {
                      const valuesCopy = [...values];

                      if (!valuesCopy[idx]) {
                        valuesCopy[idx] = {};
                      }

                      valuesCopy[idx].title = e.target.value;

                      setValues(valuesCopy);
                    }}
                  />

                  <Button
                    variant="contained"
                    size="small"
                    type="button"
                    onClick={() => {
                      setSelectedIndex(idx);
                      setTemplatesModalOpened(true);
                    }}
                  >
                    Templates
                  </Button>
                </div>
              );
            })}
          </div>
        </DialogContent>

        <DialogActions>
          <Button
            onClick={() => {
              const formattedValues = values
                .map(val =>
                  val && val.title && !!val.title.trim().length
                    ? {
                        templates: val.templates || [],
                        isWordlist: !!val.isWordlist,
                        title: val.title.trim()
                      }
                    : null
                )
                .filter(v => !!v);

              changeHandler(formattedValues);
            }}
            color="primary"
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>

      {templatesModalOpened && (
        <SelectTemplates
          initialState={values[selectedIdx] || {}}
          closeHandler={() => {
            setSelectedIndex(null);
            setTemplatesModalOpened(false);
          }}
          changeHandler={newValues => {
            const valuesCopy = [...values];

            valuesCopy[selectedIdx] = {
              ...valuesCopy[selectedIdx],
              ...newValues
            };

            setValues(valuesCopy);
            setSelectedIndex(null);
            setTemplatesModalOpened(false);
          }}
        />
      )}
    </>
  );
};

EditSubheads.propTypes = {
  closeHandler: PropTypes.func.isRequired,
  changeHandler: PropTypes.func.isRequired,
  initialState: PropTypes.array
};

export default EditSubheads;
