import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const SelectTemplates = ({
  activityTemplates,
  closeHandler,
  changeHandler,
  initialState = {}
}) => {
  const [selectedTemplates, changeSelectedTemplates] = useState(
    initialState.templates || []
  );
  const [stateIsWordlist, changeStateIsWordlist] = useState(
    initialState.isWordlist
  );

  return (
    <Dialog open onClose={closeHandler}>
      <DialogTitle>Select Templates</DialogTitle>

      <DialogContent>
        <div
          style={{
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-between'
          }}
        >
          {activityTemplates
            .sort((a, b) =>
              a.activityTemplateName > b.activityTemplateName ? 1 : -1
            )
            .map(templ => (
              <div key={templ._id} style={{ width: '48%' }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      disabled={stateIsWordlist}
                      checked={selectedTemplates.includes(templ._id) || false}
                      onChange={e => {
                        const { checked } = e.target;

                        if (checked) {
                          changeSelectedTemplates([
                            ...selectedTemplates,
                            templ._id
                          ]);
                        } else {
                          changeSelectedTemplates(
                            selectedTemplates.filter(id => id !== templ._id)
                          );
                        }
                      }}
                      color="primary"
                    />
                  }
                  label={templ.activityTemplateName}
                />
              </div>
            ))}
        </div>

        <div style={{ marginTop: '30px' }}>
          <FormControlLabel
            control={
              <Checkbox
                disabled={!!selectedTemplates.length}
                checked={stateIsWordlist || false}
                onChange={e => {
                  changeStateIsWordlist(e.target.checked);
                }}
                color="primary"
              />
            }
            label="Wordlist Activity Group"
          />
        </div>
      </DialogContent>

      <DialogActions>
        <Button
          onClick={() =>
            changeHandler({
              templates: selectedTemplates,
              isWordlist: stateIsWordlist
            })
          }
          color="primary"
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

SelectTemplates.propTypes = {
  activityTemplates: PropTypes.array.isRequired,
  closeHandler: PropTypes.func.isRequired,
  changeHandler: PropTypes.func.isRequired,
  initialState: PropTypes.object
};

const mapStateToProps = state => ({
  activityTemplates: state.common.activityTemplates
});

export default connect(mapStateToProps)(SelectTemplates);
