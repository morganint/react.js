import React, { Component } from "react";
import PropTypes from "prop-types";
import { Field } from "redux-form";

import API from "api";

// import EditSubheads from './EditSubheads/EditSubheads';
// import { renderCheckbox } from 'components/form-components';
import classes from "./ReportsConfig.module.scss";

class ReportsConfig extends Component {
  state = {
    imageFilesOptions: []
    // subheadsModalShow: false
  };

  componentDidMount() {
    API.storage.listFiles("Images/ReportMenuImages/").then(list => {
      const imageFilesOptions = (list || []).map(val => ({
        label: val.unprefixed,
        value: val.unprefixed
      }));

      this.setState({ imageFilesOptions });
    });
  }

  // showSubheadsModal = selectedItemId => {
  //   this.setState({
  //     subheadsModalShow: true,
  //     selectedItemId
  //   });
  // };

  // closeSubheadsModal = () => {
  //   this.setState({
  //     subheadsModalShow: false,
  //     selectedItemId: null
  //   });
  // };

  // changeSubheadsHandler = (data, isWordlist) => {
  //   const { change } = this.props;
  //   const { selectedItemId } = this.state;

  //   const prefix = `solutionMenu[reports][${selectedItemId}].subheads`;

  //   change(`${prefix}`, data);

  //   this.closeSubheadsModal();
  // };

  render() {
    const {
      items
      // reports
      // change
    } = this.props;
    // const { subheadsModalShow, selectedItemId } = this.state;

    const { imageFilesOptions } = this.state;

    return (
      <>
        <table className={classes.table}>
          <thead>
            <tr>
              <th>Reports</th>
              <th>Order</th>
              <th>Name</th>
              <th>Image</th>
              {/* <th>isVocabulary</th>
              <th>hasSubheads</th>
              <th>Subheads</th> */}
            </tr>
          </thead>
          <tbody>
            {items.map(i => {
              const fieldNamePrefix = `solutionMenu[reports][${i.id}]`;

              /* const shouldRenderSubheads =
                reports && reports[i.id] && reports[i.id].hasSubhead; */

              return (
                <tr key={i.id}>
                  <td className={classes.titleCol}>{i.title}</td>
                  <td>
                    <Field
                      name={`${fieldNamePrefix}[order]`}
                      component="input"
                      type="number"
                      min="1"
                      max={items.length}
                    />
                  </td>
                  <td className={classes.nameCol}>
                    <Field
                      name={`${fieldNamePrefix}[name]`}
                      component="input"
                      type="number"
                    />
                  </td>
                  <td className={classes.imageCol}>
                    {/* <Field
                      name={`${fieldNamePrefix}[image]`}
                      component="input"
                    /> */}

                    <Field
                      name={`${fieldNamePrefix}[image]`}
                      component="select"
                    >
                      <option></option>
                      {imageFilesOptions.map(opt => (
                        <option key={opt.value} value={opt.value}>
                          {opt.label}
                        </option>
                      ))}
                    </Field>
                  </td>

                  {/* <td className={classes.checkboxCol}>
                    <Field
                      name={`${fieldNamePrefix}[isVocabulary]`}
                      component={renderCheckbox}
                    />
                  </td>
                  <td className={classes.checkboxCol}>
                    <Field
                      name={`${fieldNamePrefix}[hasSubhead]`}
                      component={renderCheckbox}
                      onChange={e => {
                        const { checked } = e.target;

                        if (!checked) {
                          change(`${fieldNamePrefix}[subhead]`, '');
                          change(`${fieldNamePrefix}[subheadTemplates]`, null);
                        }
                      }}
                    />
                  </td>
                  <td className={classes.subheadsCol}>
                    {shouldRenderSubheads && (
                      <Button
                        variant="contained"
                        size="small"
                        type="button"
                        onClick={() => this.showSubheadsModal(i.id)}
                      >
                        Edit Subheads
                      </Button>
                    )}
                  </td> */}
                </tr>
              );
            })}
          </tbody>
        </table>

        {/* {subheadsModalShow && (
          <EditSubheads
            initialState={
              (reports &&
                reports[selectedItemId] &&
                reports[selectedItemId].subheads) ||
              []
            }
            closeHandler={this.closeSubheadsModal}
            changeHandler={this.changeSubheadsHandler}
          />
        )} */}
      </>
    );
  }
}

ReportsConfig.propTypes = {
  items: PropTypes.array.isRequired,
  reports: PropTypes.object
};

export default ReportsConfig;
