import React from 'react';

import SolutionForm from './SolutionForm/SolutionForm';

const CreateSolution = ({ history }) => (
  <SolutionForm onSuccess={history.goBack} />
);

export default CreateSolution;
