import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import SolutionMenuForm from './SolutionMenuForm/SolutionMenuForm';
import { Redirect } from 'react-router-dom';
import { withData } from 'components/hocs';

const EditSolutionMenu = ({ match, solutions = [] }) => {
  const solutionToEdit = useMemo(
    () => solutions.filter(solution => solution._id === match.params.id)[0],
    [solutions, match.params.id]
  );

  return (
    <>
      {solutionToEdit ? (
        <SolutionMenuForm initialValues={solutionToEdit} />
      ) : (
        <Redirect to="/admin/solutions" />
      )}
    </>
  );
};

EditSolutionMenu.propTypes = {
  solutions: PropTypes.array.isRequired
};

export default withData(['solutions'])(EditSolutionMenu);
