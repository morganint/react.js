import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
  SubmissionError,
  reset,
  formValueSelector,
} from 'redux-form';
import { compose } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withData } from 'components/hocs';

import { addData, editData } from 'actions/adminActions';
import { getAnimationsList, getHelpActivities } from 'actions/solutionsActions';

import {
  renderTextField,
  renderCheckbox,
  renderSelectField,
  renderSelect,
  CheckboxFieldsGroup,
  FormButtons,
  FieldSet,
  formatCheckboxesInArray,
  StyledError,
  StyledSuccess,
} from 'components/form-components';

const validate = (values) => {
  const errors = {};
  const requiredFields = ['name', 'displayName', 'locale', 'defaultTheme'];

  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });

  return errors;
};

export class SolutionForm extends Component {
  state = { success: false };

  componentDidMount() {
    this.init().then();
  }

  init = async () => {
    const {
      themes,
      locales,
      characters,
      getAnimationsList,
      getHelpActivities,
    } = this.props;
    const themesOptions = themes
      ? themes.map((theme) => ({
          value: theme._id,
          label: theme.name,
        }))
      : [];

    const charactersOptions = characters
      ? characters.map((character) => ({
          value: character._id,
          label: character.name,
        }))
      : [];

    const localesOptions = locales
      ? locales.map((locale) => ({
          value: locale.code,
          label: locale.name,
        }))
      : [];

    this.setState({ themesOptions, charactersOptions, localesOptions });

    const animationsList = await getAnimationsList([
      'Introduction',
      'ActivityBuilder',
    ]);
    const helpActivities = await getHelpActivities();

    const helpActivitiesOptions = helpActivities.map((act) => ({
      label: act.activityName,
      value: act._id,
    }));

    this.setState({ helpActivitiesOptions, animationsList });
  };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    const formattedValues = { ...values };

    formattedValues.themes = [
      ...formatCheckboxesInArray(formattedValues.themes),
    ];

    formattedValues.characters = [
      ...formatCheckboxesInArray(formattedValues.characters),
    ];

    formattedValues.parentAdvicesLocales = [
      ...formatCheckboxesInArray(formattedValues.parentAdvicesLocales),
    ];

    const result = isEdit
      ? await editData('solutions', formattedValues)
      : await addData('solutions', formattedValues);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('SolutionForm'));
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      submitting,
      locales,
      isEdit,
      reset,
      parentAdvicesEnabled,
      initialValues = {},
    } = this.props;
    const {
      themesOptions,
      charactersOptions,
      helpActivitiesOptions,
      localesOptions,
      animationsList,
      success,
    } = this.state;

    const successMessage = isEdit
      ? 'Solution updated successfully'
      : 'Solution created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <FieldSet>
          <Field name="name" label="Name *" component={renderTextField} />
          <Field
            name="displayName"
            label="Display Name *"
            component={renderTextField}
          />
          {localesOptions && (
            <Field
              name="locale"
              label="Language *"
              component={renderSelectField}
              width="auto"
              options={localesOptions}
            />
          )}
          <Field
            name="parentAdvicesEnabled"
            label="Parent Advice"
            component={renderCheckbox}
          />

          <Field
            name="activityBuilder"
            label="Activity Builder"
            component={renderCheckbox}
          />

          {parentAdvicesEnabled && localesOptions && (
            <CheckboxFieldsGroup
              name="parentAdvicesLocales"
              fields={localesOptions}
              groupLabel="Parent Advice Locales (if none selected then solution language will be used)"
              initialValues={initialValues}
              form="SolutionForm"
            />
          )}

          {themesOptions && (
            <CheckboxFieldsGroup
              name="themes"
              fields={themesOptions}
              groupLabel={'Themes *'}
              initialValues={initialValues}
              form="SolutionForm"
              defaultSelect={{
                name: 'defaultTheme',
                label: 'Default Theme *',
              }}
            />
          )}

          {charactersOptions && (
            <CheckboxFieldsGroup
              name="characters"
              fields={charactersOptions}
              groupLabel={'Characters'}
              initialValues={initialValues}
              form="SolutionForm"
              defaultSelect={{
                name: 'defaultCharacter',
                label: 'Default Character',
              }}
            />
          )}

          <div style={{ marginBottom: '2rem' }}>
            <b>Automatic animations:</b>
          </div>

          {animationsList &&
            locales.map((loc) => {
              const introductionAnimations =
                (animationsList.Introduction &&
                  animationsList.Introduction[loc.name]) ||
                [];

              const activityBuilderAnimations =
                (animationsList.ActivityBuilder &&
                  animationsList.ActivityBuilder[loc.name]) ||
                [];

              const introductionAnimationsOptions = introductionAnimations.map(
                (a) => ({
                  label: a.filename,
                  value: a.filename,
                })
              );

              const activityBuilderAnimationsOptions = activityBuilderAnimations.map(
                (a) => ({
                  label: a.filename,
                  value: a.filename,
                })
              );

              return (
                <FlexContainer key={loc._id}>
                  <Column width="80px">
                    <p style={{ textAlign: 'right' }}>{loc.displayName}</p>
                  </Column>

                  <Column width="200px">
                    <Field
                      name={`animations[introduction][${loc.code}]`}
                      label="Introduction"
                      component={renderSelect}
                      isClearable
                      options={introductionAnimationsOptions}
                    />
                  </Column>

                  <Column width="200px">
                    <Field
                      name={`animations[activityBuilder][${loc.code}]`}
                      label="Activity Builder"
                      component={renderSelect}
                      isClearable
                      options={activityBuilderAnimationsOptions}
                    />
                  </Column>
                </FlexContainer>
              );
            })}
        </FieldSet>

        {/* <FieldSet title="Student Reports Labels" border flex>
          {[].map((report, index) => (
            <StyledReportsGroup key={index}>
              <Field
                label={report.label + ':'}
                name={`reportLabels[${report.value}]`}
                component={renderTextField}
              />
            </StyledReportsGroup>
          ))}
        </FieldSet> */}

        <Field
          name="helpActivityId"
          label="Help Activity"
          component={renderSelect}
          options={helpActivitiesOptions || []}
          isClearable
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="Solution"
          handleReset={reset}
        />
      </form>
    );
  }
}

SolutionForm.propTypes = {
  isEdit: PropTypes.bool,
  onSuccess: PropTypes.func,
  initialValues: PropTypes.object,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  characters: PropTypes.array.isRequired,
  themes: PropTypes.array.isRequired,
  locales: PropTypes.array.isRequired,
  getHelpActivities: PropTypes.func.isRequired,
  getAnimationsList: PropTypes.func.isRequired,
  parentAdvicesEnabled: PropTypes.bool,
};

const selector = formValueSelector('SolutionForm');

const mapStateToProps = (state) => ({
  parentAdvicesEnabled: selector(state, 'parentAdvicesEnabled'),
});

export default compose(
  withData(['locales', 'themes', 'characters']),
  reduxForm({
    form: 'SolutionForm',
    initialValues: {},
    enableReinitialize: true,
    validate,
  }),
  connect(mapStateToProps, {
    addData,
    editData,
    getAnimationsList,
    getHelpActivities,
  })
)(SolutionForm);

const FlexContainer = styled.div`
  display: flex;
`;

const Column = styled.div`
  margin-right: 2rem;
  width: ${({ width }) => (width ? width : 'auto')};
`;
