import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Name',
    width: '200px'
  },
  {
    id: 'solutionNames',
    disablePadding: false,
    label: 'Solutions',
    type: 'array',
    sort: 'asc'
  }
];

const Products = ({ deleteData, solutions, products }) => {
  const deleteHandler = (id, e) => {
    deleteData('products', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/products/create"
      >
        Create product
      </Button>

      <MaterialTable
        type="product"
        data={products.map(product => {
          product.solutionNames = product.solutions
            .map(id => {
              const name = solutions.filter(solution => solution._id === id)[0];
              return name && name.displayName;
            })
            .sort();
          return product;
        })}
        rowWithName="name"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/products/edit"
      />
    </div>
  );
};

Products.propTypes = {
  products: PropTypes.array.isRequired,
  solutions: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['products', 'solutions'])(Products);
