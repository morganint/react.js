import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import ProductForm from './ProductForm/ProductForm';
import { formatCheckboxesFromArray } from 'components/form-components';
import { isEmpty } from 'validation';

export class EditProduct extends Component {
  render() {
    const { match, products } = this.props;

    const dataToEdit =
      products && products.filter(data => data._id === match.params.id)[0];

    const initialValues = dataToEdit && {
      ...dataToEdit,
      solutions: isEmpty(dataToEdit.solutions)
        ? undefined
        : formatCheckboxesFromArray(dataToEdit.solutions)
    };

    return (
      <Fragment>
        {dataToEdit ? (
          <ProductForm isEdit initialValues={initialValues} />
        ) : (
          <Redirect to="/admin/products" />
        )}
      </Fragment>
    );
  }
}

EditProduct.propTypes = {
  products: PropTypes.array
};

const mapStateToProps = ({ admin }) => ({ products: admin.products });

export default connect(mapStateToProps)(EditProduct);
