import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
  SubmissionError,
  reset,
  formValueSelector,
} from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import { addData, editData } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons,
  renderSelect,
  formatCheckboxesInArray,
  CheckboxFieldsGroup,
} from 'components/form-components';
import { reduxFormValidator } from 'validation';
import { withData } from 'components/hocs';
import classes from './ProductForm.module.scss';

const requiredFields = [
  'name',
  'defaultSolutionsForAdmins',
  'defaultSolutionsForMembers',
];

const validate = reduxFormValidator(requiredFields);

class ProductForm extends Component {
  state = {
    success: false,
    selectedSolutions: [],
    selectedSolutionsLocales: [],
  };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    const formattedValues = { ...values };

    formattedValues.solutions = [
      ...formatCheckboxesInArray(formattedValues.solutions),
    ];

    const result = isEdit
      ? await editData('products', formattedValues)
      : await addData('products', formattedValues);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('ProductForm'));
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  componentDidMount() {
    const { solutions, documents } = this.props;

    const solutionsOptions =
      solutions &&
      solutions.map((solution) => ({
        value: solution._id,
        label: solution.displayName,
      }));

    const documentsOptions =
      documents &&
      documents
        .map((doc) => ({
          value: doc._id,
          label: doc.documentName,
        }))
        .sort((a, b) => (a.label > b.label ? 1 : -1));

    this.setState({ solutionsOptions, documentsOptions });
    this.selectedSolutionsWasChanged();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedSolutionsIds !== this.props.selectedSolutionsIds) {
      this.selectedSolutionsWasChanged();
    }
  }

  selectedSolutionsWasChanged = () => {
    const {
      change,
      locales,
      solutions,
      selectedSolutionsIds,
      defaultSolutionsForAdmins,
      defaultSolutionsForMembers,
    } = this.props;

    const localesCodes = [];

    const selectedSolutions = _.chain(selectedSolutionsIds)
      .map((val, key) => val && _.find(solutions, { _id: key }))
      .filter((solution) => {
        if (solution && !localesCodes.includes(solution.locale)) {
          localesCodes.push(solution.locale);
        }

        return solution;
      })
      .value();

    const selectedSolutionsLocales = localesCodes.map((code) =>
      _.find(locales, { code })
    );

    if (defaultSolutionsForMembers) {
      const keys = Object.keys(defaultSolutionsForMembers);

      keys.forEach((localeCode) => {
        const solutionId = defaultSolutionsForMembers[localeCode];

        if (!selectedSolutionsIds[solutionId]) {
          delete defaultSolutionsForMembers[localeCode];
        }
      });

      if (keys.length !== Object.keys(defaultSolutionsForMembers).length) {
        change('defaultSolutionsForMembers', defaultSolutionsForMembers);
      }
    }

    if (defaultSolutionsForAdmins) {
      const keys = Object.keys(defaultSolutionsForAdmins);

      keys.forEach((localeCode) => {
        const solutionId = defaultSolutionsForAdmins[localeCode];

        if (!selectedSolutionsIds[solutionId]) {
          delete defaultSolutionsForAdmins[localeCode];
        }
      });

      if (keys.length !== Object.keys(defaultSolutionsForAdmins).length) {
        change('defaultSolutionsForAdmins', defaultSolutionsForAdmins);
      }
    }

    this.setState({
      selectedSolutions,
      selectedSolutionsLocales,
    });
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      isEdit,
      locales,
      initialValues = {},
    } = this.props;

    const {
      success,
      solutionsOptions,
      documentsOptions,
      selectedSolutions,
      selectedSolutionsLocales,
    } = this.state;

    const successMessage = isEdit
      ? 'Product updated successfully'
      : 'Product created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field name="name" label="Name *" component={renderTextField} />

        {documentsOptions && (
          <>
            <div style={{ marginBottom: '2rem' }}>
              <b>Quick Start Documents</b>
            </div>

            {locales
              .sort((a, b) => (a.displayName > b.displayName ? 1 : -1))
              .map((loc) => {
                return (
                  <div className={classes.documentsWrapper} key={loc._id}>
                    <Field
                      name={`quickStartDocs[forMember][${loc.code}]`}
                      label={`For Member(${loc.displayName})`}
                      component={renderSelect}
                      options={documentsOptions}
                      isClearable
                    />

                    <Field
                      name={`quickStartDocs[forAdmin][${loc.code}]`}
                      label={`For Admin(${loc.displayName})`}
                      component={renderSelect}
                      options={documentsOptions}
                      isClearable
                    />
                  </div>
                );
              })}
          </>
        )}

        {solutionsOptions && (
          <>
            <CheckboxFieldsGroup
              cols={3}
              sort="asc"
              fields={solutionsOptions}
              name="solutions"
              groupLabel="Solutions *"
              initialValues={initialValues}
              form="ProductForm"
            />

            <div style={{ margin: '2rem 0' }}>
              <b>Default Solutions</b>
            </div>

            {selectedSolutionsLocales.map((locale) => {
              const options = selectedSolutions
                .filter((sol) => sol.locale === locale.code)
                .map((sol) => ({ value: sol._id, label: sol.displayName }));

              return (
                <div className={classes.documentsWrapper} key={locale._id}>
                  <Field
                    name={`defaultSolutionsForMembers[${locale.code}]`}
                    label={`For Member(${locale.displayName})`}
                    component={renderSelect}
                    options={options}
                    isClearable
                  />

                  <Field
                    name={`defaultSolutionsForAdmins[${locale.code}]`}
                    label={`For Admin(${locale.displayName})`}
                    component={renderSelect}
                    options={options}
                    isClearable
                  />
                </div>
              );
            })}
          </>
        )}

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="product"
          handleReset={reset}
        />
      </form>
    );
  }
}

ProductForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  solutions: PropTypes.array.isRequired,
  documents: PropTypes.array.isRequired,
  locales: PropTypes.array.isRequired,
};

const selector = formValueSelector('ProductForm');

const mapStateToProps = (state) => ({
  selectedSolutionsIds: selector(state, 'solutions') || {},
  defaultSolutionsForAdmins: selector(state, 'defaultSolutionsForAdmins') || {},
  defaultSolutionsForMembers:
    selector(state, 'defaultSolutionsForMembers') || {},
});

export default compose(
  withData(['solutions', 'documents', 'locales']),
  reduxForm({
    form: 'ProductForm',
    validate,
    enableReinitialize: true,
  }),
  connect(mapStateToProps, { addData, editData })
)(ProductForm);
