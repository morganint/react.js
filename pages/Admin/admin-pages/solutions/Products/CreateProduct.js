import React from 'react';

import ProductForm from './ProductForm/ProductForm';

const CreateProduct = ({ history }) => (
  <ProductForm onSuccess={history.goBack} />
);

export default CreateProduct;
