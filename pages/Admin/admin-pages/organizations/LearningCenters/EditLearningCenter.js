import React, { Fragment, Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { compose } from "redux";
import PropTypes from "prop-types";
import { find } from "lodash";

import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import { isEmpty } from "validation";
import {
  formatCheckboxesFromArray,
  OrganizationConfigForm
} from "components/form-components";
import LearningCenterDetailsForm from "./LearningCenterDetailsForm/LearningCenterDetailsForm";
import FamiliesTab from "./FamiliesTab/FamiliesTab";
import TeachersTab from "./TeachersTab/TeachersTab";
import MembersTab from "components/admin/MembersTab/MembersTab";
import { getMasterOrganizationById } from "actions/adminActions";
import { changePageTitle } from "actions/statusActions";
import { withData } from "components/hocs";
import { LEARNING_CENTER_ADMIN } from "consts/user-roles";

class EditLearningCenter extends Component {
  constructor(props) {
    super(props);

    const {
      history: {
        location: { state: locationState = {} }
      }
    } = props;

    this.state = {
      value: locationState.tab ? locationState.tab : "detailsTab"
    };
  }

  componentDidMount() {
    this.findDataToEdit();
    this.setState({ didMount: true });
  }

  componentDidUpdate(prevProps, prevState) {
    const { learningCenters } = this.props;

    if (prevProps.learningCenters !== learningCenters) {
      this.findDataToEdit();
    }

    if (this.state.learningCenter) {
      if (
        (prevState.learningCenter && prevState.learningCenter._id) !==
        this.state.learningCenter._id
      ) {
        this.getMasterOrganization();
      }
    }
  }

  getMasterOrganization = async () => {
    const { getMasterOrganizationById, changePageTitle } = this.props;
    const { learningCenter } = this.state;

    const masterOrganization = await getMasterOrganizationById(
      learningCenter.masterOrganizationId
    );

    if (!masterOrganization) {
      return console.error("Master Organization Not Found");
    }

    const { organizationLabels } = masterOrganization;

    if (!organizationLabels || !organizationLabels.learningCenter) return;

    this.setState({ masterOrganization });

    changePageTitle("Edit " + organizationLabels.learningCenter);
  };

  findDataToEdit = () => {
    const { match, learningCenters } = this.props;

    const learningCenter = find(learningCenters, {
      _id: match.params.id
    });

    this.setState({ learningCenter });
  };

  handleChange = (event, value) => {
    if (this.state.value === value) {
      return;
    }

    const { history } = this.props;

    const { state = {} } = history.location;

    history.replace(history.location.pathname, { ...state, tab: value });
    this.setState({ value });
  };

  render() {
    const { value, learningCenter, didMount, masterOrganization } = this.state;
    const { locales, lists } = this.props;

    const labels = masterOrganization && masterOrganization.organizationLabels;

    const initialValues = learningCenter && {
      ...learningCenter,
      config: learningCenter.config && {
        ...learningCenter.config,
        locales: isEmpty(learningCenter.config.locales)
          ? undefined
          : formatCheckboxesFromArray(learningCenter.config.locales)
      }
    };

    return (
      <Fragment>
        {learningCenter ? (
          <Fragment>
            <Tabs
              value={value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
            >
              <Tab label="Details" value="detailsTab" />
              <Tab label="Configuration" value="configurationTab" />
              <Tab label="Members" value="membersTab" />
              <Tab label="Families" value="familiesTab" />
              <Tab label="Teachers" value="teachersTab" />
            </Tabs>

            {value === "detailsTab" && (
              <LearningCenterDetailsForm
                isEdit
                initialValues={learningCenter}
              />
            )}
            {value === "configurationTab" && (
              <OrganizationConfigForm
                type="learningCenters"
                initialValues={initialValues}
                locales={locales}
                lists={lists}
              />
            )}
            {value === "membersTab" && (
              <MembersTab
                memberTitle={(labels && labels.learningCenter) + " Admin"}
                organization={learningCenter}
                membersRole={LEARNING_CENTER_ADMIN}
                assign={{
                  prop: "learningCenterId",
                  value: learningCenter._id
                }}
              />
            )}
            {value === "familiesTab" && (
              <FamiliesTab learningCenter={learningCenter} />
            )}
            {value === "teachersTab" && (
              <TeachersTab learningCenter={learningCenter} />
            )}
          </Fragment>
        ) : !didMount ? (
          <p>Loading...</p>
        ) : (
          <Redirect to="/admin/learning-centers" />
        )}
      </Fragment>
    );
  }
}

EditLearningCenter.propTypes = {
  auth: PropTypes.object.isRequired,
  learningCenters: PropTypes.array.isRequired,
  fetchData: PropTypes.func.isRequired,
  lists: PropTypes.array.isRequired,
  locales: PropTypes.array.isRequired,
  getMasterOrganizationById: PropTypes.func.isRequired,
  changePageTitle: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth }) => ({
  auth
});

export default compose(
  withData(["learningCenters", "locales", "lists"]),
  connect(mapStateToProps, { getMasterOrganizationById, changePageTitle })
)(EditLearningCenter);
