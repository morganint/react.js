import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _ from 'lodash';

import Button from '@material-ui/core/Button';

import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { deleteData } from 'actions/adminActions';
import { NeedPermissions } from 'utils';
import { withData } from 'components/hocs';
import {
  REP,
  MASTER_ORGANIZATION_ADMIN,
  REGION_ADMIN,
  LEARNING_CENTER_ADMIN,
  SUPER,
  MASTER_ADMIN
} from 'consts/user-roles';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Learning Center Name'
  },
  {
    id: 'masterOrganizationName',
    disablePadding: false,
    label: 'Master Organization'
  },
  {
    id: 'regionName',
    disablePadding: false,
    label: 'Region'
  },
  {
    id: 'contactEmail',
    disablePadding: false,
    label: 'Contact Email'
  }
];

export class LearningCenters extends Component {
  state = {
    filteredLearningCenterIDs: []
  };

  componentDidMount() {
    const {
      auth: { user },
      history,
      regions,
      learningCenters,
      masterOrganizations
    } = this.props;

    let filteredLearningCenterIDs;

    // If user is rep then show learning centers belongs to organizations that assigned to this rep
    if (user.roleAlias === REP) {
      const masterOrganizationsIDsAssignedToRep = _.find(masterOrganizations, {
        repId: user._id
      }).map(org => org._id);

      filteredLearningCenterIDs = _.filter(learningCenters, learningCenter => {
        return masterOrganizationsIDsAssignedToRep.includes(
          learningCenter.masterOrganizationId
        );
      });
    }

    // If user is master organization admin then show learning centers belongs to his organization
    if (user.roleAlias === MASTER_ORGANIZATION_ADMIN) {
      const masterOrganization = _.find(masterOrganizations, {
        _id: user.masterOrganizationId
      });

      if (!masterOrganization) {
        this.setState({
          error: 'Your master organization was deleted'
        });
      } else {
        filteredLearningCenterIDs = _.filter(learningCenters, {
          masterOrganizationId: masterOrganization._id
        });
      }
    }

    // If user is region admin then show learning centers belongs to his region
    if (user.roleAlias === REGION_ADMIN) {
      const region = _.find(regions, {
        _id: user.regionId
      });

      if (!region) {
        this.setState({
          error: 'Your region was deleted'
        });
      } else {
        filteredLearningCenterIDs = _.filter(learningCenters, {
          regionId: region._id
        });
      }
    }

    // If user is rlearning center admin then redirect to his learning center
    if (user.roleAlias === LEARNING_CENTER_ADMIN) {
      // find user's learning center
      const learningCenter = _.find(learningCenters, {
        _id: user.learningCenterId
      });

      if (!learningCenter) {
        this.setState({
          error: 'Your Learning Center was deleted'
        });
      } else {
        history.replace('/admin/learning-centers/edit/' + learningCenter._id);
        return;
      }
    }

    filteredLearningCenterIDs = learningCenters.map(
      learningCenter => learningCenter._id
    );

    this.setState({
      filteredLearningCenterIDs
    });
  }

  deleteDataHandler = (id, e) => {
    e.preventDefault();
    this.props.deleteData('learningCenters', id);
  };

  render() {
    const {
      regions,
      auth,
      masterOrganizations,
      users,
      learningCenters
    } = this.props;
    const { filteredLearningCenterIDs, error } = this.state;

    const filteredLearningCenters = filteredLearningCenterIDs
      .map(_id => {
        const learningCenter = _.find(learningCenters, { _id });

        if (!learningCenter) return null;

        const masterOrganization = _.find(masterOrganizations, {
          _id: learningCenter.masterOrganizationId
        });

        const region = _.find(regions, {
          _id: learningCenter.regionId
        });

        const contactUser = _.find(users, {
          _id: learningCenter.contactUserId
        });

        return {
          ...learningCenter,
          contactEmail: contactUser && contactUser.username,
          masterOrganizationName: masterOrganization && masterOrganization.name,
          regionName: (region && region.name) || ''
        };
      })
      .filter(learningCenter => !!learningCenter);

    return error ? (
      error
    ) : (
      <div>
        <NeedPermissions permissions={[SUPER, MASTER_ADMIN]}>
          <Button
            color="primary"
            variant="outlined"
            component={Link}
            to="/admin/learning-centers/create"
          >
            Create Learning Center
          </Button>
        </NeedPermissions>

        {regions && (
          <MaterialTable
            data={filteredLearningCenters}
            rows={rows}
            deleteHandler={
              auth.user.roleAlias === SUPER ||
              auth.user.roleAlias === MASTER_ADMIN
                ? this.deleteDataHandler
                : undefined
            }
            editLink="/admin/learning-centers/edit"
            type="Learning Center"
            rowWithName="name"
          />
        )}
      </div>
    );
  }
}

LearningCenters.propTypes = {
  auth: PropTypes.object.isRequired,
  regions: PropTypes.array.isRequired,
  masterOrganizations: PropTypes.array.isRequired,
  learningCenters: PropTypes.array.isRequired,
  users: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default compose(
  withData(['masterOrganizations', 'regions', 'users', 'learningCenters']),
  connect(
    mapStateToProps,
    { deleteData }
  )
)(LearningCenters);
