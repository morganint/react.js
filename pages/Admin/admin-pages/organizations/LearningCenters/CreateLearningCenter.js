import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import LearningCenterDetailsForm from './LearningCenterDetailsForm/LearningCenterDetailsForm';

class CreateLearningCenter extends React.Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { value } = this.state;

    return (
      <div>
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="Details" />
        </Tabs>

        {value === 0 && (
          <LearningCenterDetailsForm
            onSuccess={id =>
              this.props.history.replace(`/admin/learning-centers/edit/${id}`, {
                tab: 'configurationTab'
              })
            }
          />
        )}
      </div>
    );
  }
}

CreateLearningCenter.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToPtops = state => ({ auth: state.auth });

export default compose(connect(mapStateToPtops))(CreateLearningCenter);
