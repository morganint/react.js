import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import _ from "lodash";

import FamilyForm from "./FamilyForm/FamilyForm";
import { showError } from "actions/statusActions";
import { getLearningCenterPlansStatus } from "actions/adminActions";
import {
  getFamiliesAndMembersByLearningCenterId,
  clearFamilies,
  deleteFamily
} from "actions/familiesActions";
import MaterialTable from "components/admin/MaterialTable/MaterialTable";
import { FullScreenDialog } from "components/common";
import { AssignProductsField } from "components/form-components";
import { withData } from "components/hocs";
import { SUPER } from "consts/user-roles";

const rows = [
  {
    id: "name",
    disablePadding: false,
    label: "Name"
  },
  {
    id: "year",
    disablePadding: false,
    label: "Year"
  },
  {
    id: "teachersEmails",
    disablePadding: false,
    label: "Teachers",
    type: "array",
    sort: "asc"
  }
];

class FamiliesTab extends Component {
  state = {
    isFormOpened: false
  };

  async componentDidMount() {
    const {
      getFamiliesAndMembersByLearningCenterId,
      learningCenter
    } = this.props;

    // this.getLearningCenterPlansStatus();
    getFamiliesAndMembersByLearningCenterId(learningCenter._id);
  }

  componentWillUnmount() {
    this.props.clearFamilies();
  }

  componentDidUpdate(prevProps) {
    const { families, familyMembers } = this.props;
    const { familyToEdit } = this.state;

    if (
      prevProps.families !== families ||
      prevProps.familyMembers !== familyMembers
    ) {
      this.getLearningCenterPlansStatus();
      this.formatFamilies();

      if (familyToEdit) {
        const changedFamilyToEdit = _.find(families, { _id: familyToEdit._id });
        this.setFamilyToEdit(changedFamilyToEdit);
      }
    }
  }

  getLearningCenterPlansStatus = async () => {
    const { learningCenter, getLearningCenterPlansStatus } = this.props;

    const plansStatus = await getLearningCenterPlansStatus(learningCenter._id);

    this.setState({ plansStatus });
  };

  openForm = () => {
    this.setState({ isFormOpened: true });
  };

  closeForm = () => {
    this.setState({ isFormOpened: false });
  };

  setFamilyToEdit = family => {
    if (!family) {
      return this.setState({ familyToEdit: null });
    }

    const { familyMembers: learningCenterFamilyMembers } = this.props;

    const { _id: familyId } = family;
    const familyMembers = _.filter(learningCenterFamilyMembers, { familyId });

    const familyAdmin = _.find(familyMembers, { isAdmin: true });
    const usualFamilyMembers = _.filter(familyMembers, { isAdmin: false });

    const familyToEdit = {
      ...family,
      // Family admin should be first one
      familyMembers: [familyAdmin, ...usualFamilyMembers]
    };

    this.setState({ familyToEdit });
  };

  formatFamilies = () => {
    const { families, users } = this.props;

    if (!families) return;

    const formattedFamilies = families.map(f => {
      const familyTeachers = users.filter(u => f.teachers.includes(u._id));

      return { ...f, teachersEmails: familyTeachers.map(t => t.username) };
    });

    this.setState({ formattedFamilies });
  };

  render() {
    const { auth, learningCenter, deleteFamily, translate } = this.props;
    const {
      isFormOpened,
      familyToEdit,
      plansStatus = {},
      formattedFamilies
    } = this.state;

    const availablePlans =
      plansStatus.purchasedProducts &&
      _.filter(
        plansStatus.purchasedProducts,
        prod => !!plansStatus.notAssignedPlans[prod.productId].plans
      );

    return (
      <div style={{ marginTop: "20px" }}>
        <h3>Products: </h3>

        {plansStatus.purchasedProducts &&
        plansStatus.purchasedProducts.length ? (
          <AssignProductsField
            dynamic
            alreadyInUse={plansStatus.assignedProducts}
            canBeAssigned={plansStatus.notAssignedPlans}
            value={plansStatus.purchasedProducts}
            disabled
          />
        ) : (
          <p>No Assigned Products</p>
        )}

        {!!(availablePlans && availablePlans.length) && (
          <Button color="primary" variant="outlined" onClick={this.openForm}>
            Add family
          </Button>
        )}

        <MaterialTable
          data={formattedFamilies || []}
          rows={rows}
          deleteHandler={
            auth.user.roleAlias === SUPER ? deleteFamily : undefined
          }
          editHandler={family => {
            this.setFamilyToEdit(family);
            this.openForm();
          }}
          type="Family"
          rowWithName="name"
        />

        <FullScreenDialog
          title={
            familyToEdit
              ? translate(694, "Edit Family")
              : translate(693, "Create Family")
          }
          isOpen={isFormOpened}
          closeHandler={() => {
            this.setFamilyToEdit(null);
            this.closeForm();
          }}
        >
          {isFormOpened && (
            <FamilyForm
              translate={translate}
              plansStatus={plansStatus}
              isEdit={!!familyToEdit}
              learningCenterId={learningCenter._id}
              onSuccess={!familyToEdit && this.closeForm}
              initialValues={familyToEdit}
            />
          )}
        </FullScreenDialog>
      </div>
    );
  }
}

FamiliesTab.propTypes = {
  users: PropTypes.array.isRequired,
  auth: PropTypes.object.isRequired,
  learningCenter: PropTypes.object.isRequired,
  getFamiliesAndMembersByLearningCenterId: PropTypes.func.isRequired,
  getLearningCenterPlansStatus: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  families: PropTypes.array,
  familyMembers: PropTypes.array
};

const mapStateToProps = ({ auth, families, status }) => ({
  auth,
  translate: status.translate,
  families: families.families,
  familyMembers: families.familyMembers
});

export default compose(
  withData(["users"]),
  connect(mapStateToProps, {
    showError,
    getFamiliesAndMembersByLearningCenterId,
    getLearningCenterPlansStatus,
    clearFamilies,
    deleteFamily
  })
)(FamiliesTab);
