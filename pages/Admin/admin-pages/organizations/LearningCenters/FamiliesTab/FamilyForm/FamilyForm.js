import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  Field,
  reduxForm,
  SubmissionError,
  formValueSelector,
} from 'redux-form';
import _ from 'lodash';

// import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
// import DeleteIcon from '@material-ui/icons/Delete';

import API from 'api';
import { reduxFormValidator } from 'validation';
import {
  StyledError,
  getOptionsFromLists,
  MaterialSelect,
  renderMaterialTextField,
  FormButtons,
} from 'components/form-components';
import { FlexContainer } from 'components/common';
import { withData } from 'components/hocs';
import { addFamily, editFamily } from 'actions/familiesActions';
import { showConfirmPromise, showError } from 'actions/statusActions';
import classes from './FamilyForm.module.scss';
import { TEACHER } from 'consts/user-roles';
import { FAMILY_DEFAULT_MEMBERS_NUMBER } from 'consts/settings';

const requiredFields = [
  'name',
  'year',
  'productId',
  'familyMembers[0].firstname',
  'familyMembers[0].lastname',
  'familyMembers[0].username',
  // 'familyMembers[0].grade'
];

const validate = reduxFormValidator(requiredFields);

const CustomField = ({ ...props }) => (
  <Field
    margin="dense"
    component={renderMaterialTextField}
    fullWidth
    {...props}
  />
);

class FamilyForm extends Component {
  state = {};

  componentDidMount() {
    const {
      isEdit,
      lists,
      change,
      products,
      getOptionsFromLists,
      initialValues,
      learningCenterId,
      plansStatus,
    } = this.props;

    const fieldsWithListValues = getOptionsFromLists(
      [
        // {
        //   list: 'Grades'
        // },
        {
          name: 'year',
          list: 'Family Years',
        },
      ],
      lists,
      'FamilyForm',
      initialValues
    );

    change('learningCenterId', learningCenterId);

    // const gradesOptions =
    //   _.find(fieldsWithListValues, { list: 'Grades' }).options || [];

    const yearOptions =
      _.find(fieldsWithListValues, { list: 'Family Years' }).options || [];

    this.setState({
      // gradesOptions,
      yearOptions,
    });

    const availableProductsIDs = isEdit
      ? plansStatus.purchasedProducts.map((prod) => prod.productId)
      : Object.keys(plansStatus.notAssignedPlans);

    const productsOptions = _.chain(availableProductsIDs)
      .map((prodId) => {
        const currentProd = _.find(products, { _id: prodId });
        if (!currentProd) return null;

        return { label: currentProd.name, value: currentProd._id };
      })
      .compact()
      .filter(
        (prod) =>
          !!(
            (isEdit && initialValues.productId === prod.value) ||
            (plansStatus.notAssignedPlans[prod.value] &&
              plansStatus.notAssignedPlans[prod.value].plans)
          )
      )
      .value();

    this.filterTeachers();

    this.setState({ plansStatus, productsOptions });

    if (productsOptions.length === 1 && !isEdit) {
      change('productId', productsOptions[0].value);
    } else if (isEdit) {
      this.setMaxAddons();
      this.getProductSolutions();
    }
  }

  componentDidUpdate(prevProps) {
    const { selectedProductId } = this.props;

    if (prevProps.selectedProductId !== selectedProductId) {
      this.setMaxAddons();
      this.resetFamilyMembersSolution();
      this.getProductSolutions();
    }
  }

  resetFamilyMembersSolution = () => {
    const { familyMembers, change } = this.props;

    if (!familyMembers) return;

    const changedFamilyMembers = familyMembers.map((member) => {
      if (!member) return null;

      return { ...member, reportSolution: '' };
    });

    change('familyMembers', changedFamilyMembers);
  };

  getProductSolutions = async () => {
    const { selectedProductId } = this.props;

    if (!selectedProductId) {
      return this.setState({
        solutionsOptions: null,
      });
    }

    const result = await API.products.getProductSolutionsList(
      selectedProductId
    );

    if (!result.success) {
      return this.setState({
        solutionsOptions: null,
      });
    }

    const solutionsOptions = result.data.map((sol) => ({
      label: sol.displayName,
      value: sol._id,
    }));

    this.setState({ solutionsOptions });
  };

  filterTeachers = () => {
    const { users, learningCenterId } = this.props;
    const teachers = _.filter(users, { roleAlias: TEACHER, learningCenterId });

    const teachersOptions = teachers.map((teacher) => ({
      label: `${teacher.firstname} ${teacher.lastname}`,
      value: teacher._id,
    }));
    this.setState({ teachersOptions });
  };

  setMaxAddons = () => {
    const { selectedProductId, initialValues, plansStatus } = this.props;

    if (
      !plansStatus ||
      !plansStatus.notAssignedPlans ||
      !plansStatus.notAssignedPlans[selectedProductId]
    ) {
      return;
    }

    const maxAddons =
      (plansStatus.notAssignedPlans[selectedProductId].addons || 0) +
      ((initialValues && initialValues.addons) || 0);

    this.setState({ maxAddons });
  };

  // deleteAddonHandler = () => {
  //   const {
  //     familyMembers,
  //     isEdit,
  //     familyAddons,
  //     showError,
  //     change
  //   } = this.props;

  //   const spaces = 3 + (familyAddons || 0);

  //   const memberInLastSpace = familyMembers && familyMembers[spaces - 1];

  //   console.log(memberInLastSpace);

  //   if (isEdit && memberInLastSpace && memberInLastSpace._id) {
  //     // Real member
  //     return showError({ message: 'You should delete last member firstly' });
  //   }

  //   const newAddons = familyAddons ? familyAddons - 1 : 0;
  //   change('addons', newAddons);

  //   const newFamilyMembers = familyMembers
  //     ? _.take(familyMembers, spaces - 1)
  //     : [];

  //   change('familyMembers', newFamilyMembers);
  // };

  submit = async (values, dispatch) => {
    const { onSuccess, isEdit, editFamily, addFamily, translate } = this.props;

    const familyMembersErrors = {};
    const memberRequiredFields = [
      'username',
      'firstname',
      'lastname',
      // 'grade'
    ];

    const requiredTranslate = translate(695, 'Required');

    const validateMember = (member, idx) => {
      if (_.isEmpty(member)) return;

      memberRequiredFields.forEach((field) => {
        !member[field] &&
          (familyMembersErrors[idx] = {
            ...familyMembersErrors[idx],
            [field]: requiredTranslate,
          });
      });
    };

    _.forEach(values.familyMembers, validateMember);

    if (!_.isEmpty(familyMembersErrors)) {
      throw new SubmissionError({ familyMembers: familyMembersErrors });
    }

    const result = isEdit ? await editFamily(values) : await addFamily(values);

    if (result.success) {
      onSuccess && onSuccess();
    } else {
      throw new SubmissionError(result.errors);
    }
  };

  onChangeProduct = (e, newProductId) => {
    const { products, familyAddons, showError, plansStatus } = this.props;

    const availableAddonsForNextProduct =
      plansStatus.notAssignedPlans[newProductId].addons || 0;

    if (availableAddonsForNextProduct < familyAddons) {
      const product = _.find(products, { _id: newProductId });

      showError({
        message: `Not enough add-ons in product - ${product.name}. Delete add-ons firstly`,
      });
      e.preventDefault();
    }
  };

  deleteMemberHandler = (idx) => {
    console.log(idx);
    const {
      change,
      deletedMembers,
      familyMembers,
      showConfirmPromise,
    } = this.props;

    const isMemberExistsInDB =
      familyMembers && familyMembers[idx] && familyMembers[idx]._id;

    if (!isMemberExistsInDB) {
      return change(`familyMembers[${idx}]`, null);
    }

    showConfirmPromise({
      message: `
      You sure you want to delete member <b>${familyMembers[idx].username}</b>?
      This will delete the user and all user's record-keeping data
      `,
      html: true,
    }).then(async (isConfirmed) => {
      if (!isConfirmed) return;

      change(`deletedMembers`, [...(deletedMembers || []), familyMembers[idx]]);
      change(`familyMembers[${idx}]`, null);
    });
  };

  render() {
    const {
      initialValues,
      error,
      change,
      handleSubmit,
      pristine,
      submitting,
      reset,
      isEdit,
      translate,
      familyAddons,
      selectedProductId,
    } = this.props;

    const {
      // gradesOptions,
      teachersOptions,
      yearOptions,
      productsOptions,
      solutionsOptions,
      maxAddons,
    } = this.state;

    const familyAdminTranslate = translate(535, 'Family Admin');
    const familyMemberTranslate = translate(537, 'Family Member');
    const firstNameTranslate = translate(538, 'First Name');
    const lastNameTranslate = translate(654, 'Last Name');

    const availableAddons = maxAddons - (familyAddons || 0);

    return (
      <form onSubmit={handleSubmit(this.submit)} className={classes.form}>
        <CustomField
          name="name"
          label={translate(534, 'Family Name *')}
          autoFocus
        />

        <Field
          hidden
          name="learningCenterId"
          component="input"
          normalize={(val) => (val ? +val : 0)}
        />

        {/* <Field hidden name="deletedMembers" component="input" /> */}

        <FlexContainer sm="50%">
          <Field
            label={translate(411, 'Teachers')}
            component={MaterialSelect}
            name="teachers"
            options={teachersOptions}
            multiple
          />

          {yearOptions && (
            <Field
              label={translate(525, 'Year')}
              component={MaterialSelect}
              name="year"
              options={yearOptions}
            />
          )}
        </FlexContainer>

        {productsOptions && (
          <CustomField
            onChange={this.onChangeProduct}
            label={translate(338, 'Product')}
            component={MaterialSelect}
            name="productId"
            options={productsOptions}
            disabled={
              !!(initialValues && initialValues.productId) ||
              !!(productsOptions.length === 1 && selectedProductId)
            }
          />
        )}

        <div className={classes.membersWrapper}>
          {_.times(FAMILY_DEFAULT_MEMBERS_NUMBER + (familyAddons || 0)).map(
            (idx) => {
              const memberTranslate =
                idx === 0
                  ? familyAdminTranslate
                  : `${familyMemberTranslate} ${idx + 1}`;

              return (
                <div key={idx} className={classes.memberFields}>
                  <FlexContainer sm="50%">
                    <CustomField
                      name={`familyMembers[${idx}].firstname`}
                      label={`${memberTranslate} ${firstNameTranslate}`}
                    />
                    <CustomField
                      name={`familyMembers[${idx}].lastname`}
                      label={`${memberTranslate} ${lastNameTranslate}`}
                    />
                  </FlexContainer>

                  <FlexContainer sm="33%">
                    <CustomField
                      name={`familyMembers[${idx}].username`}
                      label={translate(102, 'Username')}
                      normalize={(val) => val && val.replace(/[^\w.-]/g, '')}
                    />
                    <CustomField
                      name={`familyMembers[${idx}].password`}
                      label={translate(498, 'Password')}
                    />
                    {solutionsOptions && (
                      <CustomField
                        label={translate(499, 'Report Solution')}
                        component={MaterialSelect}
                        name={`familyMembers[${idx}].reportSolution`}
                        options={solutionsOptions}
                      />
                    )}

                    {/* <Field
                  label="Grade"
                  component={MaterialSelect}
                  name={`familyMembers[${idx}].grade`}
                  options={gradesOptions}
                /> */}
                  </FlexContainer>

                  {/* {idx !== 0 && (
                <Fab
                  size="small"
                  color="inherit"
                  aria-label="add"
                  className={classes.deleteMemberIcon}
                  onClick={this.deleteMemberHandler.bind(null, idx)}
                >
                  <DeleteIcon />
                </Fab>
              )} */}
                </div>
              );
            }
          )}
        </div>
        <div className={classes.addonsControls}>
          {!!availableAddons && (
            <Button
              onClick={() => {
                const newAddons = (familyAddons || 0) + 1;
                change('addons', newAddons);
              }}
              color="primary"
              variant="contained"
              className={classes.addAddonButton}
            >
              Add Family Member
            </Button>
          )}
          {/* {!!familyAddons && (
            <Button
              onClick={this.deleteAddonHandler}
              variant="contained"
              className={classes.deleteAddonButton}
            >
              Remove Family Member
            </Button>
          )} */}
        </div>

        {!pristine && error && <StyledError>{error}</StyledError>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="Family"
          handleReset={reset}
        />
      </form>
    );
  }
}

FamilyForm.propTypes = {
  lists: PropTypes.array.isRequired,
  products: PropTypes.array.isRequired,
  initialValues: PropTypes.object,
  isEdit: PropTypes.bool.isRequired,
  selectedProductId: PropTypes.string,
  learningCenterId: PropTypes.string.isRequired,
  getOptionsFromLists: PropTypes.func.isRequired,
  familyAddons: PropTypes.number,
  familyMembers: PropTypes.array,
  deletedMembers: PropTypes.array,
  showConfirmPromise: PropTypes.func.isRequired,
  plansStatus: PropTypes.object.isRequired,
  showError: PropTypes.func.isRequired,
  addFamily: PropTypes.func.isRequired,
  editFamily: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
};

const selector = formValueSelector('FamilyForm');

const mapStateToProps = (state) => ({
  selectedProductId: selector(state, 'productId'),
  familyAddons: selector(state, 'addons'),
  familyMembers: selector(state, 'familyMembers'),
  deletedMembers: selector(state, 'deletedMembers'),
});

export default compose(
  withData(['lists', 'products', 'users']),
  reduxForm({
    form: 'FamilyForm',
    enableReinitialize: true,
    validate,
  }),
  connect(mapStateToProps, {
    getOptionsFromLists,
    showConfirmPromise,
    showError,
    addFamily,
    editFamily,
  })
)(FamilyForm);
