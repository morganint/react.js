import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  Field,
  reduxForm,
  SubmissionError,
  change,
  formValueSelector
} from "redux-form";
import { connect } from "react-redux";
import { compose } from "redux";
import _ from "lodash";

import { addLearningCenter, editData } from "actions/adminActions";
import { validateEmail, reduxFormValidator } from "validation";
import {
  renderTextField,
  FieldSet,
  StyledError,
  StyledSuccess,
  DateField,
  renderSelect,
  FormButtons
} from "components/form-components";
import { withData } from "components/hocs";
import { FlexContainer } from "components/common";
import { LEARNING_CENTER_ADMIN, MASTER_ADMIN, SUPER } from "consts/user-roles";

const requiredFields = [
  "name",
  "url",
  "contact[username]",
  "contact[firstname]",
  "contact[lastname]",
  "masterOrganizationId"
];

const validate = reduxFormValidator(requiredFields);

export class LearningCenterDetailsForm extends Component {
  state = { success: false };

  componentDidMount() {
    const { isEdit } = this.props;

    if (isEdit) {
      this.filterAdmins();
    }
  }

  submit = async (values, dispatch) => {
    const { isEdit, editData, addLearningCenter, onSuccess } = this.props;

    const formattedValues = { ...values };

    if (isEdit && !formattedValues.contactUserId) {
      throw new SubmissionError({
        contactUserId: "Required"
      });
    }

    console.log("Values for send:", formattedValues);

    const result = isEdit
      ? await editData("learningCenters", formattedValues)
      : await addLearningCenter("learningCenters", formattedValues);

    if (!result.success) {
      this.setState({ success: false });

      console.log("Server response:", result);
      throw new SubmissionError(result.err);
    } else {
      const learningCenterId =
        result.data &&
        result.data.learningCenter &&
        result.data.learningCenter._id;
      learningCenterId && onSuccess && onSuccess(learningCenterId);
    }
  };

  setUrlFieldTouched = urlFieldTouched => {
    if (this.state.urlFieldTouched === urlFieldTouched) return;
    this.setState({ urlFieldTouched });
  };

  filterAdmins = () => {
    const { users, initialValues } = this.props;

    const learningCenterAdmins = _.filter(users, {
      roleAlias: LEARNING_CENTER_ADMIN,
      learningCenterId: initialValues._id
    });

    const contactUsersOptions = learningCenterAdmins.map(user => ({
      label: `${user.firstname} ${user.lastname}`,
      value: user._id
    }));

    this.setState({ contactUsersOptions });
  };

  render() {
    const {
      userRole,
      error,
      masterOrganizations,
      regions,
      handleSubmit,
      pristine,
      submitting,
      isEdit,
      reset,
      change,
      contactUserId,
      users,
      selectedMasterOrganizationId
    } = this.props;

    const { success, contactUsersOptions, urlFieldTouched } = this.state;

    const contactUser = isEdit && _.find(users, { _id: contactUserId });

    const selectedMasterOrganization = _.find(masterOrganizations, {
      _id: selectedMasterOrganizationId
    });

    const labels =
      (selectedMasterOrganization &&
        selectedMasterOrganization.organizationLabels) ||
      {};

    const masterOrganizationOptions = masterOrganizations.map(org => ({
      label: org.name,
      value: org._id
    }));

    const filteredRegions = _.filter(regions, {
      masterOrganizationId: selectedMasterOrganizationId
    });

    const regionsOptions = filteredRegions.map(org => ({
      label: org.name,
      value: org._id
    }));

    const learningCenterLabel = labels.learningCenter || "Learning Center";

    const successMessage = isEdit
      ? `${learningCenterLabel} updated successfully`
      : `${learningCenterLabel}Region created`;

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <FieldSet>
          <Field
            name="name"
            label={`${learningCenterLabel} Name *`}
            component={renderTextField}
            onChange={(e, val) => {
              if (urlFieldTouched || isEdit) return;

              change(
                "LearningCenterDetailsForm",
                "url",
                val
                  .toLowerCase()
                  .replace(/\s+/g, "-")
                  .replace(/[^\d\w-]/g, "")
              );
            }}
          />

          <FlexContainer xs="100%" md="50%">
            <Field
              name="url"
              label={`${learningCenterLabel} Url *`}
              disabled={![SUPER, MASTER_ADMIN].includes(userRole)}
              normalize={val => val.toLowerCase().trim()}
              component={renderTextField}
              onChange={this.setUrlFieldTouched.bind(null, true)}
            />

            <DateField
              disabled={![SUPER, MASTER_ADMIN].includes(userRole)}
              name="expirationDate"
              label="Expiration Date"
            />
          </FlexContainer>

          {isEdit ? (
            <FlexContainer xs="100%" md="50%">
              <Field
                name="contactUserId"
                label="Contact User"
                component={renderSelect}
                options={contactUsersOptions || []}
              />

              {contactUser && (
                <div style={{ marginBottom: "30px", marginTop: "10px" }}>
                  <p>
                    <b>Firstname: </b>
                    {contactUser.firstname}
                  </p>
                  <p>
                    <b>Lastname: </b>
                    {contactUser.lastname}
                  </p>
                  <p>
                    <b>Email: </b>
                    {contactUser.username}
                  </p>
                  <p>
                    <b>Phone: </b>
                    {contactUser.phone}
                  </p>
                </div>
              )}
            </FlexContainer>
          ) : (
            <Fragment>
              <FlexContainer xs="100%" md="50%">
                <Field
                  name="contact[firstname]"
                  label="Contact Firstname *"
                  component={renderTextField}
                />
                <Field
                  name="contact[lastname]"
                  label="Contact Lastname *"
                  component={renderTextField}
                />
              </FlexContainer>
              <FlexContainer xs="100%" md="50%">
                <Field
                  name="contact[username]"
                  label="Contact Email *"
                  component={renderTextField}
                  validate={validateEmail}
                />
                <Field
                  name="contact[phone]"
                  label="Contact Phone"
                  component={renderTextField}
                />
              </FlexContainer>
            </Fragment>
          )}
        </FieldSet>

        <Field
          name="masterOrganizationId"
          label={labels.masterOrganization || "Master Organization *"}
          component={renderSelect}
          disabled={isEdit && selectedMasterOrganizationId}
          onChange={(...args) => {
            change("LearningCenterDetailsForm", "regionId", undefined);
            change("LearningCenterDetailsForm", "assignedProducts", []);
          }}
          options={masterOrganizationOptions || []}
        />

        <Field
          name="regionId"
          label="Region"
          component={renderSelect}
          disabled={isEdit || !selectedMasterOrganizationId}
          onChange={(...args) => {
            change("LearningCenterDetailsForm", "assignedProducts", []);
          }}
          options={regionsOptions || []}
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="Learning Center"
          handleReset={reset}
        />
      </form>
    );
  }
}

LearningCenterDetailsForm.propTypes = {
  masterOrganizations: PropTypes.array,
  regions: PropTypes.array,
  isEdit: PropTypes.bool,
  onSuccess: PropTypes.func,
  initialValues: PropTypes.object,
  addLearningCenter: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired
};

const selector = formValueSelector("LearningCenterDetailsForm");

const mapStateToProps = state => ({
  userRole: state.auth.user.roleAlias,
  contactUserId: selector(state, "contactUserId"),
  selectedMasterOrganizationId: selector(state, "masterOrganizationId"),
  selectedRegionId: selector(state, "regionId")
});

export default compose(
  withData(["users", "masterOrganizations", "regions"]),
  reduxForm({
    form: "LearningCenterDetailsForm",
    validate,
    enableReinitialize: true,
    initialValues: {}
  }),
  connect(mapStateToProps, {
    addLearningCenter,
    editData,
    change
  })
)(LearningCenterDetailsForm);
