import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Button from '@material-ui/core/Button';

import TeacherForm from './TeacherForm/TeacherForm';
import ImportTeachers from './ImportTeachers/ImportTeachers';
import { deleteTeacher, importTeachers } from 'actions/adminActions';
import { showError, showConfirmPromise } from 'actions/statusActions';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';
import { TEACHER } from 'consts/user-roles';

const rows = [
  {
    id: 'firstname',
    disablePadding: false,
    label: 'Firstname'
  },
  {
    id: 'lastname',
    disablePadding: false,
    label: 'Lastname'
  },
  {
    id: 'username',
    disablePadding: false,
    label: 'Email'
  }
];

class TeachersTab extends Component {
  state = {
    isOpenForm: false,
    isImportModalOpened: false,
    userToEdit: null
  };

  componentDidMount() {
    this.filterUsers();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.users !== this.props.users) {
      this.filterUsers();
    }
  }

  setImportModalOpened = isImportModalOpened => {
    this.setState({ isImportModalOpened });
  };

  filterUsers = () => {
    const { learningCenter, users } = this.props;

    const teachers = _.filter(users, {
      learningCenterId: learningCenter._id,
      roleAlias: TEACHER
    });
    this.setState({ teachers });
  };

  importHandler = async data => {
    const { importTeachers, learningCenter } = this.props;

    const formattedData = data.map(d => {
      const [firstname, lastname, username] = d;
      return {
        firstname: firstname.trim(),
        lastname: lastname.trim(),
        username: username.trim()
      };
    });

    await importTeachers({
      list: formattedData,
      learningCenterId: learningCenter._id
    });

    this.setState({ isImportModalOpened: false });
  };

  setUserToEdit = userToEdit => {
    this.setState({ userToEdit });
  };

  setOpenForm = isOpenForm => {
    this.setState({ isOpenForm });
  };

  render() {
    const { learningCenter, deleteTeacher } = this.props;
    const {
      isOpenForm,
      userToEdit,
      teachers,
      isImportModalOpened
    } = this.state;

    return (
      <div style={{ marginTop: '20px' }}>
        <span style={{ marginRight: '20px' }}>
          <Button
            color="primary"
            variant="outlined"
            onClick={() => this.setOpenForm(true)}
          >
            Create Teacher
          </Button>
        </span>

        <Button
          color="primary"
          variant="outlined"
          onClick={() => this.setImportModalOpened(true)}
        >
          Import Teachers
        </Button>

        <MaterialTable
          data={teachers || []}
          rows={rows}
          deleteHandler={deleteTeacher}
          editHandler={user => {
            this.setUserToEdit(user);
            this.setOpenForm(true);
          }}
          type="Teacher"
          rowWithName="username"
        />

        {isImportModalOpened && (
          <ImportTeachers
            isOpen={isImportModalOpened}
            importHandler={this.importHandler}
            closeHandler={() => {
              this.setImportModalOpened(false);
            }}
          />
        )}

        {isOpenForm && (
          <TeacherForm
            learningCenterId={learningCenter._id}
            onSuccess={() => this.setOpenForm(false)}
            isOpen={isOpenForm}
            initialValues={userToEdit}
            closeHandler={() => {
              this.setUserToEdit(null);
              this.setOpenForm(false);
            }}
          />
        )}
      </div>
    );
  }
}

TeachersTab.propTypes = {
  users: PropTypes.array.isRequired,
  auth: PropTypes.object.isRequired,
  deleteTeacher: PropTypes.func.isRequired,
  learningCenter: PropTypes.object.isRequired
};

const mapStateToProps = ({ auth }) => ({
  auth
});

export default compose(
  withData(['users']),
  connect(
    mapStateToProps,
    { deleteTeacher, showError, showConfirmPromise, importTeachers }
  )
)(TeachersTab);
