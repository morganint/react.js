import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { reduxFormValidator } from 'validation';
import { validateEmail } from 'validation';
import {
  StyledError,
  renderMaterialTextField
} from 'components/form-components';
import { createTeacher, editData } from 'actions/adminActions';

const requiredFields = ['username', 'firstname', 'lastname'];

const validate = reduxFormValidator(requiredFields);

const TeacherForm = ({
  learningCenterId,
  initialValues,
  closeHandler,
  isOpen,
  error,
  handleSubmit,
  pristine,
  submitting,
  onSuccess,
  editData,
  createTeacher
}) => {
  const submit = async (values, dispatch) => {
    const data = { ...values, learningCenterId };

    const result = initialValues
      ? await editData('users', data)
      : await createTeacher(data);

    if (!result.success) {
      throw new SubmissionError(result.err);
    } else {
      onSuccess && onSuccess();
    }
  };

  return (
    <div>
      <Dialog open={isOpen} onClose={closeHandler}>
        <form onSubmit={handleSubmit(submit)} style={{ width: '300px' }}>
          <DialogTitle>
            {initialValues ? 'Edit Teacher' : 'Create Teacher'}
          </DialogTitle>
          <DialogContent>
            <Field
              autoFocus
              name="username"
              disabled={!!initialValues}
              label="Email Address *"
              component={renderMaterialTextField}
              validate={validateEmail}
              fullWidth
              type="email"
            />
            <Field
              name="firstname"
              label="Firstname *"
              component={renderMaterialTextField}
              fullWidth
            />
            <Field
              name="lastname"
              label="Lastname *"
              component={renderMaterialTextField}
              fullWidth
            />

            {!pristine && error && <StyledError>{error}</StyledError>}
          </DialogContent>
          <DialogActions>
            <Button onClick={closeHandler} color="primary">
              Cancel
            </Button>
            <Button
              type="submit"
              color="primary"
              disabled={pristine || submitting}
            >
              {initialValues ? 'Edit' : 'Create'}
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

export default compose(
  reduxForm({
    form: 'TeacherForm',
    validate
  }),
  connect(
    null,
    { createTeacher, editData }
  )
)(TeacherForm);
