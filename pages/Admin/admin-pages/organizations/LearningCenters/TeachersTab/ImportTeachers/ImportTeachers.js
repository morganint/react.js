import React, { useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import validateEmail from 'validation/validate-email';
import { ImportCSV } from 'components/form-components';

const validateTeacher = data => {
  if (!data) return false;
  const [firstname, lastname, username] = data;

  return !(
    firstname &&
    firstname.trim() &&
    lastname &&
    lastname.trim() &&
    username &&
    username.trim()
  );
};

const validateTeachersCSV = data => {
  if (!data || !data.length) {
    return 'You trying to import empty CSV file!';
  }

  const notValidRows = data.filter(validateTeacher);

  if (notValidRows.length) {
    return 'All rows should contain 3 values: firstname,lastname,username';
  }

  // Check emails
  const invalidEmails = data.map(d => d[2]).filter(validateEmail);

  if (invalidEmails.length) {
    return `Following emails are invalid - ${invalidEmails.join(', ')}`;
  }

  const uniqRows = _.chain(data)
    .map(d => d[2])
    .uniq()
    .value();
  if (uniqRows.length !== data.length) {
    return 'Some of the rows have the same username';
  }
};

const ImportTeachers = ({ isOpen, closeHandler, importHandler }) => {
  const [teachersToImport, setTeachersToImport] = useState();

  return (
    <div>
      <Dialog open={isOpen} onClose={closeHandler}>
        <DialogTitle>Import Teachers</DialogTitle>
        <DialogContent>
          <p>Import CSV file in format: firstname,lastname,username.</p>
          <ImportCSV
            validate={validateTeachersCSV}
            label="CSV File:"
            callback={setTeachersToImport}
            parse
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={closeHandler} color="primary">
            Cancel
          </Button>
          <Button
            type="submit"
            color="primary"
            disabled={!teachersToImport}
            onClick={() => importHandler(teachersToImport)}
          >
            Import
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

ImportTeachers.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeHandler: PropTypes.func.isRequired,
  importHandler: PropTypes.func.isRequired
};

export default ImportTeachers;
