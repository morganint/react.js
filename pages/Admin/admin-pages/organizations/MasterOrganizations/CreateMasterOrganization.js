import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import MasterOrganizationDetailsForm from './MasterOrganizationDetailsForm/MasterOrganizationDetailsForm';
// import { checkPermissions } from 'utils';

class CreateMasterOrganization extends React.Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { value } = this.state;

    return (
      <div>
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="Details" />
        </Tabs>

        {value === 0 && (
          <MasterOrganizationDetailsForm
            initialValues={{
              // locales: ['en'],
              // defaultLocale: 'en',
              reportsRate: 'Monthly',
              organizationLabels: {
                masterOrganization: 'Master Organization',
                region: 'Region',
                learningCenter: 'Learning Center'
              }
            }}
            onSuccess={masterOrganizationId =>
              this.props.history.replace(
                `/admin/master-organizations/edit/${masterOrganizationId}`,
                {
                  tab: 'configurationTab'
                }
              )
            }
          />
        )}
      </div>
    );
  }
}

CreateMasterOrganization.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToPtops = state => ({ auth: state.auth });

export default compose(connect(mapStateToPtops))(CreateMasterOrganization);
