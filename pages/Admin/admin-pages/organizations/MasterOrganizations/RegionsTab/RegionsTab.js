import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import { AssignableProductsList, SelectOrganization } from 'components/admin';
import { AssignProductsField } from 'components/form-components';
import { withData } from 'components/hocs';
import {
  editData,
  getRegionPlansStatus,
  getMasterOrganizationPlansStatus
} from 'actions/adminActions';

const styles = theme => ({
  resetButton: {
    marginTop: '2rem',
    marginLeft: '1rem'
  },
  updateButton: {
    marginTop: '2rem',
    color: '#fff',
    borderColor: '#5cb85c',
    backgroundColor: '#5cb85c'
  }
});

class RegionsTab extends Component {
  state = {};

  componentDidMount() {
    this.getPlansStatus();
  }

  getPlansStatus = async () => {
    const {
      masterOrganization,
      regions,
      getMasterOrganizationPlansStatus
    } = this.props;
    const { selectedId } = this.state;

    const filteredRegions = _.filter(regions, {
      masterOrganizationId: masterOrganization._id
    });

    try {
      const plansStatus = await getMasterOrganizationPlansStatus(
        masterOrganization._id
      );

      if (!plansStatus) return;

      const maxValues = {};

      _.map(plansStatus.notAssignedPlans, (val, productId) => {
        const alreadyAssigned =
          plansStatus.assignedProductsToRegions[productId] || {};

        maxValues[productId] = {
          plans: (val.plans || 0) + (alreadyAssigned.plans || 0),
          addons: (val.addons || 0) + (alreadyAssigned.addons || 0)
        };
      });

      this.setState({
        filteredRegions,
        modified: false,
        plansStatus,
        selectedId:
          selectedId || (filteredRegions[0] && filteredRegions[0]._id),
        maxValues,
        availableProducts: plansStatus.notAssignedPlans
      });
    } catch (err) {
      console.log(err);
    }
  };

  getRegionPlansStatus = async () => {
    try {
      const regionPlansStatus = await this.props.getRegionPlansStatus(
        this.state.selectedId
      );

      this.setState({ regionPlansStatus });
    } catch (err) {
      this.setState({ regionPlansStatus: null });
    }
  };

  calculateAvailableProducts = () => {
    const { filteredRegions, maxValues } = this.state;

    const availableProducts = { ...maxValues };

    filteredRegions.forEach(region => {
      const { assignedProducts } = region;

      assignedProducts.forEach(({ productId, plans, addons }) => {
        if (!availableProducts[productId]) {
          availableProducts[productId] = { plans: 0, addons: 0 };
        }

        availableProducts[productId] = {
          plans: availableProducts[productId].plans - (plans || 0),
          addons: availableProducts[productId].addons - (addons || 0)
        };
      });
    });

    this.setState({ availableProducts });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.filteredRegions !== this.state.filteredRegions) {
      this.calculateAvailableProducts();
    }

    if (this.state.selectedId !== prevState.selectedId) {
      this.getRegionPlansStatus();
    }
  }

  onAssignHandler = val => {
    const { selectedId, filteredRegions } = this.state;

    const changedFilteredRegions = filteredRegions.map(region =>
      region._id === selectedId
        ? { ...region, assignedProducts: val, wasChanged: true }
        : region
    );

    console.log(changedFilteredRegions);

    this.setState({
      modified: true,
      filteredRegions: changedFilteredRegions
    });
  };

  updateHandler = () => {
    const { filteredRegions } = this.state;
    const { editData } = this.props;

    const changedRegions = filteredRegions.filter(region => region.wasChanged);

    Promise.all(changedRegions.map(region => editData('regions', region))).then(
      results => {
        console.log(results);
        this.getPlansStatus();
      }
    );
  };

  changeSelectedHandler = selectedId => {
    this.setState({ selectedId });
  };

  render() {
    const { classes, labels } = this.props;
    const {
      plansStatus,
      availableProducts,
      filteredRegions,
      selectedId,
      modified,
      regionPlansStatus
    } = this.state;

    if (!plansStatus) {
      return null;
    }

    if (!filteredRegions.length) {
      return <p>No regions</p>;
    }

    const selected = _.find(filteredRegions, { _id: selectedId });

    return (
      <div>
        <AssignableProductsList available={availableProducts} />

        {filteredRegions && (
          <Fragment>
            <SelectOrganization
              label={`Select ${labels.region || 'Region'}:`}
              editLink={'/admin/regions/edit'}
              selectedId={selectedId}
              organizations={filteredRegions}
              changeSelectedHandler={this.changeSelectedHandler}
            />

            {selected && (
              <Fragment>
                {regionPlansStatus && (
                  <AssignProductsField
                    dynamic
                    alreadyInUse={regionPlansStatus.assignedProducts}
                    id={selectedId}
                    canBeAssigned={availableProducts}
                    value={selected.assignedProducts}
                    onChange={this.onAssignHandler}
                  />
                )}
                <div>
                  <Button
                    className={classes.updateButton}
                    color="secondary"
                    variant="contained"
                    onClick={this.updateHandler}
                    disabled={!modified}
                  >
                    Update
                  </Button>

                  <Button
                    className={classes.resetButton}
                    color="primary"
                    variant="contained"
                    onClick={this.getPlansStatus}
                    disabled={!modified}
                  >
                    Reset
                  </Button>
                </div>
              </Fragment>
            )}
          </Fragment>
        )}
      </div>
    );
  }
}

RegionsTab.propTypes = {};

const mapStateToProps = state => ({});

export default compose(
  withData(['regions']),
  withStyles(styles),
  connect(
    mapStateToProps,
    { editData, getMasterOrganizationPlansStatus, getRegionPlansStatus }
  )
)(RegionsTab);
