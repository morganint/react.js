import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _ from 'lodash';

import Button from '@material-ui/core/Button';

import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { deleteData } from 'actions/adminActions';
import { NeedPermissions, getReps } from 'utils';
import { withData } from 'components/hocs';
import {
  REP,
  MASTER_ORGANIZATION_ADMIN,
  SUPER,
  MASTER_ADMIN
} from 'consts/user-roles';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Master Organization Name'
  },
  {
    id: 'repName',
    disablePadding: false,
    label: 'Rep'
  },
  {
    id: 'productsNames',
    disablePadding: false,
    label: 'Products Purchased',
    type: 'array'
  },
  {
    id: 'firstPurchaseDate',
    disablePadding: false,
    label: 'First purchase',
    type: 'date'
  }
];

export class MasterOrganizations extends Component {
  constructor(props) {
    super(props);

    // Determine if user role is REP
    const { auth } = props;
    const isRep = auth.userRole.alias === REP;

    this.state = {
      isRep,
      userId: auth.user._id
    };
  }

  componentDidMount() {
    const { auth, history, masterOrganizations } = this.props;

    const isMasterOrganizationAdmin =
      auth.userRole.alias === MASTER_ORGANIZATION_ADMIN;

    if (isMasterOrganizationAdmin) {
      const { masterOrganizationId } = auth.user;
      const isMasterOrganizationExist = _.find(masterOrganizations, {
        _id: masterOrganizationId
      });

      !!isMasterOrganizationExist
        ? history.replace(
            '/admin/master-organizations/edit/' + masterOrganizationId
          )
        : this.setState({
            error: 'Sorry, your master organization was deleted'
          });
    }
  }

  deleteDataHandler = (id, e) => {
    e.preventDefault();
    this.props.deleteData('masterOrganizations', id);
  };

  render() {
    const { masterOrganizations, products, users, auth } = this.props;
    const { userId, isRep, error } = this.state;

    const reps = getReps(users);

    let filteredMasterOrganizations;

    if (isRep) {
      filteredMasterOrganizations = masterOrganizations.filter(
        masterOrganization => masterOrganization.repId === userId
      );
    } else {
      filteredMasterOrganizations = masterOrganizations;
    }

    filteredMasterOrganizations = filteredMasterOrganizations.map(dist => {
      const productsNames = _.chain(dist.purchasedProducts)
        .map(({ productId }) => {
          const prod = _.filter(products, { _id: productId })[0];

          return prod && prod.name;
        })
        .filter(prod => !!prod)
        .value();

      const rep = _.find(reps, { _id: dist.repId });

      return {
        ...dist,
        productsNames,
        repName: rep && rep.name
      };
    });

    return error ? (
      error
    ) : (
      <div>
        <NeedPermissions permissions={[SUPER, MASTER_ADMIN]}>
          <Button
            color="primary"
            variant="outlined"
            component={Link}
            to="/admin/master-organizations/create"
          >
            Create Master Organization
          </Button>
        </NeedPermissions>

        {masterOrganizations && (
          <MaterialTable
            data={filteredMasterOrganizations}
            rows={rows}
            deleteHandler={
              auth.user.roleAlias === SUPER ||
              auth.user.roleAlias === MASTER_ADMIN
                ? this.deleteDataHandler
                : undefined
            }
            editLink="/admin/master-organizations/edit"
            type="Master Organization"
            rowWithName="name"
          />
        )}
      </div>
    );
  }
}

MasterOrganizations.propTypes = {
  auth: PropTypes.object.isRequired,
  masterOrganizations: PropTypes.array.isRequired,
  products: PropTypes.array.isRequired,
  users: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default compose(
  withData(['masterOrganizations', 'products', 'users']),
  connect(
    mapStateToProps,
    { deleteData }
  )
)(MasterOrganizations);
