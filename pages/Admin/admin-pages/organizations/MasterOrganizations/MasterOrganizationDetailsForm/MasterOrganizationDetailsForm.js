import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
  SubmissionError,
  change,
  formValueSelector
} from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import {
  addMasterOrganization,
  editData,
  getMasterOrganizationPlansStatus
} from 'actions/adminActions';
import { validateEmail, reduxFormValidator } from 'validation';
import {
  renderTextField,
  FieldSet,
  DateField,
  StyledError,
  StyledSuccess,
  renderSelect,
  RepField,
  ProductsField,
  FormButtons
} from 'components/form-components';
import { withData } from 'components/hocs';
import { FlexContainer } from 'components/common';
import {
  MASTER_ORGANIZATION_ADMIN,
  MASTER_ADMIN,
  SUPER
} from 'consts/user-roles';

const requiredFields = [
  'name',
  'contact[username]',
  'contact[firstname]',
  'contact[lastname]',
  'firstPurchaseDate',
  'products'
];

const validate = reduxFormValidator(requiredFields);

export class MasterOrganizationDetailsForm extends Component {
  state = { success: false };

  componentDidMount() {
    const { isEdit } = this.props;

    if (isEdit) {
      this.filterMasterOrganizationAdmins();
      this.getMasterOrganizationPlansStatus();
    }
  }

  submit = async (values, dispatch) => {
    const { isEdit, editData, addMasterOrganization, onSuccess } = this.props;

    const formattedValues = { ...values };

    if (isEdit && !formattedValues.contactUserId) {
      throw new SubmissionError({
        contactUserId: 'Required'
      });
    }

    // At least one product must be filled
    const filledProducts = _.filter(
      values.purchasedProducts,
      item => !!item.plans
    );

    if (!filledProducts.length) {
      throw new SubmissionError({
        _error:
          '* - Master Organization must have plans for at least one product!'
      });
    }

    console.log('Values for send:', formattedValues);

    const result = isEdit
      ? await editData('masterOrganizations', formattedValues)
      : await addMasterOrganization('masterOrganizations', formattedValues);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      // dispatch(reset('MasterOrganizationDetailsForm'));
      // this.setState({ success: true });
      // onSuccess && onSuccess();
      const masterOrganizationId =
        result.data &&
        result.data.masterOrganization &&
        result.data.masterOrganization._id;
      masterOrganizationId && onSuccess && onSuccess(masterOrganizationId);
    }
  };

  getMasterOrganizationPlansStatus = async () => {
    const {
      initialValues: { _id },
      getMasterOrganizationPlansStatus
    } = this.props;

    if (!_id) {
      return;
    }

    try {
      const plansStatus = await getMasterOrganizationPlansStatus(_id);

      this.setState({ plansStatus });
    } catch (err) {
      console.log(err);
      this.setState({ plansStatus: null });
    }
  };

  filterMasterOrganizationAdmins = () => {
    const { users, initialValues } = this.props;

    const masterOrganizationAdmins = _.filter(users, {
      roleAlias: MASTER_ORGANIZATION_ADMIN,
      masterOrganizationId: initialValues._id
    });

    const contactUsersOptions = masterOrganizationAdmins.map(user => ({
      label: `${user.firstname} ${user.lastname}`,
      value: user._id
    }));

    this.setState({ contactUsersOptions });
  };

  render() {
    const {
      userRole,
      error,
      purchasedProducts,
      handleSubmit,
      products,
      pristine,
      submitting,
      isEdit,
      reset,
      initialValues,
      change,
      contactUserId,
      users
    } = this.props;

    const { success, contactUsersOptions, plansStatus } = this.state;

    const contactUser = isEdit && _.find(users, { _id: contactUserId });

    const labels = (initialValues && initialValues.organizationLabels) || {};

    const successMessage = isEdit
      ? `${labels.masterOrganization ||
          'Master Organization'} updated successfully`
      : `${labels.masterOrganization || 'Master Organization'} created`;

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        {userRole !== MASTER_ORGANIZATION_ADMIN && (
          <FieldSet>
            <FlexContainer xs="50%" md="33%">
              <Field
                name="organizationLabels[masterOrganization]"
                label={`${labels.masterOrganization ||
                  'Master Organization'} Label`}
                component={renderTextField}
              />
              <Field
                name="organizationLabels[region]"
                label={`${labels.region || 'Region'} Label`}
                component={renderTextField}
              />
              <Field
                name="organizationLabels[learningCenter]"
                label={`${labels.learningCenter || 'Learning Center'} Label`}
                component={renderTextField}
              />
            </FlexContainer>
          </FieldSet>
        )}

        <FieldSet>
          <Field
            name="name"
            label={
              ((initialValues &&
                initialValues.organizationLabels &&
                initialValues.organizationLabels.masterOrganization) ||
                'Master Organization') + ' Name *'
            }
            component={renderTextField}
          />

          {isEdit ? (
            <FlexContainer xs="100%" md="50%">
              <Field
                name="contactUserId"
                label="Contact User"
                component={renderSelect}
                options={contactUsersOptions || []}
              />

              {contactUser && (
                <div style={{ marginBottom: '30px', marginTop: '10px' }}>
                  <p>
                    <b>Firstname: </b>
                    {contactUser.firstname}
                  </p>
                  <p>
                    <b>Lastname: </b>
                    {contactUser.lastname}
                  </p>
                  <p>
                    <b>Email: </b>
                    {contactUser.username}
                  </p>
                  <p>
                    <b>Phone: </b>
                    {contactUser.phone}
                  </p>
                </div>
              )}
            </FlexContainer>
          ) : (
            <Fragment>
              <FlexContainer xs="100%" md="50%">
                <Field
                  name="contact[firstname]"
                  label="Contact Firstname *"
                  component={renderTextField}
                />
                <Field
                  name="contact[lastname]"
                  label="Contact Lastname *"
                  component={renderTextField}
                />
              </FlexContainer>
              <FlexContainer xs="100%" md="50%">
                <Field
                  name="contact[username]"
                  label="Contact Email *"
                  component={renderTextField}
                  validate={validateEmail}
                />
                <Field
                  name="contact[phone]"
                  label="Contact Phone"
                  component={renderTextField}
                />
              </FlexContainer>
            </Fragment>
          )}

          <DateField name="firstPurchaseDate" label="First Purchase Date *" />
        </FieldSet>

        {(!isEdit || plansStatus) && (
          <FieldSet title="Products *">
            <ProductsField
              disabled={userRole === MASTER_ORGANIZATION_ADMIN}
              products={products}
              value={purchasedProducts}
              assignedProducts={plansStatus && plansStatus.assignedProducts}
              usingPlans={plansStatus && plansStatus.usingPlans}
              onChange={val =>
                change(
                  'MasterOrganizationDetailsForm',
                  'purchasedProducts',
                  val
                )
              }
            />
          </FieldSet>
        )}

        {(userRole === MASTER_ADMIN || userRole === SUPER) && (
          <RepField form="MasterOrganizationDetailsForm" />
        )}

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="MasterOrganization"
          handleReset={reset}
        />
      </form>
    );
  }
}

MasterOrganizationDetailsForm.propTypes = {
  purchasedProducts: PropTypes.array,
  products: PropTypes.array.isRequired,
  isEdit: PropTypes.bool,
  onSuccess: PropTypes.func,
  initialValues: PropTypes.object,
  addMasterOrganization: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired
};

const selector = formValueSelector('MasterOrganizationDetailsForm');

const mapStateToProps = state => ({
  userRole: state.auth.user.roleAlias,
  purchasedProducts: selector(state, 'purchasedProducts'),
  contactUserId: selector(state, 'contactUserId')
});

export default compose(
  withData(['products', 'users']),
  reduxForm({
    form: 'MasterOrganizationDetailsForm',
    validate,
    enableReinitialize: true,
    initialValues: {}
  }),
  connect(
    mapStateToProps,
    {
      addMasterOrganization,
      editData,
      change,
      getMasterOrganizationPlansStatus
    }
  )
)(MasterOrganizationDetailsForm);
