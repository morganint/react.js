import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import { AssignableProductsList, SelectOrganization } from 'components/admin';
import { AssignProductsField } from 'components/form-components';
import { withData } from 'components/hocs';
import {
  editData,
  getMasterOrganizationPlansStatus,
  getRegionPlansStatus,
  getLearningCenterPlansStatus
} from 'actions/adminActions';
import { showInfo } from 'actions/statusActions';

const styles = theme => ({
  resetButton: {
    marginTop: '2rem',
    marginLeft: '1rem'
  },
  updateButton: {
    marginTop: '2rem',
    color: '#fff',
    borderColor: '#5cb85c',
    backgroundColor: '#5cb85c'
  }
});

class LearningCentersTab extends Component {
  state = {};

  componentDidMount() {
    this.getPlansStatus();

    const { regions, masterOrganization } = this.props;

    const filteredRegions = _.filter(regions, {
      masterOrganizationId: masterOrganization._id
    });

    this.setState({ filteredRegions });
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.filteredLearningCenters !== this.state.filteredLearningCenters
    ) {
      this.calculateAvailableProducts();
    }

    const { selectedRegionId, selectedId } = this.state;

    if (prevState.selectedRegionId !== selectedRegionId) {
      this.getPlansStatus();
    }

    if (prevState.selectedId !== selectedId) {
      this.getLearningCenterPlans();
    }
  }

  getPlansStatus = async () => {
    const {
      masterOrganization,
      learningCenters,
      getRegionPlansStatus,
      getMasterOrganizationPlansStatus
    } = this.props;
    const { selectedId, selectedRegionId } = this.state;

    const filteredLearningCenters = _.filter(learningCenters, {
      masterOrganizationId: masterOrganization._id,
      regionId: selectedRegionId || null
    });

    const selectedLearningCenter =
      (selectedId && _.find(filteredLearningCenters, { _id: selectedId })) ||
      filteredLearningCenters[0];

    try {
      const plansStatus = selectedRegionId
        ? await getRegionPlansStatus(selectedRegionId)
        : await getMasterOrganizationPlansStatus(masterOrganization._id);

      const maxValues = {};

      if (plansStatus) {
        _.map(plansStatus.notAssignedPlans, (val, productId) => {
          const alreadyAssigned =
            (selectedRegionId
              ? plansStatus.assignedProducts[productId]
              : plansStatus.assignedProductsToLearningCenters[productId]) || {};

          maxValues[productId] = {
            plans: (val.plans || 0) + (alreadyAssigned.plans || 0),
            addons: (val.addons || 0) + (alreadyAssigned.addons || 0)
          };
        });
      }

      this.setState({
        filteredLearningCenters,
        modified: false,
        plansStatus,
        selectedId: selectedLearningCenter && selectedLearningCenter._id,
        maxValues,
        availableProducts: plansStatus.notAssignedPlans
      });
    } catch (err) {
      console.log(err);
    }
  };

  getLearningCenterPlans = async () => {
    const { getLearningCenterPlansStatus } = this.props;
    const { selectedId } = this.state;

    const learningCenterPlansStatus = await getLearningCenterPlansStatus(
      selectedId
    );

    this.setState({ learningCenterPlansStatus });
  };

  calculateAvailableProducts = () => {
    const { filteredLearningCenters, maxValues, selectedId } = this.state;

    const selectedLearningCenter = _.find(filteredLearningCenters, {
      _id: selectedId
    });

    if (!selectedLearningCenter) return;

    const isBelongToRegion = !!selectedLearningCenter.regionId;

    const availableProducts = { ...maxValues };

    filteredLearningCenters.forEach(learningCenter => {
      if (isBelongToRegion !== !!learningCenter.regionId) return;

      const { assignedProducts } = learningCenter;

      assignedProducts.forEach(({ productId, plans, addons }) => {
        if (!availableProducts[productId]) {
          availableProducts[productId] = { plans: 0, addons: 0 };
        }

        availableProducts[productId] = {
          plans: availableProducts[productId].plans - (plans || 0),
          addons: availableProducts[productId].addons - (addons || 0)
        };
      });
    });

    this.setState({ availableProducts });
  };

  onAssignHandler = val => {
    const { selectedId, filteredLearningCenters } = this.state;

    const changedFilteredLearningCenters = filteredLearningCenters.map(
      learningCenter =>
        learningCenter._id === selectedId
          ? { ...learningCenter, assignedProducts: val, wasChanged: true }
          : learningCenter
    );

    console.log(changedFilteredLearningCenters);

    this.setState({
      modified: true,
      filteredLearningCenters: changedFilteredLearningCenters
    });
  };

  updateHandler = () => {
    const { filteredLearningCenters } = this.state;
    const { editData } = this.props;

    const changedLearningCenters = filteredLearningCenters.filter(
      learningCenter => learningCenter.wasChanged
    );

    Promise.all(
      changedLearningCenters.map(learningCenter =>
        editData('learningCenters', learningCenter)
      )
    ).then(results => {
      console.log(results);
      this.getPlansStatus();
    });
  };

  changeSelectedHandler = selectedId => {
    this.setState({ selectedId });
  };

  changeSelectedRegionHandler = selectedRegionId => {
    const { modified } = this.state;

    if (modified) {
      return this.props.showInfo({
        message: 'Update or reset changes before switching region'
      });
    }

    this.setState({ selectedRegionId });
  };

  render() {
    const { classes, labels } = this.props;
    const {
      plansStatus,
      learningCenterPlansStatus,
      availableProducts,
      filteredLearningCenters,
      filteredRegions,
      selectedRegionId,
      selectedId,
      modified
    } = this.state;

    if (!plansStatus) {
      return null;
    }

    const selected = _.find(filteredLearningCenters, { _id: selectedId });
    return (
      <div>
        {filteredRegions && !!filteredRegions.length && (
          <SelectOrganization
            label={`Select ${labels.region || 'Region'}:`}
            emptyValue="No region"
            editLink={'/admin/regions/edit'}
            selectedId={selectedRegionId}
            organizations={filteredRegions}
            changeSelectedHandler={this.changeSelectedRegionHandler}
          />
        )}

        <AssignableProductsList available={availableProducts} />

        {filteredLearningCenters && (
          <Fragment>
            <SelectOrganization
              label={`Select ${labels.learningCenter || 'Learning Center'}:`}
              editLink={'/admin/learning-centers/edit'}
              selectedId={selectedId}
              organizations={filteredLearningCenters}
              changeSelectedHandler={this.changeSelectedHandler}
            />

            {selected && learningCenterPlansStatus && (
              <Fragment>
                <AssignProductsField
                  dynamic
                  alreadyInUse={learningCenterPlansStatus.assignedProducts}
                  id={selectedId}
                  withoutAvailable
                  canBeAssigned={availableProducts}
                  value={selected.assignedProducts}
                  onChange={this.onAssignHandler}
                />
                <div>
                  <Button
                    className={classes.updateButton}
                    color="secondary"
                    variant="contained"
                    onClick={this.updateHandler}
                    disabled={!modified}
                  >
                    Update
                  </Button>

                  <Button
                    className={classes.resetButton}
                    color="primary"
                    variant="contained"
                    onClick={this.getPlansStatus}
                    disabled={!modified}
                  >
                    Reset
                  </Button>
                </div>
              </Fragment>
            )}
          </Fragment>
        )}
      </div>
    );
  }
}

LearningCentersTab.propTypes = {};

const mapStateToProps = state => ({});

export default compose(
  withData(['learningCenters', 'regions']),
  withStyles(styles),
  connect(
    mapStateToProps,
    {
      editData,
      getMasterOrganizationPlansStatus,
      getRegionPlansStatus,
      getLearningCenterPlansStatus,
      showInfo
    }
  )
)(LearningCentersTab);
