import React, { Fragment, Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { compose } from "redux";
import PropTypes from "prop-types";
import { find } from "lodash";

import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import { isEmpty } from "validation";
import {
  formatCheckboxesFromArray,
  OrganizationConfigForm
} from "components/form-components";
import MasterOrganizationDetailsForm from "./MasterOrganizationDetailsForm/MasterOrganizationDetailsForm";

import MembersTab from "components/admin/MembersTab/MembersTab";
import RegionsTab from "./RegionsTab/RegionsTab";
import LearningCentersTab from "./LearningCentersTab/LearningCentersTab";
// import { checkPermissions } from 'utils';
import { withData } from "components/hocs";
import { changePageTitle } from "actions/statusActions";
import { MASTER_ORGANIZATION_ADMIN } from "consts/user-roles";

class EditMasterOrganization extends Component {
  constructor(props) {
    super(props);

    const {
      history: {
        location: { state: locationState = {} }
      }
    } = props;

    this.state = {
      value: locationState.tab ? locationState.tab : "detailsTab"
    };
  }

  componentDidMount() {
    this.findDataToEdit();
    this.setState({ didMount: true });
  }

  componentDidUpdate(prevProps) {
    const { masterOrganizations } = this.props;

    if (prevProps.masterOrganizations !== masterOrganizations) {
      this.findDataToEdit();
    }
  }

  findDataToEdit = () => {
    const { match, masterOrganizations, changePageTitle } = this.props;

    const masterOrganization = find(masterOrganizations, {
      _id: match.params.id
    });

    if (masterOrganization && masterOrganization.organizationLabels) {
      const { organizationLabels } = masterOrganization;

      organizationLabels &&
        organizationLabels.masterOrganization &&
        changePageTitle("Edit " + organizationLabels.masterOrganization);
    }

    this.setState({ masterOrganization });
  };

  handleChange = (event, value) => {
    if (this.state.value === value) {
      return;
    }

    const { history } = this.props;

    const { state = {} } = history.location;

    history.replace(history.location.pathname, { ...state, tab: value });
    this.setState({ value });
  };

  render() {
    const { value, masterOrganization, didMount } = this.state;
    const { locales, lists } = this.props;

    const labels =
      (masterOrganization && masterOrganization.organizationLabels) || {};

    const initialValues = masterOrganization && {
      ...masterOrganization,
      config: masterOrganization.config && {
        ...masterOrganization.config,
        locales: isEmpty(masterOrganization.config.locales)
          ? undefined
          : formatCheckboxesFromArray(masterOrganization.config.locales)
      }
    };

    return (
      <Fragment>
        {masterOrganization ? (
          <Fragment>
            <Tabs
              value={value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
            >
              <Tab label="Details" value="detailsTab" />
              <Tab label="Configuration" value="configurationTab" />
              <Tab label="Members" value="membersTab" />
              <Tab
                label={(labels.region || "Region") + "s"}
                value="regionsTab"
              />
              <Tab
                label={(labels.learningCenter || "Learning Center") + "s"}
                value="learningCentersTab"
              />
            </Tabs>

            <div style={{ marginTop: "20px" }}>
              {value === "detailsTab" && (
                <MasterOrganizationDetailsForm
                  isEdit
                  initialValues={masterOrganization}
                />
              )}
              {value === "configurationTab" && (
                <OrganizationConfigForm
                  type="masterOrganizations"
                  initialValues={initialValues}
                  locales={locales}
                  lists={lists}
                />
              )}
              {value === "membersTab" && (
                <MembersTab
                  memberTitle={labels.masterOrganization + " Admin"}
                  organization={masterOrganization}
                  membersRole={MASTER_ORGANIZATION_ADMIN}
                  assign={{
                    prop: "masterOrganizationId",
                    value: masterOrganization._id
                  }}
                />
              )}
              {value === "regionsTab" && (
                <RegionsTab
                  labels={labels}
                  masterOrganization={masterOrganization}
                />
              )}
              {value === "learningCentersTab" && (
                <LearningCentersTab
                  labels={labels}
                  masterOrganization={masterOrganization}
                />
              )}
            </div>
          </Fragment>
        ) : !didMount ? (
          <p>Loading</p>
        ) : (
          <Redirect to="/admin/master-organizations" />
        )}
      </Fragment>
    );
  }
}

EditMasterOrganization.propTypes = {
  auth: PropTypes.object.isRequired,
  masterOrganizations: PropTypes.array.isRequired,
  lists: PropTypes.array.isRequired,
  locales: PropTypes.array.isRequired,
  changePageTitle: PropTypes.func.isRequired,
  fetchData: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth }) => ({
  auth
});

export default compose(
  withData(["masterOrganizations", "locales", "lists"]),
  connect(mapStateToProps, { changePageTitle })
)(EditMasterOrganization);
