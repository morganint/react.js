import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import RegionDetailsForm from './RegionDetailsForm/RegionDetailsForm';

class CreateRegion extends React.Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { value } = this.state;

    return (
      <div>
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="Details" />
        </Tabs>

        {value === 0 && (
          <RegionDetailsForm
            onSuccess={regionId =>
              this.props.history.replace(`/admin/regions/edit/${regionId}`, {
                tab: 'membersTab'
              })
            }
          />
        )}
      </div>
    );
  }
}

CreateRegion.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToPtops = state => ({ auth: state.auth });

export default compose(connect(mapStateToPtops))(CreateRegion);
