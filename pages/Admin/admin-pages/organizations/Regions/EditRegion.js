import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import RegionDetailsForm from './RegionDetailsForm/RegionDetailsForm';
import LearningCentersTab from './LearningCentersTab/LearningCentersTab';
import MembersTab from 'components/admin/MembersTab/MembersTab';
import { getMasterOrganizationById } from 'actions/adminActions';
import { changePageTitle } from 'actions/statusActions';
import { withData } from 'components/hocs';
import { REGION_ADMIN } from 'consts/user-roles';

class EditRegion extends Component {
  constructor(props) {
    super(props);

    const {
      history: {
        location: { state: locationState = {} }
      }
    } = props;

    this.state = {
      value: locationState.tab ? locationState.tab : 'detailsTab'
    };
  }

  async componentDidMount() {
    this.findDataToEdit();
    this.setState({ didMount: true });
  }

  componentDidUpdate(prevProps, prevState) {
    const { regions } = this.props;

    if (prevProps.regions !== regions) {
      this.findDataToEdit();
    }

    if (this.state.region) {
      if (
        (prevState.region && prevState.region._id) !== this.state.region._id
      ) {
        this.getMasterOrganization();
      }
    }
  }

  getMasterOrganization = async () => {
    const { getMasterOrganizationById, changePageTitle } = this.props;
    const { region } = this.state;

    const masterOrganization = await getMasterOrganizationById(
      region.masterOrganizationId
    );

    if (!masterOrganization) {
      return console.error('Master Organization Not Found');
    }

    const { organizationLabels } = masterOrganization;

    if (!organizationLabels || !organizationLabels.region) return;

    this.setState({ masterOrganization });

    changePageTitle('Edit ' + organizationLabels.region);
  };

  findDataToEdit = () => {
    const { match, regions } = this.props;

    const region = find(regions, {
      _id: match.params.id
    });

    this.setState({ region });
  };

  handleChange = (event, value) => {
    if (this.state.value === value) {
      return;
    }

    const { history } = this.props;

    const { state = {} } = history.location;

    history.replace(history.location.pathname, { ...state, tab: value });
    this.setState({ value });
  };

  render() {
    const { value, region, didMount, masterOrganization } = this.state;

    const labels =
      (masterOrganization && masterOrganization.organizationLabels) || {};

    return (
      <Fragment>
        {region ? (
          <Fragment>
            <Tabs
              value={value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
            >
              <Tab label="Details" value="detailsTab" />
              <Tab label="Members" value="membersTab" />
              <Tab
                label={(labels.learningCenter || 'Learning Center') + 's'}
                value="learningCentersTab"
              />
            </Tabs>

            {value === 'detailsTab' && (
              <RegionDetailsForm isEdit initialValues={region} />
            )}
            {value === 'membersTab' && (
              <MembersTab
                memberTitle={labels.region + ' Admin'}
                organization={region}
                membersRole={REGION_ADMIN}
                assign={{
                  prop: 'regionId',
                  value: region._id
                }}
              />
            )}
            {value === 'learningCentersTab' && (
              <LearningCentersTab labels={labels} region={region} />
            )}
          </Fragment>
        ) : !didMount ? (
          <p>Loading...</p>
        ) : (
          <Redirect to="/admin/regions" />
        )}
      </Fragment>
    );
  }
}

EditRegion.propTypes = {
  auth: PropTypes.object.isRequired,
  regions: PropTypes.array.isRequired,
  fetchData: PropTypes.func.isRequired,
  getMasterOrganizationById: PropTypes.func.isRequired,
  changePageTitle: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth }) => ({
  auth
});

export default compose(
  withData(['regions']),
  connect(
    mapStateToProps,
    { getMasterOrganizationById, changePageTitle }
  )
)(EditRegion);
