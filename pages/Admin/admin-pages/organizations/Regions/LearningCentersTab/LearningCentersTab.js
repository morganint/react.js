import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import { AssignableProductsList, SelectOrganization } from 'components/admin';
import { AssignProductsField } from 'components/form-components';
import { withData } from 'components/hocs';
import {
  editData,
  getLearningCenterPlansStatus,
  getRegionPlansStatus
} from 'actions/adminActions';

const styles = theme => ({
  resetButton: {
    marginTop: '2rem',
    marginLeft: '1rem'
  },
  updateButton: {
    marginTop: '2rem',
    color: '#fff',
    borderColor: '#5cb85c',
    backgroundColor: '#5cb85c'
  }
});

class LearningCenterTab extends Component {
  state = {};

  componentDidMount() {
    this.getPlansStatus();
  }

  getPlansStatus = async () => {
    const { region, learningCenters, getRegionPlansStatus } = this.props;
    const { selectedId } = this.state;

    const filteredLearningCenters = _.filter(learningCenters, {
      regionId: region._id
    });

    try {
      const plansStatus = await getRegionPlansStatus(region._id);

      if (!plansStatus) return;

      const maxValues = {};

      _.map(plansStatus.notAssignedPlans, (val, productId) => {
        const alreadyAssigned = plansStatus.assignedProducts[productId] || {};

        maxValues[productId] = {
          plans: (val.plans || 0) + (alreadyAssigned.plans || 0),
          addons: (val.addons || 0) + (alreadyAssigned.addons || 0)
        };
      });

      this.setState({
        filteredLearningCenters,
        modified: false,
        plansStatus,
        selectedId:
          selectedId ||
          (filteredLearningCenters[0] && filteredLearningCenters[0]._id),
        maxValues,
        availableProducts: plansStatus.notAssignedPlans
      });
    } catch (err) {
      console.log(err);
    }
  };

  getLearningCenterPlansStatus = async () => {
    try {
      const learningCenterPlansStatus = await this.props.getLearningCenterPlansStatus(
        this.state.selectedId
      );

      this.setState({ learningCenterPlansStatus });
    } catch (err) {
      this.setState({ learningCenterPlansStatus: null });
    }
  };

  calculateAvailableProducts = () => {
    const { filteredLearningCenters, maxValues } = this.state;

    const availableProducts = { ...maxValues };

    filteredLearningCenters.forEach(region => {
      const { assignedProducts } = region;

      assignedProducts.forEach(({ productId, plans, addons }) => {
        if (!availableProducts[productId]) {
          availableProducts[productId] = { plans: 0, addons: 0 };
        }

        availableProducts[productId] = {
          plans: availableProducts[productId].plans - (plans || 0),
          addons: availableProducts[productId].addons - (addons || 0)
        };
      });
    });

    this.setState({ availableProducts });
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.filteredLearningCenters !== this.state.filteredLearningCenters
    ) {
      this.calculateAvailableProducts();
    }

    if (this.state.selectedId !== prevState.selectedId) {
      this.getLearningCenterPlansStatus();
    }
  }

  onAssignHandler = val => {
    const { selectedId, filteredLearningCenters } = this.state;

    const changedFilteredLearningCenters = filteredLearningCenters.map(region =>
      region._id === selectedId
        ? { ...region, assignedProducts: val, wasChanged: true }
        : region
    );

    this.setState({
      modified: true,
      filteredLearningCenters: changedFilteredLearningCenters
    });
  };

  updateHandler = () => {
    const { filteredLearningCenters } = this.state;
    const { editData } = this.props;

    const changedLearningCenters = filteredLearningCenters.filter(
      learningCenter => learningCenter.wasChanged
    );

    Promise.all(
      changedLearningCenters.map(learningCenter =>
        editData('learningCenters', learningCenter)
      )
    ).then(results => {
      console.log(results);
      this.getPlansStatus();
    });
  };

  changeSelectedHandler = selectedId => {
    this.setState({ selectedId });
  };

  render() {
    const { classes, labels } = this.props;
    const {
      plansStatus,
      availableProducts,
      filteredLearningCenters,
      selectedId,
      modified,
      learningCenterPlansStatus
    } = this.state;

    if (!plansStatus) {
      return null;
    }

    if (!filteredLearningCenters.length) {
      return <p>No regions</p>;
    }

    const selected = _.find(filteredLearningCenters, { _id: selectedId });

    return (
      <div>
        <AssignableProductsList available={availableProducts} />

        {filteredLearningCenters && (
          <Fragment>
            <SelectOrganization
              label={`Select ${labels.learningCenter || 'Learning Center'}:`}
              editLink={'/admin/learning-centers/edit'}
              selectedId={selectedId}
              organizations={filteredLearningCenters}
              changeSelectedHandler={this.changeSelectedHandler}
            />

            {selected && (
              <Fragment>
                {learningCenterPlansStatus && (
                  <AssignProductsField
                    dynamic
                    alreadyInUse={learningCenterPlansStatus.assignedProducts}
                    id={selectedId}
                    canBeAssigned={availableProducts}
                    value={selected.assignedProducts}
                    onChange={this.onAssignHandler}
                  />
                )}
                <div>
                  <Button
                    className={classes.updateButton}
                    color="secondary"
                    variant="contained"
                    onClick={this.updateHandler}
                    disabled={!modified}
                  >
                    Update
                  </Button>

                  <Button
                    className={classes.resetButton}
                    color="primary"
                    variant="contained"
                    onClick={this.getPlansStatus}
                    disabled={!modified}
                  >
                    Reset
                  </Button>
                </div>
              </Fragment>
            )}
          </Fragment>
        )}
      </div>
    );
  }
}

LearningCenterTab.propTypes = {
  region: PropTypes.object.isRequired,
  learningCenters: PropTypes.array.isRequired,
  editData: PropTypes.func.isRequired,
  getRegionPlansStatus: PropTypes.func.isRequired,
  getLearningCenterPlansStatus: PropTypes.func.isRequired
};

export default compose(
  withData(['learningCenters']),
  withStyles(styles),
  connect(
    null,
    { editData, getRegionPlansStatus, getLearningCenterPlansStatus }
  )
)(LearningCenterTab);
