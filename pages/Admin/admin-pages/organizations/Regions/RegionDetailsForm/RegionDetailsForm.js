import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
  SubmissionError,
  change,
  formValueSelector
} from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import { addRegion, editData } from 'actions/adminActions';
import { validateEmail, reduxFormValidator } from 'validation';
import {
  renderTextField,
  FieldSet,
  StyledError,
  StyledSuccess,
  renderSelect,
  FormButtons
} from 'components/form-components';
import { withData } from 'components/hocs';
import { FlexContainer } from 'components/common';
import { REGION_ADMIN } from 'consts/user-roles';

const requiredFields = [
  'name',
  'contact[username]',
  'contact[firstname]',
  'contact[lastname]',
  'masterOrganizationId'
];

const validate = reduxFormValidator(requiredFields);

export class RegionDetailsForm extends Component {
  state = { success: false };

  componentDidMount() {
    const { isEdit } = this.props;

    if (isEdit) {
      this.filterRegionAdmins();
    }
  }

  submit = async (values, dispatch) => {
    const { isEdit, editData, addRegion, onSuccess } = this.props;

    const formattedValues = { ...values };

    if (isEdit && !formattedValues.contactUserId) {
      throw new SubmissionError({
        contactUserId: 'Required'
      });
    }

    const result = isEdit
      ? await editData('regions', formattedValues)
      : await addRegion('regions', formattedValues);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      const regionId =
        result.data && result.data.region && result.data.region._id;
      regionId && onSuccess && onSuccess(regionId);
    }
  };

  filterRegionAdmins = () => {
    const { users, initialValues } = this.props;

    const regionAdmins = _.filter(users, {
      roleAlias: REGION_ADMIN,
      regionId: initialValues._id
    });

    const contactUsersOptions = regionAdmins.map(user => ({
      label: `${user.firstname} ${user.lastname}`,
      value: user._id
    }));

    this.setState({ contactUsersOptions });
  };

  render() {
    const {
      error,
      masterOrganizations,
      handleSubmit,
      pristine,
      submitting,
      isEdit,
      reset,
      change,
      contactUserId,
      users,
      selectedMasterOrganizationId
    } = this.props;

    const { success, contactUsersOptions } = this.state;

    const contactUser = isEdit && _.find(users, { _id: contactUserId });

    const selectedMasterOrganization = _.find(masterOrganizations, {
      _id: selectedMasterOrganizationId
    });

    const labels =
      (selectedMasterOrganization &&
        selectedMasterOrganization.organizationLabels) ||
      {};

    const masterOrganizationOptions = masterOrganizations.map(org => ({
      label: org.name,
      value: org._id
    }));

    const successMessage = isEdit
      ? `${labels.region || 'Region'} updated successfully`
      : `${labels.region || 'Region'}Region created`;

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <FieldSet>
          <Field
            name="name"
            label={`${labels.region || 'Region'} Name *`}
            component={renderTextField}
          />

          {isEdit ? (
            <FlexContainer xs="100%" md="50%">
              <Field
                name="contactUserId"
                label="Contact User"
                component={renderSelect}
                options={contactUsersOptions || []}
              />

              {contactUser && (
                <div style={{ marginBottom: '30px', marginTop: '10px' }}>
                  <p>
                    <b>Firstname: </b>
                    {contactUser.firstname}
                  </p>
                  <p>
                    <b>Lastname: </b>
                    {contactUser.lastname}
                  </p>
                  <p>
                    <b>Email: </b>
                    {contactUser.username}
                  </p>
                  <p>
                    <b>Phone: </b>
                    {contactUser.phone}
                  </p>
                </div>
              )}
            </FlexContainer>
          ) : (
            <Fragment>
              <FlexContainer xs="100%" md="50%">
                <Field
                  name="contact[firstname]"
                  label="Contact Firstname *"
                  component={renderTextField}
                />
                <Field
                  name="contact[lastname]"
                  label="Contact Lastname *"
                  component={renderTextField}
                />
              </FlexContainer>
              <FlexContainer xs="100%" md="50%">
                <Field
                  name="contact[username]"
                  label="Contact Email *"
                  component={renderTextField}
                  validate={validateEmail}
                />
                <Field
                  name="contact[phone]"
                  label="Contact Phone"
                  component={renderTextField}
                />
              </FlexContainer>
            </Fragment>
          )}
        </FieldSet>

        <Field
          name="masterOrganizationId"
          label={labels.masterOrganization || 'Master Organization *'}
          component={renderSelect}
          disabled={isEdit && selectedMasterOrganizationId}
          onChange={(...args) => {
            change('LearningCenterDetailsForm', 'assignedProducts', []);
          }}
          options={masterOrganizationOptions || []}
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="Region"
          handleReset={reset}
        />
      </form>
    );
  }
}

RegionDetailsForm.propTypes = {
  assignedProducts: PropTypes.array,
  masterOrganizations: PropTypes.array,
  isEdit: PropTypes.bool,
  onSuccess: PropTypes.func,
  initialValues: PropTypes.object,
  addRegion: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired
};

const selector = formValueSelector('RegionDetailsForm');

const mapStateToProps = state => ({
  userRole: state.auth.user.roleAlias,
  contactUserId: selector(state, 'contactUserId'),
  selectedMasterOrganizationId: selector(state, 'masterOrganizationId')
});

export default compose(
  withData(['users', 'masterOrganizations']),
  reduxForm({
    form: 'RegionDetailsForm',
    validate,
    enableReinitialize: true,
    initialValues: {}
  }),
  connect(
    mapStateToProps,
    {
      addRegion,
      editData,
      change
    }
  )
)(RegionDetailsForm);
