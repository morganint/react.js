import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _ from 'lodash';

import Button from '@material-ui/core/Button';

import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { deleteData } from 'actions/adminActions';
import { NeedPermissions } from 'utils';
import { withData } from 'components/hocs';
import {
  REP,
  MASTER_ORGANIZATION_ADMIN,
  REGION_ADMIN,
  SUPER,
  MASTER_ADMIN
} from 'consts/user-roles';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Region Name'
  },
  {
    id: 'masterOrganizationName',
    disablePadding: false,
    label: 'Master Organization'
  },
  {
    id: 'contactEmail',
    disablePadding: false,
    label: 'Contact Email'
  }
];

export class Regions extends Component {
  state = {
    filteredRegionsIDs: []
  };

  componentDidMount() {
    const {
      auth: { user },
      history,
      regions,
      masterOrganizations
    } = this.props;

    let filteredRegionsIDs;

    // If user is rep then show regions belongs to organizations that assigned to this rep
    if (user.roleAlias === REP) {
      const masterOrganizationsIDsAssignedToRep = _.find(masterOrganizations, {
        repId: user._id
      }).map(org => org._id);

      filteredRegionsIDs = _.filter(regions, region => {
        return masterOrganizationsIDsAssignedToRep.includes(
          region.masterOrganizationId
        );
      });
    }

    // If user is master organization admin then show regions belongs to his organization
    if (user.roleAlias === MASTER_ORGANIZATION_ADMIN) {
      const masterOrganization = _.find(masterOrganizations, {
        _id: user.masterOrganizationId
      });

      if (!masterOrganization) {
        this.setState({
          error: 'Your master organization was deleted'
        });
      } else {
        filteredRegionsIDs = _.filter(regions, {
          masterOrganizationId: masterOrganization._id
        });
      }
    }

    // If user is region admin then redirect to his region
    if (user.roleAlias === REGION_ADMIN) {
      // find user's region
      const region = _.find(regions, { _id: user.regionId });

      if (!region) {
        this.setState({
          error: 'Your region was deleted'
        });
      } else {
        history.replace('/admin/regions/edit/' + region._id);
        return;
      }
    }

    filteredRegionsIDs = regions.map(reg => reg._id);

    this.setState({
      filteredRegionsIDs
    });
  }

  deleteDataHandler = (id, e) => {
    e.preventDefault();
    this.props.deleteData('regions', id);
  };

  render() {
    const { regions, auth, masterOrganizations, users } = this.props;
    const { filteredRegionsIDs, error } = this.state;

    const filteredRegions = filteredRegionsIDs
      .map(_id => {
        const region = _.find(regions, { _id });

        if (!region) return null;

        const masterOrganization = _.find(masterOrganizations, {
          _id: region.masterOrganizationId
        });

        const contactUser = _.find(users, { _id: region.contactUserId });

        return {
          ...region,
          contactEmail: contactUser && contactUser.username,
          masterOrganizationName: masterOrganization && masterOrganization.name
        };
      })
      .filter(region => !!region);

    return error ? (
      error
    ) : (
      <div>
        <NeedPermissions permissions={[SUPER, MASTER_ADMIN]}>
          <Button
            color="primary"
            variant="outlined"
            component={Link}
            to="/admin/regions/create"
          >
            Create Region
          </Button>
        </NeedPermissions>

        {regions && (
          <MaterialTable
            data={filteredRegions}
            rows={rows}
            deleteHandler={
              auth.user.roleAlias === SUPER ||
              auth.user.roleAlias === MASTER_ADMIN
                ? this.deleteDataHandler
                : undefined
            }
            editLink="/admin/regions/edit"
            type="Region"
            rowWithName="name"
          />
        )}
      </div>
    );
  }
}

Regions.propTypes = {
  auth: PropTypes.object.isRequired,
  regions: PropTypes.array.isRequired,
  masterOrganizations: PropTypes.array.isRequired,
  users: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default compose(
  withData(['masterOrganizations', 'regions', 'users']),
  connect(
    mapStateToProps,
    { deleteData }
  )
)(Regions);
