import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError, reset } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { addData, editData } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons
} from 'components/form-components';
import { reduxFormValidator } from 'validation';

const requiredFields = [
  // 1. Edit required fields
];

const validate = reduxFormValidator(requiredFields);

// 2. Rename class name and all matches of it
class ExampleForm extends Component {
  state = { success: false };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    console.log('@TODO Enable Server requests');
    // Change data types in axios requests
    // const result = isEdit

    //   ? await editData('examples', values)
    //   : await addData('examples', values);

    // if (!result.success) {
    //   this.setState({ success: false });

    //   console.log('Server response:', result);
    //   throw new SubmissionError(result.err);
    // } else {
    //   dispatch(reset('ExampleForm'));
    //   this.setState({ success: true });
    //   onSuccess && onSuccess();
    // }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      isEdit
    } = this.props;

    const { success } = this.state;

    // Rename success message
    const successMessage = isEdit
      ? 'Example updated successfully'
      : 'Example created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field
          disabled={isEdit}
          name="name"
          label="Example Name *"
          component={renderTextField}
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="example" // Rename
          handleReset={reset}
        />
      </form>
    );
  }
}

ExampleForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

export default compose(
  reduxForm({
    form: 'ExampleForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    null,
    { addData, editData }
  )
)(ExampleForm);
