import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ExampleForm from './ExampleForm/ExampleForm';
import { setCurrentPage } from 'actions/statusActions';

const CreateExample = ({ setCurrentPage, match, history }) => {
  useEffect(() => {
    setCurrentPage(match);
  });

  return <ExampleForm onSuccess={history.goBack} />;
};

CreateExample.propTypes = {
  setCurrentPage: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default connect(
  null,
  { setCurrentPage }
)(CreateExample);
