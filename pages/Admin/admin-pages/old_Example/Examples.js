import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  // {
  //   id: 'role',
  //   disablePadding: false,
  //   label: 'Roles'
  // }
];

const Examples = ({ examples, deleteData }) => {
  const deleteHandler = id => {
    deleteData('examples', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/examples/create"
      >
        Create example
      </Button>

      <MaterialTable
        type="example"
        data={examples}
        rowWithName="name"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/examples/edit"
      />
    </div>
  );
};

Examples.propTypes = {
  examples: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['examples'])(Examples);
