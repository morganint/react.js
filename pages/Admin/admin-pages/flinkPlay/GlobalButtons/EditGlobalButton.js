import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import GlobalButtonForm from './GlobalButtonForm/GlobalButtonForm';
import { withData } from 'components/hocs';

const EditGlobalButton = ({ match, globalButtons }) => {
  const dataToEdit = useMemo(
    () => find(globalButtons, { _id: match.params.id }),
    [globalButtons, match.params.id]
  );

  return dataToEdit ? (
    <GlobalButtonForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/global-buttons" />
  );
};

EditGlobalButton.propTypes = {
  globalButtons: PropTypes.array.isRequired
};

export default withData(['globalButtons'])(EditGlobalButton);
