import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError, reset } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import { addData, editData } from 'actions/adminActions';
import {
  FormTextfield,
  renderRadioGroup,
  StyledError,
  renderSelect,
  StyledSuccess,
  FormButtons
} from 'components/form-components';
import * as buttons from 'consts/buttons';
import { FlexContainer } from 'components/common';
import { reduxFormValidator } from 'validation';

const requiredFields = [
  'name',
  'position',
  'alias',
  'stringNumber',
  'imageFilename'
];

const validate = reduxFormValidator(requiredFields);

const positions = _.map(_.range(1, 8), num => ({
  value: String(num),
  label: num
}));

class GlobalButtonForm extends Component {
  state = { success: false };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    const result = isEdit
      ? await editData('globalButtons', values)
      : await addData('globalButtons', values);

    if (!result.success) {
      this.setState({ success: false });

      if (result.err.code === 11000) {
        // Duplicate code error, means ALIAS already used
        throw new SubmissionError({
          alias: 'Document with that alias already exists'
        });
      }

      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('GlobalButtonForm'));
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      isEdit
    } = this.props;

    const { success } = this.state;

    const successMessage = isEdit
      ? 'Global Button updated successfully'
      : 'Global Button created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <FlexContainer xs="100%" sm="50%">
          <FormTextfield name="name" label="Button Name *" />
          <Field
            name="alias"
            label="Alias Button *"
            component={renderSelect}
            options={_.map(buttons, btn => ({
              value: btn,
              label: btn
            }))}
          />
        </FlexContainer>

        <FlexContainer xs="100%" sm="50%">
          <FormTextfield
            name="stringNumber"
            type="number"
            label="String Number Name *"
          />

          <FormTextfield name="imageFilename" label="Image Filename *" />
        </FlexContainer>

        <Field
          name="position"
          label="Button Position *"
          component={renderRadioGroup}
          options={positions}
          format={val => String(val)}
          normalize={val => +val}
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="Global Button"
          handleReset={reset}
        />
      </form>
    );
  }
}

GlobalButtonForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

export default compose(
  reduxForm({
    form: 'GlobalButtonForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    null,
    { addData, editData }
  )
)(GlobalButtonForm);
