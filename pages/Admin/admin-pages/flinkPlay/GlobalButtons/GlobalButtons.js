import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'position',
    disablePadding: false,
    label: 'Position'
  }
];

const GlobalButtons = ({ globalButtons, deleteData }) => {
  const deleteHandler = id => {
    deleteData('globalButtons', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/global-buttons/create"
      >
        Create Global Button
      </Button>

      <MaterialTable
        type="global button"
        data={globalButtons}
        rowWithName="name"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/global-buttons/edit"
      />
    </div>
  );
};

GlobalButtons.propTypes = {
  globalButtons: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['globalButtons'])(GlobalButtons);
