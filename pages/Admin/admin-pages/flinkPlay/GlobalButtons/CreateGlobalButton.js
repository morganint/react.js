import React from 'react';
import PropTypes from 'prop-types';

import GlobalButtonForm from './GlobalButtonForm/GlobalButtonForm';

const CreateGlobalButton = ({ history }) => {
  return <GlobalButtonForm onSuccess={history.goBack} />;
};

CreateGlobalButton.propTypes = {
  history: PropTypes.object.isRequired
};

export default CreateGlobalButton;
