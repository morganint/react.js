import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { find } from 'lodash';

import { addData, editData } from 'actions/adminActions';

import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons,
  renderCheckbox,
  getOptionsFromLists,
  renderSelect,
  CheckboxFieldsGroup,
  formatCheckboxesInArray,
  ParentVideosControls,
} from 'components/form-components';
import { FlexContainer } from 'components/common';
import { withData } from 'components/hocs';
// import * as menuTypes from 'consts/menu-types';

const validate = (values) => {
  const errors = {};

  const requiredFields = [
    'menuName',
    'alias',
    'characterLocation',
    'characterSize',
    'labelString',
    'helpString',
    'helpAudio',
  ];

  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });

  if (!values.globalButtons || !values.globalButtons.length) {
    errors.globalButtons = 'Required';
  }

  return errors;
};

const fieldsWithListValues = [
  { name: 'characterSize', list: 'Character Sizes' },
];

class MenuForm extends Component {
  constructor(props) {
    super(props);

    const { getOptionsFromLists, lists, initialValues } = props;

    this.state = {
      success: false,
      fieldsWithListValues: getOptionsFromLists(
        fieldsWithListValues,
        lists,
        'MenuForm',
        initialValues
      ),
    };
  }

  async componentDidMount() {
    const { menus, isEdit, initialValues } = this.props;

    const forWordlists = find(menus, { forWordlists: true });
    const forMainMenu = find(menus, { forMainMenu: true });

    const isForWordlistsExists = isEdit
      ? !!forWordlists && forWordlists._id !== initialValues._id
      : !!forWordlists;

    const isForMainMenuExists = isEdit
      ? !!forMainMenu && forMainMenu._id !== initialValues._id
      : !!forMainMenu;

    this.setState({ isForMainMenuExists, isForWordlistsExists });
  }

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    const formattedValues = { ...values };

    formattedValues.globalButtons = [
      ...formatCheckboxesInArray(formattedValues.globalButtons),
    ];

    const result = isEdit
      ? await editData('menus', formattedValues)
      : await addData('menus', formattedValues);

    if (!result.success) {
      this.setState({ success: false });
      throw new SubmissionError(result.err);
    } else {
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      globalButtons,
      initialValues,
      isEdit,
    } = this.props;

    const {
      success,
      fieldsWithListValues,
      isForMainMenuExists,
      isForWordlistsExists,
    } = this.state;

    const buttonsOptions =
      globalButtons &&
      globalButtons.map((btn) => ({
        value: btn.alias,
        label: btn.name,
      }));

    const successMessage = isEdit
      ? 'Menu updated successfully'
      : 'Menu created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <FlexContainer xs="50%">
          <Field
            name="menuName"
            label="Menu Name *"
            component={renderTextField}
          />

          <Field
            name="menuDesc"
            label="Menu Description"
            component={renderTextField}
          />

          {/* <Field
            name="alias"
            label="Menu Alias *"
            component={renderSelect}
            options={map(menuTypes, type => ({
              value: type,
              label: type
            }))}
          /> */}
        </FlexContainer>

        <FlexContainer xs="33%">
          <Field
            name="labelString"
            label="Label String *"
            component={renderTextField}
            type="number"
          />
          <Field
            name="helpString"
            label="Help String *"
            component={renderTextField}
            type="number"
          />
          <Field
            name="helpAudio"
            label="Help Audio *"
            component={renderTextField}
          />
        </FlexContainer>

        <FlexContainer xs="50%">
          <Field
            name="characterSize"
            label="Character Size *"
            component={renderSelect}
            options={
              fieldsWithListValues.filter(
                (field) => field.name === 'characterSize'
              )[0].options || []
            }
            isClearable
          />

          <Field
            name="characterLocation"
            label="Character Location *"
            component={renderTextField}
          />
        </FlexContainer>

        <div>
          <Field
            name="forMainMenu"
            label="For Main Menu"
            disabled={isForMainMenuExists}
            component={renderCheckbox}
          />
          <Field
            name="forWordlists"
            label="For Wordlists"
            disabled={isForWordlistsExists}
            component={renderCheckbox}
          />
        </div>

        {buttonsOptions && (
          <CheckboxFieldsGroup
            name="globalButtons"
            fields={buttonsOptions}
            groupLabel={'Global Buttons'}
            initialValues={initialValues}
            form="MenuForm"
          />
        )}

        <ParentVideosControls />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="menu"
          handleReset={reset}
        />
      </form>
    );
  }
}

MenuForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  menus: PropTypes.array.isRequired,
  getOptionsFromLists: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  globalButtons: PropTypes.array.isRequired,
};

export default compose(
  withData(['lists', 'globalButtons', 'menus']),
  reduxForm({
    form: 'MenuForm',
    validate,
    enableReinitialize: true,
  }),
  connect(null, { addData, editData, getOptionsFromLists })
)(MenuForm);
