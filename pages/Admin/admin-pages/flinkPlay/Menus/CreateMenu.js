import React, { Component } from 'react';

import MenuForm from './MenuForm/MenuForm';

export class CreateMenu extends Component {
  render() {
    return <MenuForm onSuccess={this.props.history.goBack} />;
  }
}

export default CreateMenu;
