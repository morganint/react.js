import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import { formatCheckboxesFromArray } from 'components/form-components';
import MenuForm from './MenuForm/MenuForm';
import { withData } from 'components/hocs';
import { isEmpty } from 'validation';

const EditMenu = ({ match, menus }) => {
  const dataToEdit = find(menus, { _id: match.params.id });

  const initialValues = dataToEdit && {
    ...dataToEdit,
    globalButtons: isEmpty(dataToEdit.globalButtons)
      ? undefined
      : formatCheckboxesFromArray(dataToEdit.globalButtons)
  };

  return dataToEdit ? (
    <MenuForm isEdit initialValues={initialValues} />
  ) : (
    <Redirect to="/admin/menus" />
  );
};

EditMenu.propTypes = {
  menus: PropTypes.array.isRequired
};

export default withData(['menus'])(EditMenu);
