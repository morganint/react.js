import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'menuName',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'menuDesc',
    disablePadding: false,
    label: 'Description'
  }
];

const Menus = ({ deleteData, menus }) => {
  const deleteHandler = (id, e) => {
    deleteData('menus', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/menus/create"
      >
        Create menu
      </Button>

      <MaterialTable
        type="menu"
        data={menus}
        rowWithName="menuName"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/menus/edit"
      />
    </div>
  );
};

Menus.propTypes = {
  menus: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['menus'])(Menus);
