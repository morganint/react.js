import React, { Component } from 'react';

import CharacterForm from './CharacterForm/CharacterForm';

export class CreateCharacter extends Component {
  render() {
    return <CharacterForm onSuccess={this.props.history.goBack} />;
  }
}

export default CreateCharacter;
