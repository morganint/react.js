import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import CharacterForm from './CharacterForm/CharacterForm';
import { withData } from 'components/hocs';

const EditCharacter = ({ match, characters }) => {
  const dataToEdit = find(characters, { _id: match.params.id });

  return dataToEdit ? (
    <CharacterForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/characters" />
  );
};

EditCharacter.propTypes = {
  characters: PropTypes.array.isRequired
};

export default withData(['characters'])(EditCharacter);
