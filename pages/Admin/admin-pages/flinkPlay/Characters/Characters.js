import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'description',
    disablePadding: false,
    label: 'Description'
  }
];

const Characters = ({ deleteData, characters }) => {
  const deleteHandler = (id, e) => {
    deleteData('characters', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/characters/create"
      >
        Create character
      </Button>

      <MaterialTable
        type="character"
        data={characters}
        rowWithName="name"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/characters/edit"
      />
    </div>
  );
};

Characters.propTypes = {
  characters: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['characters'])(Characters);
