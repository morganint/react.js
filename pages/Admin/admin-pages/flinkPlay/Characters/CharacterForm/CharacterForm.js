import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { addData, editData } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons
} from 'components/form-components';
import { reduxFormValidator } from 'validation';

const requiredFields = ['name'];

const validate = reduxFormValidator(requiredFields);

class CharacterForm extends Component {
  state = { success: false };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    const result = isEdit
      ? await editData('characters', values)
      : await addData('characters', values);

    if (!result.success) {
      this.setState({ success: false });
      throw new SubmissionError(result.err);
    } else {
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      isEdit
    } = this.props;

    const { success } = this.state;

    const successMessage = isEdit
      ? 'Character updated successfully'
      : 'Character created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field
          name="name"
          label="Character Name *"
          component={renderTextField}
        />

        <Field
          name="description"
          label="Description"
          component={renderTextField}
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="character"
          handleReset={reset}
        />
      </form>
    );
  }
}

CharacterForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

export default compose(
  reduxForm({
    form: 'CharacterForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    null,
    { addData, editData }
  )
)(CharacterForm);
