import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { addData, editData } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons
} from 'components/form-components';
import { reduxFormValidator } from 'validation';

const requiredFields = ['name'];

const validate = reduxFormValidator(requiredFields);

class ThemeForm extends Component {
  state = { success: false };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    const result = isEdit
      ? await editData('themes', values)
      : await addData('themes', values);

    if (!result.success) {
      this.setState({ success: false });
      throw new SubmissionError(result.err);
    } else {
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      isEdit
    } = this.props;

    const { success } = this.state;

    const successMessage = isEdit
      ? 'Theme updated successfully'
      : 'Theme created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field name="name" label="Theme Name *" component={renderTextField} />

        <Field
          name="description"
          label="Description"
          component={renderTextField}
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="theme"
          handleReset={reset}
        />
      </form>
    );
  }
}

ThemeForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

export default compose(
  reduxForm({
    form: 'ThemeForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    null,
    { addData, editData }
  )
)(ThemeForm);
