import React, { Component } from 'react';

import ThemeForm from './ThemeForm/ThemeForm';

export class CreateTheme extends Component {
  render() {
    return <ThemeForm onSuccess={this.props.history.goBack} />;
  }
}

export default CreateTheme;
