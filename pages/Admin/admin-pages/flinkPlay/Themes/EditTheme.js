import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import ThemeForm from './ThemeForm/ThemeForm';
import { withData } from 'components/hocs';

const EditTheme = ({ match, themes }) => {
  const dataToEdit = find(themes, { _id: match.params.id });

  return dataToEdit ? (
    <ThemeForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/themes" />
  );
};

EditTheme.propTypes = {
  themes: PropTypes.array.isRequired
};

export default withData(['themes'])(EditTheme);
