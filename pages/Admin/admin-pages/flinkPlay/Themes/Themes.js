import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'description',
    disablePadding: false,
    label: 'Description'
  }
];

const Themes = ({ deleteData, themes }) => {
  const deleteHandler = (id, e) => {
    deleteData('themes', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/themes/create"
      >
        Create theme
      </Button>

      <MaterialTable
        type="theme"
        data={themes}
        rowWithName="name"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/themes/edit"
      />
    </div>
  );
};

Themes.propTypes = {
  themes: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['themes'])(Themes);
