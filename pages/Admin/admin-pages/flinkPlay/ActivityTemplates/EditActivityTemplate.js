import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import ActivityTemplateForm from './ActivityTemplateForm/ActivityTemplateForm';
import { withData } from 'components/hocs';

const EditActivityTemplate = ({ match, activityTemplates }) => {
  const dataToEdit = useMemo(
    () => find(activityTemplates, { _id: match.params.id }),
    [activityTemplates, match.params.id]
  );

  return dataToEdit ? (
    <ActivityTemplateForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/activity-templates" />
  );
};

EditActivityTemplate.propTypes = {
  activityTemplates: PropTypes.array.isRequired
};

export default withData(['activityTemplates'])(EditActivityTemplate);
