import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'activityTemplateName',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'availableForWordlists',
    disablePadding: false,
    label: 'Available for Wordlists',
    type: 'bool'
  },
  {
    id: 'wordlistName1',
    disablePadding: false,
    label: 'Wordlist Name 1'
  },
  {
    id: 'inActivityBuilder',
    disablePadding: false,
    label: 'In Activity Builder',
    type: 'bool'
  },
  {
    id: 'characters',
    disablePadding: false,
    label: 'Characters',
    type: 'bool'
  },
  {
    id: 'characterSize',
    disablePadding: false,
    label: 'Character Size'
  },
];

const ActivityTemplates = ({ activityTemplates, deleteData }) => {
  const deleteHandler = id => {
    deleteData('activityTemplates', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/activity-templates/create"
      >
        Create Template
      </Button>

      <MaterialTable
        type="activity template"
        data={activityTemplates}
        rowWithName="activityTemplateName"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/activity-templates/edit"
      />
    </div>
  );
};

ActivityTemplates.propTypes = {
  activityTemplates: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['activityTemplates'])(ActivityTemplates);
