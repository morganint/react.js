import React, { Component } from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { set } from "lodash";

import { ActivityOptions } from "components/flink-components";

import ActivityTemplates from "activity-templates";
import classes from "./DefaultActivityOptions.module.scss";

class DefaultActivityOptions extends Component {
  state = {
    options: {}
  };

  componentDidMount() {
    const { initialOptions = {}, template: templateAlias } = this.props;

    const TemplateData = ActivityTemplates[templateAlias];

    if (!TemplateData || !TemplateData.optionsArray) {
      return console.error("template not found");
    }

    const optionsToRender = TemplateData.optionsArray;

    this.setState({ options: initialOptions, optionsToRender });
  }

  changeOption = (prop, val) => {
    const changedOptions = { ...this.state.options };
    set(changedOptions, prop, val);

    this.setState({
      options: changedOptions
    });
  };

  componentWillUnmount() {
    this.props.saveHandler(this.state.options);
  }

  render() {
    const { optionsToRender, options } = this.state;

    if (!optionsToRender) return null;

    return (
      <div className={classes.wrapper}>
        <Typography align="center" gutterBottom variant="h5">
          Default Options
        </Typography>

        <ActivityOptions
          changeHandler={this.changeOption}
          options={options}
          optionsToRender={optionsToRender}
          adminMode
        />
      </div>
    );
  }
}

DefaultActivityOptions.propTypes = {
  template: PropTypes.string.isRequired,
  saveHandler: PropTypes.func.isRequired
};

export default DefaultActivityOptions;
