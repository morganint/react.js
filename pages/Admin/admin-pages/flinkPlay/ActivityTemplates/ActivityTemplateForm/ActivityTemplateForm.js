import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';
import Modal from '@material-ui/core/Modal';
import {
  Field,
  reduxForm,
  SubmissionError,
  reset,
  change,
  formValueSelector
} from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import { addData, editData } from 'actions/adminActions';
import {
  StyledError,
  StyledSuccess,
  renderSelect,
  FormCheckbox,
  FormTextfield,
  FormButtons,
  getOptionsFromLists,
  ParentVideosControls
} from 'components/form-components';
import { FlexContainer } from 'components/common';
import { withData } from 'components/hocs';
import * as activityTemplates from 'consts/activity-templates';
import DefaultActivityOptions from './DefaultActivityOptions/DefaultActivityOptions';

const validate = values => {
  const errors = {};

  const requiredFields = [
    'alias',
    'helpStrings[activity]',
    'helpStrings[team]',
    'activityTemplateName',
    'nameString',
    'globalButtons',
    'thumbFilename',
    'logoFilename'
  ];

  if (values.availableForWordlists) {
    requiredFields.push(
      'wordlistName1',
      'wordlistName2',
      'helpStrings[wordlist]'
    );
  }

  if (!values.characters) {
    requiredFields.push('characterLocation', 'characterPath', 'characterSize');
  }

  if (!values.defaultOptions) {
    errors._error = 'Need to set default options';
  }

  requiredFields.forEach(field => {
    if (!_.get(values, field)) {
      _.set(errors, field, 'Required');
    }
  });

  if (
    !values.activityTemplateName ||
    !values.activityTemplateName.trim().length
  ) {
    errors.activityTemplateName = 'Required';
  }

  return errors;
};

class ActivityTemplateForm extends Component {
  state = {
    success: false,
    fieldsWithListValues: null,
    isOptionsModalOpen: false
  };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    !values.characters && (values.characterSize = '');

    values.activityTemplateName =
      values.activityTemplateName && values.activityTemplateName.trim();

    const result = isEdit
      ? await editData('activityTemplates', values)
      : await addData('activityTemplates', values);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('ActivityTemplateForm'));
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  saveOptionsHandler = options => {
    const { change } = this.props;

    change('ActivityTemplateForm', 'defaultOptions', options);

    this.closeDefaultOptions();
  };

  closeDefaultOptions = () => this.setState({ isOptionsModalOpen: false });

  openDefaultOptions = () => this.setState({ isOptionsModalOpen: true });

  componentDidMount() {
    const {
      getOptionsFromLists,
      lists,
      initialValues,
      activityTemplates,
      isEdit
    } = this.props;

    const defaultForReports = _.find(activityTemplates, {
      defaultForReports: true
    });

    const isDefaultForReportsExists = isEdit
      ? !!defaultForReports && defaultForReports._id !== initialValues._id
      : !!defaultForReports;

    const fieldsWithListValues = getOptionsFromLists(
      [
        {
          name: 'characterSize',
          list: 'Character Sizes'
        }
      ],
      lists,
      'ActivityTemplateForm',
      initialValues
    );

    this.setState({ fieldsWithListValues, isDefaultForReportsExists });
  }

  render() {
    const {
      error,
      reset,
      alias,
      isEdit,
      pristine,
      characters,
      submitting,
      handleSubmit,
      defaultOptions,
      inActivityBuilder,
      availableForWordlists
    } = this.props;

    const { success, fieldsWithListValues, isOptionsModalOpen } = this.state;

    const successMessage = isEdit
      ? 'Activity Template updated successfully'
      : 'Activity Template created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <FlexContainer xs="100%" sm="50%" md="33%">
          <FormTextfield name="activityTemplateName" label="Name *" />

          <FormTextfield
            name="nameString"
            label="Name String Number *"
            type="number"
          />

          <Field
            disabled={isEdit}
            name="alias"
            label="Alias Template *"
            component={renderSelect}
            options={_.map(activityTemplates, templ => ({
              value: templ,
              label: templ
            }))}
          />
        </FlexContainer>

        <FlexContainer xs="100%" sm="50%" md="33%">
          <FormTextfield
            name="explanationString"
            label="Explanation String Number"
            type="number"
          />
          <FormTextfield name="thumbFilename" label="Thumbnail Filename *" />
          <FormTextfield name="logoFilename" label="Logo Filename *" />
        </FlexContainer>

        <FlexContainer xs="100%" sm="50%" md="25%">
          <FormTextfield
            name="helpStrings[activity]"
            label="Help String Number *"
            type="number"
          />
          <FormTextfield
            name="helpStrings[team]"
            label="Team Help String Number *"
            type="number"
          />
          <FormTextfield
            name="helpStrings[contentTab]"
            label="Content Tab Help String"
            type="number"
          />
          <FormTextfield
            name="helpStrings[optionsTab]"
            label="Options Tab Help String"
            type="number"
          />
        </FlexContainer>

        {alias && (
          <Fragment>
            <div style={{ marginBottom: '20px' }}>
              <Button
                variant="contained"
                color="primary"
                onClick={this.openDefaultOptions}
              >
                Default Options
              </Button>
            </div>

            <Modal onClose={this.closeDefaultOptions} open={isOptionsModalOpen}>
              <DefaultActivityOptions
                template={alias}
                initialOptions={defaultOptions}
                saveHandler={this.saveOptionsHandler}
              />
            </Modal>
          </Fragment>
        )}

        <FormCheckbox
          name="availableForWordlists"
          label="Available for Wordlists"
        />

        {availableForWordlists && this.renderWordlistFields()}

        <FlexContainer xs="100%" sm="auto">
          <div>
            <FormCheckbox
              name="inActivityBuilder"
              label="In Activity Builder"
            />
          </div>
          {inActivityBuilder && (
            <div>
              <FormTextfield
                name="nameActivityBuilder"
                label="Activity Builder Name"
              />
              <FormTextfield
                name="helpStrings[activityBuilder]"
                type="number"
                label="Activity Builder Help String"
              />
            </div>
          )}
        </FlexContainer>

        <FormCheckbox name="characters" label="Characters" />

        {characters && (
          <FlexContainer xs="100%" sm="33%">
            <FormTextfield
              name="characterLocation"
              label="Character Location"
            />
            <Field
              name="characterSize"
              label="Character Size"
              component={renderSelect}
              options={
                fieldsWithListValues
                  ? fieldsWithListValues.filter(
                      field => field.name === 'characterSize'
                    )[0].options || []
                  : []
              }
            />
          </FlexContainer>
        )}

        <ParentVideosControls />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="activity template"
          handleReset={reset}
        />
      </form>
    );
  }

  renderWordlistFields = () => {
    const { isDefaultForReportsExists } = this.state;

    return (
      <StyledFieldset>
        <p>
          <FormCheckbox name="onlyForWordlists" label="Only for Wordlists" />
          <FormCheckbox
            name="alwaysIncludedInWordlists"
            label="Always included in Wordlists"
          />
          <FormCheckbox
            name="defaultForReports"
            label="Default For Reports"
            disabled={isDefaultForReportsExists}
          />
        </p>
        <FlexContainer xs="100%" md="50%">
          <FormTextfield
            name="wordlistName1"
            label="Wordlist Name 1 (String Number) *"
            type="number"
          />
          <FormTextfield
            name="wordlistName2"
            label="Wordlist Name 2 (String Number) *"
            type="number"
          />
        </FlexContainer>
        <FlexContainer xs="50%" sm="33%" xl="25%">
          <FormTextfield
            name="minWords"
            label="Minimum # Words"
            type="number"
          />
          <FormTextfield
            name="maxWordLength"
            label="Max Word Length"
            type="number"
          />
          <FormTextfield
            name="helpStrings[wordlist]"
            label="WordList Help String Number *"
            type="number"
            required
          />
        </FlexContainer>
        <FlexContainer xs="50%" sm="33%" lg="25%" xl="16%">
          <FormCheckbox name="imageRequired" label="Image Required" />
          <FormCheckbox name="audioRequired" label="Audio Required" />
          <FormCheckbox name="spacesAllowed" label="Spaces Allowed" />
          <FormCheckbox name="hyphenAllowed" label="Hyphen Allowed" />
        </FlexContainer>
      </StyledFieldset>
    );
  };
}

ActivityTemplateForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  availableForWordlists: PropTypes.bool,
  inActivityBuilder: PropTypes.bool,
  characters: PropTypes.bool,
  getOptionsFromLists: PropTypes.func.isRequired
};

const selector = formValueSelector('ActivityTemplateForm');

const mapStateToProps = state => ({
  availableForWordlists: selector(state, 'availableForWordlists'),
  inActivityBuilder: selector(state, 'inActivityBuilder'),
  alias: selector(state, 'alias'),
  defaultOptions: selector(state, 'defaultOptions'),
  characters: selector(state, 'characters'),
  activityTemplates: state.admin.activityTemplates
});

export default compose(
  withData(['lists']),
  reduxForm({
    form: 'ActivityTemplateForm',
    validate,
    enableReinitialize: true
  }),
  connect(mapStateToProps, { addData, editData, getOptionsFromLists, change })
)(ActivityTemplateForm);

const StyledFieldset = styled.fieldset`
  border: 1px solid #ccc;
  border-radius: 4px;
  margin-bottom: 20px;
  padding-top: 20px;
`;
