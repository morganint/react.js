import React from 'react';

import ActivityTemplateForm from './ActivityTemplateForm/ActivityTemplateForm';

const CreateActivityTemplate = ({ history }) => {
  return <ActivityTemplateForm onSuccess={history.goBack} />;
};

export default CreateActivityTemplate;
