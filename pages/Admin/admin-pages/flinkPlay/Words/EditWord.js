import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import WordForm from './WordForm/WordForm';
import { withData } from 'components/hocs';

const EditWord = ({ match, words }) => {
  const dataToEdit = useMemo(() => find(words, { _id: match.params.id }), [
    words,
    match.params.id
  ]);

  return dataToEdit ? (
    <WordForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/words" />
  );
};

EditWord.propTypes = {
  words: PropTypes.array.isRequired
};

export default withData(['words'])(EditWord);
