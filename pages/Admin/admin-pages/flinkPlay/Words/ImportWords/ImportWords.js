import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import { ImportCSV } from "components/form-components";
import classes from "./ImportWords.module.scss";
import { importWords } from "actions/adminActions";
import { showError } from "actions/statusActions";
import { formatLocalesToOptions } from "utils";

class ImportWords extends Component {
  constructor(props) {
    super(props);

    const localesOptions = formatLocalesToOptions(this.props.locales);

    this.state = {
      localesOptions,
      overwrite: false,
      words: false,
      locale: ""
    };
  }

  handleImport = parsedCSV => {
    const errors = [];

    const words = parsedCSV.slice(1).map(wordData => {
      const [
        word,
        definition,
        sentence,
        misspelling,
        wordAudio,
        definitionAudio,
        hasPicture,
        relatedWord
      ] = wordData;

      if (!word || !sentence || !definition) {
        errors.push(wordData.join(", "));
        return null;
      }

      const picture = relatedWord
        ? undefined
        : !!hasPicture &&
          (hasPicture === "true" || hasPicture.toLowerCase() === "yes");

      return {
        word,
        definition,
        sentence,
        misspelling,
        wordAudio,
        definitionAudio,
        hasPicture: picture,
        relatedWord
      };
    });

    if (errors.length) {
      const errorMessage = `Following lines dont have one or all of required fields (word,
          definition, sentence): <br><br><i>
          ${errors.join("<br><br>")}</i>`;

      this.props.showError({ message: errorMessage, html: true });
    } else {
      this.setState({ words: _.uniqBy(words, "word") });
    }
  };

  handleChangeLocale = locale => {
    this.setState({
      locale
    });
  };

  handleOverwriteChange = e => {
    this.setState({
      overwrite: e.target.checked
    });
  };

  handleSubmit = async () => {
    const { locale, words, overwrite } = this.state;
    const { importWords, closeHandler } = this.props;

    await importWords({
      words: words.map(w => ({ ...w, locale })),
      overwrite
    });

    closeHandler();
  };

  render() {
    const { closeHandler } = this.props;
    const { locale, words, localesOptions } = this.state;

    return (
      <div className={classes.wrapper} tabIndex="-1">
        <ImportCSV label="CSV File:" callback={this.handleImport} parse />

        <FormControlLabel
          control={
            <Checkbox onChange={this.handleOverwriteChange} color="primary" />
          }
          label="Overwrite Existing Words?"
        />

        <p>
          <select
            className={classes.select}
            value={locale}
            onChange={e => this.handleChangeLocale(e.target.value)}
          >
            <option value="">Select language</option>
            {localesOptions.map(opt => (
              <option key={opt.value} value={opt.value}>
                {opt.label}
              </option>
            ))}
          </select>
        </p>

        <div className={classes.btnsWrapper}>
          <Button
            color="primary"
            variant="contained"
            onClick={this.handleSubmit}
            disabled={!locale || !words.length}
          >
            Import
          </Button>
          <Button
            color="secondary"
            variant="contained"
            onClick={closeHandler}
            classes={{ root: classes.cancelBtn }}
          >
            Cancel
          </Button>
        </div>
      </div>
    );
  }
}

export default connect(null, { importWords, showError })(ImportWords);
