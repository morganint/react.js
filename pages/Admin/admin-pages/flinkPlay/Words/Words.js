import React, { useState } from "react";
import PropTypes from "prop-types";

import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import MaterialTable from "components/admin/MaterialTable/MaterialTable";
import { withData } from "components/hocs";
import ImportWords from "./ImportWords/ImportWords";

const rows = [
  {
    id: "word",
    disablePadding: false,
    label: "Word"
  },
  {
    id: "locale",
    disablePadding: false,
    label: "Locale"
  },
  {
    id: "definition",
    disablePadding: false,
    label: "Definition"
  },
  {
    id: "sentence",
    disablePadding: false,
    label: "Sentence"
  },
  {
    id: "misspelling",
    disablePadding: false,
    label: "Misspelling"
  }
];

const Words = ({ words, locales, deleteData }) => {
  const deleteHandler = id => {
    deleteData("words", id);
  };

  const [isImportOpen, setImportOpen] = useState(false);

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        onClick={() => setImportOpen(true)}
      >
        Import Words
      </Button>

      <Modal onClose={() => setImportOpen(false)} open={isImportOpen}>
        <ImportWords
          locales={locales}
          closeHandler={() => setImportOpen(false)}
        />
      </Modal>

      <MaterialTable
        type="word"
        data={words}
        rowWithName="word"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/words/edit"
      />
    </div>
  );
};

Words.propTypes = {
  words: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(["words", "locales"])(Words);
