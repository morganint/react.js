import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError, reset } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { addData, editData } from 'actions/adminActions';
import {
  renderTextField,
  renderCheckbox,
  StyledError,
  StyledSuccess,
  FormButtons
} from 'components/form-components';
import { reduxFormValidator } from 'validation';

const requiredFields = ['word', 'definition', 'sentence', 'locale'];

const validate = reduxFormValidator(requiredFields);

class WordForm extends Component {
  state = { success: false };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    const result = isEdit
      ? await editData('words', values)
      : await addData('words', values);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('WordForm'));
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      initialValues,
      submitting,
      isEdit
    } = this.props;

    const { success } = this.state;

    const successMessage = isEdit
      ? 'Word updated successfully'
      : 'Word created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field name="word" label="Word *" component={renderTextField} />
        <Field
          name="definition"
          label="Definition *"
          component={renderTextField}
        />
        <Field name="sentence" label="Sentence *" component={renderTextField} />
        <Field
          name="misspelling"
          label="Misspelling"
          component={renderTextField}
        />
        <Field
          name="wordAudio"
          label="Word Audio"
          component={renderTextField}
        />
        <Field
          name="definitionAudio"
          label="Definition Audio"
          component={renderTextField}
        />

        <Field
          name="hasPicture"
          label="Has Picture?"
          component={renderCheckbox}
        />

        <Field
          disabled={initialValues && initialValues.locale === 'en'}
          name="relatedWord"
          label="Related English Word"
          component={renderTextField}
        />
        <Field
          disabled={isEdit}
          name="locale"
          label="Locale *"
          component={renderTextField}
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="word"
          handleReset={reset}
        />
      </form>
    );
  }
}

WordForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

export default compose(
  reduxForm({
    form: 'WordForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    null,
    { addData, editData }
  )
)(WordForm);
