import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
  SubmissionError,
  reset,
  change,
  getFormValues
} from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { ScopeBuilder } from 'components/admin';
import _ from 'lodash';

import { addData, editData } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons,
  getOptionsFromLists
} from 'components/form-components';
import { validateScope } from 'validation';
import { withData } from 'components/hocs';

const validateScopeAndAssign = scope => {
  if (!validateScope(scope)) return false;

  // At least one subject should be associated with level menu
  const assignedFirstCategoryItems = scope.firstCategory.items.filter(
    item => item[scope.firstMenu] && item[scope.firstMenu].length
  );

  if (!assignedFirstCategoryItems.length) {
    return false;
  }

  return true;
};

const validate = values => {
  const errors = {};
  const requiredFields = ['name'];

  errors._error = '';

  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required';
      errors._error += `${field} is required; `;
    }
  });

  if (!validateScopeAndAssign(values)) {
    errors._error = `
      Those instructions are required: 
      1) Scope name is required.
      2) At least one grade or one level should be selected.
      3) If level(s) and grade(s) are selected, all levels should be associated with grades.
      4) At least one "subject" (category in left column) should be created and associated with first menu category
      `;
  }

  // if (
  //   (!values.grades || !values.grades.length) &&
  //   (!values.levels || !values.levels.length)
  // ) {
  //   errors._error = 'At least one level or one grade should be selected';
  // }

  return errors;
};

class ScopeForm extends Component {
  state = { success: false };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    const result = isEdit
      ? await editData('scopes', values)
      : await addData('scopes', values);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('ScopeForm'));
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  componentDidMount() {
    const { lists, getOptionsFromLists } = this.props;
    const [grades, levels] = getOptionsFromLists(
      [
        {
          name: 'grades',
          list: 'Grades'
        },
        {
          name: 'levels',
          list: 'Levels'
        }
      ],
      lists
    );

    this.setState({
      gradesOptions: grades.options,
      levelsOptions: levels.options
    });
  }

  addDataToForm = data => {
    const { change } = this.props;
    _.forOwn(data, (val, key) => {
      change('ScopeForm', key, val);
    });
  };

  render() {
    const {
      error,
      values,
      reset,
      isEdit,
      pristine,
      submitting,
      handleSubmit
    } = this.props;

    const { success, gradesOptions, levelsOptions } = this.state;

    const successMessage = isEdit
      ? 'Scope updated successfully'
      : 'Scope created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field name="name" label="Scope Name *" component={renderTextField} />

        <Field
          name="description"
          label="Description"
          component={renderTextField}
        />

        <ScopeBuilder
          data={values}
          isEdit={isEdit}
          options={{ gradesOptions, levelsOptions }}
          changeHandler={this.addDataToForm}
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="scope"
          handleReset={reset}
          invalid={!validateScopeAndAssign(values)}
        />
      </form>
    );
  }
}

ScopeForm.propTypes = {
  lists: PropTypes.array.isRequired,
  values: PropTypes.object,
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  values: getFormValues('ScopeForm')(state)
});

const defaultData = {
  grades: [],
  levels: [],
  assignedLevels: [],
  firstCategory: {
    title: 'Subjects',
    items: []
  },
  secondCategory: {
    title: 'Topics',
    lists: []
  },
  thirdCategory: {
    title: 'Skills',
    lists: []
  }
};

export default compose(
  withData(['lists']),
  reduxForm({
    form: 'ScopeForm',
    validate,
    initialValues: defaultData,
    enableReinitialize: true
  }),
  connect(
    mapStateToProps,
    { addData, editData, getOptionsFromLists, change }
  )
)(ScopeForm);
