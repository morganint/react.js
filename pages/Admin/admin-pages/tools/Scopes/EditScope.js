import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import ScopeForm from './ScopeForm/ScopeForm';
import { withData } from 'components/hocs';

const EditScope = ({ match, scopes }) => {
  const dataToEdit = useMemo(() => find(scopes, { _id: match.params.id }), [
    scopes,
    match.params.id
  ]);

  return dataToEdit ? (
    <ScopeForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/scopes" />
  );
};

EditScope.propTypes = {
  scopes: PropTypes.array.isRequired
};

export default withData(['scopes'])(EditScope);
