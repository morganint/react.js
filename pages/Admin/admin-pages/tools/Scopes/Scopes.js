import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Name'
  }
];

const Scopes = ({ scopes, deleteData }) => {
  const deleteHandler = id => {
    deleteData('scopes', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/scopes/create"
      >
        Create scope
      </Button>

      <MaterialTable
        type="scope"
        data={scopes}
        rowWithName="name"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/scopes/edit"
      />
    </div>
  );
};

Scopes.propTypes = {
  scopes: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['scopes'])(Scopes);
