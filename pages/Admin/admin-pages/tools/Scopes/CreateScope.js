import React from 'react';
import PropTypes from 'prop-types';

import ScopeForm from './ScopeForm/ScopeForm';

const CreateScope = ({ history }) => {
  return <ScopeForm onSuccess={history.goBack} />;
};

CreateScope.propTypes = {
  history: PropTypes.object.isRequired
};

export default CreateScope;
