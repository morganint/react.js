import React, { Component } from 'react';
import { reduxForm, SubmissionError, reset } from 'redux-form';
import { compose } from 'redux';
import _ from 'lodash';

import {
  FormButtons,
  StyledError,
  StyledSuccess,
  ParentVideosControls,
} from 'components/form-components';

const validate = (values) => {
  const errors = {};

  const requiredFields = [];

  requiredFields.forEach((field) => {
    if (!_.get(values, field)) {
      _.set(errors, field, 'Required');
    }
  });

  return errors;
};

class GeneralForm extends Component {
  state = {
    success: false,
  };

  submit = async (values, dispatch) => {
    const { submitHandler } = this.props;
    const result = await submitHandler(values);

    if (!result.success) {
      this.setState({ success: false });
      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('GeneralForm'));
      this.setState({ success: true });
    }
  };

  render() {
    const {
      error,
      reset,
      pristine,
      submitting,
      handleSubmit,
    } = this.props;
    const { success } = this.state;

    const successMessage = 'Config Updated';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <div style={{ display: 'flex' }}>
          <ParentVideosControls
            title="Family Reports Parent Videos:"
            name="parentVideos[familyReports]"
          />
          <ParentVideosControls
            title="Settings Parent Videos:"
            name="parentVideos[settings]"
          />
        </div>

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit
          itemName="settings"
          handleReset={reset}
        />
      </form>
    );
  }
}

export default compose(
  reduxForm({
    form: 'GeneralForm',
    validate,
    enableReinitialize: true,
  })
)(GeneralForm);
