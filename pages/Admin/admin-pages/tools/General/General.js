import React from 'react';
import { connect } from 'react-redux';

import GeneralForm from './GeneralForm';
import { updateConfig } from 'actions/commonActions';

const General = ({ config, updateConfig }) => {
  if (!config) return <p>Getting config</p>;

  return <GeneralForm initialValues={config} submitHandler={updateConfig} />;
};

const mapStateToProps = ({ common }) => ({
  config: common.config,
});

export default connect(mapStateToProps, { updateConfig })(General);
