import React, { useMemo } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";

import { Link } from "react-router-dom";

import Button from "@material-ui/core/Button";
import MaterialTable from "components/admin/MaterialTable/MaterialTable";
import { withData } from "components/hocs";
import { TRANSLATOR } from "consts/user-roles";

const rows = [
  {
    id: "stringValue",
    disablePadding: false,
    label: "String Value",
    width: "300px"
  },
  {
    id: "stringCategory",
    disablePadding: false,
    label: "String Category"
  },
  {
    id: "stringNumber",
    align: "right",
    disablePadding: false,
    label: "Number"
  },
  // {
  //   id: "languageDisplayName",
  //   disablePadding: false,
  //   label: "Language"
  // },
  {
    id: "locale",
    disablePadding: false,
    label: "Locale",
    align: "center"
  },
  {
    id: "screen",
    disablePadding: false,
    label: "Screen"
  }
];

const Strings = ({ deleteData, strings, auth }) => {
  const deleteHandler = id => {
    deleteData("strings", id);
  };

  const filteredData = useMemo(() => {
    if (auth.user.roleAlias !== TRANSLATOR) {
      return strings;
    }

    const availableLocales =
      (auth.user.available && auth.user.available.locales) || {};

    return strings.filter(str => availableLocales[str.locale]);
  }, [auth, strings]);

  // Add field languageDisplayName for each string
  // to show in table
  // const formattedData = useMemo(
  //   () =>
  //     _.map(filteredData, str => {
  //       const locale = _.find(locales, { code: str.locale });

  //       return { ...str, languageDisplayName: locale && locale.displayName };
  //     }),
  //   [filteredData, locales]
  // );

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/strings/create"
      >
        Create string
      </Button>

      <MaterialTable
        type="string"
        data={filteredData}
        rows={rows}
        rowWithName="stringValue"
        deleteHandler={
          auth.user.roleAlias !== TRANSLATOR ? deleteHandler : undefined
        }
        editLink="/admin/strings/edit"
      />
    </div>
  );
};

const mapStateToProps = ({ auth }) => ({ auth });

Strings.propTypes = {
  auth: PropTypes.object.isRequired,
  strings: PropTypes.array.isRequired,
  fetchData: PropTypes.func.isRequired
};

export default compose(
  withData(["strings"]),
  connect(mapStateToProps)
)(Strings);
