import React from 'react';
import PropTypes from 'prop-types';

import StringForm from './StringForm/StringForm';

const CreateString = ({ history }) => {
  return <StringForm onSuccess={history.goBack} />;
};

CreateString.propTypes = {
  history: PropTypes.object.isRequired
};

export default CreateString;
