import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import StringForm from './StringForm/StringForm';
import { withData } from 'components/hocs';

const EditString = ({ match, strings }) => {
  const dataToEdit = useMemo(() => find(strings, { _id: match.params.id }), [
    strings,
    match.params.id
  ]);

  return dataToEdit ? (
    <StringForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/strings" />
  );
};

EditString.propTypes = {
  strings: PropTypes.array.isRequired
};

export default withData(['strings'])(EditString);
