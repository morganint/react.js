import React, { useState, useMemo } from "react";
import PropTypes from "prop-types";
import { Field, reduxForm, SubmissionError } from "redux-form";
import { connect } from "react-redux";
import { compose } from "redux";

import { addData, editData } from "actions/adminActions";
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons,
  renderSelect,
  getOptionsFromLists,
  FieldWysiwyg
} from "components/form-components";
import { FlexContainer } from "components/common";
import { withData } from "components/hocs";
import { reduxFormValidator } from "validation";
import { TRANSLATOR } from "consts/user-roles";
import { formatLocalesToOptions } from "utils";

const requiredFields = ["stringValue", "stringNumber", "locale"];

const validate = reduxFormValidator(requiredFields);

const StringForm = ({
  user,
  isEdit,
  editData,
  addData,
  onSuccess,
  error,
  handleSubmit,
  pristine,
  reset,
  lists,
  initialValues,
  getOptionsFromLists,
  submitting,
  locales
}) => {
  const [success, setSuccess] = useState(false);

  const fieldsWithListValues = useMemo(
    () =>
      getOptionsFromLists(
        [
          {
            name: "stringCategory",
            list: "String Categories"
          }
        ],
        lists,
        "StringForm",
        initialValues
      ),
    [getOptionsFromLists, initialValues, lists]
  );

  const submitHandler = async (values, dispatch) => {
    const result = isEdit
      ? await editData("strings", values)
      : await addData("strings", values);

    if (!result.success) {
      setSuccess(false);
      throw new SubmissionError(result.err);
    } else {
      setSuccess(true);
      onSuccess && onSuccess();
    }
  };

  const successMessage = isEdit
    ? "String updated successfully"
    : "String created";

  const localesOptions = useMemo(() => {
    let filteredLocales = locales;

    if (user.roleAlias === TRANSLATOR) {
      const availableLocales = (user.available && user.available.locales) || {};

      filteredLocales = locales.filter(loc => availableLocales[loc.code]);
    }

    return formatLocalesToOptions(filteredLocales);
  }, [locales, user]);

  return (
    <form onSubmit={handleSubmit(submitHandler)}>
      <Field
        name="stringValue"
        label="String Value *"
        component={renderTextField}
      />

      <FieldWysiwyg
        name="prettyValue"
        label="Pretty Value"
        formName="StringForm"
      />

      <FlexContainer xs="50%">
        <Field
          name="stringCategory"
          label="String Category"
          component={renderSelect}
          options={
            fieldsWithListValues.filter(
              field => field.name === "stringCategory"
            )[0].options || []
          }
          isClearable
        />

        <Field
          name="stringNumber"
          type="number"
          label="String Number *"
          component={renderTextField}
        />
      </FlexContainer>

      <FlexContainer xs="50%">
        <Field
          name="locale"
          label="Language *"
          component={renderSelect}
          options={localesOptions || []}
          isClearable
        />

        <Field name="screen" label="Screen" component={renderTextField} />
      </FlexContainer>

      {!pristine && error && <StyledError>{error}</StyledError>}
      {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

      <FormButtons
        pristine={pristine}
        submitting={submitting}
        isEdit={isEdit}
        itemName="string"
        handleReset={reset}
      />
    </form>
  );
};

StringForm.propTypes = {
  user: PropTypes.object.isRequired,
  locales: PropTypes.array.isRequired,
  lists: PropTypes.array.isRequired,
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  fetchData: PropTypes.func.isRequired,
  getOptionsFromLists: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth: { user } }) => ({ user });

export default compose(
  withData(["locales", "lists"]),
  reduxForm({
    form: "StringForm",
    validate,
    enableReinitialize: true
  }),
  connect(mapStateToProps, { addData, editData, getOptionsFromLists })
)(StringForm);
