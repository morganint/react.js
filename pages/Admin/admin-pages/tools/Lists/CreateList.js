import React from 'react';
import PropTypes from 'prop-types';

import ListForm from './ListForm/ListForm';

const CreateList = ({ history }) => {
  return <ListForm onSuccess={history.goBack} />;
};

CreateList.propTypes = {
  history: PropTypes.object.isRequired
};

export default CreateList;
