import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'values',
    disablePadding: false,
    label: 'Values'
  },
  {
    id: 'default',
    disablePadding: false,
    label: 'Default Item'
  }
];

const Lists = ({ lists, deleteData }) => {
  const deleteHandler = id => {
    deleteData('lists', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/lists/create"
      >
        Create List
      </Button>

      <MaterialTable
        type="list"
        data={lists}
        rowWithName="name"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/lists/edit"
      />
    </div>
  );
};

Lists.propTypes = {
  lists: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['lists'])(Lists);
