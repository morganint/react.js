import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import ListForm from './ListForm/ListForm';
import { withData } from 'components/hocs';

const EditList = ({ match, lists }) => {
  const dataToEdit = useMemo(() => find(lists, { _id: match.params.id }), [
    lists,
    match.params.id
  ]);

  return dataToEdit ? (
    <ListForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/lists" />
  );
};

EditList.propTypes = {
  lists: PropTypes.array.isRequired
};

export default withData(['lists'])(EditList);
