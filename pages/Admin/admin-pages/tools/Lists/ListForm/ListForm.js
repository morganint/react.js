import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { addData, editData } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons,
  StringValues
} from 'components/form-components';
import { reduxFormValidator } from 'validation';

const requiredFields = ['name', 'values', 'default', 'description'];

const validate = reduxFormValidator(requiredFields);

const ListForm = ({
  isEdit,
  editData,
  addData,
  onSuccess,
  error,
  handleSubmit,
  pristine,
  reset,
  submitting
}) => {
  const [success, setSuccess] = useState(false);

  const submitHandler = async (values, dispatch) => {
    const result = isEdit
      ? await editData('lists', values)
      : await addData('lists', values);

    if (!result.success) {
      setSuccess(false);
      throw new SubmissionError(result.err);
    } else {
      setSuccess(true);
      onSuccess && onSuccess();
    }
  };

  const successMessage = isEdit ? 'List updated successfully' : 'List created';

  return (
    <form onSubmit={handleSubmit(submitHandler)}>
      <Field
        disabled={isEdit}
        name="name"
        label="List Name *"
        component={renderTextField}
      />

      <Field
        name="description"
        label="Description *"
        component={renderTextField}
      />

      <StringValues
        name="values"
        label="Values *"
        defaultSelect={{
          name: 'default',
          label: 'Default *',
          form: 'ListForm'
        }}
      />

      {!pristine && error && <StyledError>{error}</StyledError>}
      {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

      <FormButtons
        pristine={pristine}
        submitting={submitting}
        isEdit={isEdit}
        itemName="list"
        handleReset={reset}
      />
    </form>
  );
};

ListForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

export default compose(
  reduxForm({
    form: 'ListForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    null,
    { addData, editData }
  )
)(ListForm);
