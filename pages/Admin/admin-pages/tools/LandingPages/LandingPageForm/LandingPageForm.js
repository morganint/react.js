import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
  SubmissionError,
  reset,
  formValueSelector,
} from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';
import Button from '@material-ui/core/Button';

import API from 'api';
import {
  addData,
  editData,
  getLearningCenterPlansStatus,
} from 'actions/adminActions';
import { FlexContainer } from 'components/common';
import {
  CheckboxFieldsGroup,
  formatCheckboxesInArray,
  renderSelect,
  renderCheckbox,
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons,
} from 'components/form-components';
import { reduxFormValidator } from 'validation';
import { formatLocalesToOptions } from 'utils';
import { withData } from 'components/hocs';
import CodesTable from './CodesTable/CodesTable';

const requiredFields = [
  'name',
  'learningCenter',
  'urls',
  'product',
  'adultReportSolution',
  'childReportSolution',
  'paymentServiceName',
  'instructionStringNumber',
  'emailStringNumber',
  'emailLocale',
];

const BRAINTREE = 'braintree';
const SHOPIFY = 'shopify';
const FREE = 'free';

const paymentOptions = [
  { label: 'Braintree', value: BRAINTREE },
  { label: 'Shopify', value: SHOPIFY },
  { label: 'Free (by code)', value: FREE },
];

const validate = reduxFormValidator(requiredFields);

class LandingPageForm extends Component {
  state = { success: false, resourcesOptions: [], imagesOptions: [] };

  componentDidMount() {
    this.fetchResources();
    this.fetchHeroImages();
    this.onLearningCenterChange();
    this.onProductChange();
    this.onPaymentServiceChange();

    const { learningCenters, locales } = this.props;
    const learningCentersOptions = learningCenters.map((l) => ({
      label: l.name,
      value: l._id,
    }));

    const localesOptions = formatLocalesToOptions(locales);

    this.setState({ learningCentersOptions, localesOptions });
  }

  componentDidUpdate(prevProps) {
    const {
      selectedLearningCenter,
      selectedProduct,
      selectedPaymentServiceName,
    } = this.props;

    if (prevProps.selectedLearningCenter !== selectedLearningCenter) {
      this.onLearningCenterChange();
    }

    if (prevProps.selectedProduct !== selectedProduct) {
      this.onProductChange();
    }

    if (prevProps.selectedPaymentServiceName !== selectedPaymentServiceName) {
      this.onPaymentServiceChange();
    }
  }

  fetchResources = async () => {
    const result = await API.storage.listFiles('Files/resources', false);

    const resourcesOptions = (result || []).map((obj) => ({
      label: obj.unprefixed,
      value: obj.url,
    }));
    this.setState({ resourcesOptions });
  };

  fetchHeroImages = async () => {
    const result = await API.storage.listFiles(
      'Images/Images/LandingPage/Hero/',
      false
    );

    const imagesOptions = (result || []).map((obj) => ({
      label: obj.unprefixed,
      value: obj.url,
    }));
    this.setState({ imagesOptions });
  };

  getShopifyProducts = async () => {
    const result = await API.purchase.getShopifyProducts();

    const shopifyProductOptions = result.data.map((p) => ({
      label: p.title,
      value: p.handle,
    }));

    this.setState({ shopifyProductOptions });
  };

  getBraintreePlans = async () => {
    const result = await API.purchase.getBraintreePlans();

    const braintreePlansOptions = result.data.map((p) => ({
      label: p.name,
      value: p.id,
    }));

    this.setState({ braintreePlansOptions });
  };

  onLearningCenterChange = async () => {
    const {
      change,
      selectedProduct,
      selectedLearningCenter,
      getLearningCenterPlansStatus,
    } = this.props;

    if (!selectedLearningCenter) {
      change('product', '');
      return this.setState({ productsOptions: [] });
    }

    // Fetch products
    const plansStatus = await getLearningCenterPlansStatus(
      selectedLearningCenter
    );

    if (!plansStatus) {
      return change('learningCenter', '');
    }

    const availableProducts = plansStatus.purchasedProducts.map(
      (p) => p.productId
    );

    if (!availableProducts.includes(selectedProduct)) {
      change('product', '');
    }

    const result = await API.products.getList({
      _id: { in: availableProducts },
    });

    const productsOptions = result.data.map((l) => ({
      label: l.name,
      value: l._id,
    }));

    this.setState({ productsOptions });
  };

  onProductChange = async () => {
    const { change, selectedProduct } = this.props;

    if (!selectedProduct) {
      change('adultReportSolution', '');
      change('childReportSolution', '');
      return this.setState({ solutionsOptions: [] });
    }

    const result = await API.products.getProductSolutionsList(selectedProduct);

    const solutionsOptions = result.data.map((l) => ({
      label: l.displayName,
      value: l._id,
    }));

    this.setState({ solutionsOptions });
  };

  onPaymentServiceChange = () => {
    const { selectedPaymentServiceName, change, codeAccess } = this.props;
    const { shopifyProductOptions, braintreePlansOptions } = this.state;

    if (selectedPaymentServiceName !== BRAINTREE) {
      change('isLevelSubscription', false);
      change('levelSubscriptionPlans', []);
    }

    if (selectedPaymentServiceName === BRAINTREE && !braintreePlansOptions) {
      this.getBraintreePlans();
    }

    if (selectedPaymentServiceName === SHOPIFY && !shopifyProductOptions) {
      this.getShopifyProducts();
    }

    if (selectedPaymentServiceName === FREE && !codeAccess) {
      change('codeAccess', true);
    }
  };

  deletePlanConfigItem = (idx) => {
    const { change, levelSubscriptionPlans } = this.props;
    change('levelSubscriptionPlans', [
      ...levelSubscriptionPlans.slice(0, idx),
      ...levelSubscriptionPlans.slice(idx + 1),
    ]);
  };

  addPlanConfigItem = () => {
    const { change, levelSubscriptionPlans } = this.props;
    change('levelSubscriptionPlans', [...levelSubscriptionPlans, {}]);
  };

  addLink = () => {
    const { change, links } = this.props;
    change('links', [...(links || []), {}]);
  };

  deleteLink = (idx) => {
    const { change, links } = this.props;
    change('links', [...links.slice(0, idx), ...links.slice(idx + 1)]);
  };

  submit = async (values, dispatch) => {
    const {
      isEdit,
      editData,
      addData,
      onSuccess,
      isLevelSubscription,
      levelSubscriptionPlans,
    } = this.props;

    const errors = {};

    if (isLevelSubscription) {
      // Validate subscription plans
      const required = ['id', 'planId', 'label', 'initialLevels', 'nextLevels'];

      levelSubscriptionPlans.forEach((p, idx) => {
        required.forEach((field) => {
          if (!p[field]) {
            _.set(
              errors,
              `levelSubscriptionPlans[${idx}][${field}]`,
              'Required'
            );
          }
        });
      });
    }

    if (Object.keys(errors).length) {
      throw new SubmissionError(errors);
    }

    const formattedValues = JSON.parse(JSON.stringify(values));

    formattedValues.locales = [
      ...formatCheckboxesInArray(formattedValues.locales),
    ];

    const result = isEdit
      ? await editData('purchaseConfigs', formattedValues)
      : await addData('purchaseConfigs', formattedValues);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('LandingPageForm'));
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  changeBraintreeMethodHandler = (e, value, prevValue, name) => {
    if (value === false) {
      return;
    }

    const offProps = [];

    if (name === 'isLevelSubscription') {
      offProps.push(['isSubscription', false]);
      offProps.push(['braintreeSubscriptionPlanId', '']);
      offProps.push(['price', '']);
    }

    if (name === 'isSubscription') {
      offProps.push(['isLevelSubscription', false]);
      offProps.push(['levelSubscriptionPlans', []]);
      offProps.push(['price', '']);
    }

    offProps.forEach((p) => this.props.change(p[0], p[1]));
  };

  changePrice = () => {
    const {
      change,
      isSubscription,
      isLevelSubscription,
      braintreeSubscriptionPlanId,
      levelSubscriptionPlans,
    } = this.props;

    isSubscription && change('isSubscription', false);
    isLevelSubscription && change('isLevelSubscription', false);
    braintreeSubscriptionPlanId && change('braintreeSubscriptionPlanId', '');
    levelSubscriptionPlans &&
      levelSubscriptionPlans.length &&
      change('levelSubscriptionPlans', []);
  };

  render() {
    const {
      initialValues,
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      codes,
      links,
      isEdit,
      codeAccess,
      isSubscription,
      isLevelSubscription,
      levelSubscriptionPlans,
      selectedPaymentServiceName,
    } = this.props;

    const {
      success,
      learningCentersOptions,
      productsOptions,
      solutionsOptions,
      localesOptions,
      shopifyProductOptions,
      braintreePlansOptions,
      resourcesOptions,
      imagesOptions,
    } = this.state;

    const successMessage = isEdit
      ? 'Landing Page updated successfully'
      : 'Landing Page created';

    const isBraintreeOneTimePurchase =
      selectedPaymentServiceName === BRAINTREE &&
      !isLevelSubscription &&
      !isSubscription;

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field
          name="name"
          label="Landing Page Name *"
          component={renderTextField}
        />

        <Field
          name="urls"
          // disabled
          label="URLs (comma separated) *"
          component={renderTextField}
        />

        <FlexContainer md="50%">
          <Field
            name="learningCenter"
            options={learningCentersOptions || []}
            label="Learning Center *"
            component={renderSelect}
          />

          <Field
            name="product"
            options={productsOptions || []}
            label="Product *"
            component={renderSelect}
          />
        </FlexContainer>

        <FlexContainer md="50%">
          <Field
            name="adultReportSolution"
            options={solutionsOptions || []}
            label="Adult Report Solution *"
            component={renderSelect}
          />

          <Field
            name="childReportSolution"
            options={solutionsOptions || []}
            label="Child Report Solution *"
            component={renderSelect}
          />
        </FlexContainer>

        <FlexContainer md="25%">
          <Field
            name="emailLocale"
            label="Email Locale *"
            options={localesOptions || []}
            component={renderSelect}
          />
          <Field
            name="emailStringNumber"
            label="Email String Number *"
            type="number"
            component={renderTextField}
          />
          <Field
            name="instructionStringNumber"
            label="Instructions String Number *"
            type="number"
            component={renderTextField}
          />
          <Field
            name="sibContactsList"
            label="SendinBlue List ID"
            type="number"
            component={renderTextField}
          />
        </FlexContainer>

        <Field
          name="facebookTracking"
          label="Facebook Tracking"
          component={renderCheckbox}
        />
        <Field name="withLogin" label="With Login" component={renderCheckbox} />

        <CheckboxFieldsGroup
          name="locales"
          fields={[
            { label: 'English', value: 'en' },
            { label: 'Spanish', value: 'es' },
            { label: 'Kiswahili', value: 'sw' },
          ]}
          groupLabel={'Locales'}
          initialValues={initialValues}
          form="LandingPageForm"
          defaultSelect={{
            name: 'defaultLocale',
            label: 'Default locale *',
          }}
        />

        <Field
          name="codeAccess"
          label="Access by code"
          component={renderCheckbox}
        />

        {codeAccess && (
          <CodesTable
            value={codes}
            onChange={(value) => this.props.change('codes', value)}
          />
        )}

        <FlexContainer md="33%">
          <Field
            name="paymentServiceName"
            options={paymentOptions}
            label="Payment Service *"
            component={renderSelect}
          />

          {selectedPaymentServiceName === SHOPIFY && (
            <>
              <Field
                name="paymentServiceProduct"
                options={shopifyProductOptions || []}
                label="Shopify Product Handle *"
                component={renderSelect}
              />

              <Field
                name="discount"
                label="Discount Code"
                component={renderTextField}
              />
            </>
          )}
        </FlexContainer>

        {selectedPaymentServiceName === BRAINTREE && (
          <>
            <Field
              name="isSubscription"
              label="Is subscription"
              onChange={this.changeBraintreeMethodHandler}
              component={renderCheckbox}
            />
            <Field
              name="isLevelSubscription"
              label="Is level subscription"
              onChange={this.changeBraintreeMethodHandler}
              component={renderCheckbox}
            />
          </>
        )}

        {isBraintreeOneTimePurchase && (
          <Field
            width="150px"
            name="price"
            type="number"
            step="0.01"
            min="0.01"
            label="Price"
            onChange={this.changePrice}
            component={renderTextField}
          />
        )}

        {isSubscription && (
          <Field
            name="braintreeSubscriptionPlanId"
            label="Braintree Plan ID"
            component={renderSelect}
            options={braintreePlansOptions || []}
          />
        )}

        {isLevelSubscription && (
          <div>
            {levelSubscriptionPlans.map((item, idx) => (
              <FlexContainer key={idx} xs="auto">
                <Field
                  width="15%"
                  name={`levelSubscriptionPlans[${idx}][label]`}
                  label="Label"
                  component={renderTextField}
                />

                <Field
                  width="15%"
                  name={`levelSubscriptionPlans[${idx}][id]`}
                  label="ID"
                  component={renderTextField}
                />

                <Field
                  width="15%"
                  name={`levelSubscriptionPlans[${idx}][planId]`}
                  label="Braintree Plan ID"
                  component={renderSelect}
                  options={braintreePlansOptions || []}
                />

                <Field
                  width="10%"
                  name={`levelSubscriptionPlans[${idx}][initialLevels]`}
                  label="Initial Levels"
                  component={renderTextField}
                />

                <Field
                  width="20%"
                  name={`levelSubscriptionPlans[${idx}][nextLevels]`}
                  label="Next Levels"
                  component={renderTextField}
                />

                <Button
                  style={{ margin: 0 }}
                  type="button"
                  onClick={() => this.deletePlanConfigItem(idx)}
                >
                  Delete
                </Button>
              </FlexContainer>
            ))}

            <Button
              variant="contained"
              color="primary"
              type="button"
              onClick={this.addPlanConfigItem}
            >
              Add
            </Button>
          </div>
        )}

        <div style={{ marginTop: 30 }}>
          <FlexContainer xs="auto" md="50%" lg="33%">
            <Field
              name={`assets[hero]`}
              label="Landing Page Hero:"
              component={renderSelect}
              options={imagesOptions}
            />
            <Field
              name={`assets[videoPoster]`}
              label="Video Poster:"
              component={renderSelect}
              options={imagesOptions}
            />
          </FlexContainer>
        </div>

        <p style={{ marginTop: 30, fontSize: 16 }}>
          <b>Footer Links:</b>
        </p>
        {(links || []).map((item, idx) => (
          <FlexContainer key={idx} xs="auto">
            <Field
              width="25%"
              name={`links[${idx}][text]`}
              label="Text"
              component={renderTextField}
            />

            <Field
              width="15%"
              name={`links[${idx}][isCustom]`}
              label="Custom"
              component={renderCheckbox}
            />

            {item.isCustom ? (
              <Field
                width="40%"
                name={`links[${idx}][url]`}
                label="URL"
                component={renderTextField}
              />
            ) : (
              <Field
                width="40%"
                name={`links[${idx}][url]`}
                label="Resource"
                component={renderSelect}
                options={resourcesOptions}
              />
            )}

            <Button
              style={{ margin: 0 }}
              type="button"
              onClick={() => this.deleteLink(idx)}
            >
              Delete
            </Button>
          </FlexContainer>
        ))}

        <Button
          variant="contained"
          color="primary"
          type="button"
          onClick={this.addLink}
        >
          Add
        </Button>

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="Landing Page"
          handleReset={reset}
        />
      </form>
    );
  }
}

LandingPageForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  learningCenters: PropTypes.array.isRequired,
};

const selector = formValueSelector('LandingPageForm');

const mapStateToProps = (state) => ({
  locales: state.common.locales,
  codes: selector(state, 'codes'),
  codeAccess: selector(state, 'codeAccess'),
  links: selector(state, 'links'),
  levelSubscriptionPlans: selector(state, 'levelSubscriptionPlans'),
  isSubscription: selector(state, 'isSubscription'),
  isLevelSubscription: selector(state, 'isLevelSubscription'),
  selectedProduct: selector(state, 'product'),
  selectedLearningCenter: selector(state, 'learningCenter'),
  selectedPaymentServiceName: selector(state, 'paymentServiceName'),
  braintreeSubscriptionPlanId: selector(state, 'braintreeSubscriptionPlanId'),
});

export default compose(
  withData(['learningCenters']),
  reduxForm({
    form: 'LandingPageForm',
    validate,
    enableReinitialize: true,
  }),
  connect(mapStateToProps, { addData, editData, getLearningCenterPlansStatus })
)(LandingPageForm);
