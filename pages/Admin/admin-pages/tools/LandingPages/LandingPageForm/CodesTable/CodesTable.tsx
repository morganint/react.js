import React, { useState, useRef, useMemo } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { MaterialFaIcon } from 'utils';
import { Spinner } from 'components/common';
import { ConfirmDeleteModal } from 'components/admin';
import API from 'api';
import AddCodesDialog from './AddCodesDialog';
import SelectExport from './SelectExport';

export interface Code {
  value: string;
  usedDate: string | undefined;
  usedByEmail: string | undefined;
}

export interface CodeSet {
  _id: string;
  title: string;
  codes: Code[];
}

const useStyles = makeStyles({
  wrapper: {
    marginBottom: 40,
  },
  tableWrapper: {
    marginBottom: 20,
  },
  table: {
    maxWidth: 800,
  },
});

const CodesTable: React.FC<{
  value: CodeSet[] | null;
  onChange: (codes: CodeSet[]) => void;
}> = ({ value: codeSets, onChange }) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [showExport, setShowExport] = useState<boolean>(false);
  const [addDialogOpened, setAddDialogOpened] = useState<boolean>(false);
  const [confirmDeleteOpened, setConfirmDeleteOpened] = useState<boolean>(
    false
  );
  const classes = useStyles();

  const codeSetToDelete = useRef<CodeSet | null>(null);
  const codeSetToExport = useRef<CodeSet | null>(null);

  const closeAddDialogHandler = () => {
    setAddDialogOpened(false);
  };

  const addHandlerStart = () => {
    setAddDialogOpened(true);
  };

  const generateCodesHandler = async (title: string, number: number) => {
    try {
      setIsLoading(true);
      // Generate codes
      const result = await API.purchase.generateCodes(title, number);

      const modifiedValue = [...(codeSets || []), result.data];
      onChange(modifiedValue);
      setAddDialogOpened(false);
    } catch (err) {
      alert('Server error');
    }

    setIsLoading(false);
  };

  const exportHandler = (row: CodeSet) => {
    codeSetToExport.current = row;
    setShowExport(true);
  };

  const closeExport = () => {
    codeSetToExport.current = null;
    setShowExport(false);
  };

  const deleteHandlerStart = (row: CodeSet) => {
    codeSetToDelete.current = row;
    setConfirmDeleteOpened(true);
  };

  const closeConfirmDeleteHandler = () => {
    codeSetToDelete.current = null;
    setConfirmDeleteOpened(false);
  };

  const deleteHandler = () => {
    if (!codeSetToDelete.current) {
      return closeConfirmDeleteHandler();
    }

    const modifiedValue = codeSets!.filter(
      (s) => s._id !== codeSetToDelete.current!._id
    );

    onChange(modifiedValue);
    closeConfirmDeleteHandler();
  };

  const numbers = useMemo(() => {
    const numbers: any = {};

    (codeSets || []).forEach((set) => {
      let all = 0,
        used = 0,
        notUsed = 0;

      set.codes.forEach((code) => {
        all++;
        !!code.usedDate ? used++ : notUsed++;
      });

      numbers[set._id] = { all, used, notUsed };
    });

    return numbers;
  }, [codeSets]);

  let content = <p>No codes</p>;

  if (codeSets && codeSets.length) {
    content = (
      <TableContainer className={classes.tableWrapper}>
        <Table className={classes.table} size="small">
          <TableHead>
            <TableRow>
              <TableCell>Title</TableCell>
              <TableCell align="center">Number</TableCell>
              <TableCell align="center">Used</TableCell>
              <TableCell align="center">Not Used</TableCell>
              <TableCell align="right"></TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {codeSets.map((row, idx) => (
              <TableRow key={row._id}>
                <TableCell>{row.title}</TableCell>
                <TableCell align="center">{numbers[row._id].all}</TableCell>
                <TableCell align="center">{numbers[row._id].used}</TableCell>
                <TableCell align="center">{numbers[row._id].notUsed}</TableCell>
                <TableCell align="right">
                  <Tooltip title="Export">
                    <IconButton
                      aria-label="Export"
                      onClick={exportHandler.bind(null, row)}
                    >
                      <MaterialFaIcon icon="download" />
                    </IconButton>
                  </Tooltip>

                  <Tooltip title="Delete">
                    <IconButton
                      aria-label="Delete"
                      onClick={deleteHandlerStart.bind(null, row)}
                    >
                      <MaterialFaIcon icon="trash-alt" />
                    </IconButton>
                  </Tooltip>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  const confirmDeleteProps = {
    text: 'codes',
    isOpen: confirmDeleteOpened,
    handleClose: closeConfirmDeleteHandler,
    handleDelete: deleteHandler,
  };

  return (
    <>
      {isLoading && <Spinner uploading={null} />}

      {codeSetToExport.current && (
        <SelectExport
          codeSet={codeSetToExport.current}
          open={showExport}
          onClose={closeExport}
        />
      )}

      <ConfirmDeleteModal {...confirmDeleteProps} />

      <AddCodesDialog
        open={addDialogOpened}
        addHandler={generateCodesHandler}
        closeHandler={closeAddDialogHandler}
      />

      <div className={classes.wrapper}>
        {content}

        <Button variant="contained" onClick={addHandlerStart}>
          Add Codes
        </Button>
      </div>
    </>
  );
};

export default CodesTable;
