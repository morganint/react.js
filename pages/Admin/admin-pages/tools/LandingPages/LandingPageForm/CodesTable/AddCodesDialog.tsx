import React, { useRef, useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles({
  titleInput: {
    width: 200,
    marginRight: 30,
  },
  numberInput: {
    width: 100,
  },
  inputsWrapper: {
    marginBottom: 40,
  },
  error: {
    color: 'red',
  },
});

const AddCodesDialog: React.FC<{
  open: boolean;
  addHandler: (title: string, number: number) => void;
  closeHandler: () => void;
}> = ({ open, addHandler, closeHandler }) => {
  const [error, setError] = useState<string>('');
  const classes = useStyles();

  const titleInputRef = useRef<HTMLInputElement>(null);
  const numberInputRef = useRef<HTMLInputElement>(null);

  const submitHandler = () => {
    let title: string = titleInputRef.current!.value.trim();
    let numberString: string = numberInputRef.current!.value;

    if (!title || !numberString || isNaN(+numberString) || +numberString <= 0) {
      return setError('Please enter a valid values');
    }

    if (error) setError('');

    const number = +numberString;

    addHandler(title, number);
  };

  return (
    <Dialog open={open} onClose={closeHandler}>
      <DialogTitle>Generate new codes</DialogTitle>
      <DialogContent>
        <div className={classes.inputsWrapper}>
          <TextField
            inputRef={titleInputRef}
            label="Title"
            classes={{ root: classes.titleInput }}
            autoFocus
          />

          <TextField
            inputRef={numberInputRef}
            label="Number"
            type="number"
            inputProps={{ min: 1 }}
            classes={{ root: classes.numberInput }}
          />

          {error && <p className={classes.error}>{error}</p>}
        </div>
      </DialogContent>

      <DialogActions>
        <Button onClick={closeHandler}>Cancel</Button>
        <Button onClick={submitHandler} color="primary">
          Generate
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddCodesDialog;
