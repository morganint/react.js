import React from 'react';
import { CsvBuilder } from 'filefy';

import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';

import { CodeSet } from './CodesTable';

const useStyles = makeStyles({
  list: { textAlign: 'center' },
});

const SelectExport: React.FC<{
  onClose: () => void;
  open: boolean;
  codeSet: CodeSet;
}> = ({ onClose, open, codeSet }) => {
  const classes = useStyles();

  const handleListItemClick = (type: string) => {
    // onClose(value);
    const filename = `${codeSet.title}_codes_${type}.csv`;

    const builder = new CsvBuilder(filename);

    if (type === 'not-used') {
      const filteredCodes = codeSet.codes.filter((c) => !c.usedDate);

      builder.setColumns(['code']).addRows(filteredCodes.map((c) => [c.value]));
    } else {
      builder.setColumns(['code', 'used at', 'email']);

      const filteredCodes =
        type === 'used'
          ? codeSet.codes.filter((c) => !!c.usedDate)
          : codeSet.codes;

      builder.addRows(
        filteredCodes.map((c) => [
          c.value,
          c.usedDate ? new Date(c.usedDate).toLocaleDateString() : 'Not used',
          c.usedByEmail,
        ])
      );
    }

    builder.exportFile();
  };

  return (
    <Dialog onClose={onClose} open={open}>
      <DialogTitle>Select export</DialogTitle>

      <List className={classes.list}>
        <ListItem button onClick={() => handleListItemClick('all')}>
          <ListItemText primary={`All Codes`} />
        </ListItem>
        <ListItem button onClick={() => handleListItemClick('used')}>
          <ListItemText primary={`Used Codes`} />
        </ListItem>
        <ListItem button onClick={() => handleListItemClick('not-used')}>
          <ListItemText primary={`Not Used Codes`} />
        </ListItem>
      </List>
    </Dialog>
  );
};

export default SelectExport;
