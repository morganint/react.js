import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Name',
  },
  {
    id: 'urls',
    disablePadding: false,
    label: 'URLs',
  },
  {
    id: 'paymentServiceName',
    disablePadding: false,
    label: 'Behavior',
  },
];

const LandingPages = ({ purchaseConfigs, deleteData }) => {
  const deleteHandler = (id) => {
    deleteData('landing-pages', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/landing-pages/create"
      >
        Create Landing Page Config
      </Button>

      <MaterialTable
        type="landing page"
        data={purchaseConfigs}
        rowWithName="name"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/landing-pages/edit"
      />
    </div>
  );
};

LandingPages.propTypes = {
  purchaseConfigs: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired,
};

export default withData(['purchaseConfigs'])(LandingPages);
