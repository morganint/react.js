import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import { formatCheckboxesFromArray } from 'components/form-components';
import LandingPageForm from './LandingPageForm/LandingPageForm';
import { withData } from 'components/hocs';

const EditLandingPage = ({ match, purchaseConfigs }) => {
  const dataToEdit = useMemo(() => {
    const data = find(purchaseConfigs, { _id: match.params.id });
    if (!data) return null;

    const formattedData = JSON.parse(JSON.stringify(data));
    formattedData.locales = formatCheckboxesFromArray(formattedData.locales);

    return formattedData;
  }, [purchaseConfigs, match.params]);

  return dataToEdit ? (
    <LandingPageForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/landing-pages" />
  );
};

EditLandingPage.propTypes = {
  purchaseConfigs: PropTypes.array.isRequired,
};

export default withData(['purchaseConfigs'])(EditLandingPage);
