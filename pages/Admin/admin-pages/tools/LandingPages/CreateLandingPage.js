import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import LandingPageForm from './LandingPageForm/LandingPageForm';
import { setCurrentPage } from 'actions/statusActions';

const CreateLandingPage = ({ setCurrentPage, match, history }) => {
  useEffect(() => {
    setCurrentPage(match);
  });

  return <LandingPageForm onSuccess={history.goBack} />;
};

CreateLandingPage.propTypes = {
  setCurrentPage: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default connect(
  null,
  { setCurrentPage }
)(CreateLandingPage);
