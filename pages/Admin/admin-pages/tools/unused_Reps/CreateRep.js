import React from 'react';
import PropTypes from 'prop-types';

import RepForm from './RepForm/RepForm';

const CreateRep = ({ history }) => {
  return <RepForm onSuccess={history.goBack} />;
};

CreateRep.propTypes = {
  history: PropTypes.object.isRequired
};

export default CreateRep;
