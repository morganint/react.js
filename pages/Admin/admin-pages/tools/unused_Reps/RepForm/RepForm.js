import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import { addData, editData } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons,
  formatCheckboxesInArray,
  CheckboxFieldsGroup,
  DateField
} from 'components/form-components';
import { withData } from 'components/hocs';
import { REP } from 'consts/user-roles';
import { validateEmail } from 'validation';
import { reduxFormValidator } from 'validation';

const requiredFields = [
  'name',
  'phone',
  'username',
  'address',
  'expirationDate'
];

const validate = reduxFormValidator(requiredFields);

class RepForm extends Component {
  constructor(props) {
    super(props);

    const repRole = _.find(props.roles, { alias: REP });

    this.state = { success: false, repRole };
  }

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;
    const { repRole } = this.state;

    const formattedValues = _.cloneDeep(values);

    const { name } = formattedValues;

    const spaceIndx = name.indexOf(' ');

    const firstname = spaceIndx !== -1 ? name.substring(0, spaceIndx) : name;
    const lastname = spaceIndx !== -1 ? name.substring(spaceIndx + 1) : '';

    formattedValues.firstname = firstname;
    formattedValues.lastname = lastname;

    delete formattedValues.name;

    !isEdit && (formattedValues.roleId = repRole._id);

    formattedValues.products = [
      ...formatCheckboxesInArray(formattedValues.products)
    ];

    const result = isEdit
      ? await editData('users', formattedValues)
      : await addData('users', formattedValues);

    if (!result.success) {
      this.setState({ success: false });
      throw new SubmissionError(result.err);
    } else {
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      products,
      submitting,
      initialValues,
      isEdit
    } = this.props;

    const { success } = this.state;

    const successMessage = isEdit ? 'Rep updated successfully' : 'Rep created';

    const productsFields = products.map(prod => ({
      value: prod._id,
      label: prod.name
    }));

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field name="name" label="Name *" component={renderTextField} />

        <Field name="phone" label="Phone *" component={renderTextField} />

        <Field
          validate={validateEmail}
          name="username"
          label="Email *"
          component={renderTextField}
        />

        <Field name="address" label="Address *" component={renderTextField} />

        <DateField name={`expirationDate`} label={`Expiration Date *`} />

        <CheckboxFieldsGroup
          fields={productsFields}
          name="products"
          groupLabel="Products *"
          initialValues={initialValues}
          form="RepForm"
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="rep"
          handleReset={reset}
        />
      </form>
    );
  }
}

RepForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

export default compose(
  withData(['products', 'roles']),
  reduxForm({
    form: 'RepForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    null,
    { addData, editData }
  )
)(RepForm);
