import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

import { getReps } from 'utils';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'username',
    disablePadding: false,
    label: 'Email'
  }
];

const Reps = ({ deleteData, users, roles }) => {
  const deleteHandler = id => {
    deleteData('users', id);
  };

  const reps = useMemo(() => getReps(users, roles), [users, roles]);

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/reps/create"
      >
        Create rep
      </Button>

      <MaterialTable
        type="rep"
        data={reps}
        rowWithName="name"
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/reps/edit"
      />
    </div>
  );
};

Reps.propTypes = {
  users: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['users', 'roles'])(Reps);
