import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find, cloneDeep } from 'lodash';

import { formatCheckboxesFromArray } from 'components/form-components';
import RepForm from './RepForm/RepForm';
import { withData } from 'components/hocs';
import { getName } from 'utils';

const EditRep = ({ match, users }) => {
  const dataToEdit = useMemo(() => find(users, { _id: match.params.id }), [
    users
  ]);

  const { firstname, lastname } = dataToEdit;

  const formattedData = cloneDeep(dataToEdit);

  formattedData.name = getName(firstname, lastname);
  formattedData.products = formatCheckboxesFromArray(formattedData.products);

  return dataToEdit ? (
    <RepForm isEdit initialValues={formattedData} />
  ) : (
    <Redirect to="/admin/reps" />
  );
};

EditRep.propTypes = {
  users: PropTypes.array.isRequired
};

export default withData(['users'])(EditRep);
