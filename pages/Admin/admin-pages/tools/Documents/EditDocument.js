import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import DocumentForm from './DocumentForm/DocumentForm';
import { withData } from 'components/hocs';

const EditDocument = ({ match, documents }) => {
  const dataToEdit = useMemo(() => find(documents, { _id: match.params.id }), [
    documents,
    match.params.id
  ]);

  return dataToEdit ? (
    <DocumentForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/documents" />
  );
};

EditDocument.propTypes = {
  documents: PropTypes.array.isRequired
};

export default withData(['documents'])(EditDocument);
