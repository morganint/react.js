import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';
import { viewDocument, downloadDocument } from 'actions/adminActions';

const rows = [
  {
    id: 'documentName',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'category',
    disablePadding: false,
    label: 'Category'
  }
];

const Documents = ({
  deleteData,
  documents,
  viewDocument,
  downloadDocument
}) => {
  const deleteHandler = id => {
    deleteData('documents', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/documents/create"
      >
        Create document
      </Button>

      <MaterialTable
        type="document"
        data={documents}
        rows={rows}
        rowWithName="documentName"
        deleteHandler={deleteHandler}
        editLink="/admin/documents/edit"
        additionalButtons={[
          {
            title: 'View',
            icon: { type: 'fa', name: 'eye' },
            action: doc => viewDocument(doc.filename)
          },
          {
            title: 'Download',
            icon: { type: 'fa', name: 'arrow-alt-circle-down' },
            action: doc => downloadDocument(doc.filename)
          }
        ]}
      />
    </div>
  );
};

Documents.propTypes = {
  documents: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired,
  viewDocument: PropTypes.func.isRequired,
  downloadDocument: PropTypes.func.isRequired
};

export default compose(
  withData(['documents']),
  connect(
    null,
    { viewDocument, downloadDocument }
  )
)(Documents);
