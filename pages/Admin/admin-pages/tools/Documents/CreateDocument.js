import React from 'react';
import PropTypes from 'prop-types';

import DocumentForm from './DocumentForm/DocumentForm';

const CreateDocument = ({ history }) => {
  return <DocumentForm onSuccess={history.goBack} />;
};

CreateDocument.propTypes = {
  history: PropTypes.object.isRequired
};

export default CreateDocument;
