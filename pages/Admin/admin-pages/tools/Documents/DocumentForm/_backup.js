import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError, reset } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import { addDocument, editData, fetchData } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons,
  renderSelect,
  UploadField,
  getOptionsFromLists
} from 'components/form-components';

const validate = values => {
  const errors = {};

  const requiredFields = ['documentName', 'category'];

  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });

  return errors;
};

class DocumentForm extends Component {
  state = {
    success: false,
    fieldsWithListValues: [
      {
        name: 'category',
        list: 'Documents Categories'
      }
    ],
    listsLoaded: false
  };

  componentDidMount() {
    if (!this.props.lists) {
      this.props.fetchData('lists');
    } else {
      this.setState({
        listsLoaded: true,
        fieldsWithListValues: this.props.getOptionsFromLists(
          this.state.fieldsWithListValues,
          this.props.lists,
          'DocumentForm',
          this.props.initialValues
        )
      });
    }
  }

  componentDidUpdate() {
    if (!this.state.listsLoaded && this.props.lists) {
      this.setState({
        listsLoaded: true,
        fieldsWithListValues: this.props.getOptionsFromLists(
          this.state.fieldsWithListValues,
          this.props.lists,
          'DocumentForm',
          this.props.initialValues
        )
      });
    }
  }

  submit = async (values, dispatch) => {
    const { addDocument, onSuccess, editData, isEdit } = this.props;

    if (!isEdit && !values.file) {
      throw new SubmissionError({ file: 'required' });
    }

    const formData = new FormData();

    _.forEach(values, (val, key) => {
      formData.append(key, val);
    });

    const result = isEdit
      ? await editData('documents', formData)
      : await addDocument('documents', formData);

    if (!result.success) {
      this.setState({ success: false });

      console.log('Server response:', result);
      throw new SubmissionError(result.err);
    } else {
      dispatch(reset('DocumentForm'));
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      isEdit
    } = this.props;

    const { success, fieldsWithListValues } = this.state;

    const successMessage = isEdit
      ? 'Document updated successfully'
      : 'Document created';

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field
          name="documentName"
          label="Document Name *"
          component={renderTextField}
        />

        <Field
          name="documentDesc"
          label="Document Description"
          component={renderTextField}
        />

        <Field
          name="file"
          label="Add PDF File *"
          fileExtensions=".pdf"
          component={UploadField}
        />

        <Field
          name="category"
          label="Category *"
          component={renderSelect}
          options={
            fieldsWithListValues.filter(field => field.name === 'category')[0]
              .options || []
          }
          isClearable
        />

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="document"
          handleReset={reset}
        />
      </form>
    );
  }
}

DocumentForm.propTypes = {
  lists: PropTypes.array,
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addDocument: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  fetchData: PropTypes.func.isRequired,
  getOptionsFromLists: PropTypes.func.isRequired
};

const mapStateToProps = ({ admin }) => ({
  lists: admin.lists
});

export default compose(
  reduxForm({
    form: 'DocumentForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    mapStateToProps,
    { addDocument, fetchData, editData, getOptionsFromLists }
  )
)(DocumentForm);
