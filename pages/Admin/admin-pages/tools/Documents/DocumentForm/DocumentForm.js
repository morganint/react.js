import React, { useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError, change } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { addDocument, editDocument } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons,
  renderSelect,
  UploadField,
  getOptionsFromLists
} from 'components/form-components';
import { withData } from 'components/hocs';
import { reduxFormValidator } from 'validation';

const requiredFields = ['documentName', 'category', 'filename'];

const validate = reduxFormValidator(requiredFields);

const DocumentForm = ({
  change,
  error,
  handleSubmit,
  pristine,
  submitting,
  lists,
  initialValues,
  getOptionsFromLists,
  addDocument,
  onSuccess,
  editDocument,
  reset,
  isEdit
}) => {
  const [success, setSuccess] = useState(false);

  const fieldsWithListValues = useMemo(
    () =>
      getOptionsFromLists(
        [
          {
            name: 'category',
            list: 'Documents Categories'
          }
        ],
        lists,
        'DocumentForm',
        initialValues
      ),
    [getOptionsFromLists, initialValues, lists]
  );

  const submitHandler = async (values, dispatch) => {
    if (!isEdit && !values.file) {
      throw new SubmissionError({ file: 'required' });
    }

    const result = isEdit
      ? await editDocument(values)
      : await addDocument(values);

    if (!result.success) {
      setSuccess(false);

      if (result.err.code === 11000) {
        // Duplicate code error, means Name already used
        throw new SubmissionError({
          documentName: 'Name already exists'
        });
      }

      throw new SubmissionError(result.err);
    } else {
      setSuccess(true);
      onSuccess && onSuccess();
    }
  };

  const successMessage = isEdit
    ? 'Document updated successfully'
    : 'Document created';

  return (
    <form onSubmit={handleSubmit(submitHandler)}>
      <Field
        name="documentName"
        label="Document Name *"
        component={renderTextField}
        onChange={e => {
          change(
            'DocumentForm',
            'filename',
            e.target.value.trim().replace(/\s|\W/g, '_') + '.pdf'
          );
        }}
      />

      <Field
        name="documentDesc"
        label="Document Description"
        component={renderTextField}
      />

      <Field
        name="filename"
        component={renderTextField}
        disabled
        hidden
      />

      <Field
        name="file"
        label={isEdit ? 'Replace existing PDF File' : 'Add PDF File *'}
        fileExtensions=".pdf"
        component={UploadField}
      />

      <Field
        name="category"
        label="Category *"
        component={renderSelect}
        options={
          fieldsWithListValues.filter(field => field.name === 'category')[0]
            .options || []
        }
        isClearable
      />

      {!pristine && error && <StyledError>{error}</StyledError>}
      {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

      <FormButtons
        pristine={pristine}
        submitting={submitting}
        isEdit={isEdit}
        itemName="document"
        handleReset={reset}
      />
    </form>
  );
};

DocumentForm.propTypes = {
  lists: PropTypes.array.isRequired,
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addDocument: PropTypes.func.isRequired,
  editDocument: PropTypes.func.isRequired,
  getOptionsFromLists: PropTypes.func.isRequired
};

export default compose(
  withData(['lists']),
  reduxForm({
    form: 'DocumentForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    null,
    { addDocument, editDocument, getOptionsFromLists, change }
  )
)(DocumentForm);
