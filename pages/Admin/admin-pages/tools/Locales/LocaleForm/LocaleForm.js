import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { addData, editData } from 'actions/adminActions';
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  FormButtons
} from 'components/form-components';
import { reduxFormValidator } from 'validation';

const requiredFields = ['name', 'displayName', 'code'];

const validate = reduxFormValidator(requiredFields);

const LocaleForm = ({
  isEdit,
  editData,
  addData,
  onSuccess,
  error,
  handleSubmit,
  pristine,
  reset,
  submitting
}) => {
  const [success, setSuccess] = useState(false);

  const submitHandler = async (values, dispatch) => {
    const result = isEdit
      ? await editData('locales', values)
      : await addData('locales', values);

    if (!result.success) {
      setSuccess(false);
      throw new SubmissionError(result.err);
    } else {
      setSuccess(true);
      onSuccess && onSuccess();
    }
  };

  const successMessage = isEdit
    ? 'Locale updated successfully'
    : 'Locale created';

  return (
    <form onSubmit={handleSubmit(submitHandler)}>
      <Field name="name" label="Name *" component={renderTextField} />

      <Field name="descr" label="Description" component={renderTextField} />

      <Field
        name="displayName"
        label="Display Name *"
        component={renderTextField}
      />

      <Field name="code" label="Code *" component={renderTextField} />

      {!pristine && error && <StyledError>{error}</StyledError>}
      {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

      <FormButtons
        pristine={pristine}
        submitting={submitting}
        isEdit={isEdit}
        itemName="locale"
        handleReset={reset}
      />
    </form>
  );
};

LocaleForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

export default compose(
  reduxForm({
    form: 'LocaleForm',
    validate,
    enableReinitialize: true
  }),
  connect(
    null,
    { addData, editData }
  )
)(LocaleForm);
