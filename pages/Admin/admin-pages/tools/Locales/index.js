export { default as Locales } from './Locales';
export { default as EditLocale } from './EditLocale';
export { default as CreateLocale } from './CreateLocale';
