import React from 'react';
import PropTypes from 'prop-types';

import LocaleForm from './LocaleForm/LocaleForm';

const CreateLocale = ({ history }) => {
  return <LocaleForm onSuccess={history.goBack} />;
};

CreateLocale.propTypes = {
  history: PropTypes.object.isRequired
};

export default CreateLocale;
