import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'name',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'displayName',
    disablePadding: false,
    label: 'Display Name'
  }
];

const Locales = ({ locales, deleteData }) => {
  const deleteHandler = id => {
    deleteData('locales', id);
  };

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/locales/create"
      >
        Create locale
      </Button>

      {locales && (
        <MaterialTable
          type="locale"
          rowWithName="name"
          data={locales}
          rows={rows}
          deleteHandler={deleteHandler}
          editLink="/admin/locales/edit"
        />
      )}
    </div>
  );
};

Locales.propTypes = {
  locales: PropTypes.array.isRequired,
  deleteData: PropTypes.func.isRequired
};

export default withData(['locales'])(Locales);
