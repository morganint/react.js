import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import LocaleForm from './LocaleForm/LocaleForm';
import { withData } from 'components/hocs';

const EditLocale = ({ match, locales }) => {
  const dataToEdit = useMemo(() => find(locales, { _id: match.params.id }), [
    locales,
    match.params.id
  ]);

  return dataToEdit ? (
    <LocaleForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/locales" />
  );
};

EditLocale.propTypes = {
  locales: PropTypes.array.isRequired
};

export default withData(['locales'])(EditLocale);
