import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import RoleForm from './RoleForm/RoleForm';
import { withData } from 'components/hocs';

const EditRole = ({ match, roles }) => {
  const dataToEdit = find(roles, { _id: match.params.id });

  return dataToEdit ? (
    <RoleForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/roles" />
  );
};

EditRole.propTypes = {
  roles: PropTypes.array.isRequired
};

export default withData(['roles'])(EditRole);
