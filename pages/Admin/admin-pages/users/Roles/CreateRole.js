import React from 'react';

import RoleForm from './RoleForm/RoleForm';

const CreateRole = ({ history }) => {
  return <RoleForm onSuccess={history.goBack} />;
};

export default CreateRole;

