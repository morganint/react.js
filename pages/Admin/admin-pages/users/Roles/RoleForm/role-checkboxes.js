export const adminToolbar = [
  {
    groupLabel: 'Tools',
    groupName: 'tools',
    checkboxes: [
      { label: 'General', value: 'general' },
      { label: 'Documents', value: 'documents' },
      { label: 'Lists', value: 'lists' },
      { label: 'Locales', value: 'locales' },
      // { label: 'Reps', value: 'reps' },
      { label: 'Strings', value: 'strings' },
      { label: 'Scopes', value: 'scopes' },
      { label: 'Landing Pages', value: 'landingPages' },
    ],
  },
  {
    groupLabel: 'Flink Play',
    groupName: 'flinkPlay',
    checkboxes: [
      { label: 'Activity Templates', value: 'activityTemplates' },
      { label: 'Characters', value: 'characters' },
      { label: 'Global Buttons', value: 'globalButtons' },
      { label: 'Menus', value: 'menus' },
      { label: 'Themes', value: 'themes' },
      { label: 'Words', value: 'words' },
    ],
  },
  {
    groupLabel: 'Users',
    groupName: 'users',
    checkboxes: [
      { label: 'Users', value: 'users' },
      { label: 'Roles', value: 'roles' },
    ],
  },
  {
    groupLabel: 'Organizations',
    groupName: 'organizations',
    checkboxes: [
      { label: 'Master Organizations', value: 'masterOrganizations' },
      { label: 'Regions', value: 'regions' },
      { label: 'Learning Centers', value: 'learningCenters' },
      // { label: 'Teachers', value: 'teachers' }
    ],
  },
  {
    groupLabel: 'Solutions',
    groupName: 'solutions',
    checkboxes: [
      { label: 'Solutions', value: 'solutions' },
      { label: 'Products', value: 'products' },
    ],
  },
  {
    groupLabel: 'Reports',
    groupName: 'reports',
    checkboxes: [
      { label: 'By Organization', value: 'byOrganization' },
      { label: 'Cohort', value: 'cohort' },
      { label: 'Continuity', value: 'continuity' },
      { label: 'Revenue', value: 'revenue' },
      { label: 'Content Partners', value: 'contentPartners' },
    ],
  },
];

export const apps = [
  {
    checkboxes: [
      { label: 'FlinkMake', value: 'flinkMake' },
      { label: 'FlinkAdmin', value: 'flinkAdmin' },
    ],
  },
];

export const editActivitiesCheckboxes = [
  {
    groupLabel: 'Edit Activities Controls',
    groupName: 'editActivityControls',
    checkboxes: [
      { label: 'Edit Name', value: 'editName' },
      { label: 'Set Edit List', value: 'setEditList' },
      { label: 'Educational Categories', value: 'eduCategories' },
      { label: 'Help Activity', value: 'helpActivity' },
    ],
  },
  {
    groupLabel: 'Edit List Buttons',
    groupName: 'editListButtons',
    checkboxes: [
      { label: 'Add Activity', value: 'add' },
      { label: 'Copy Activity', value: 'copy' },
      { label: 'Author/Edit Activity', value: 'edit' },
      { label: 'Delete Activity', value: 'delete' },
      { label: 'Undo Delete Activity', value: 'undoDelete' },
      { label: 'Preview Activity', value: 'preview' },
      { label: 'Remove from Edit List', value: 'removeFromList' },
      { label: 'Remove All', value: 'removeAllFromList' },
    ],
  },
];

export const searchActivitiesCheckboxes = [
  { label: 'Move To My Edit List', value: 'move' },
  { label: 'Move All to My Edit List', value: 'moveAll' },
  { label: 'Copy to My Edit List', value: 'copy' },
  { label: 'Preview', value: 'preview' },
];
