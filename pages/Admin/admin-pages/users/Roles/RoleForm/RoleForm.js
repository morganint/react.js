import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Field,
  reduxForm,
  SubmissionError,
  formValueSelector,
  change
} from "redux-form";
import { connect } from "react-redux";
import { compose } from "redux";
import styled from "styled-components";

import { addData, editData, fetchData } from "actions/adminActions";
import {
  renderTextField,
  StyledError,
  StyledSuccess,
  CheckboxGroups,
  CheckboxFieldsGroup,
  FormButtons,
  renderCheckbox,
  renderRadioGroup
} from "components/form-components";
import { withData } from "components/hocs";
import { reduxFormValidator } from "validation";
import {
  adminToolbar,
  apps,
  editActivitiesCheckboxes,
  searchActivitiesCheckboxes
} from "./role-checkboxes";
const requiredFields = ["name", "redirect"];

const validate = reduxFormValidator(requiredFields);

// Static

class RoleForm extends Component {
  state = { success: false };

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess } = this.props;

    const formattedValues = { ...values };

    const result = isEdit
      ? await editData("roles", formattedValues)
      : await addData("roles", formattedValues);

    if (!result.success) {
      this.setState({ success: false });
      throw new SubmissionError(result.err);
    } else {
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  componentDidUpdate() {
    if (!this.props.flinkMake) {
      this.props.change("RoleForm", "permissions[flinkMake]", undefined);
    }
  }

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      isEdit,
      flinkMake: isFlinkMakeChecked,
      documents,
      initialValues = {},
      isEditActivitiesTabChecked,
      isSearchActivitiesTabChecked
    } = this.props;

    const { success } = this.state;

    let viewDocuments =
      documents &&
      documents.map(doc => ({
        value: doc._id,
        label: doc.documentName,
        category: doc.category
      }));

    const successMessage = isEdit
      ? "Role updated successfully"
      : "Role created";

    return (
      <form onSubmit={handleSubmit(this.submit)}>
        <Field name="role" label="Role Name *" component={renderTextField} />
        <Field
          name="description"
          label="Description"
          component={renderTextField}
        />

        <Field
          name="redirect"
          label="Redirect after login to *"
          component={renderRadioGroup}
          options={[
            { value: "adminPages", label: "Admin Pages" },
            { value: "flinkAdmin", label: "Reports" },
            { value: "flinkMake", label: "Make" }
          ]}
        />

        <FlexContainer>
          <Column>
            <ColumnTitle>Admin Toolbars</ColumnTitle>
            <CheckboxGroups
              name="permissions[adminToolbar]"
              groups={adminToolbar}
              column
              form="RoleForm"
              hidden="show"
              initialValues={initialValues}
              cols={1}
            />
          </Column>
          <Column>
            <ColumnTitle>View Documents</ColumnTitle>
            {viewDocuments && (
              <CheckboxFieldsGroup
                name="permissions[viewDocuments]"
                fields={viewDocuments}
                category
                column
                hidden="show"
                form="RoleForm"
                initialValues={initialValues}
                cols={1}
              />
            )}
          </Column>
          <Column>
            <ColumnTitle>Apps</ColumnTitle>
            <CheckboxGroups
              name="permissions[apps]"
              groups={apps}
              column
              form="RoleForm"
              initialValues={initialValues}
              cols={1}
            />
          </Column>

          <Column notShow={!isFlinkMakeChecked}>
            <ColumnTitle>FlinkMake Configuration</ColumnTitle>

            <Field
              name={"permissions[flinkMake][tabs][editActivities]"}
              label="Edit Activities Tab"
              component={renderCheckbox}
            />

            {isEditActivitiesTabChecked && (
              <div
                style={{
                  marginLeft: "30px"
                }}
              >
                <CheckboxGroups
                  name="permissions[flinkMake][editActivitiesTab]"
                  groups={editActivitiesCheckboxes}
                  column
                  form="RoleForm"
                  initialValues={initialValues}
                  cols={1}
                />
              </div>
            )}

            <Field
              name={"permissions[flinkMake][tabs][searchActivities]"}
              label="Search Activities Tab"
              component={renderCheckbox}
            />

            {isSearchActivitiesTabChecked && (
              <div
                style={{
                  marginLeft: "30px"
                }}
              >
                <CheckboxFieldsGroup
                  name="permissions[flinkMake][searchActivitiesTab][actions]"
                  fields={searchActivitiesCheckboxes}
                  column
                  form="RoleForm"
                  initialValues={initialValues}
                  cols={1}
                />
              </div>
            )}

            <div>
              <Field
                name={"permissions[flinkMake][tabs][activityGroups]"}
                label="Activity Groups Tab"
                component={renderCheckbox}
              />
            </div>
            <div>
              <Field
                name={"permissions[flinkMake][tabs][wordlists]"}
                label="Wordlists Tab"
                component={renderCheckbox}
              />
            </div>
            <div>
              <Field
                name={"permissions[flinkMake][tabs][searchWordlists]"}
                label="Search Wordlists Tab"
                component={renderCheckbox}
              />
            </div>
          </Column>
        </FlexContainer>

        {!pristine && error && <StyledError>{error}</StyledError>}
        {pristine && success && <StyledSuccess>{successMessage}</StyledSuccess>}

        <FormButtons
          pristine={pristine}
          submitting={submitting}
          isEdit={isEdit}
          itemName="Role"
          handleReset={reset}
        />
      </form>
    );
  }
}

RoleForm.propTypes = {
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired,
  documents: PropTypes.array.isRequired,
  change: PropTypes.func.isRequired,
  flinkMake: PropTypes.bool,
  flinkReports: PropTypes.bool,
  isEditActivitiesTabChecked: PropTypes.bool,
  isSearchActivitiesTabChecked: PropTypes.bool
};

const selector = formValueSelector("RoleForm");

const mapStateToProps = state => ({
  flinkMake: !!selector(state, "permissions[apps][flinkMake]"),
  flinkReports: !!selector(state, "permissions[apps][flinkReports]"),
  isEditActivitiesTabChecked: !!selector(
    state,
    "permissions[flinkMake][tabs][editActivities]"
  ),
  isSearchActivitiesTabChecked: !!selector(
    state,
    "permissions[flinkMake][tabs][searchActivities]"
  )
});

export default compose(
  withData(["documents"]),
  reduxForm({
    form: "RoleForm",
    validate,
    enableReinitialize: true
  }),
  connect(mapStateToProps, { addData, editData, fetchData, change })
)(RoleForm);

const FlexContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const Column = styled.div`
  display: block;
  width: 100%;
  flex-shrink: 0;
  visibility: ${props => (props.notShow ? "hidden" : "visible")};

  @media only screen and (min-width: 576px) {
    width: 48%;
  }

  @media only screen and (min-width: 992px) {
    width: 23%;
  }
`;

const ColumnTitle = styled.p`
  font-size: 1.6rem;
  font-weight: 500;
`;
