import React from 'react';
import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';

// import Button from '@material-ui/core/Button';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'role',
    disablePadding: false,
    label: 'Roles'
  }
];

const Roles = ({ deleteData, roles }) => {
  const deleteHandler = (id, e) => {
    deleteData('roles', id);
  };

  return (
    <div>
      {/* <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/roles/create"
      >
        Create role
      </Button> */}

      <MaterialTable
        data={roles.filter(role => role.alias !== 'SUPER')}
        rows={rows}
        deleteHandler={deleteHandler}
        editLink="/admin/roles/edit"
        type="role"
        rowWithName="role"
      />
    </div>
  );
};

Roles.propTypes = {
  roles: PropTypes.array,
  deleteData: PropTypes.func.isRequired
};

export default withData(['roles'])(Roles);
