import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Field,
  reduxForm,
  SubmissionError,
  formValueSelector
} from "redux-form";
import { connect } from "react-redux";
import { compose } from "redux";

import { addData, editData } from "actions/adminActions";
import { validateEmail } from "validation";
import {
  renderTextField,
  renderSelect,
  StyledError,
  StyledSuccess,
  FormButtons,
  CheckboxFieldsGroup
} from "components/form-components";
import { MASTER_ADMIN, AUTHOR, REP, TRANSLATOR, CONSUMER_REP } from "consts/user-roles";
import { FlexContainer } from "components/common";
import { withData } from "components/hocs";
import { reduxFormValidator } from "validation";
import { formatLocalesToOptions } from "utils";

const availableRolesToCreate = [MASTER_ADMIN, AUTHOR, REP, TRANSLATOR, CONSUMER_REP];

const requiredFields = ["firstname", "lastname", "username", "roleAlias"];

const validate = reduxFormValidator(requiredFields);

class UserForm extends Component {
  state = { success: false };

  componentDidMount() {
    const { locales, scopes } = this.props;

    const localesOptions = formatLocalesToOptions(locales);

    const scopesOptions = scopes
      .map(scope => ({
        value: scope._id,
        label: scope.name
      }))
      .sort((a, b) => (a.label > b.label ? 1 : -1));

    this.setState({ scopesOptions, localesOptions });
  }

  submit = async (values, dispatch) => {
    const { isEdit, editData, addData, onSuccess, roleAlias } = this.props;

    const formattedValues = { ...values };

    if (values.available) {
      if (roleAlias !== AUTHOR) {
        delete formattedValues.available.scopes;
      }
      if (roleAlias !== TRANSLATOR) {
        delete formattedValues.available.locales;
      }
    }

    const result = isEdit
      ? await editData("users", formattedValues)
      : await addData("users", formattedValues);

    if (!result.success) {
      this.setState({ success: false });
      throw new SubmissionError(result.err);
    } else {
      this.setState({ success: true });
      onSuccess && onSuccess();
    }
  };

  render() {
    const {
      error,
      handleSubmit,
      pristine,
      reset,
      submitting,
      roleAlias,
      isEdit,
      learningCenters,
      roles,
      initialValues = {}
    } = this.props;

    const { success, scopesOptions, localesOptions } = this.state;

    // If form mode is "edit" and editing user have roleAlias
    // then in select available all roles for show,
    // otherwise show only 3 roles that can be created from this form
    const availableRoles =
      isEdit && roleAlias
        ? [...roles]
        : roles.filter(role => availableRolesToCreate.includes(role.alias));

    const rolesOptions = availableRoles.map(role => ({
      value: role.alias,
      label: role.role
    }));

    const learningCentersOptions = learningCenters.map(learningCenter => ({
      value: learningCenter._id,
      label: learningCenter.name
    }));

    const successMessage = isEdit
      ? "User updated successfully"
      : "User created";

    return (
      <div>
        <form onSubmit={handleSubmit(this.submit)}>
          <FlexContainer xs="100%" md="50%">
            <Field
              name="username"
              disabled={isEdit}
              label="Email *"
              component={renderTextField}
              validate={validateEmail}
            />
            <Field
              name="roleAlias"
              disabled={isEdit && roleAlias}
              label="User Role *"
              component={renderSelect}
              options={rolesOptions}
              isClearable
              onChange={this.handleChangeUserRole}
            />
          </FlexContainer>

          <FlexContainer xs="100%" md="50%">
            <Field
              name="firstname"
              label="First Name *"
              component={renderTextField}
            />
            <Field
              name="lastname"
              label="Last Name *"
              component={renderTextField}
            />
          </FlexContainer>

          <Field name="phone" label="Phone" component={renderTextField} />

          {isEdit && initialValues.learningCenterId && (
            <Field
              name="learningCenterId"
              disabled={isEdit}
              label="Assign to Learning Center *"
              component={renderSelect}
              options={learningCentersOptions || []}
              isClearable
            />
          )}

          {scopesOptions && roleAlias === AUTHOR && (
            <CheckboxFieldsGroup
              cols={3}
              sort="asc"
              fields={scopesOptions}
              name="available[scopes]"
              groupLabel="Educational Categories"
              initialValues={initialValues}
              form="UserForm"
            />
          )}

          {localesOptions && roleAlias === TRANSLATOR && (
            <CheckboxFieldsGroup
              cols={3}
              sort="asc"
              fields={localesOptions}
              name="available[locales]"
              groupLabel="Languages"
              initialValues={initialValues}
              form="UserForm"
            />
          )}

          {!pristine && error && <StyledError>{error}</StyledError>}
          {pristine && success && (
            <StyledSuccess>{successMessage}</StyledSuccess>
          )}

          <FormButtons
            pristine={pristine}
            submitting={submitting}
            isEdit={isEdit}
            itemName="User"
            handleReset={reset}
          />
        </form>
      </div>
    );
  }
}

UserForm.propTypes = {
  learningCenters: PropTypes.array.isRequired,
  roles: PropTypes.array.isRequired,
  scopes: PropTypes.array.isRequired,
  locales: PropTypes.array.isRequired,
  isEdit: PropTypes.bool,
  initialValues: PropTypes.object,
  onSuccess: PropTypes.func,
  addData: PropTypes.func.isRequired,
  editData: PropTypes.func.isRequired
};

const selector = formValueSelector("UserForm");

const mapStateToProps = state => ({
  roleAlias: selector(state, "roleAlias")
});

export default compose(
  withData(["learningCenters", "roles", "locales", "scopes"]),
  reduxForm({
    form: "UserForm",
    validate,
    enableReinitialize: true
  }),
  connect(mapStateToProps, { addData, editData })
)(UserForm);
