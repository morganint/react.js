import React from 'react';

import UserForm from './UserForm/UserForm';

const CreateUser = ({ history }) => {
  return <UserForm onSuccess={history.goBack} />;
};

export default CreateUser;
