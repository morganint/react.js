import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import UserForm from './UserForm/UserForm';
import { withData } from 'components/hocs';

const EditUser = ({ match, users }) => {
  const dataToEdit = find(users, { _id: match.params.id });

  return dataToEdit ? (
    <UserForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/users" />
  );
};

EditUser.propTypes = {
  users: PropTypes.array.isRequired
};

export default withData(['users'])(EditUser);
