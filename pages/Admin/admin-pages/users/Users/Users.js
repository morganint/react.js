import React, { useMemo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import Button from '@material-ui/core/Button';
import { deleteData } from 'actions/adminActions';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'username',
    disablePadding: false,
    label: 'Username'
  },
  {
    id: 'firstname',
    disablePadding: false,
    label: 'Firstname'
  },
  {
    id: 'lastname',
    disablePadding: false,
    label: 'Lastname'
  },
  {
    id: 'learningCenterAssign',
    disablePadding: false,
    label: 'Learning Center Assign',
    width: '200px'
  },
  {
    id: 'userRole',
    disablePadding: false,
    label: 'User Role',
    width: '150px'
  }
];

const filterUsers = (auth, users, roles, learningCenters) => {
  const { user: currentUser } = auth;

  return _.reject(
    users,
    user => user._id === currentUser._id || user.username === 'r@r.com'
  ).map(user => {
    const userCopy = _.cloneDeep(user);
    // find user role in roles list
    const role = roles.filter(role => role.alias === userCopy.roleAlias)[0];
    const learningCenter = learningCenters.filter(
      learningCenter => learningCenter._id === userCopy.learningCenterId
    )[0];

    userCopy.userRole = role && role.role;
    userCopy.learningCenterAssign = learningCenter && learningCenter.name;

    return userCopy;
  });
};

const Users = ({ deleteData, auth, users, roles, learningCenters }) => {
  const deleteUserHandler = (id, e) => {
    deleteData('users', id);
  };

  const filteredUsers = useMemo(
    () => filterUsers(auth, users, roles, learningCenters),
    [users, roles, learningCenters, auth]
  );

  return (
    <div>
      <Button
        color="primary"
        variant="outlined"
        component={Link}
        to="/admin/users/create"
      >
        Create user
      </Button>

      <MaterialTable
        data={filteredUsers}
        rows={rows}
        deleteHandler={deleteUserHandler}
        editLink="/admin/users/edit"
        type="user"
        rowWithName="username"
      />
    </div>
  );
};

Users.propTypes = {
  learningCenters: PropTypes.array.isRequired,
  users: PropTypes.array.isRequired,
  roles: PropTypes.array.isRequired,
  auth: PropTypes.object.isRequired,
  deleteData: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth }) => ({
  auth
});

export default compose(
  withData(['users', 'learningCenters', 'roles']),
  connect(
    mapStateToProps,
    { deleteData }
  )
)(Users);
