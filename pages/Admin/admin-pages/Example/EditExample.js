import React, { useMemo } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { find } from 'lodash';

import ExampleForm from './ExampleForm/ExampleForm';
import { withData } from 'components/hocs';

const EditExample = ({ match, examples }) => {
  const dataToEdit = useMemo(() => find(examples, { _id: match.params.id }), [
    examples
  ]);

  return dataToEdit ? (
    <ExampleForm isEdit initialValues={dataToEdit} />
  ) : (
    <Redirect to="/admin/examples" />
  );
};

EditExample.propTypes = {
  examples: PropTypes.array.isRequired
};

export default withData(['examples'])(EditExample);
