import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { SUPER } from 'consts/user-roles';
import { withData } from 'components/hocs';
import { viewDocument, downloadDocument } from 'actions/adminActions';

const rows = [
  {
    id: 'documentName',
    disablePadding: false,
    label: 'Name'
  },
  {
    id: 'category',
    disablePadding: false,
    label: 'Category'
  }
];

const ViewDocuments = ({ documents, auth, viewDocument, downloadDocument }) => {
  let filteredDocuments;

  if (auth.userRole.alias === SUPER) {
    filteredDocuments = documents;
  } else {
    const { viewDocuments } = auth.userRole.permissions;

    filteredDocuments = documents
      ? documents.filter(doc => viewDocuments[doc._id])
      : [];
  }

  return (
    <div>
      <MaterialTable
        type="document"
        data={filteredDocuments}
        rows={rows}
        rowWithName="documentName"
        additionalButtons={[
          {
            title: 'View',
            icon: { type: 'fa', name: 'eye' },
            action: doc => viewDocument(doc.filename)
          },
          {
            title: 'Download',
            icon: { type: 'fa', name: 'arrow-alt-circle-down' },
            action: doc => downloadDocument(doc.filename)
          }
        ]}
      />
    </div>
  );
};

ViewDocuments.propTypes = {
  documents: PropTypes.array.isRequired,
  viewDocument: PropTypes.func.isRequired,
  downloadDocument: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth, admin }) => ({
  auth
});

export default compose(
  withData(['documents']),
  connect(
    mapStateToProps,
    { viewDocument, downloadDocument }
  )
)(ViewDocuments);
