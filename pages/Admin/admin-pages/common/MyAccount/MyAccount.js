import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import ChangePasswordForm from 'components/admin/ChangePasswordForm/ChangePasswordForm';

const MyAccount = props => {
  const { firstname, lastname, _id, isTempPassword } = props.auth.user;

  const { role } = props.auth.userRole;

  return (
    <div>
      <StyledGreetings>
        Hello {firstname} {lastname}! You role is {role.split('_').join(' ')}
      </StyledGreetings>

      {isTempPassword && (
        <StyledError>You need to change temporary password!</StyledError>
      )}

      <ChangePasswordForm userId={_id} />
    </div>
  );
};

const mapStateToProps = ({ auth }) => ({ auth });

export default connect(mapStateToProps)(MyAccount);

const StyledError = styled.h2`
  color: red;
  text-align: center;
  margin-top: 20px;
`;

const StyledGreetings = styled.h1`
  text-align: center;
  margin-top: 20px;
`;
