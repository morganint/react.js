import * as Documents from 'pages/Admin/admin-pages/tools/Documents';
import * as Locales from 'pages/Admin/admin-pages/tools/Locales';
import * as Lists from 'pages/Admin/admin-pages/tools/Lists';
import * as Strings from 'pages/Admin/admin-pages/tools/Strings';
import * as Scopes from 'pages/Admin/admin-pages/tools/Scopes';
import * as LandingPages from 'pages/Admin/admin-pages/tools/LandingPages';

import * as ActivityTemplates from 'pages/Admin/admin-pages/flinkPlay/ActivityTemplates';
import * as Characters from 'pages/Admin/admin-pages/flinkPlay/Characters';
import * as GlobalButtons from 'pages/Admin/admin-pages/flinkPlay/GlobalButtons';
import * as Menus from 'pages/Admin/admin-pages/flinkPlay/Menus';
import * as Themes from 'pages/Admin/admin-pages/flinkPlay/Themes';
import * as Words from 'pages/Admin/admin-pages/flinkPlay/Words';

import * as Users from 'pages/Admin/admin-pages/users/Users';
import * as Roles from 'pages/Admin/admin-pages/users/Roles';

import * as MasterOrganizations from 'pages/Admin/admin-pages/organizations/MasterOrganizations';
import * as Regions from 'pages/Admin/admin-pages/organizations/Regions';
import * as LearningCenters from 'pages/Admin/admin-pages/organizations/LearningCenters';
// import * as Teachers from 'pages/Admin/admin-pages/schools/Teachers';

import * as Reports from './admin-pages/reports';
import General from './admin-pages/tools/General/General';

import * as Solutions from 'pages/Admin/admin-pages/solutions/Solutions';
import * as Products from 'pages/Admin/admin-pages/solutions/Products';

import MyAccount from 'pages/Admin/admin-pages/common/MyAccount/MyAccount';
import { ViewDocuments } from 'pages/Admin/admin-pages/common/ViewDocuments';
import NotFoundPage from 'pages/NotFoundPage/NotFoundPage';

import { allRoles } from 'consts/user-roles';

export default [
  ['/admin/my-account', true, MyAccount, allRoles],
  // Tools
  [
    '/admin/general',
    true,
    General,
    { adminToolbar: { tools: { general: true } } },
  ],
  [
    '/admin/documents',
    true,
    Documents.Documents,
    { adminToolbar: { tools: { documents: true } } },
  ],
  [
    '/admin/documents/create',
    true,
    Documents.CreateDocument,
    { adminToolbar: { tools: { documents: true } } },
  ],
  [
    '/admin/documents/edit/:id',
    true,
    Documents.EditDocument,
    { adminToolbar: { tools: { documents: true } } },
  ],
  [
    '/admin/locales',
    true,
    Locales.Locales,
    { adminToolbar: { tools: { locales: true } } },
  ],
  [
    '/admin/locales/create',
    true,
    Locales.CreateLocale,
    { adminToolbar: { tools: { locales: true } } },
  ],
  [
    '/admin/locales/edit/:id',
    true,
    Locales.EditLocale,
    { adminToolbar: { tools: { locales: true } } },
  ],
  [
    '/admin/lists',
    true,
    Lists.Lists,
    { adminToolbar: { tools: { lists: true } } },
  ],
  [
    '/admin/lists/create',
    true,
    Lists.CreateList,
    { adminToolbar: { tools: { lists: true } } },
  ],
  [
    '/admin/lists/edit/:id',
    true,
    Lists.EditList,
    { adminToolbar: { tools: { lists: true } } },
  ],
  [
    '/admin/strings',
    true,
    Strings.Strings,
    { adminToolbar: { tools: { strings: true } } },
  ],
  [
    '/admin/strings/create',
    true,
    Strings.CreateString,
    { adminToolbar: { tools: { strings: true } } },
  ],
  [
    '/admin/strings/edit/:id',
    true,
    Strings.EditString,
    { adminToolbar: { tools: { strings: true } } },
  ],
  [
    '/admin/scopes',
    true,
    Scopes.Scopes,
    { adminToolbar: { tools: { scopes: true } } },
  ],
  [
    '/admin/scopes/create',
    true,
    Scopes.CreateScope,
    { adminToolbar: { tools: { scopes: true } } },
  ],
  [
    '/admin/scopes/edit/:id',
    true,
    Scopes.EditScope,
    { adminToolbar: { tools: { scopes: true } } },
  ],
  [
    '/admin/landing-pages',
    true,
    LandingPages.LandingPages,
    { adminToolbar: { tools: { landingPages: true } } },
  ],
  [
    '/admin/landing-pages/create',
    true,
    LandingPages.CreateLandingPage,
    { adminToolbar: { tools: { landingPages: true } } },
  ],
  [
    '/admin/landing-pages/edit/:id',
    true,
    LandingPages.EditLandingPage,
    { adminToolbar: { tools: { landingPages: true } } },
  ],
  // Flink PLay
  [
    '/admin/activity-templates',
    true,
    ActivityTemplates.ActivityTemplates,
    { adminToolbar: { flinkPlay: { activityTemplates: true } } },
  ],
  [
    '/admin/activity-templates/create',
    true,
    ActivityTemplates.CreateActivityTemplate,
    { adminToolbar: { flinkPlay: { activityTemplates: true } } },
  ],
  [
    '/admin/activity-templates/edit/:id',
    true,
    ActivityTemplates.EditActivityTemplate,
    { adminToolbar: { flinkPlay: { activityTemplates: true } } },
  ],
  [
    '/admin/characters',
    true,
    Characters.Characters,
    { adminToolbar: { flinkPlay: { characters: true } } },
  ],
  [
    '/admin/characters/create',
    true,
    Characters.CreateCharacter,
    { adminToolbar: { flinkPlay: { characters: true } } },
  ],
  [
    '/admin/characters/edit/:id',
    true,
    Characters.EditCharacter,
    { adminToolbar: { flinkPlay: { characters: true } } },
  ],
  [
    '/admin/global-buttons',
    true,
    GlobalButtons.GlobalButtons,
    { adminToolbar: { flinkPlay: { globalButtons: true } } },
  ],
  [
    '/admin/global-buttons/create',
    true,
    GlobalButtons.CreateGlobalButton,
    { adminToolbar: { flinkPlay: { globalButtons: true } } },
  ],
  [
    '/admin/global-buttons/edit/:id',
    true,
    GlobalButtons.EditGlobalButton,
    { adminToolbar: { flinkPlay: { globalButtons: true } } },
  ],
  [
    '/admin/menus',
    true,
    Menus.Menus,
    { adminToolbar: { flinkPlay: { menus: true } } },
  ],
  [
    '/admin/menus/create',
    true,
    Menus.CreateMenu,
    { adminToolbar: { flinkPlay: { menus: true } } },
  ],
  [
    '/admin/menus/edit/:id',
    true,
    Menus.EditMenu,
    { adminToolbar: { flinkPlay: { menus: true } } },
  ],
  [
    '/admin/themes',
    true,
    Themes.Themes,
    { adminToolbar: { flinkPlay: { themes: true } } },
  ],
  [
    '/admin/themes/create',
    true,
    Themes.CreateTheme,
    { adminToolbar: { flinkPlay: { themes: true } } },
  ],
  [
    '/admin/themes/edit/:id',
    true,
    Themes.EditTheme,
    { adminToolbar: { flinkPlay: { themes: true } } },
  ],
  [
    '/admin/words',
    true,
    Words.Words,
    { adminToolbar: { flinkPlay: { words: true } } },
  ],
  [
    '/admin/words/edit/:id',
    true,
    Words.EditWord,
    { adminToolbar: { flinkPlay: { words: true } } },
  ],
  // Users
  [
    '/admin/users',
    true,
    Users.Users,
    { adminToolbar: { users: { users: true } } },
  ],
  [
    '/admin/users/create',
    true,
    Users.CreateUser,
    { adminToolbar: { users: { users: true } } },
  ],
  [
    '/admin/users/edit/:id',
    true,
    Users.EditUser,
    { adminToolbar: { users: { users: true } } },
  ],
  [
    '/admin/roles',
    true,
    Roles.Roles,
    { adminToolbar: { users: { roles: true } } },
  ],
  [
    '/admin/roles/create',
    true,
    Roles.CreateRole,
    { adminToolbar: { users: { roles: true } } },
  ],
  [
    '/admin/roles/edit/:id',
    true,
    Roles.EditRole,
    { adminToolbar: { users: { roles: true } } },
  ],
  // Master Organizations
  [
    '/admin/master-organizations',
    true,
    MasterOrganizations.MasterOrganizations,
    { adminToolbar: { organizations: { masterOrganizations: true } } },
  ],
  [
    '/admin/master-organizations/create',
    true,
    MasterOrganizations.CreateMasterOrganization,
    {
      adminToolbar: {
        organizations: { masterOrganizations: true },
      },
    },
  ],
  [
    '/admin/master-organizations/edit/:id',
    true,
    MasterOrganizations.EditMasterOrganization,
    { adminToolbar: { organizations: { masterOrganizations: true } } },
  ],
  // Regions
  [
    '/admin/regions',
    true,
    Regions.Regions,
    { adminToolbar: { organizations: { regions: true } } },
  ],
  [
    '/admin/regions/create',
    true,
    Regions.CreateRegion,
    {
      adminToolbar: {
        organizations: { regions: true },
      },
    },
  ],
  [
    '/admin/regions/edit/:id',
    true,
    Regions.EditRegion,
    { adminToolbar: { organizations: { regions: true } } },
  ],
  // Learning Centers
  [
    '/admin/learning-centers',
    true,
    LearningCenters.LearningCenters,
    { adminToolbar: { organizations: { learningCenters: true } } },
  ],
  [
    '/admin/learning-centers/create',
    true,
    LearningCenters.CreateLearningCenter,
    {
      adminToolbar: { organizations: { learningCenters: true } },
    },
  ],
  [
    '/admin/learning-centers/edit/:id',
    true,
    LearningCenters.EditLearningCenter,
    { adminToolbar: { organizations: { learningCenters: true } } },
  ],
  // [
  //   '/admin/masterOrganizations/edit/:masterOrganizationId/schools/create',
  //   true,
  //   MasterOrganizations.CreateMasterOrganizationSchool,
  //   {
  //     adminToolbar: {
  //       organizations: { masterOrganizations: true,  }
  //     }
  //   }
  // ],
  // [
  //   '/admin/masterOrganizations/edit/:masterOrganizationId/schools/edit/:id',
  //   true,
  //   MasterOrganizations.EditMasterOrganizationSchool,
  //   {
  //     adminToolbar: {
  //       organizations: { masterOrganizations: true,  }
  //     }
  //   }
  // ],
  // [
  //   '/admin/teachers',
  //   true,
  //   Teachers.Teachers,
  //   { adminToolbar: { organizations: { teachers: true } } }
  // ],
  // [
  //   '/admin/teachers/create/:schoolId',
  //   true,
  //   Teachers.CreateTeacher,
  //   { adminToolbar: { organizations: { teachers: true } } }
  // ],
  // [
  //   '/admin/teachers/edit/:id',
  //   true,
  //   Teachers.EditTeacher,
  //   { adminToolbar: { organizations: { teachers: true } } }
  // ],
  // [
  //   '/admin/teachers/import/:schoolId',
  //   true,
  //   Teachers.ImportTeachers,
  //   { adminToolbar: { organizations: { teachers: true } } }
  // ],
  // Solutions
  [
    '/admin/solutions',
    true,
    Solutions.Solutions,
    { adminToolbar: { solutions: { solutions: true } } },
  ],
  [
    '/admin/solutions/create',
    true,
    Solutions.CreateSolution,
    { adminToolbar: { solutions: { solutions: true } } },
  ],
  [
    '/admin/solutions/edit/:id',
    true,
    Solutions.EditSolution,
    { adminToolbar: { solutions: { solutions: true } } },
  ],
  [
    '/admin/solutions/solution-menu/:id',
    true,
    Solutions.EditSolutionMenu,
    { adminToolbar: { solutions: { solutions: true } } },
  ],
  [
    '/admin/products',
    true,
    Products.Products,
    { adminToolbar: { solutions: { products: true } } },
  ],
  [
    '/admin/products/create',
    true,
    Products.CreateProduct,
    { adminToolbar: { solutions: { products: true } } },
  ],
  [
    '/admin/products/edit/:id',
    true,
    Products.EditProduct,
    { adminToolbar: { solutions: { products: true } } },
  ],
  [
    '/admin/view-documents',
    true,
    ViewDocuments,
    { viewDocuments: { show: true } },
  ],
  [
    '/admin/reports/by-organization',
    true,
    Reports.ByOrganization,
    { adminToolbar: { reports: { byOrganization: true } } },
  ],
  [
    '/admin/reports/cohort',
    true,
    Reports.Cohort,
    { adminToolbar: { reports: { cohort: true } } },
  ],
  [
    '/admin/reports/continuity',
    true,
    Reports.Continuity,
    { adminToolbar: { reports: { continuity: true } } },
  ],
  [
    '/admin/reports/revenue',
    true,
    Reports.Revenue,
    { adminToolbar: { reports: { revenue: true } } },
  ],
  // 404 Page
  [null, false, NotFoundPage],
];
