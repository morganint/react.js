import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { Link, withRouter } from "react-router-dom";

import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import Button from "@material-ui/core/Button";

import { logout } from "actions/authActions";
import Sidebar from "./adminUI/Sidebar";
import { NeedPermissions, MaterialFaIcon } from "utils";

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    position: "relative",
    display: "flex",
    width: "100%"
  },
  appBar: {
    position: "fixed",
    marginLeft: drawerWidth,
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  navIconHide: {
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  flex: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "center",
    textTransform: "uppercase"
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
    [theme.breakpoints.up("md")]: {
      marginLeft: drawerWidth
    }
  }
});

class AdminUI extends Component {
  state = {
    mobileOpen: false,
    anchorEl: null
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  handleListItemClick = event => {
    if (this.state.mobileOpen) {
      this.setState({ mobileOpen: false });
    }
  };

  // goBack = () => {
  //   const { history } = this.props;
  //   const prevPath = history.location.pathname.split('/').slice(0, -1).join('/')

  //   console.log(this.props)

  //   // history.push(prevPath)
  // }

  render() {
    const { classes, logout, status, history } = this.props;
    const { anchorEl, mobileOpen } = this.state;
    const open = Boolean(anchorEl);

    return (
      <Fragment>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.navIconHide}
            >
              <MaterialFaIcon icon="bars" />
            </IconButton>

            <Button onClick={history.goBack} color="inherit">
              Go back
            </Button>

            <Typography
              to="/admin"
              variant="subtitle1"
              color="inherit"
              className={classes.flex}
            >
              {status.currentPage && status.currentPage.title}
            </Typography>

            <div>
              <IconButton
                aria-owns={open ? "menu-appbar" : null}
                aria-haspopup="true"
                onClick={this.handleMenu}
                color="inherit"
              >
                <MaterialFaIcon icon="user-circle" />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                open={open}
                onClose={this.handleClose}
              >
                <MenuItem
                  to="/admin/my-account"
                  onClick={this.handleClose}
                  component={Link}
                >
                  My Account
                </MenuItem>

                <NeedPermissions permissions={{ apps: { flinkAdmin: true } }}>
                  <MenuItem
                    to="/flink-admin"
                    onClick={this.handleClose}
                    component={Link}
                  >
                    Report
                  </MenuItem>
                </NeedPermissions>
                <NeedPermissions permissions={{ apps: { flinkMake: true } }}>
                  <MenuItem
                    to="/flink-make"
                    onClick={this.handleClose}
                    component={Link}
                  >
                    Make
                  </MenuItem>
                </NeedPermissions>
                <MenuItem onClick={logout}>Logout</MenuItem>
              </Menu>
            </div>
          </Toolbar>
        </AppBar>

        <Sidebar
          mobileOpen={mobileOpen}
          handleListItemClick={this.handleListItemClick}
          handleMobileClose={this.handleDrawerToggle}
        />
      </Fragment>
    );
  }
}

AdminUI.propTypes = {
  classes: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  status: state.status
});

export default compose(
  withRouter,
  withStyles(styles),
  connect(mapStateToProps, { logout })
)(AdminUI);
