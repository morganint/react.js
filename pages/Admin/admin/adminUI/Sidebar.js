import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';

import { imagesURL } from 'config';
import CollapsableMenu from './sidebar/CollapsableMenu';
import NavListItem from './sidebar/NavListItem';
import { checkPermissions } from 'utils';

const drawerWidth = 250;

const styles = (theme) => ({
  toolbar: {
    ...theme.mixins.toolbar,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  drawerPaper: {
    width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'fixed',
    },
  },
  logoWrapper: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoImg: {
    width: '100px',
  },
  nested: {
    paddingLeft: theme.spacing(5),
  },
});

class Sidebar extends Component {
  state = {
    currentOpen: true,
  };

  handleListDropdownClick = (currentName) => {
    this.setState((state) => ({
      currentOpen: state.currentOpen === currentName ? '' : currentName,
    }));
  };

  render() {
    const {
      mobileOpen,
      classes,
      handleMobileClose,
      handleListItemClick,
      auth,
    } = this.props;

    const drawer = (
      <div>
        <div className={classes.toolbar}>
          <Link to="/" className={classes.logoWrapper}>
            <img
              className={classes.logoImg}
              src={`${imagesURL}/Images/Login/FLCLogo.png`}
              alt=""
            />
          </Link>
        </div>

        <Divider />

        <List component="nav">
          <CollapsableMenu
            onTogglerClick={this.handleListDropdownClick.bind(null, 'Tools')}
            icon="wrench"
            title="Tools"
            currentOpen={this.state.currentOpen}
            block={
              !checkPermissions(auth, {
                adminToolbar: { tools: { show: true } },
              })
            }
          >
            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/general"
              icon="cogs"
              text="General"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { tools: { general: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/documents"
              icon="file-alt"
              text="Documents"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { tools: { documents: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/lists"
              icon="list"
              text="Lists"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { tools: { lists: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/locales"
              icon="globe"
              text="Locales"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { tools: { locales: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/strings"
              icon="font"
              text="Strings"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { tools: { strings: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/scopes"
              icon="file-signature"
              text="Scopes"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { tools: { scopes: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/landing-pages"
              icon="columns"
              text="Landing Pages"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { tools: { landingPages: true } },
                })
              }
            />
          </CollapsableMenu>

          <CollapsableMenu
            onTogglerClick={this.handleListDropdownClick.bind(
              null,
              'Flink Play'
            )}
            icon="gamepad"
            title="Flink Play"
            currentOpen={this.state.currentOpen}
            block={
              !checkPermissions(auth, {
                adminToolbar: { flinkPlay: { show: true } },
              })
            }
          >
            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/activity-templates"
              icon="tools"
              text="Activity Templates"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { flinkPlay: { activityTemplates: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/characters"
              icon="child"
              text="Characters"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { flinkPlay: { characters: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/global-buttons"
              icon="keyboard"
              text="Global Buttons"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { flinkPlay: { globalButtons: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/menus"
              icon="bars"
              text="Menus"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { flinkPlay: { menus: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/themes"
              icon="palette"
              text="Themes"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { flinkPlay: { themes: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/words"
              icon="spell-check"
              text="Words"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { flinkPlay: { words: true } },
                })
              }
            />
          </CollapsableMenu>

          <CollapsableMenu
            onTogglerClick={this.handleListDropdownClick.bind(null, 'Users')}
            icon="users"
            title="Users"
            currentOpen={this.state.currentOpen}
            block={
              !checkPermissions(auth, {
                adminToolbar: { users: { show: true } },
              })
            }
          >
            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/users"
              icon="users-cog"
              text="Users"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { users: { users: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/roles"
              icon="user-check"
              text="Roles"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { users: { roles: true } },
                })
              }
            />
          </CollapsableMenu>

          <CollapsableMenu
            onTogglerClick={this.handleListDropdownClick.bind(
              null,
              'Organizations'
            )}
            title="Organizations"
            icon="graduation-cap"
            currentOpen={this.state.currentOpen}
            block={
              !checkPermissions(auth, {
                adminToolbar: { organizations: { show: true } },
              })
            }
          >
            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/master-organizations"
              icon="city"
              text="Master Organizations"
              block={
                !checkPermissions(auth, {
                  adminToolbar: {
                    organizations: { masterOrganizations: true },
                  },
                })
              }
            />
            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/regions"
              icon="building"
              text="Regions"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { organizations: { regions: true } },
                })
              }
            />
            {/* <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/teachers"
              icon="chalkboard-teacher"
              text="Teachers"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { organizations: { teachers: true } }
                })
              }
            /> */}
            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/learning-centers"
              icon="school"
              text="Learning Centers"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { organizations: { learningCenters: true } },
                })
              }
            />
          </CollapsableMenu>

          <CollapsableMenu
            onTogglerClick={this.handleListDropdownClick.bind(
              null,
              'Solutions'
            )}
            icon="briefcase"
            title="Solutions"
            currentOpen={this.state.currentOpen}
            block={
              !checkPermissions(auth, {
                adminToolbar: { solutions: { show: true } },
              })
            }
          >
            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/solutions"
              icon="layer-group"
              text="Solutions"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { solutions: { solutions: true } },
                })
              }
            />
            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/products"
              icon="briefcase"
              text="Products"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { solutions: { products: true } },
                })
              }
            />
          </CollapsableMenu>

          <CollapsableMenu
            onTogglerClick={this.handleListDropdownClick.bind(null, 'Reports')}
            icon="clipboard-list"
            title="Reports"
            currentOpen={this.state.currentOpen}
            block={
              !checkPermissions(auth, {
                adminToolbar: { reports: { show: true } },
              })
            }
          >
            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/reports/by-organization"
              icon="clipboard-list"
              text="By Organization"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { reports: { byOrganization: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/reports/cohort"
              icon="clipboard-list"
              text="Cohort"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { reports: { cohort: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/reports/continuity"
              icon="clipboard-list"
              text="Continuity"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { reports: { continuity: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/reports/revenue"
              icon="clipboard-list"
              text="Revenue"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { reports: { revenue: true } },
                })
              }
            />

            <NavListItem
              className={classes.nested}
              onClick={handleListItemClick}
              to="/admin/reports/content-partners"
              icon="clipboard-list"
              text="Content Partners"
              block={
                !checkPermissions(auth, {
                  adminToolbar: { reports: { contentPartners: true } },
                })
              }
            />
          </CollapsableMenu>

          <NavListItem
            onClick={handleListItemClick}
            to="/admin/view-documents"
            icon="file-pdf"
            text="View Documents"
            block={
              !checkPermissions(auth, {
                viewDocuments: { show: true },
              })
            }
          />
        </List>
      </div>
    );

    return (
      <Fragment>
        <Hidden smDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>

        <Hidden mdUp>
          <Drawer
            variant="temporary"
            anchor="left"
            open={mobileOpen}
            onClose={handleMobileClose}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
      </Fragment>
    );
  }
}

Sidebar.propTypes = {
  auth: PropTypes.object.isRequired,
  mobileOpen: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  handleMobileClose: PropTypes.func.isRequired,
  handleListItemClick: PropTypes.func.isRequired,
};

const mapStateToProps = ({ auth }) => ({ auth });

export default compose(
  withRouter,
  withStyles(styles),
  connect(mapStateToProps)
)(Sidebar);
