import React, { Fragment } from 'react';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { MaterialFaIcon } from 'utils';

const CollapsableMenu = ({
  onTogglerClick,
  icon,
  title,
  children,
  currentOpen,
  block
}) => {
  return block ? null : (
    <Fragment>
      <ListItem dense button onClick={onTogglerClick}>
        <ListItemIcon>
          <MaterialFaIcon icon={icon} />
        </ListItemIcon>

        <ListItemText disableTypography primary={title} />
        <span style={{ color: '#2196F3' }}>
          {currentOpen === title ? (
            <MaterialFaIcon icon="angle-up" />
          ) : (
            <MaterialFaIcon icon="angle-down" />
          )}
        </span>
      </ListItem>

      <Collapse in={currentOpen === title} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {children}
        </List>
      </Collapse>
    </Fragment>
  );
};

export default CollapsableMenu;
