import React from 'react';
import { NavLink } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { MaterialFaIcon } from 'utils';

const styles = theme => ({
  activeNavLink: {
    color: theme.palette.primary.main,
    backgroundColor: '#e8e8e8'
  },
  listItemIcon: {
    marginRight: 0,
    color: theme.palette.action.active,
    flexShrink: 0
  }
});

const NavListItem = ({ text, classes, block, icon, ...rest }) => {
  return block ? null : (
    <ListItem
      dense
      button
      component={React.forwardRef((props, ref) => (
        <span ref={ref}>
          <NavLink {...props} />
        </span>
      ))}
      activeClassName={classes.activeNavLink}
      {...rest}
    >
      <ListItemIcon>
        <MaterialFaIcon className={classes.listItemIcon} icon={icon} />
      </ListItemIcon>

      <ListItemText disableTypography primary={text} />
    </ListItem>
  );
};

export default withStyles(styles)(NavListItem);
