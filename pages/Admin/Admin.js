import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Switch, Redirect } from 'react-router-dom';

import { clearAdminData } from 'actions/adminActions';
// import { MuiThemeProvider } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';

import adminRoutes from './admin-routes';
import { renderRoutes } from 'utils';

import AdminUI from './admin/AdminUI';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    position: 'relative',
    display: 'flex',
    width: '100%'
  },
  appBar: {
    position: 'fixed',
    marginLeft: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  flex: {
    flexGrow: 1,
    display: 'flex',
    justifyContent: 'center'
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: '#fff',
    padding: theme.spacing(3),
    [theme.breakpoints.up('md')]: {
      marginLeft: drawerWidth
    }
  }
});

export const Admin = ({ auth, classes, clearAdminData }) => {
  useEffect(() => {
    return clearAdminData;
  }, [clearAdminData]);
  return (
    // <MuiThemeProvider theme={muiTheme}>
    <div className={classes.root}>
      <AdminUI />

      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Switch>
          <Redirect from="/admin" exact to="/admin/my-account" />
          {renderRoutes(adminRoutes, auth)}
        </Switch>
      </main>
    </div>
    // </MuiThemeProvider>
  );
};

Admin.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = ({ auth }) => ({ auth });

export default compose(
  connect(
    mapStateToProps,
    { clearAdminData }
  ),
  withStyles(styles)
)(Admin);
