import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm, SubmissionError, Field } from 'redux-form';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { login } from 'actions/authActions';
import { validateLogins } from 'validation';
import LoginFieldsGroup from './loginForm/LoginFieldsGroup';
import ForgotPasswordForm from './loginForm/ForgotPasswordForm';
import { Modal } from 'components/common';
import classes from './LoginForm.module.scss';

class LoginForm extends Component {
  state = { forgotPasswordOpened: false };

  submitHandler = async (values, dispatch) => {
    const { learningCenter, login } = this.props;

    if (values.username2 || values.username3) {
      values.isTeam = true;
    } else {
      values.isTeam = false;
    }

    const learningCenterId = (learningCenter && learningCenter._id) || null;

    const result = await login({ ...values, learningCenterId });

    if (!result.success) {
      this.props.onError && this.props.onError(result.data);

      if (result.data && result.data.errors) {
        throw new SubmissionError(result.data.errors);
      }
    }
  };

  setForgotPasswordOpened = (state) => {
    this.setState({ forgotPasswordOpened: state });
  };

  renderLogins = () => {
    const { learningCenter } = this.props;

    const allLoginsFields = !!learningCenter;

    return (
      <div className={classes.loginsWrapper}>
        <LoginFieldsGroup primary noTitle={!allLoginsFields} />

        {allLoginsFields && (
          <>
            <LoginFieldsGroup number="2" />
            <LoginFieldsGroup number="3" />
          </>
        )}
      </div>
    );
  };

  renderLanguages = () => {
    const { availableLocales, onChangeLocale, lang: currentLang } = this.props;

    return (
      <div className={classes.radioWrapper}>
        {availableLocales.length === 1
          ? null
          : availableLocales.map((locale) => {
              const checked = locale.code === currentLang.code;

              return (
                <label
                  className={classnames(classes.radioLabel, {
                    [classes.checked]: checked,
                  })}
                  key={locale._id}
                >
                  <Field
                    name="lang"
                    checked={checked}
                    component="input"
                    type="radio"
                    value={locale.code}
                    onChange={onChangeLocale}
                  />{' '}
                  {locale.displayName}
                </label>
              );
            })}
      </div>
    );
  };

  render() {
    const {
      handleSubmit,
      // pristine,
      submitting,
      translate,
    } = this.props;

    const { forgotPasswordOpened } = this.state;

    if (!translate) return null;

    return (
      <>
        <form
          className={classes.loginForm}
          autoComplete="off"
          onSubmit={handleSubmit(this.submitHandler)}
        >
          <div className={classes.formContent}>
            {this.renderLanguages()}
            {this.renderLogins()}
          </div>

          <div className={classes.buttonsWrapper}>
            <button
              className={classes.button + ' ' + classes.purple}
              disabled={submitting}
              type="submit"
            >
              {translate(104, 'Login')}
            </button>

            <button
              className={classes.button + ' ' + classes.blue}
              type="button"
              onClick={() => this.setForgotPasswordOpened(true)}
            >
              {translate(105, 'Forgot Password')}
            </button>
          </div>
        </form>

        <Modal
          on={forgotPasswordOpened}
          toggle={() => this.setForgotPasswordOpened(false)}
        >
          <ForgotPasswordForm
            translate={translate}
            onSuccess={() => this.setForgotPasswordOpened(false)}
          />
        </Modal>
      </>
    );
  }
}

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
  onChangeLocale: PropTypes.func.isRequired,
  lang: PropTypes.object,
  availableLocales: PropTypes.array.isRequired,
};

const mapStateProps = ({ status }) => ({
  lang: status.lang,
  translate: status.translate,
});

export default compose(
  connect(mapStateProps, { login }),
  reduxForm({
    form: 'loginForm',
    validate: validateLogins,
  })
)(LoginForm);
