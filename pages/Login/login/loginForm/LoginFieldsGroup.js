import React from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { withTranslate } from 'components/hocs';
import classes from './LoginFieldsGroup.module.scss';

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error },
  translate,
  ...otherProps
}) => {
  const errorMessage =
    typeof error === 'object'
      ? translate(error.stringNumber, error.message)
      : error;

  return (
    <div
      className={classnames(classes.inputGroup, {
        [classes.errorState]: !!(touched && errorMessage),
      })}
    >
      <input {...input} {...otherProps} type={type} placeholder={label} />
      {touched && errorMessage && (
        <span className={classes.error}>{errorMessage}</span>
      )}
    </div>
  );
};

const LoginFieldsGroup = ({ number, primary, translate, noTitle }) => {
  return (
    <div className={classes.loginGroup}>
      {!noTitle && (
        <b className={classes.loginGroupTitle}>
          {translate(537)} #{primary ? 1 : number}
        </b>
      )}

      <Field
        name={`username${primary ? '' : number}`}
        type="text"
        autoFocus={primary}
        component={renderField}
        label={translate(102, 'Username')}
        translate={translate}
      />

      <Field
        name={`password${primary ? '' : number}`}
        type="password"
        component={renderField}
        label={translate(103, 'Password')}
        translate={translate}
      />
    </div>
  );
};

LoginFieldsGroup.propTypes = {
  number: PropTypes.string,
  primary: PropTypes.bool,
};

export default withTranslate(LoginFieldsGroup);
