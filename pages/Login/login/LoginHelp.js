import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import { toggleAudio } from 'actions/audioActions';
import { formatHtmlForPlay } from 'utils';
import { imagesURL, audioURL } from 'config';

class LoginHelp extends Component {
  state = {
    helpHtml: '',
  };

  componentDidMount() {
    this.setHelpHtml();
  }

  componentDidUpdate(prevProps) {
    const { translate } = this.props;

    if (prevProps.translate !== translate) {
      this.setHelpHtml();
    }
  }

  setHelpHtml = () => {
    const { translate } = this.props;

    const helpHtml = formatHtmlForPlay({
      html: translate(1, '', false, true),
      units: 'vw',
      calc: (size) => (size / 1360) * 100,
    });
    this.setState({ helpHtml });
  };

  render() {
    const { lang, classes, stateAudioSrc, isPlaying } = this.props;
    const { helpHtml } = this.state;

    const audioSrc = `${audioURL}/Generic/LoginHelp/${
      lang.name || 'English'
    }/Online/LoginHelp.mp3`;

    const playBtnIcon = `${imagesURL}/Images/Login/play-button-purple.png`;

    return (
      <div className={classes.helpWrapper}>
        <div
          className={classes.helpText}
          dangerouslySetInnerHTML={{ __html: helpHtml }}
        />

        <div
          className={classnames(classes.playButton, {
            [classes.playing]: audioSrc === stateAudioSrc && isPlaying,
          })}
          onClick={() => toggleAudio(audioSrc)}
          title="Audio help"
        >
          <img src={playBtnIcon} alt="" />
          <span className={classes.pauseIcon}>&times;</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ status, audio }) => ({
  translate: status.translate,
  isPlaying: audio.isPlaying,
  stateAudioSrc: audio.audioSrc,
});

export default connect(mapStateToProps)(LoginHelp);
