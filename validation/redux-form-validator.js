import _ from "lodash";
import store from "store";

export default requiredFields => values => {
  const errors = {};

  const {
    status: { translate }
  } = store.getState();

  const requiredTranslate = translate(695, "Required");

  requiredFields.forEach(field => {
    const val = _.get(values, field);

    if (!val) {
      _.set(errors, field, requiredTranslate);
    }
  });

  return errors;
};
