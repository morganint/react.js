const useNameModes = {
  FIRST_NAME: 'FIRST_NAME',
  LAST_NAME: 'LAST_NAME',
  FIRST_AND_LAST_NAME: 'FIRST_AND_LAST_NAME',
};

export default useNameModes;
