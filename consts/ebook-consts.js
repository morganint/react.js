export const EBOOK_WIDTH = 760;
export const EBOOK_HEIGHT = 490;

export const PRESENTATION = "PRESENTATION";
export const MANUAL = "MANUAL";
export const BOTH = "BOTH";
