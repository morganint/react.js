export const MAIN_MENU = 'MAIN_MENU';
export const ACTIVITIES = 'ACTIVITIES';
export const ACTIVITIES_AND_GROUPS = 'ACTIVITIES_AND_GROUPS';
export const ACTIVITIES_WORDLISTS = 'ACTIVITIES_WORDLISTS';
export const ACTIVITY_GROUP_BOOKS = 'ACTIVITY_GROUP_BOOKS';
export const ACTIVITY_GROUP_WORDLIST = 'ACTIVITY_GROUP_WORDLIST';
