import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import reducers from 'reducers';
import { composeWithDevTools } from 'redux-devtools-extension';

let store;

if (process.env.NODE_ENV === 'production') {
  store = createStore(reducers, {}, applyMiddleware(reduxThunk));
} else {
  store = createStore(
    reducers,
    {},
    composeWithDevTools(applyMiddleware(reduxThunk))
  );
}

export default store;
