import React, { lazy, Suspense, Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Switch, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { debounce, find } from 'lodash';

import { MuiThemeProvider } from '@material-ui/core/styles';
import Home from 'pages/Home/Home';
import Login from 'pages/Login/Login';

import NotFoundPage from 'pages/NotFoundPage/NotFoundPage';

import { Loader, Dialogs, Spinner } from 'components/common';
import RefreshScreen from 'components/common/RefreshScreen';
import { renderRoutes, setAuthToken, muiTheme, parseQueryString } from 'utils';
import { updateToken } from 'actions/authActions';
import { getCommonData } from 'actions/commonActions';
import { changeLocale, setAspectRatio } from 'actions/statusActions';
import store from 'store';
import { allRoles } from 'consts/user-roles';

import './utils/audioPlayer';
import './settings';
import 'normalize.css';
import 'animate.css';
import 'react-datepicker/dist/react-datepicker.css';

const Admin = lazy(() => import('pages/Admin/Admin'));
const FlinkAdmin = lazy(() => import('pages/FlinkAdmin/FlinkAdmin'));
const FlinkPlay = lazy(() => import('pages/FlinkPlay/FlinkPlay'));
const FlinkMake = lazy(() => import('pages/FlinkMake/FlinkMake'));

const routes = [
  ['/', true, Home],
  ['/login', true, Login],
  ['/admin', false, Admin, allRoles],
  ['/flink-admin', false, FlinkAdmin, { apps: { flinkAdmin: true } }],
  ['/flink-make', false, FlinkMake, { apps: { flinkMake: true } }],
  ['/play', true, FlinkPlay, 'learner'],

  ['/:learningCenterUrl', true, Login],
  // 404 Page
  [null, false, NotFoundPage],
];

class App extends Component {
  constructor(props) {
    super(props);

    // Check for token
    if (localStorage.jwtToken) {
      const query = parseQueryString(window.location.search);

      if (query.logout === '1') {
        props.history.replace(
          props.location.pathname +
            props.location.search.replace(new RegExp(`logout=1(&)?`), '')
        );
      } else {
        // Set token from local storage
        try {
          setAuthToken(localStorage.jwtToken);
          // Update token from server
          updateToken(localStorage.jwtToken)(store.dispatch);
        } catch (err) {
          localStorage.removeItem('jwtToken');
        }
      }
    }

    this.state = { redirectTo: '/' };
  }

  componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    const { learner, auth } = this.props;

    if (prevProps.learner !== learner || prevProps.auth.user !== auth.user) {
      this.setRedirectTo();
    }
  }

  init = async () => {
    const { getCommonData } = this.props;

    if (this.state.error) {
      this.setState({ error: '' });
    }

    const success = await getCommonData();

    if (!success) {
      return this.setState({
        error: 'Error connecting to server.',
      });
    }

    this.checkAspectRatio();
    window.addEventListener('resize', debounce(this.checkAspectRatio, 100));

    this.setLanguage();
    this.setRedirectTo();
  };

  setRedirectTo = () => {
    const { learner, auth, common } = this.props;

    if (!common) return;

    const { learningCenters } = common;

    const learningCenterId =
      (learner && learner.learningCenterId) ||
      (auth.user && auth.user.learningCenterId);

    let redirectTo = '/';

    if (learningCenterId) {
      const learningCenter = find(learningCenters, { _id: learningCenterId });

      if (learningCenter) {
        redirectTo = learningCenter.url;
      }
    }

    this.setState({ redirectTo });
  };

  checkAspectRatio = () => {
    const { setAspectRatio } = this.props;
    const { innerWidth, innerHeight } = window;
    const aspectRatio = innerWidth / innerHeight;

    setAspectRatio(aspectRatio);
  };

  setLanguage = () => {
    const { changeLocale } = this.props;

    // Set language from Local Storage if exists
    const localeCode = localStorage.getItem('localeCode') || 'en';
    changeLocale(localeCode);
  };

  render() {
    const { auth, common } = this.props;
    const { error, redirectTo } = this.state;

    if (error) {
      return (
        <RefreshScreen
          refreshHandler={this.init}
          text="Error connecting to server."
        />
      );
    }

    return (
      <MuiThemeProvider theme={muiTheme}>
        <Loader />

        {common && <Dialogs />}

        <Suspense fallback={<Spinner />}>
          {common && <Switch>{renderRoutes(routes, auth, redirectTo)}</Switch>}
        </Suspense>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  getCommonData: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  learner: PropTypes.object,
};

const mapStateToProps = ({ auth, common, flinkPlay }) => ({
  auth,
  learner: flinkPlay.learner,
  common,
});

export default compose(
  withRouter,
  connect(mapStateToProps, {
    getCommonData,
    changeLocale,
    setAspectRatio,
  })
)(App);
