import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Root from 'Root';
import App from 'App';

import 'react-quill/dist/quill.snow.css';
import './scss/scaffolding.scss';
import './scss/utils.scss';
import { ErrorBoundary } from 'components/hocs';

ReactDOM.render(
  <Root>
    <ErrorBoundary>
      <Router>
        <App />
      </Router>
    </ErrorBoundary>
  </Root>,
  document.getElementById('root')
);
