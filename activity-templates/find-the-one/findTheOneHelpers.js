import _ from "lodash";
import shortid from "shortid";

import { addIdToFilename } from "utils";
import { getWordPictureFilename } from "utils/wordlistHelpers";
import { getInstructionsAudioFilesFromProblem } from "activity-templates/utils";

export const validateItem = item =>
  !!(item && (item.text || item.image || item.audio));

export const validateProblem = problem => {
  return !!(
    problem.questions &&
    problem.questions.length &&
    _.filter(problem.questions, item => validateItem(item)).length &&
    problem.correctAnswers &&
    problem.correctAnswers.length &&
    _.filter(problem.correctAnswers, item => validateItem(item)).length &&
    problem.incorrectAnswers &&
    problem.incorrectAnswers.length &&
    _.filter(problem.incorrectAnswers, item => validateItem(item)).length
  );
};

export const createProblem = () => ({
  id: shortid.generate(),
  questions: [],
  correctAnswers: [],
  incorrectAnswers: [],
  fontColor: "#000",
  fontFamily: "Arial",
  incorrectItemsShown: 2
});

export const getContentFilesFromItem = item => {
  const files = [];
  if (!item) return files;

  const { image, audio } = item;
  image && files.push(image);
  audio && files.push(audio);

  return files;
};

export const getContentFilesFromProblem = problem => {
  let files = [];
  const { questions, correctAnswers, incorrectAnswers } = problem;

  const instructionsAudioFiles = getInstructionsAudioFilesFromProblem(problem);
  files.push(...instructionsAudioFiles);

  _.concat(questions, correctAnswers, incorrectAnswers).forEach(item => {
    files = _.concat(files, getContentFilesFromItem(item));
  });
  console.log(files);
  return files;
};

export const validateWords = (words, templateDBrecord) => {
  const filteredWords = words.filter(w => w.hasPicture);

  if (filteredWords.length < 3) {
    return { success: false, filteredWords };
  }

  return { success: true, filteredWords };
};

export const generateActivityFromWords = ({ words, template }) => {
  const wordsImages = [];
  const wordsAudio = [];

  const { audioRequired } = template;

  const filteredWords = words.filter(w => w.hasPicture);

  const problems = filteredWords.map(w => {
    const incorrectWords = _.chain(words)
      .filter(word => word._id !== w._id)
      .shuffle()
      .take(2)
      .value();

    const questionImageSrc = getWordPictureFilename(w);
    const questionImage = addIdToFilename(questionImageSrc);

    questionImage &&
      wordsImages.push({
        src: questionImageSrc,
        filename: questionImage
      });

    const question = { image: questionImage };

    let correctAnswers;

    if (audioRequired) {
      const correctAnswerAudio = addIdToFilename(w.wordAudio);
      correctAnswers = [{ audio: correctAnswerAudio }];
      wordsAudio.push({ src: w.wordAudio, filename: correctAnswerAudio });
    } else {
      correctAnswers = [{ text: w.word }];
    }

    const incorrectAnswers = incorrectWords.map(w => {
      if (!audioRequired) {
        return { text: w.word };
      }

      const filename = addIdToFilename(w.wordAudio);
      wordsAudio.push({ src: w.wordAudio, filename });

      return { audio: filename };
    });

    return {
      id: shortid.generate(),
      questions: [question],
      correctAnswers,
      incorrectAnswers,
      fontColor: "#000",
      fontFamily: "Roboto",
      incorrectItemsShown: 2
    };
  });

  const gameData = { problems };

  return { gameData, wordsImages, wordsAudio };
};
