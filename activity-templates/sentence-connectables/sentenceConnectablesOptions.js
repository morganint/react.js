export default [
  'delayBeforeNext',
  'font',
  'randomOrder',
  'showAnswer',
  'questionsLimit',
  'audioBefore'
];
