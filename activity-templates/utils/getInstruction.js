export default (
  problem,
  solutionLocale,
  textPath = 'instructionText',
  audioPath = 'instructionAudio'
) => {
  if (!problem) return {};

  const { multiLocaleInstructions } = problem;

  const locale =
    (solutionLocale && solutionLocale.code) ||
    (multiLocaleInstructions && multiLocaleInstructions.defaultLocale) ||
    'en';

  let audio = multiLocaleInstructions
    ? multiLocaleInstructions[locale] &&
      multiLocaleInstructions[locale][audioPath]
    : problem[audioPath];

  if (audio && audio.match(/^0+$/)) {
    audio = '';
  }

  const text = multiLocaleInstructions
    ? multiLocaleInstructions[locale] &&
      multiLocaleInstructions[locale][textPath]
    : problem[textPath];

  return { audio, text };
};
