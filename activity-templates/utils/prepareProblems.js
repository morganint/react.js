import _ from 'lodash';

export default (problems, options, validateFn) => {
  // Remove invalid problems
  let filteredProblems = problems.filter(validateFn);

  const { questionsLimit, randomOrder } = options;

  filteredProblems = randomOrder
    ? _.shuffle(filteredProblems)
    : filteredProblems;

  // Need to trim?
  if (
    questionsLimit &&
    questionsLimit > 0 &&
    questionsLimit < filteredProblems.length
  ) {
    filteredProblems = _.take(filteredProblems, questionsLimit);
  }

  return filteredProblems;
};
