import _ from 'lodash';
import shortId from 'shortid';

import { getInstructionsAudioFilesFromProblem } from 'activity-templates/utils';
import { addIdToFilename } from 'utils';

export const validateItem = item => !!(item && item.text);

export const validateProblem = problem => {
  const { questions, correctAnswers } = problem;
  if (
    !(questions && questions.length && correctAnswers && correctAnswers.length)
  ) {
    return false;
  }

  const validQuestions = questions.filter(validateItem);
  const validAnswers = correctAnswers.filter(validateItem);

  if (
    !validQuestions.length ||
    !validAnswers.length ||
    validQuestions.length < 3 ||
    validQuestions.length !== validAnswers.length
  ) {
    return false;
  }

  for (let i = 0; i < questions.length; i++) {
    const isValidQuestion = validateItem(questions[i]);
    const isValidAnswer = validateItem(correctAnswers[i]);

    if (isValidQuestion && !isValidAnswer) return false;
  }

  return true;
};

export const createProblem = () => ({
  id: shortId.generate(),
  questions: [],
  correctAnswers: []
});

export const getContentFilesFromItem = item => {
  const files = [];
  if (!item) return files;

  const { image, audio } = item;
  image && files.push(image);
  audio && files.push(audio);

  return files;
};

export const getContentFilesFromProblem = problem => {
  let files = [];
  const { questions, correctAnswers, incorrectAnswers } = problem;

  const instructionsAudioFiles = getInstructionsAudioFilesFromProblem(problem);
  files.push(...instructionsAudioFiles);

  _.concat(questions, correctAnswers, incorrectAnswers).forEach(item => {
    files = _.concat(files, getContentFilesFromItem(item));
  });
  console.log(files);
  return files;
};

export const prepareProblem = problem => {
  if (!problem) return null;

  const { questions, correctAnswers } = problem;

  const validQuestions = questions.filter(validateItem);
  const validCorrectAnswers = correctAnswers.filter(validateItem);

  let preparedCorrectAnswers = [];

  let preparedQuestionItems = validQuestions.map((q, idx) => {
    const id = shortId.generate();
    const answerId = shortId.generate();

    preparedCorrectAnswers.push({
      id: answerId,
      data: validCorrectAnswers[idx],
      correct: true,
      relatedToId: id
    });

    return {
      id,
      answerId,
      data: q
    };
  });

  preparedQuestionItems = _.shuffle(preparedQuestionItems);

  preparedCorrectAnswers = preparedCorrectAnswers.filter(ans => {
    const question = _.find(preparedQuestionItems, { id: ans.relatedToId });
    return !!question;
  });

  const startPositions = _.chain(preparedCorrectAnswers.length)
    .times()
    .shuffle()
    .value();

  return {
    ...problem,
    questions: preparedQuestionItems,
    items: _.chain(preparedCorrectAnswers)
      .map((item, idx) => ({
        ...item,
        boxIdx: startPositions[idx],
        inSlot: false
      }))
      .shuffle()
      .value()
  };
};

export const validateWords = (words, templateDBrecord) => {
  // Should be minimum 3 words
  if (words.length < 3) {
    return { success: false, error: 'Should be minimum 3 words' };
  }

  return { success: true, filteredWords: words };
};

export const generateActivityFromWords = ({
  words,
  includeDefinitionAudio
}) => {
  const definitionsAudio = [];

  let chunks = _.chunk(words, 3);
  const lastChunk = chunks.pop();

  if (lastChunk.length === 1) {
    chunks[0].push(lastChunk[0]);
  } else if (lastChunk.length === 2) {
    if (chunks.length > 1) {
      chunks[0].push(lastChunk[0]);
      chunks[1].push(lastChunk[1]);
    }
  } else {
    chunks.push(lastChunk);
  }

  const problems = chunks.map(chunk => {
    const questions = [];
    const correctAnswers = [];

    chunk.forEach(w => {
      let definitionAudio = null;

      if (includeDefinitionAudio) {
        definitionAudio =
          w.definitionAudio && addIdToFilename(w.definitionAudio);

        definitionAudio &&
          definitionsAudio.push({
            src: w.definitionAudio,
            filename: definitionAudio
          });
      }

      questions.push({
        text: w.definition,
        audio: definitionAudio
      });

      correctAnswers.push({
        text: w.word
      });
    });

    const problem = {
      id: shortId.generate(),
      questions,
      correctAnswers
    };

    return problem;
  });

  const gameData = { problems };

  return { gameData, definitionsAudio };
};
