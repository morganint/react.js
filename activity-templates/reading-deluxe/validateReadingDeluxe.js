import { validateProblem } from "./readingDeluxeHelpers";

export default ({ options, gameData }) => {
  return !(
    !options ||
    !gameData ||
    !gameData.problems ||
    !gameData.problems.filter(validateProblem).length
  );
};
