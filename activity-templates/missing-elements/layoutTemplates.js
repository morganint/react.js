import {
  IMAGE_IN_TOP,
  IMAGE_IN_RIGHT,
  BIG_TEXT,
  SMALL_TEXT
} from './layoutTypes';

import classes from './MissingElements.module.scss';

const imageInTop = (text, img) => `
  <div class="${classes.layoutImageTop}">
    <div class="${classes.imageWrapper}">
      ${img ? `<img  src="${img}" alt=""/>` : ''}
    </div>

    <div class="${classes.textWrapper}">${text}</div>
  </div>
`;

const imageInRight = (text, img) => `
  <div class="${classes.layoutImageRight}">
    <div class="${classes.textWrapper}">${text}</div>
    <div class="${classes.imageWrapper}">
      ${img ? `<img  src="${img}" alt=""/>` : ''}
    </div>
  </div>
`;

const bigText = (text) => `
  <div class="${classes.layoutBigText}">
    <div class="${classes.textWrapper}">${text}</div>
  </div>
`;

const smallText = (text) => `
  <div class="${classes.layoutSmallText}">
    <div class="${classes.textWrapper}">${text}</div>
  </div>
`;

export default {
  [IMAGE_IN_TOP]: imageInTop,
  [IMAGE_IN_RIGHT]: imageInRight,
  [BIG_TEXT]: bigText,
  [SMALL_TEXT]: smallText
};
