import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import admin from './adminReducer';
import audio from './audioReducer';
import auth from './authReducer';
import common from './commonReducer';
import families from './familiesReducer';
import flinkAdmin from './flinkAdminReducer';
import flinkPlay from './flinkPlayReducer';
import status from './statusReducer';
import flinkMake from './flinkMakeReducer';
import game from './gameReducer';

import { LOGOUT } from 'actions/types';

const appReducer = combineReducers({
  audio,
  auth,
  admin,
  common,
  flinkAdmin,
  status,
  families,
  flinkMake,
  flinkPlay,
  game,
  form: formReducer
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
    // state = undefined;
    state = {
      common: state.common
    };
  }

  return appReducer(state, action);
};

export default rootReducer;
