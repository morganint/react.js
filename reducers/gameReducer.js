import {
  SHOW_STATS,
  SET_CURRENT_ACTIVITY,
  OPEN_ACTIVITY_HELP,
  CLOSE_ACTIVITY_HELP,
  GAME_INITIALIZED,
  RESTART_ACTIVITY,
  SHOW_GAME_PARENT_ADVICE,
  OPEN_TEAM_ACTIVITY_HELP,
  CLOSE_TEAM_ACTIVITY_HELP,
} from 'actions/types';

const INITIAL_STATE = {
  isPreview: false,
  goBackHandler: null,
  currentActivity: null,
  shouldGameInit: false,
  initialTeamActivityHelpShown: false,
  isTeamActivityHelpOpened: false,
  initialActivityHelpShown: false,
  isActivityHelpOpened: false,
  stats: null,
  gameParentAdvice: false,
  numberOfPlays: 0,
  gameInitialized: false,
};

const filter = 'game';

export default function (state = INITIAL_STATE, action) {
  if (action.filter !== filter) {
    return state;
  }

  switch (action.type) {
    case SET_CURRENT_ACTIVITY:
      const { payload } = action;

      if (!payload) return INITIAL_STATE;

      return {
        ...INITIAL_STATE,
        ...payload,
      };
    case RESTART_ACTIVITY:
      return {
        ...INITIAL_STATE,
        isPreview: state.isPreview,
        goBackHandler: state.goBackHandler,
        currentActivity: { ...state.currentActivity },
        gameStartedAt: state.gameStartedAt,
        numberOfPlays: state.numberOfPlays,
      };
    case GAME_INITIALIZED:
      return {
        ...state,
        gameInitialized: true,
        gameStartedAt: state.gameStartedAt || new Date(),
        numberOfPlays: state.numberOfPlays + 1,
      };
    case OPEN_ACTIVITY_HELP:
      return { ...state, isActivityHelpOpened: true };
    case OPEN_TEAM_ACTIVITY_HELP:
      return { ...state, isTeamActivityHelpOpened: true };
    case SHOW_STATS:
      return { ...state, stats: action.payload };
    case CLOSE_ACTIVITY_HELP:
      if (!state.initialActivityHelpShown) {
        return {
          ...state,
          isActivityHelpOpened: false,
          initialActivityHelpShown: true,
          shouldGameInit: true,
        };
      } else {
        return {
          ...state,
          isActivityHelpOpened: false,
        };
      }
    case CLOSE_TEAM_ACTIVITY_HELP:
      if (!state.initialTeamActivityHelpShown) {
        return {
          ...state,
          isTeamActivityHelpOpened: false,
          initialTeamActivityHelpShown: true,
          shouldGameInit: true,
        };
      } else {
        return {
          ...state,
          isTeamActivityHelpOpened: false,
        };
      }
    case SHOW_GAME_PARENT_ADVICE:
      return {
        ...state,
        gameParentAdvice: action.payload,
      };
    default:
      return state;
  }
}
