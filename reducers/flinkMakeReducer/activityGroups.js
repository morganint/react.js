// import {
//   SET_ACTIVITY_GROUPS_LIST,
//   ADD_ACTIVITY_GROUP,
//   EDIT_ACTIVITY_GROUP,
//   SET_ACTIVE_ACTIVITY_GROUP,
//   CHANGE_ACTIVITY_GROUP_WITHOUT_SAVING
// } from 'actions/types';

// const INITIAL_STATE = {
//   list: null,
//   notSaved: [],
//   active: null
// };

// const filter = 'flinkMake/activityGroups';

// export default function(state = INITIAL_STATE, action) {
//   if (action.filter !== filter) {
//     return state;
//   }

//   switch (action.type) {
//     case SET_ACTIVE_ACTIVITY_GROUP:
//       return {
//         ...state,
//         active: action.payload
//       };
//     case SET_ACTIVITY_GROUPS_LIST:
//       return {
//         ...state,
//         list: action.payload,
//         notSaved: []
//       };
//     case ADD_ACTIVITY_GROUP:
//       return {
//         ...state,
//         list: [...state.list, action.payload]
//       };
//     case EDIT_ACTIVITY_GROUP:
//       return {
//         ...state,
//         list: state.list.map(item =>
//           item._id === action.payload._id ? action.payload : item
//         ),
//         notSaved: state.notSaved.filter(
//           groupId => groupId !== action.payload._id
//         )
//       };
//     case CHANGE_ACTIVITY_GROUP_WITHOUT_SAVING:
//       return {
//         ...state,
//         list: state.list.map(item =>
//           item._id === action.payload._id ? action.payload : item
//         ),
//         notSaved: state.notSaved.includes(action.payload._id)
//           ? state.notSaved
//           : [...state.notSaved, action.payload._id]
//       };
//     default:
//       return state;
//   }
// }
