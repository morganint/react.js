import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

import classes from './JsonBuilderModal.module.scss';
import JsonBuilder from '../JsonBuilder/JsonBuilder';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class JsonBuilderModal extends Component {
  state = {
    isOpen: false
  };

  handleClickOpen = () => {
    this.setState({ isOpen: true });
  };

  handleClose = () => {
    this.setState({ isOpen: false });
  };

  render() {
    const { isOpen } = this.state;

    return (
      <Fragment>
        <div className={classes.buttonWrapper}>
          JSON File
          <Button
            color="primary"
            variant="outlined"
            onClick={this.handleClickOpen}
          >
            Create
          </Button>
        </div>

        <div>
          <Dialog
            fullScreen
            open={isOpen}
            onClose={this.handleClose}
            TransitionComponent={Transition}
          >
            <AppBar className={classes.appBar} position="static"> 
              <Toolbar>
                <IconButton
                  color="inherit"
                  onClick={this.handleClose}
                  aria-label="Close"
                >
                  <CloseIcon />
                </IconButton>
                <Typography
                  variant="h6"
                  color="inherit"
                  className={classes.flex}
                >
                  Category JSON file builder
                </Typography>
                {/* <Button color="inherit" onClick={() => console.log('clear')} className={classes.clearBtn}>
                  clear
                </Button>
                <Button color="inherit" onClick={() => console.log('save')}>
                  save
                </Button> */}
              </Toolbar>
            </AppBar>
            <JsonBuilder />
            
          </Dialog>
        </div>
      </Fragment>
    );
  }
}

JsonBuilderModal.propTypes = {};

export default JsonBuilderModal;
