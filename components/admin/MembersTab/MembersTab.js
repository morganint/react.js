import React, { useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Button from '@material-ui/core/Button';

import MemberForm from './MemberForm';
import { deleteData } from 'actions/adminActions';
import { showError } from 'actions/statusActions';
import MaterialTable from 'components/admin/MaterialTable/MaterialTable';
import { withData } from 'components/hocs';

const rows = [
  {
    id: 'firstname',
    disablePadding: false,
    label: 'Firstname'
  },
  {
    id: 'lastname',
    disablePadding: false,
    label: 'Lastname'
  },
  {
    id: 'username',
    disablePadding: false,
    label: 'Email'
  }
];

const Members = ({
  memberTitle,
  deleteData,
  showError,
  auth,
  users,
  membersRole,
  assign,
  organization
}) => {
  const deleteUserHandler = (id, e) => {
    if (id === organization.contactUserId) {
      showError({
        message: 'You cant delete that member cause it is used as Contact User'
      });
      return;
    }

    deleteData('users', id);
  };

  const filteredUsers = useMemo(
    () =>
      _.reject(
        users,
        user =>
          user._id === auth.user._id ||
          user.roleAlias !== membersRole ||
          user[assign.prop] !== assign.value
      ),
    [users, assign, auth, membersRole]
  );

  const [isOpenForm, setOpenForm] = useState(false);
  const [userToEdit, setUserToEdit] = useState(null);

  return (
    <div style={{ marginTop: '20px' }}>
      <Button
        color="primary"
        variant="outlined"
        onClick={() => setOpenForm(true)}
      >
        Create {memberTitle}
      </Button>

      <MaterialTable
        data={filteredUsers}
        rows={rows}
        deleteHandler={deleteUserHandler}
        editHandler={user => {
          setUserToEdit(user);
          setOpenForm(true);
        }}
        type="Member"
        rowWithName="username"
      />

      {isOpenForm && (
        <MemberForm
          onSuccess={() => setOpenForm(false)}
          isOpen={isOpenForm}
          membersRole={membersRole}
          assign={assign}
          initialValues={userToEdit}
          closeHandler={() => {
            setUserToEdit(null);
            setOpenForm(false);
          }}
        />
      )}
    </div>
  );
};

Members.propTypes = {
  users: PropTypes.array.isRequired,
  auth: PropTypes.object.isRequired,
  deleteData: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth }) => ({
  auth
});

export default compose(
  withData(['users']),
  connect(
    mapStateToProps,
    { deleteData, showError }
  )
)(Members);
