import React from 'react';
import styled from 'styled-components';
import { transform, filter } from 'lodash';
import PropTypes from 'prop-types';

function ProductsList({ purchasedProducts, products }) {
  const purchasedProductsArray = transform(
    purchasedProducts,
    (result, value, key) => {
      const prod = filter(products, { _id: key })[0];

      if (prod) {
        const purchasedProdWithName = { ...value, name: prod.name, id: key };
        return result.push(purchasedProdWithName);
      }

      return result;
    },
    []
  );

  return (
    <StyledTable>
      <thead>
        <tr>
          <th>Products Purchased</th>
          <th>Max Number Teachers</th>
          <th>Teachers Available</th>
          <th>Max Number Students</th>
          <th>Students Available</th>
        </tr>
      </thead>
      <tbody>
        {purchasedProductsArray.map(
          (
            {
              id,
              name,
              maxTeachers,
              maxStudents,
              availableTeachers,
              availableStudents
            },
            indx
          ) => (
            <tr key={id}>
              <td>
                {indx + 1}. {name}
              </td>
              <td align="center">
                <StyledNumberCell>{maxTeachers}</StyledNumberCell>
              </td>
              <td align="center">
                <StyledNumberCell>{availableTeachers}</StyledNumberCell>
              </td>
              <td align="center">
                <StyledNumberCell>{maxStudents}</StyledNumberCell>
              </td>
              <td align="center">
                <StyledNumberCell>{availableStudents}</StyledNumberCell>
              </td>
            </tr>
          )
        )}
      </tbody>
    </StyledTable>
  );
}

ProductsList.propTypes = {
  products: PropTypes.array.isRequired,
  purchasedProducts: PropTypes.object.isRequired
};

export default ProductsList;

const StyledTable = styled.table`
  margin-top: 20px;
  margin-bottom: 20px;
  margin-left: 4px;
  border-collapse: collapse;

  tbody tr {
    &:hover {
      background-color: #e8e8e8;
    }
  }

  th {
    font-weight: 500;
    width: 150px;

    &:first-child {
      text-align: left;
      width: 250px;
    }
  }
`;

const StyledNumberCell = styled.span`
  border: 1px solid #e8e8e8;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 30px;
  width: 60px;
`;
