import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Select from 'react-select';
import makeAnimated from 'react-select/lib/animated';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';

import ScopeDataColumn from './ScopeDataColumn/ScopeDataColumn';
import classes from './ScopeBuilder.module.scss';

const LEVELS = 'levels';
const GRADES = 'grades';

class ScopeBuilder extends Component {
  state = {
    selected: {},
    data: {
      grades: [],
      levels: [],
      assignedLevels: [],
      titleCategory1: 'Subjects',
      titleCategory2: 'Topics',
      titleCategory3: 'Skills',
      dataCategory1: [],
      dataCategory2: [],
      dataCategory3: []
    }
  };

  componentDidMount() {
    const { initialData: data } = this.props;

    if (data) {
      this.setState({ data });
    } else {
      this.changeData(this.state.data);
    }
  }

  changeGrades = e => {
    const {
      data: { grades, levels, firstMenu, assignedLevels }
    } = this.state;
    const {
      options: { gradesOptions }
    } = this.props;

    const { checked, value: grade } = e.target;
    const newGrades = [...grades];

    checked ? newGrades.push(grade) : _.pull(newGrades, grade);

    const newAssignedLevels = checked
      ? assignedLevels
      : assignedLevels.filter(lev => lev.grade !== grade);

    const sortedGrades = newGrades.sort(
      (a, b) =>
        _.findIndex(gradesOptions, { value: a }) -
        _.findIndex(gradesOptions, { value: b })
    );

    const newFirstMenu = this.getFirstMenu(sortedGrades, levels, firstMenu);

    this.changeData({
      grades: sortedGrades,
      firstMenu: newFirstMenu,
      assignedLevels: newAssignedLevels
    });
  };

  changeLevels = e => {
    const {
      data: { assignedLevels, levels, grades, firstMenu }
    } = this.state;
    const {
      options: { levelsOptions }
    } = this.props;

    const { checked, value: level } = e.target;
    const newLevels = [...levels];

    checked ? newLevels.push(level) : _.pull(newLevels, level);

    const newAssignedLevels = checked
      ? assignedLevels
      : assignedLevels.filter(lev => lev.level !== level);

    const sortedLevels = newLevels.sort(
      (a, b) =>
        _.findIndex(levelsOptions, { value: a }) -
        _.findIndex(levelsOptions, { value: b })
    );

    const newFirsMenu = this.getFirstMenu(grades, sortedLevels, firstMenu);

    this.changeData({
      levels: sortedLevels,
      firstMenu: newFirsMenu,
      assignedLevels: newAssignedLevels
    });
  };

  

  getFirstMenu = (grades, levels, current) => {
    if (grades.length && levels.length) {
      return current || GRADES;
    }

    if (grades.length && !levels.length) {
      return GRADES;
    }

    if (levels.length && !grades.length) {
      return LEVELS;
    }
  };

  assignLevels = (options, info) => {
    const {
      data: { assignedLevels = [] }
    } = this.state;

    const { action, name: grade } = info;
    let newAssociatedLevels = [...assignedLevels];

    if (action === 'select-option') {
      const { value: selectedLevel } = info.option;
      newAssociatedLevels.push({ level: selectedLevel, grade });
    }

    if (action === 'clear') {
      newAssociatedLevels = _.reject(newAssociatedLevels, { grade });
    }

    if (action === 'remove-value') {
      const { value: removedLevel } = info.removedValue;
      newAssociatedLevels = _.reject(newAssociatedLevels, {
        level: removedLevel
      });
    }

    this.changeData({ assignedLevels: newAssociatedLevels });
  };

  changeData = data => {
    this.props.changeHandler(data);

    this.setState(state => ({
      data: { ...state.data, ...data }
    }));
  };

  changeFirstMenu = firstMenu => {
    this.changeData({ firstMenu });
  };

  handleSelect = (category, id) => {
    const { selected } = this.state;

    let newSelected;

    if (category === 'category1') {
      newSelected = { [category]: id };
    } else if (category === 'category2') {
      newSelected = { category1: selected.category1, [category]: id };
    } else {
      newSelected = { ...selected, [category]: id };
    }

    this.setState({ selected: newSelected });
  };

  render() {
    const {
      options: { gradesOptions, levelsOptions }
    } = this.props;
    const {
      selected,
      data: {
        grades,
        levels,
        titleCategory1,
        titleCategory2,
        titleCategory3,
        dataCategory1,
        dataCategory2,
        dataCategory3
      }
    } = this.state;

    return (
      <div className={classes.wrapper}>
        <span className={classes.formLabel}>Select Grades:</span>
        {gradesOptions &&
          gradesOptions.map((opt, idx) => (
            <FormControlLabel
              key={opt.value}
              control={
                <Checkbox
                  color="primary"
                  checked={grades.includes(opt.value)}
                  onChange={this.changeGrades}
                  value={opt.value}
                />
              }
              label={opt.label}
            />
          ))}

        <span className={classes.formLabel}>Select Levels:</span>
        {levelsOptions &&
          levelsOptions.map(opt => (
            <FormControlLabel
              key={opt.value}
              control={
                <Checkbox
                  color="primary"
                  checked={levels.includes(opt.value)}
                  onChange={this.changeLevels}
                  value={opt.value}
                />
              }
              label={opt.label}
            />
          ))}

        {this.renderAssociateLevels()}

        <div className={classes.columnsWrapper}>
          <ScopeDataColumn
            classes={classes}
            title={titleCategory1}
            titleProp="titleCategory1"
            data={dataCategory1}
            dataProp="dataCategory1"
            selected={selected.category1}
            handleSelect={this.handleSelect.bind(null, 'category1')}
            changeDataHandler={this.changeData}
          />
          <ScopeDataColumn
            classes={classes}
            title={titleCategory2}
            titleProp="titleCategory2"
            data={dataCategory2}
            dataProp="dataCategory2"
            selected={selected.category2}
            handleSelect={this.handleSelect.bind(null, 'category2')}
            dependencies={{ category1_id: selected.category1 }}
            changeDataHandler={this.changeData}
          />
          <ScopeDataColumn
            classes={classes}
            title={titleCategory3}
            titleProp="titleCategory3"
            data={dataCategory3}
            dataProp="dataCategory3"
            selected={selected.category3}
            handleSelect={this.handleSelect.bind(null, 'category3')}
            dependencies={{
              category1_id: selected.category1,
              category2_id: selected.category2
            }}
            changeDataHandler={this.changeData}
          />
        </div>
      </div>
    );
  }

  renderAssociateLevels = () => {
    const {
      data: { grades, levels, assignedLevels, firstMenu }
    } = this.state;
    if (!grades.length || !levels.length) return null;

    return (
      <Fragment>
        <span className={classes.formLabel}>Associate grades with levels:</span>
        <div className={classes.gradeGroups}>
          {grades.map(grade => {
            const options = levels
              .filter(lev => {
                const associatedLevel = _.find(assignedLevels, {
                  level: lev
                });
                return !associatedLevel || associatedLevel.grade === grade;
              })
              .map(lev => ({ value: lev, label: lev }));

            const selectedOptions = options.filter(
              opt =>
                _.findIndex(assignedLevels, { level: opt.value, grade }) !== -1
            );

            return (
              <div key={grade}>
                <span className={classes.formLabel}>{grade}:</span>
                <Select
                  value={selectedOptions}
                  closeMenuOnSelect={false}
                  isMulti
                  name={grade}
                  components={makeAnimated()}
                  options={options}
                  onChange={this.assignLevels}
                  className="basic-multi-select"
                  classNamePrefix="select"
                />
              </div>
            );
          })}
        </div>

        <span className={classes.formLabel}>First Menu:</span>
        <RadioGroup
          name="firstMenu"
          value={firstMenu}
          onChange={(e, val) => this.changeFirstMenu(val)}
          options={[]}
          className={classes.radioGroup}
        >
          <FormControlLabel
            value={GRADES}
            control={<Radio color="primary" />}
            label="Grades"
          />
          <FormControlLabel
            value={LEVELS}
            control={<Radio color="primary" />}
            label="Levels"
          />
        </RadioGroup>
      </Fragment>
    );
  };
}

ScopeBuilder.propTypes = {
  options: PropTypes.object.isRequired,
  initialData: PropTypes.object,
  changeHandler: PropTypes.func.isRequired
};

export default ScopeBuilder;
