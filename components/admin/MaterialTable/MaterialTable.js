import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { isArray, isEqual } from "lodash";
import shortid from "shortid";
import memoize from "memoize-one";
import { connect } from "react-redux";
import { compose } from "redux";

import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";

import ConfirmDeleteModal from "components/admin/ConfirmDeleteModal/ConfirmDeleteModal";
import EnhancedTableHead from "./EnhancedTableHead";
import AdditionalButtons from "./AdditionalButtons";
import TablePaginationActions from "./TablePaginationActions";
// import EnhancedTableToolbar from './EnhancedTableToolbar';
import { formatValue, stableSort, filterFunction } from "./helpers";
import { setTableState } from "actions/adminActions";
import {
  MaterialFaIcon,
  getSorting,
  getStudentsByTeacherAndProduct
} from "utils";

const styles = theme => ({
  table: {
    // minWidth: 1020
  },
  tableWrapper: {
    overflowX: "auto"
  },
  nowrap: {
    whiteSpace: "nowrap",
    textAlign: "right"
  }
});

class MaterialTable extends React.Component {
  constructor(props) {
    super(props);

    const { rows } = props;

    const filterBy = rows[0];

    this.state = {
      order: "asc",
      orderBy: filterBy ? filterBy.id : "username",
      selected: [],
      page: 0,
      rowsPerPage: 10,
      modalOpen: false,
      selectedItemToDelete: {},
      filter: {}
    };
  }

  componentDidMount() {
    const { type, tablesStates } = this.props;

    const state = tablesStates[type];

    if (state) {
      this.setState(JSON.parse(state));
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { type, setTableState } = this.props;
    const { order, orderBy, selected, page, rowsPerPage, filter } = this.state;

    if (
      order !== prevState.order ||
      orderBy !== prevState.orderBy ||
      selected !== prevState.selected ||
      page !== prevState.page ||
      rowsPerPage !== prevState.rowsPerPage ||
      filter !== prevState.filter
    ) {
      setTableState(type, JSON.stringify(this.state));
    }
  }

  // componentWillReceiveProps(nextProps, nextState) {
  //   this.setState({ data: nextProps.data, filteredData: nextProps.data });
  // }

  handleFilter = (prop, selectedOption) => {
    const filter = { ...this.state.filter };

    if (isArray(selectedOption) || !selectedOption) {
      delete filter[prop];
    } else {
      filter[prop] = selectedOption.value;
    }

    this.setState({
      page: 0,
      filter
    });
  };

  filterData = memoize((data, filter) => {
    const filteredData = data.filter(ele => filterFunction(ele, filter));

    return filteredData;
  }, isEqual);

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = event => {
    const { data } = this.props;
    if (event.target.checked) {
      this.setState(state => ({ selected: data.map(n => n._id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  handleCloseModal = () => {
    this.setState({ modalOpen: false, selectedItemToDelete: {} });
  };

  handleOpenDeleteModal = selectedItemToDelete => {
    this.setState({ modalOpen: true, selectedItemToDelete });
  };

  render() {
    const {
      classes,
      rows,
      deleteHandler,
      editLink,
      editHandler,
      type,
      rowWithName,
      additionalButtons,
      productRows,
      additionalFunctions,
      students,
      data
    } = this.props;
    const {
      order,
      orderBy,
      selected,
      rowsPerPage,
      page,
      selectedItemToDelete,
      filter
    } = this.state;

    const filteredData = this.filterData(data, filter);

    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <div>
        <ConfirmDeleteModal
          isOpen={this.state.modalOpen}
          handleClose={this.handleCloseModal}
          handleDelete={e => {
            deleteHandler(selectedItemToDelete._id, e);
            this.handleCloseModal();
          }}
          text={`${type} - ${selectedItemToDelete[rowWithName]}`}
        />
        {/* <EnhancedTableToolbar title={title} numSelected={selected.length} /> */}
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              filter={filter}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              rows={rows}
              productRows={productRows}
              filteredData={filteredData}
              handleFilter={this.handleFilter}
            />
            <TableBody>
              {stableSort(filteredData, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, index) => {
                  const isSelected = this.isSelected(item._id);
                  return (
                    <TableRow
                      hover
                      // onClick={event => this.handleClick(event, item._id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={item._id}
                      selected={isSelected}
                    >
                      {/* <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell> */}
                      {/* <TableCell padding="checkbox">{index + 1}</TableCell> */}

                      {this.renderRows(item)}

                      <TableCell
                        component="td"
                        scope="row"
                        padding="none"
                        className={classes.nowrap}
                      >
                        {additionalButtons && (
                          <AdditionalButtons
                            additionalButtons={additionalButtons}
                            item={item}
                          />
                        )}

                        {editLink && (
                          <Link to={`${editLink}/${item._id}`}>
                            <Tooltip title="Edit">
                              <IconButton aria-label="Edit">
                                <MaterialFaIcon icon="pencil-alt" />
                              </IconButton>
                            </Tooltip>
                          </Link>
                        )}

                        {editHandler && (
                          <Tooltip title="Edit">
                            <IconButton
                              aria-label="Edit"
                              onClick={() => editHandler(item)}
                            >
                              <MaterialFaIcon icon="pencil-alt" />
                            </IconButton>
                          </Tooltip>
                        )}

                        {deleteHandler &&
                        !item.alias && ( // if item has alias in code, then in can't be deleted
                            <Tooltip title="Delete">
                              <IconButton
                                onClick={this.handleOpenDeleteModal.bind(
                                  null,
                                  item
                                )}
                                aria-label="Delete"
                              >
                                <MaterialFaIcon icon="trash-alt" />
                              </IconButton>
                            </Tooltip>
                          )}
                      </TableCell>

                      {productRows &&
                        productRows.map(({ productId, productInfo }) => {
                          // This code will run only in Teachers Admin Page,
                          // It's render additional columns in table for
                          // Associating teachers with products and for
                          // Show how much students have this teacher, and Max number
                          // of students that he can associate with current product
                          const teacher = item;

                          // check if current product assigned to current teacher
                          const isAssigned =
                            !!teacher.products &&
                            teacher.products.includes(productId);

                          const {
                            productAssignHandler,
                            changeMaxStudentsHandler
                          } = additionalFunctions;

                          const tooltipForCheckbox = isAssigned
                            ? `Unassign from product "${productInfo.name}"`
                            : `Assign to product "${productInfo.name}"`;

                          // get max number of students for this product
                          const maxNumber =
                            !!teacher.maxStudents &&
                            teacher.maxStudents[productId];

                          const studentsOfCurrentTeacher = getStudentsByTeacherAndProduct(
                            students,
                            teacher._id,
                            productId
                          );

                          const actualNumber = studentsOfCurrentTeacher.length;

                          return (
                            <Fragment key={shortid.generate()}>
                              <TableCell
                                padding="checkbox"
                                style={{ textAlign: "right" }}
                              >
                                <Tooltip title={tooltipForCheckbox}>
                                  <Checkbox
                                    onChange={(e, checked) =>
                                      productAssignHandler(
                                        teacher,
                                        productId,
                                        checked
                                      )
                                    }
                                    checked={isAssigned}
                                    color="secondary"
                                  />
                                </Tooltip>
                              </TableCell>
                              <TableCell
                                padding="checkbox"
                                style={{ textAlign: "center" }}
                              >
                                <Button
                                  disabled={!isAssigned}
                                  onClick={e =>
                                    changeMaxStudentsHandler(
                                      teacher,
                                      productId,
                                      actualNumber
                                    )
                                  }
                                >
                                  {maxNumber || "Set"}
                                </Button>
                              </TableCell>
                              <TableCell
                                padding="checkbox"
                                style={{ textAlign: "center" }}
                              >
                                {actualNumber}
                              </TableCell>
                            </Fragment>
                          );
                        })}
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={filteredData.length}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[5, 10, 25, 50, 100]}
          page={page}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
          ActionsComponent={TablePaginationActions}
        />
      </div>
    );
  }

  renderRows = item => {
    const { rows } = this.props;

    return rows.map(row => {
      let cellContent = null;

      if (row.type === "cols" && Array.isArray(item[row.id])) {
        const list = row.order
          ? item[row.id].sort(getSorting(row.order))
          : item[row.id];

        cellContent = (
          <ul
            style={{
              columnCount: row.colsCount,
              columnGap: 10
            }}
          >
            {list.map((value, i) => {
              return (
                <li
                  key={value + "" + i}
                  style={{
                    display: "block"
                  }}
                >
                  {value}
                </li>
              );
            })}
          </ul>
        );
      } else {
        cellContent = formatValue(item[row.id], row, item[row.add]);
      }

      return (
        <TableCell
          key={row.id}
          component="td"
          scope="row"
          align={row.align}
          padding={row.disablePadding ? "none" : "default"}
        >
          {cellContent}
        </TableCell>
      );
    });
  };
}

MaterialTable.defaultProps = {
  tablesStates: {}
};

MaterialTable.propTypes = {
  classes: PropTypes.object.isRequired,
  rows: PropTypes.array.isRequired,
  deleteHandler: PropTypes.func,
  editLink: PropTypes.string,
  type: PropTypes.string.isRequired,
  rowWithName: PropTypes.string.isRequired,
  additionalButtons: PropTypes.array,
  tablesStates: PropTypes.object,
  setTableState: PropTypes.func.isRequired
};

const mapStateToProps = ({ admin }) => ({
  tablesStates: admin.tablesStates
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, { setTableState })
)(MaterialTable);
