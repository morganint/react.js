import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { MaterialFaIcon } from 'utils';

const AdditionalButtons = ({ additionalButtons, item }) => {
  return additionalButtons.map(btn =>
    btn.url ? (
      <Link key={btn.title} to={`${btn.url}/${item._id}`}>
        <Tooltip title={btn.title}>
          <IconButton aria-label={btn.title}>
            <MaterialFaIcon icon={btn.icon.name} />
          </IconButton>
        </Tooltip>
      </Link>
    ) : (
      <Tooltip key={btn.title} title={btn.title}>
        <IconButton onClick={() => btn.action(item)} aria-label={btn.title}>
          <MaterialFaIcon icon={btn.icon.name} />
        </IconButton>
      </Tooltip>
    )
  );
};

AdditionalButtons.propTypes = {
  additionalButtons: PropTypes.array.isRequired
};

export default AdditionalButtons;
