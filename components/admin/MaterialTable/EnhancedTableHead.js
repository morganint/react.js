import React, { useMemo } from "react";
import PropTypes from "prop-types";

import Select from "react-select";
import _ from "lodash";
import TableHead from "@material-ui/core/TableHead";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import { formatValue } from "./helpers";
import { getSorting } from "utils";

const customSelectStyles = {
  dropdownIndicator: () => ({
    display: "none"
  }),
  indicatorSeparator: () => ({
    display: "none"
  })
};

const EnhancedTableHead = props => {
  const {
    order,
    orderBy,
    rows,
    filter,
    filteredData,
    handleFilter,
    onRequestSort
    // onSelectAllClick,
    // numSelected,
    // rowCount,
  } = props;

  const searchFields = useMemo(() => {
    return rows.map(row => {
      const options = _.chain(filteredData)
        .reduce((acc, curr) => {
          Array.isArray(curr[row.id])
            ? acc.push(...curr[row.id])
            : acc.push(curr[row.id]);
          return acc;
        }, [])
        .uniq()
        .sort(getSorting())
        .value()
        .map(data => ({
          label: formatValue(data, row),
          value: data
        }));

      const currentValue = filter[row.id];
      const currentOption = currentValue
        ? _.find(options, { value: currentValue })
        : // || {
          //     label: currentValue,
          //     value: currentValue
          //   }
          "";

      if (currentValue && !currentOption) {
        setTimeout(() => {
          handleFilter(row.id, "");
        }, 50);
      }

      return (
        <TableCell
          key={row.id}
          align={row.align}
          padding={row.disablePadding ? "none" : "default"}
        >
          <Select
            placeholder=""
            styles={customSelectStyles}
            onChange={handleFilter.bind(null, row.id)}
            isClearable
            value={currentOption}
            options={options}
          />
        </TableCell>
      );
    });
  }, [filteredData, handleFilter, rows, filter]);

  const titleFields = useMemo(() => {
    const createSortHandler = property => event => {
      onRequestSort(event, property);
    };

    return rows.map(row => {
      return (
        <TableCell
          key={row.id}
          padding={row.disablePadding ? "none" : "default"}
          sortDirection={orderBy === row.id ? order : false}
          style={{ width: row.width || "auto" }}
        >
          <Tooltip
            title="Sort"
            placement={row.align === "right" ? "bottom-end" : "bottom-start"}
            enterDelay={300}
          >
            <TableSortLabel
              active={orderBy === row.id}
              direction={order}
              onClick={createSortHandler(row.id)}
            >
              {row.label}
            </TableSortLabel>
          </Tooltip>
        </TableCell>
      );
    });
  }, [orderBy, order, rows, onRequestSort]);

  return (
    <TableHead>
      <TableRow>
        {/* <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell> */}
        {/* <TableCell padding="checkbox">#</TableCell> */}
        {titleFields}
        <TableCell padding="none" />
        {/* 
        {productRows &&
          productRows.map(({ productId, prodCount }) => (
            <TableCell
              colSpan={3}
              key={productId}
              padding="dense"
              style={{ textAlign: 'center' }}
            >
              {prodCount}
            </TableCell>
          ))} */}
      </TableRow>

      <TableRow>
        {searchFields}

        <TableCell padding="none" />

        {/* {productRows &&
          productRows.map(({ productId }) => (
            <Fragment key={productId}>
              <TableCell padding="checkbox" />
              <TableCell padding="checkbox" style={{ textAlign: 'center' }}>
                Max
              </TableCell>
              <TableCell padding="checkbox" style={{ textAlign: 'center' }}>
                Act
              </TableCell>
            </Fragment>
          ))} */}
      </TableRow>
    </TableHead>
  );
};

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

export default EnhancedTableHead;
