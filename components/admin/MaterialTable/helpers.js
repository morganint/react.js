import { getSorting } from "utils";

export const stableSort = (array, cmp) => {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
};

export const formatValue = (value, row = {}, add) => {
  const addValue = add ? ` ${add}` : "";

  const { type } = row;

  switch (type) {
    case "date":
      return value && new Date(value).toISOString().split("T")[0];
    case "array":
      return value
        ? Array.isArray(value)
          ? row.sort
            ? value.sort(getSorting(row.sort)).join(", ")
            : value.join(", ")
          : value
        : "";
    case "bool":
      return value ? "Yes" : "No";
    default:
      return (value ? value : "(not set)") + addValue;
  }
};

const getType = function(elem) {
  return Object.prototype.toString.call(elem).slice(8, -1);
};
const isObject = function(elem) {
  return getType(elem) === "Object";
};
const isArray = function(elem) {
  return getType(elem) === "Array";
};
// const isString = function(elem) {
//   return getType(elem) === "String";
// };

export const filterFunction = (val, val2) => {
  // if (getType(val) !== getType(val2)) return false;

  if (isObject(val) && isObject(val2)) {
    // eslint-disable-next-line
    for (const key in val2) {
      if (!filterFunction(val[key], val2[key])) return false;
    }

    return true;
  }

  if (isArray(val) && isArray(val2)) {
    // eslint-disable-next-line
    for (const i of val2) {
      if (!val.includes(i)) return false;
    }

    return true;
  }

  if (isArray(val) && getType(val2) === "String") {
    return val.includes(val2);
  }

  // if (isString(val) && isString(val2)) {
  //   return val.toLowerCase().indexOf(val2.toLowerCase()) !== -1;
  // }

  return val === val2;
};
