import React, { Fragment, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import { imagesURL } from 'config';
import { printElem } from 'utils';

const styles = {
  wrapper: {
    margin: '30px 0',
    width: '100%',
    maxWidth: 700,
    fontFamily: 'Roboto, sans-serif',
  },
  header: {
    padding: 5,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginBottom: 20,
    fontSize: 16,
  },
  title: {
    fontSize: 30,
    fontWeight: 400,
    marginTop: 0,
    marginBottom: 15,
  },
  subtitle: {
    fontSize: 20,
    fontWeight: 400,
    marginTop: 0,
    marginBottom: 15,
  },
  table: {
    width: '100%',
    borderCollapse: 'collapse',
  },
  info: {
    marginRight: 20,
  },
  logo: {
    width: '20%',
    flexShrink: 0,
  },
  th: {
    fontSize: '120%',
    fontWeight: 500,
    textAlign: 'left',
    padding: '20px 8px 8px',
    borderBottom: '1px solid #d8d8d8',
  },
  thSmall: {
    fontSize: '90%',
    fontWeight: 500,
    textAlign: 'left',
    padding: '20px 8px 8px',
    borderBottom: '1px solid #d8d8d8',
  },
  td: {
    padding: 8,
  },
  b: {
    fontWeight: 500,
  },
  valueCell: {
    fontWeight: 500,
  },
  printViewport: {},
};

const ReportView = ({ title, subtitle, afterSubtitle, data }) => {
  const printViewportRef = useRef();

  const print = useCallback(
    () => printViewportRef && printElem(printViewportRef.current),
    []
  );

  if (!data) return null;

  const columnsNumber = Math.max(
    ...data.map((chunk) =>
      chunk && chunk.rows
        ? Math.max(...chunk.rows.map((row) => (row ? row.length : 0)))
        : 0
    )
  );

  return (
    <div style={{ marginTop: 30 }}>
      <Button variant="contained" color="primary" onClick={print}>
        <label>Print</label>
      </Button>

      <div ref={printViewportRef}>
        <div style={styles.wrapper}>
          <div style={styles.header}>
            <div style={styles.info}>
              {title && <h3 style={styles.title}>{title}</h3>}
              {subtitle && <h4 style={styles.subtitle}>{subtitle}</h4>}

              {afterSubtitle}
            </div>

            <img
              style={styles.logo}
              src={`${imagesURL}/Images/Login/FLCLogo.png`}
              alt="Flink logo"
            />
          </div>

          <table style={styles.table}>
            <tbody>
              {data.map((chunk, idx) => {
                if (!chunk) return null;

                return (
                  <Fragment key={idx}>
                    <tr>
                      <th colSpan={columnsNumber} style={styles.th}>
                        {chunk.header}
                      </th>
                    </tr>

                    {chunk.headers && (
                      <tr>
                        {chunk.headers.map((header, idx) => (
                          <th style={styles.thSmall} key={idx}>
                            {header}
                          </th>
                        ))}
                      </tr>
                    )}

                    {chunk.rows &&
                      chunk.rows.map(
                        (row, idx) =>
                          row && (
                            <tr key={idx}>
                              {row.map((val, idx) => {
                                const colSpan = val.colSpan || 1;
                                const content = val.content !== undefined ? val.content : val;
                                const align = val.align || 'left';

                                return (
                                  <td
                                    key={idx}
                                    style={styles.td}
                                    colSpan={colSpan}
                                    align={align}
                                  >
                                    {idx ? (
                                      <b style={styles.b}>{content}</b>
                                    ) : (
                                      content
                                    )}
                                  </td>
                                );
                              })}
                            </tr>
                          )
                      )}
                  </Fragment>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

ReportView.propTypes = {
  data: PropTypes.array,
};

export default ReportView;
