import React from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { changePassword } from 'actions/userActions';
import { reduxFormValidator } from 'validation';

const requiredFields = [
  'currentPassword',
  'newPassword',
  'confirmNewPassword'
];

const validate = reduxFormValidator(requiredFields);

const renderTextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}) => (
  <TextField
    label={label}
    helperText={touched && error}
    error={touched && error ? true : false}
    placeholder={label}
    {...input}
    {...custom}
  />
);

const ChangePasswordForm = props => {
  const submit = async (submitOpts, values, dispatch) => {
    if (values.newPassword !== values.confirmPassword) {
      throw new SubmissionError({
        confirmPassword: "Passwords don't match"
      });
    }

    if (values.currentPassword === values.newPassword) {
      throw new SubmissionError({
        newPassword: 'New and current passwords must be different'
      });
    }

    const { onSuccess } = submitOpts;

    const result = await props.changePassword(values);

    if (!result.success) {
      throw new SubmissionError(result);
    }

    dispatch(reset('ChangePasswordForm'));
    onSuccess && onSuccess();
  };

  const {
    error,
    handleSubmit,
    pristine,
    reset,
    submitting,
    userId,
    onSuccess
  } = props;

  const submitOpts = {
    onSuccess,
    userId
  };

  return (
    <StyledFormWrapper className="user-form__wrapper">
      <form onSubmit={handleSubmit(submit.bind(null, submitOpts))}>
        <Field
          name="currentPassword"
          label="Current password"
          component={renderTextField}
          fullWidth
          type="password"
          margin="normal"
        />
        <Field
          name="newPassword"
          label="New password"
          component={renderTextField}
          fullWidth
          type="password"
          margin="normal"
        />
        <Field
          name="confirmPassword"
          label="Confirm password"
          component={renderTextField}
          fullWidth
          type="password"
          margin="normal"
        />
        {error && <Typography variant="subtitle1">{error}</Typography>}
        <StyledButtonsWrapper>
          <Button
            disabled={pristine || submitting}
            variant="contained"
            color="primary"
            size="large"
            type="submit"
            className="user-form__button"
          >
            Confirm
          </Button>
          <Button
            disabled={pristine || submitting}
            variant="contained"
            color="secondary"
            size="large"
            onClick={reset}
            className="user-form__button"
          >
            Clear
          </Button>
        </StyledButtonsWrapper>
      </form>
    </StyledFormWrapper>
  );
};

ChangePasswordForm.propTypes = {
  userId: PropTypes.string.isRequired,
  onSubmitSuccess: PropTypes.func,
  changePassword: PropTypes.func.isRequired
};

export default compose(
  reduxForm({
    form: 'ChangePasswordForm',
    validate
  }),
  connect(
    null,
    { changePassword }
  )
)(ChangePasswordForm);

const StyledFormWrapper = styled.div`
  width: 30rem;
  margin: 0 auto;
`;

const StyledButtonsWrapper = styled.div`
  margin-top: 3rem;
  display: flex;
  justify-content: space-between;
`;
