import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

import { renderTextField } from 'components/form-components';

const FormTextfield = props => {
  return <Field component={renderTextField} {...props} />;
};

FormTextfield.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
};

export default FormTextfield;
