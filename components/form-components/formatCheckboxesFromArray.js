export default values => {
  if (!values || !Array.isArray(values)) return undefined;

  const formattedValues = {};

  values.forEach(value => (formattedValues[value] = true));

  return formattedValues;
};
