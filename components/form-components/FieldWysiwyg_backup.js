import React, { Component } from 'react';
import { Field, change } from 'redux-form';
import { withStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import renderTextField from './renderTextField';
import { MaterialFaIcon } from 'utils';

const styles = theme => ({
  wrapper: {
    position: 'relative'
  },
  popup: {
    position: 'absolute',
    zIndex: 10,
    background: '#fff',
    width: '100%',
    border: '1px solid #eee',
    borderRadius: '4px',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    paddingBottom: '3rem'
  },
  editorWrapper: {},
  editor: {
    padding: '1rem'
  },
  finishButton: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    zIndex: 11
  }
});

class FieldWysiwyg extends Component {
  state = {
    editorState: EditorState.createEmpty(),
    inputFocused: false
  };

  inputField = React.createRef();

  componentDidMount() {
    // Set initial Value in Editor
    const initialValue = this.inputField.current.value;

    if (initialValue) {
      const contentBlock = htmlToDraft(initialValue);

      const contentState = ContentState.createFromBlockArray(
        contentBlock.contentBlocks
      );
      const editorState = EditorState.createWithContent(contentState);

      this.setState({
        editorState
      });
    }
  }

  onEditorStateChange = editorState => {
    const { change, formName, name } = this.props;

    change(
      formName,
      name,
      draftToHtml(convertToRaw(editorState.getCurrentContent()))
    );

    this.setState({
      editorState
    });
  };

  onInputChange = e => {
    // disable editing
    e.preventDefault();
  };

  onFocus = () => {
    this.setState({ inputFocused: true });
  };

  onFinish = () => {
    this.setState({ inputFocused: false });
  };

  render() {
    const { editorState } = this.state;
    const { name, classes, label } = this.props;

    return (
      <div className={classes.wrapper}>
        <Field
          ref={this.inputField}
          name={name}
          component={renderTextField}
          label={label}
          onChange={this.onInputChange}
          onFocus={this.onFocus}
        />
        {this.state.inputFocused && (
          <div className={classes.popup}>
            <Editor
              toolbar={{
                options: [
                  'inline',
                  'blockType',
                  'fontSize',
                  'fontFamily',
                  'list',
                  'textAlign',
                  'colorPicker',
                  'link',
                  'image',
                  'remove',
                  'history'
                ]
              }}
              editorState={editorState}
              wrapperClassName={classes.editorWrapper}
              editorClassName={classes.editor}
              onEditorStateChange={this.onEditorStateChange}
            />
            <Button
              className={classes.finishButton}
              onClick={this.onFinish}
              variant="fab"
              color="primary"
              size="medium"
            >
              <MaterialFaIcon icon="check" />
            </Button>
          </div>
        )}
        {/* <textarea
          disabled
          value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
        /> */}
      </div>
    );
  }
}

FieldWysiwyg.propTypes = {
  classes: PropTypes.object.isRequired
};

export default compose(
  connect(
    null,
    { change }
  ),
  withStyles(styles)
)(FieldWysiwyg);
