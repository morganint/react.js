import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import classes from './ProductsField.module.scss';

const changeProductsPurchased = (
  value,
  type,
  currentProduct,
  purchasedProducts,
  onChange
) => {
  const purchasedProduct = _.find(purchasedProducts, {
    productId: currentProduct._id
  }) || { productId: currentProduct._id };

  const changedPurchasedProduct = { ...purchasedProduct, [type]: value };

  const changedPurchasedProducts = _.reject(purchasedProducts, {
    productId: currentProduct._id
  });

  const isProductHavePlans = changedPurchasedProduct.plans;

  if (isProductHavePlans) {
    changedPurchasedProducts.push(changedPurchasedProduct);
  }

  onChange(changedPurchasedProducts);
};

const ProductsField = ({
  products,
  value,
  onChange,
  usingPlans = {},
  disabled,
  assignedProducts = {}
}) => {
  return (
    <div>
      {products.map(product => {
        const purchasedProduct = _.find(value, { productId: product._id });
        const assignedProduct = assignedProducts[product._id] || {};
        const usingProduct = usingPlans[product._id] || {};

        const plansPurchased =
          (purchasedProduct && purchasedProduct.plans) || 0;
        const plansAvailable =
          plansPurchased - ((usingProduct && usingProduct.plans) || 0);
        const plansAssigned = (assignedProduct && assignedProduct.plans) || 0;
        const plansAssignable = plansPurchased - plansAssigned || 0;

        const addonsPurchased =
          (purchasedProduct && purchasedProduct.addons) || 0;
        const addonsAvailable =
          addonsPurchased - ((usingProduct && usingProduct.addons) || 0);
        const addonsAssigned = (assignedProduct && assignedProduct.addons) || 0;
        const addonsAssignable = addonsPurchased - addonsAssigned || 0;

        return (
          <div className={classes.wrapper} key={product._id}>
            <b className={classes.productName}>{product.name}</b>

            <div className={classes.planColumn}>
              <div className={classes.fieldGroup}>
                <label>Plans Purchased:</label>
                <input
                  type="number"
                  min={plansAssigned}
                  disabled={disabled}
                  value={plansPurchased}
                  onChange={e => {
                    let newVal = +e.target.value;

                    // Dont let to purchase less than already assigned to regions and learning centers
                    if (newVal < plansAssigned) {
                      alert(
                        'You cant purchase less than already assigned to regions and learning centers'
                      );
                      newVal = plansAssigned;
                    }

                    changeProductsPurchased(
                      newVal,
                      'plans',
                      product,
                      value,
                      onChange
                    );
                  }}
                />
              </div>
              <div className={classes.fieldGroup}>
                <label>Plans Assignable:</label>
                <input disabled type="number" value={plansAssignable} />
              </div>
              <div className={classes.fieldGroup}>
                <label>Plans Available:</label>
                <input disabled type="number" value={plansAvailable} />
              </div>
            </div>

            <div className={classes.planColumn}>
              <div className={classes.fieldGroup}>
                <label>Add-ons Purchased:</label>
                <input
                  type="number"
                  min={addonsAssigned}
                  disabled={disabled}
                  value={addonsPurchased}
                  onChange={e => {
                    let newVal = +e.target.value;

                    // Dont let to purchase less than already assigned to regions and learning centers
                    if (newVal < addonsAssigned) {
                      alert(
                        'You cant purchase less than already assigned to regions and learning centers'
                      );
                      newVal = addonsAssigned;
                    }

                    changeProductsPurchased(
                      newVal,
                      'addons',
                      product,
                      value,
                      onChange
                    );
                  }}
                />
              </div>
              <div className={classes.fieldGroup}>
                <label>Addons Assignable:</label>
                <input disabled type="number" value={addonsAssignable} />
              </div>
              <div className={classes.fieldGroup}>
                <label>Add-ons Available:</label>
                <input disabled type="number" value={addonsAvailable} />
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

ProductsField.defaultProps = {
  products: [],
  value: [],
  available: []
};

ProductsField.propTypes = {
  products: PropTypes.array,
  value: PropTypes.array,
  available: PropTypes.array
};

export default ProductsField;
