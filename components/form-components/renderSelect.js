import React from 'react';
import Select from 'react-select';
import styled, { css } from 'styled-components';

const findOptionByValue = (value, options) => {
  return options.filter(option => option.value === value)[0] || {};
};

const renderSelect = ({
  input,
  options,
  label,
  width,
  disabled,
  meta: { touched, error, dirty },
  ...props
}) => (
  <StyledInputGroup width={width}>
    <label>
      <StyledLabel error={error} touched={touched} dirty={dirty}>
        {label}
      </StyledLabel>

      <Select
        value={findOptionByValue(input.value, options)}
        isDisabled={disabled}
        onChange={option => input.onChange(option && option.value)}
        onBlur={() => input.onBlur(input.value)}
        options={options}
        {...props}
      />
    </label>
    {touched && error && <StyledError>{error}</StyledError>}
  </StyledInputGroup>
);

export default renderSelect;

const StyledInputGroup = styled.div`
  margin-bottom: 1.5rem;
  ${({ width }) =>
    width &&
    css`
      width: ${width}!important;
    `};
`;

const StyledLabel = styled.p`
  font-size: 1.4rem;
  font-weight: bold;
  margin-bottom: 5px;
  margin-top: 0;

  ${({ touched, error, dirty }) =>
    touched &&
    (error
      ? css`
          color: #df2a26;
        `
      : dirty &&
        css`
          color: green;
        `)};
`;

const StyledError = styled.p`
  font-size: 1.2rem;
  margin-top: 2px;
  margin-bottom: 0;
  color: #df2a26;
`;
