export default values => {
  if (!values) return [];

  const keys = Object.keys(values);

  return keys.filter(key => values[key]);
};
