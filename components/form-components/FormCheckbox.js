import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

import { renderCheckbox } from 'components/form-components';

const FormCheckbox = props => {
  return <Field component={renderCheckbox} {...props} />;
};

FormCheckbox.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
};

export default FormCheckbox;
