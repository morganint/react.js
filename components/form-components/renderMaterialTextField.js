import React from 'react';
import TextField from '@material-ui/core/TextField';

const renderMaterialTextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}) => (
  <div style={{ marginBottom: '15px' }}>
    <TextField
      margin="dense"
      variant="outlined"
      label={label}
      helperText={touched && error}
      error={touched && error ? true : false}
      placeholder={label}
      {...input}
      {...custom}
    />
  </div>
);

export default renderMaterialTextField;
