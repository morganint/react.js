import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";

import { AddImageBlock, ActivityAddAudio } from "components/flink-components";
import classes from "./ItemConfigurator.module.scss";
import { changeGameData, removeContentFiles } from "actions/flinkMakeActions";
import { imagesURL } from "config";
import { withTranslate } from "components/hocs";

const ItemConfigurator = ({
  removeContentFiles,
  activeItem,
  translate,
  selectedItemPath,
  changeGameData,
  activeProblemIdx,
  withoutText,
  withoutImage,
  withoutAudio,
  textAreaWidth,
  maxLetters
}) => {
  const { text, image, audio } = activeItem || {};

  const textareaRef = useRef();

  useEffect(() => {
    if (
      !image &&
      textareaRef.current &&
      document.activeElement.id !== "problem-list"
    ) {
      // Move caret in textarea to the end
      const temp = textareaRef.current.value;
      textareaRef.current.value = "";
      textareaRef.current.value = temp;
      textareaRef.current.focus();
    }
  }, [activeProblemIdx, selectedItemPath, image]);

  return (
    <div className={classes.wrapper}>
      {!withoutText && (
        <div className={classes.textareaWrapper}>
          <label className={classes.label}>{translate(18, "Add Text")}</label>
          <textarea
            tabIndex="-1"
            ref={textareaRef}
            className={classes.textarea}
            value={text || ""}
            style={{ width: textAreaWidth }}
            maxLength={maxLetters}
            onChange={e => {
              changeGameData(
                `problems[${activeProblemIdx}].${selectedItemPath}.text`,
                e.target.value
              );

              if (image) {
                changeGameData(
                  `problems[${activeProblemIdx}].${selectedItemPath}.image`,
                  null
                );
                removeContentFiles([image]);
              }
            }}
          />
          {text && (
            <button
              className={classes.deleteBtn}
              onClick={() => {
                changeGameData(
                  `problems[${activeProblemIdx}].${selectedItemPath}.text`,
                  null
                );
              }}
            >
              <img
                src={`${imagesURL}/FrontendUtilities/delete1.png`}
                alt="delete"
              />
            </button>
          )}
        </div>
      )}

      {!withoutImage && (
        <div className={classes.imageWrapper}>
          <AddImageBlock
            withOverlay
            withId
            pathToProp={`gameData.problems[${activeProblemIdx}].${selectedItemPath}.image`}
            withoutPreview
            onChangeHandler={() => {
              if (!text) return;
              changeGameData(
                `problems[${activeProblemIdx}].${selectedItemPath}.text`,
                null
              );
            }}
          />
        </div>
      )}

      {!withoutAudio && (
        <div className={classes.audioWrapper}>
          <ActivityAddAudio
            current={audio}
            onChangeHandler={filename => {
              changeGameData(
                `problems[${activeProblemIdx}].${selectedItemPath}.audio`,
                filename
              );
            }}
          />
        </div>
      )}
    </div>
  );
};

ItemConfigurator.propTypes = {
  activeItem: PropTypes.object,
  selectedItemPath: PropTypes.string,
  changeGameData: PropTypes.func.isRequired,
  activeProblemIdx: PropTypes.number
};

export default compose(
  withTranslate,
  connect(null, { changeGameData, removeContentFiles })
)(ItemConfigurator);
