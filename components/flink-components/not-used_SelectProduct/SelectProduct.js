import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';
import { find } from 'lodash';

const styles = theme => ({
  root: {
    display: 'flex'
  },
  group: {
    margin: `${theme.spacing()}px 0`
  }
});

class SelectProduct extends Component {
  state = {
    selectedId: null,
    selected: null
  };

  componentDidMount() {
    const { products, onSelect } = this.props;

    if (products && products.length === 1) {
      onSelect(products[0]);
    }
  }

  handleChange = (e, prodId) => {
    console.log(prodId);

    this.setState({
      selectedId: prodId,
      selected: find(this.props.products, { _id: prodId })
    });
  };

  render() {
    const { selected, selectedId } = this.state;
    const { products, onSelect, classes } = this.props;

    return products ? (
      <FormControl component="fieldset">
        <FormLabel component="legend">Select Product:</FormLabel>
        <RadioGroup
          aria-label="Product"
          name="product"
          className={classes.group}
          value={selectedId}
          onChange={this.handleChange}
          options={[]}
        >
          {products.map(prod => (
            <FormControlLabel
              key={prod._id}
              color="primary"
              value={prod._id}
              control={<Radio />}
              label={prod.name}
            />
          ))}
        </RadioGroup>

        <Button
          color="primary"
          size="small"
          variant="contained"
          onClick={() => onSelect(selected)}
        >
          Ok
        </Button>
      </FormControl>
    ) : (
      <p>No available products</p>
    );
  }
}

SelectProduct.propTypes = {
  products: PropTypes.array,
  onSelect: PropTypes.func.isRequired
};

export default withStyles(styles)(SelectProduct);
