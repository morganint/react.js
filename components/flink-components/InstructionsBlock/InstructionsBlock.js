import React from "react";
import PropTypes from "prop-types";

import classes from "./InstructionsBlock.module.scss";
import { ActivityAddAudio } from "components/flink-components";

const InstructionsBlock = ({
  maxCharacters = 200,
  withoutAudio,
  label,
  onTextChange,
  onAudioChange,
  textValue,
  audioValue
}) => {
  return (
    <div className={classes.wrapper}>
      <div className={classes.textareaWrapper}>
        {label && <label className={classes.label}>{label}</label>}
        <textarea
          onChange={e => onTextChange(e.target.value)}
          value={textValue || ""}
          className={classes.textarea}
          maxLength={maxCharacters}
        />
      </div>

      {!withoutAudio && (
        <ActivityAddAudio
          current={audioValue}
          onChangeHandler={onAudioChange}
        />
      )}
    </div>
  );
};

InstructionsBlock.defaultProps = {
  withoutAudio: false
};

InstructionsBlock.propTypes = {
  textValue: PropTypes.string,
  audioValue: PropTypes.string,
  onAudioChange: PropTypes.func.isRequired,
  onTextChange: PropTypes.func.isRequired,
  label: PropTypes.string
};

export default InstructionsBlock;
