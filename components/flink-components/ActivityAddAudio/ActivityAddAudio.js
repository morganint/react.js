import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { publicKey } from 'config';

import AddAudio from '../AddAudio/AddAudio';

const ActivityAddAudio = ({ contentFolder, ...otherProps }) => {
  return (
    <AddAudio
      saveAfterChange
      withOverlay
      path={`${publicKey}/${contentFolder}/gamedata`}
      withId={!otherProps.filename}
      removeOldOnChange={!otherProps.filename}
      {...otherProps}
    />
  );
};

ActivityAddAudio.propTypes = {
  contentFolder: PropTypes.string.isRequired
};

const mapStateToProps = ({ flinkMake: { activity } }) => ({
  contentFolder: activity.editingActivity.activity.contentFolder
});

export default connect(mapStateToProps)(ActivityAddAudio);
