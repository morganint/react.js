import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { options as selectOptions } from 'activity-templates/utils';
import { ColorPickerField } from 'components/common';
import classes from './ProblemOptions.module.scss';
import { withTranslate } from 'components/hocs';

const ProblemOptions = ({
  options,
  activeProblem,
  activeProblemIdx,
  translate,
  changeHandler,
  questionItems,
  correctItems,
  incorrectItems
}) => {
  return (
    <div className={classes.options}>
      {options.includes('answerBelongs') && (
        <div className={classes.radioSelect}>
          <label className={classes.radioLabel}>
            <input
              type="radio"
              checked={(activeProblem && activeProblem.answerBelongs) || false}
              onChange={e => {
                changeHandler(
                  `problems[${activeProblemIdx}].answerBelongs`,
                  e.target.checked
                );
              }}
            />
            {translate(582, 'Answer "Belongs"')}
          </label>
          <label className={classes.radioLabel}>
            <input
              type="radio"
              checked={(activeProblem && !activeProblem.answerBelongs) || false}
              onChange={e => {
                changeHandler(
                  `problems[${activeProblemIdx}].answerBelongs`,
                  !e.target.checked
                );
              }}
            />
            {translate(583, 'Answer "Does Not Belong"')}
          </label>
        </div>
      )}

      {questionItems && (
        <div className={classes.itemsShownSelect}>
          <label>{translate(0, 'Question Items Shown:')}</label>
          <select
            value={activeProblem && activeProblem.questionItemsShown}
            onChange={e => {
              changeHandler(
                `problems[${activeProblemIdx}].questionItemsShown`,
                +e.target.value
              );
            }}
          >
            {_.range(questionItems.min, questionItems.max + 1).map(num => (
              <option key={num} value={num}>
                {num}
              </option>
            ))}
          </select>
        </div>
      )}

      {correctItems && (
        <div className={classes.itemsShownSelect}>
          <label>{translate(588, 'Correct Items Shown:')}</label>
          <select
            value={activeProblem && activeProblem.correctItemsShown}
            onChange={e => {
              changeHandler(
                `problems[${activeProblemIdx}].correctItemsShown`,
                +e.target.value
              );
            }}
          >
            {_.range(correctItems.min, correctItems.max + 1).map(num => (
              <option key={num} value={num}>
                {num}
              </option>
            ))}
          </select>
        </div>
      )}

      {incorrectItems && (
        <div className={classes.itemsShownSelect}>
          <label>{translate(589, 'Incorrect Items Shown:')}</label>
          <select
            value={activeProblem && activeProblem.incorrectItemsShown}
            onChange={e => {
              changeHandler(
                `problems[${activeProblemIdx}].incorrectItemsShown`,
                +e.target.value
              );
            }}
          >
            {_.range(incorrectItems.min, incorrectItems.max + 1).map(num => (
              <option key={num} value={num}>
                {num}
              </option>
            ))}
          </select>
        </div>
      )}

      {(options.includes('fontFamily') ||
        options.includes('fontSize') ||
        options.includes('fontColor')) && (
        <div className={classes.fontControls}>
          <label>{translate(369, 'Font:')}</label>
          {options.includes('fontFamily') && (
            <div className={classes.fontFamilySelect}>
              <select
                value={activeProblem && activeProblem.fontFamily}
                onChange={e => {
                  changeHandler(
                    `problems[${activeProblemIdx}].fontFamily`,
                    e.target.value
                  );
                }}
              >
                {selectOptions.fontFamily.map(font => (
                  <option key={font} value={font}>
                    {font}
                  </option>
                ))}
              </select>
            </div>
          )}

          {options.includes('fontSize') && (
            <div className={classes.fontSizeSelect}>
              <select
                value={activeProblem && activeProblem.fontSize}
                onChange={e => {
                  changeHandler(
                    `problems[${activeProblemIdx}].fontSize`,
                    e.target.value
                  );
                }}
              >
                {selectOptions.fontSize.map(size => (
                  <option key={size} value={size}>
                    {size}
                  </option>
                ))}
              </select>
            </div>
          )}

          {options.includes('fontColor') && (
            <ColorPickerField
              label={translate(455, 'Color:')}
              changeHandler={color =>
                changeHandler(
                  `problems[${activeProblemIdx}].fontColor`,
                  color.hex
                )
              }
              currentColor={activeProblem && activeProblem.fontColor}
            />
          )}
        </div>
      )}
    </div>
  );
};

ProblemOptions.propTypes = {
  activeProblem: PropTypes.object,
  changeHandler: PropTypes.func.isRequired,
  activeProblemIdx: PropTypes.number,
  translate: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired
};

export default withTranslate(ProblemOptions);
