import React from 'react';
import styled from 'styled-components';

const AssignBlock = ({
  title,
  items,
  selectHandler,
  selected,
  reverseBtns,
  singleArrowFn,
  doubleArrowFn,
  arrows
}) => {
  return (
    <div>
      <StyledListTitle>{title}</StyledListTitle>
      <StyledContainer>
        <StyledList>
          {items.map(item => (
            <StyledLi
              onClick={selectHandler.bind(null, item.value)}
              key={item.value._id}
              className={
                item.value._id === (selected && selected._id)
                  ? 'active'
                  : undefined
              }
            >
              {item.label}
            </StyledLi>
          ))}
        </StyledList>
        <StyledBtnsWrapper reverse={reverseBtns}>
          <StyledBtn disabled={!selected || !items.length} onClick={singleArrowFn.bind(null, selected)}>
            <img  src={`/files/Images/FlinkAdmin/Buttons/arrow-${arrows}.png`} alt="" />
          </StyledBtn>
          <StyledBtn disabled={!items.length}>
            <img
              src={`/files/Images/FlinkAdmin/Buttons/double-arrow-${arrows}.png`}
              alt=""
              onClick={doubleArrowFn}
            />
          </StyledBtn>
        </StyledBtnsWrapper>
      </StyledContainer>
    </div>
  );
};

export default AssignBlock;

const StyledListTitle = styled.b`
  display: block;
  text-align: center;
  margin-bottom: 5px;
  text-transform: capitalize;
  font-size: 1.3rem;
  flex-shrink: 0;
`;

const StyledContainer = styled.div`
  display: flex;
  margin-bottom: 2rem;
`;

const StyledBtnsWrapper = styled.div`
  display: flex;
  flex-direction: ${({ reverse }) =>
    reverse ? 'column-reverse' : 'column'};
  align-items: center;
  flex-grow: 1;
  justify-content: center;
`;

const StyledBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  background: none;
  border: none;
  cursor: pointer;
  margin: 5px;
  opacity: .8;

  &:disabled {
    opacity: 0.4;
    cursor: default;
  }

  &:hover:not(:disabled) {
    opacity: 1;
  }

  &:focus {
    outline: none;
  }
`;

const StyledLi = styled.li`
  font-size: 1.3rem;
  padding: 2px 5px;
  cursor: pointer;

  &:hover {
    background-color: #eee;
  }

  &.active {
    background-color: lightblue;
  }
`;

const StyledList = styled.ul`
  list-style: none;
  margin: 0;
  height: 100px;
  overflow: auto;
  background-color: #fff;
  padding: 0;
  width: 80%;
  border-radius: 2px;
`;
