import React, { useState } from 'react';
import PropTypes from 'prop-types';

import AssignBlock from './AssignBlock/AssignBlock';

const AssignList = ({ topList, bottomList }) => {
  const [topSelected, setTopSelected] = useState(null);
  const [bottomSelected, setBottomSelected] = useState(null);

  return (
    <div>
      <AssignBlock
        selected={topSelected}
        selectHandler={setTopSelected}
        {...topList}
        arrows="down-green"
        reverseBtns
      />
      <AssignBlock
        selected={bottomSelected}
        selectHandler={setBottomSelected}
        {...bottomList}
        arrows="up-red"
      />
    </div>
  );
};

AssignList.propTypes = {
  topList: PropTypes.shape({
    title: PropTypes.string,
    items: PropTypes.array.isRequired,
  }).isRequired,
  bottomList: PropTypes.shape({
    title: PropTypes.string,
    items: PropTypes.array.isRequired,
  }).isRequired,
  assignDirection: PropTypes.string.isRequired
};

export default AssignList;
