import React, { Component } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { imagesURL } from "config";
import { Editor } from "components/flink-components";
import { Rnd } from "react-rnd";
import classes from "./EbookEditor.module.scss";

export class EbookArea extends Component {
  state = {};

  onEditorChange = html => {
    const { changeHandler, data } = this.props;
    changeHandler({ ...data, text: html });
  };

  updateSize = (e, direction, ref, delta, position) => {
    const { changeHandler, data } = this.props;
    changeHandler({
      ...data,
      dim: { ...data.dim, width: ref.offsetWidth, height: ref.offsetHeight }
    });
  };

  updatePosition = (e, d) => {
    const { changeHandler, data } = this.props;
    changeHandler({ ...data, dim: { ...data.dim, left: d.x, top: d.y } });
  };

  render() {
    const {
      disableEditing = false,
      data,
      removeHandler,
      boundsSelector,
      selectHandler,
      inFocus,
      disableDelete,
      disableDrag,
      disableResize
    } = this.props;

    const { dim, text } = data;
    const position = { x: parseInt(dim.left), y: parseInt(dim.top) };

    return (
      <Rnd
        className={classnames({
          [classes.inFocus]: inFocus
        })}
        disableDragging={disableDrag}
        size={dim}
        onResize={this.updateSize}
        position={position}
        onDragStop={this.updatePosition}
        bounds={boundsSelector}
        enableResizing={
          !disableResize && {
            bottom: true,
            bottomRight: true,
            bottomLeft: false,
            top: false,
            right: true,
            topRight: false,
            topLeft: false
          }
        }
        dragHandleClassName={classes.moveButton}
      >
        <div
          className={classnames(classes.wrapper, {
            [classes.disabled]: disableEditing
          })}
          onFocus={selectHandler}
          tabIndex="1"
        >
          {!disableResize && (
            <button type="button" className={classes.moveButton}>
              <img
                src={`${imagesURL}/FlinkMake/MoveEditor.png`}
                alt="Move"
                draggable="false"
              />
            </button>
          )}

          {!disableDelete && (
            <button onClick={removeHandler} className={classes.removeButton}>
              <img src={`${imagesURL}/FlinkMake/delete.png`} alt="Delete" />
            </button>
          )}

          <Editor
            readOnly={disableEditing}
            classes={{
              toolbar: classnames(classes.editorToolbar, {
                [classes.visibleToolbar]: inFocus,
                [classes.hidden]: disableEditing
              })
            }}
            className={classes.editor}
            initialValue={text || ""}
            changeHandler={this.onEditorChange}
            withImage={false}
            withVideo={false}
            withLink={false}
          />
        </div>
      </Rnd>
    );
  }
}

EbookArea.defaultProps = {
  disableEditing: false,
  disableDelete: false,
  disableDrag: false,
  disableResize: false
};

EbookArea.propTypes = {
  data: PropTypes.object.isRequired,
  boundsSelector: PropTypes.string.isRequired,
  removeHandler: PropTypes.func.isRequired,
  selectHandler: PropTypes.func.isRequired,
  changeHandler: PropTypes.func.isRequired,
  disableEditing: PropTypes.bool,
  disableDelete: PropTypes.bool,
  disableDrag: PropTypes.bool,
  disableResize: PropTypes.bool
};

export default EbookArea;
