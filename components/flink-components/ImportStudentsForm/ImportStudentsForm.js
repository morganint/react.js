import React, { Component } from 'react';
import { ImportCSV } from 'components/form-components';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class ImportStudentsForm extends Component {
  state = { file: null };

  changeFileHandler = file => {
    this.setState({ file });
  };

  render() {
    const { callback } = this.props;
    const { file } = this.state;

    return (
      <div>
        <Typography>Import students</Typography>

        <ImportCSV label="CSV File:" callback={this.changeFileHandler} />

        <Button
          disabled={!file}
          onClick={() => callback(file)}
          color="primary"
          size="small"
          variant="contained"
        >
          Ok
        </Button>
      </div>
    );
  }
}

export default ImportStudentsForm;
