import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import shortid from 'shortid';

import { DraggableModal } from 'components/flink-components';
import { imagesURL, uploadsURL } from 'config';
import { playAudio, stopAudio } from 'actions/audioActions';
import { uploadAudioFile, saveEditingActivity } from 'actions/flinkMakeActions';
import API from 'api';
import classes from './AddAudio.module.scss';
import Record from './_Record';

class AddAudio extends Component {
  state = {
    currentUrl: '',
    show: false,
    file: null,
  };

  componentDidUpdate(prevProps) {
    const { currentUrl } = this.state;
    const { current: prevCurrent, playingSrc, isPlaying } = prevProps;
    const { current } = this.props;

    if (current !== prevCurrent && currentUrl === playingSrc && isPlaying) {
      this.stopHandler();
    }
  }

  componentWillUnmount() {
    stopAudio();
  }

  onDrop = (files) => {
    this.setState({ file: files[0] });
    console.log('uploadHandler', files[0]);
  };

  playHandler = () => {
    const { current, path, isUrl } = this.props;
    const audioUrl = isUrl ? current : `${uploadsURL}/${path}/${current}`;

    playAudio(audioUrl);
    this.setState({ currentUrl: audioUrl })
  };

  stopHandler = () => {
    stopAudio();
  };

  removeHandler = () => {
    this.stopHandler();
    const { path, current, isUrl } = this.props;

    if (!isUrl) {
      const key = `${path}/${current}`;
      API.storage.deleteFiles([key]);
    }

    this.changeHandler();
  };

  uploadHandler = async () => {
    const {
      path,
      isUrl,
      withId,
      current,
      uploadAudioFile,
      filename: filenameFromProps,
      removeOldOnChange,
    } = this.props;

    const { file } = this.state;

    const [, originFileName, extension] = file.name.match(/\/?([^/]+)\.(\S+)$/);

    const withoutExt = filenameFromProps ? filenameFromProps : originFileName;
    const id = withId ? '_' + shortid.generate() : '';

    const filename = `${withoutExt}${id}.${extension}`;

    const params = {
      path,
      file: this.state.file,
      filename,
      fileToDelete: !isUrl && removeOldOnChange ? current : null,
    };

    const success = await uploadAudioFile(params);

    if (!success) return;

    this.changeHandler(filename);
    this.setState({ show: false, file: null });
  };

  changeHandler = (value) => {
    const {
      onChangeHandler,
      saveAfterChange,
      saveEditingActivity,
    } = this.props;

    onChangeHandler(value);
    saveAfterChange && saveEditingActivity();
  };

  useRecorderFile = async (file) => {
    const {
      path,
      isUrl,
      current,
      filename: filenameFromProps,
      uploadAudioFile,
      removeOldOnChange,
    } = this.props;

    const filename = filenameFromProps || file.name;

    const params = {
      path,
      file,
      filename,
      fileToDelete: !isUrl && removeOldOnChange ? current : null,
    };

    const success = await uploadAudioFile(params);

    if (!success) return;

    this.changeHandler(filename);
    this.setState({ show: false, file: null });
  };

  fromDirectoryHandler = () => {
    console.log('Choose from Directory');
  };

  toggleShow = () => {
    stopAudio();
    this.setState({ show: !this.state.show });
  };

  render() {
    const { show, file, currentUrl } = this.state;
    const {
      current,
      translate,
      withOverlay,
      isUrl,
      isPlaying,
      playingSrc,
      wrapperClassname = '',
    } = this.props;

    const isCurrentPlaying = currentUrl === playingSrc && isPlaying;

    return (
      <>
        <div className={wrapperClassname}>
          <span className={classes.label}>{translate(13, 'Add Audio')}</span>
          <div className={classes['buttons-wrapper']}>
            <button
              title={translate(13, 'Add Audio')}
              className={`${classes['btn']} btn-effect`}
              onClick={this.toggleShow}
            >
              <img
                src={`${imagesURL}/FrontendUtilities/addaudio.png`}
                alt={translate(13, 'Add Audio')}
              />
            </button>

            {(current || isUrl) && (
              <>
                {isCurrentPlaying ? (
                  <button
                    title={translate(73, 'Stop Audio')}
                    className={`${classes['btn']} btn-effect`}
                    onClick={this.stopHandler}
                  >
                    <img
                      src={`${imagesURL}/FrontendUtilities/stop1.png`}
                      alt={translate(73, 'Stop Audio')}
                    />
                  </button>
                ) : (
                  <button
                    title={translate(514, 'Play')}
                    className={`${classes['btn']} btn-effect`}
                    onClick={this.playHandler}
                  >
                    <img
                      src={`${imagesURL}/Images/Audio/audio_make.png`}
                      alt={translate(514, 'Play')}
                    />
                  </button>
                )}
                <button
                  title={translate(96, 'Delete')}
                  className={`${classes['btn']} btn-effect`}
                  onClick={this.removeHandler}
                >
                  <img
                    src={`${imagesURL}/FrontendUtilities/delete1.png`}
                    alt={translate(96, 'Delete')}
                  />
                </button>
              </>
            )}
          </div>
        </div>

        {show && (
          <DraggableModal
            title={translate(13, 'Add Audio')}
            show={show}
            withOverlay={withOverlay}
            onClose={this.toggleShow}
          >
            <div className={classes['add-audio-modal']}>
              <div className={classes['col']}>
                {/* <h3>{translate(445, 'Upload a File')}</h3> */}
                <Dropzone
                  onDrop={this.onDrop}
                  accept=".mp3"
                  multiple={false}
                  maxSize={6 * 1000000} // 6mb
                >
                  {({ getRootProps, getInputProps, isDragActive }) => {
                    return (
                      <div className={classes['dropzone-wrapper']}>
                        <div
                          {...getRootProps()}
                          className={classNames(classes['dropzone'], {
                            [`${classes['dropzone--active']}`]: isDragActive,
                          })}
                        >
                          <input {...getInputProps()} />
                          <p>
                            {translate(
                              441,
                              `Drop .mp3 file here, or click to select file to upload. Max 6mb`
                            )}
                          </p>
                          {/* {isDragActive ? (
                            <p>Drop file here...</p>
                          ) : (
                            <p>
                              {translate(
                                441,
                                `Drop .mp3 file here, or click to select file to upload. Max 6mb`
                              )}
                            </p>
                          )} */}
                        </div>
                        {file && (
                          <>
                            <p>{file.name}</p>
                            <button
                              className={classes['upload-button']}
                              onClick={() => this.uploadHandler()}
                            >
                              Upload
                            </button>
                          </>
                        )}
                      </div>
                    );
                  }}
                </Dropzone>
                <p />
              </div>
              <div className={classes['col']}>
                <p style={{ marginTop: 0 }}>
                  {translate(436, 'Click the start button to record')}
                </p>
                <div className={classes['record-wrapper']}>
                  <Record
                    translate={translate}
                    addRecordedFileHandler={this.useRecorderFile}
                  />
                </div>
              </div>
            </div>
          </DraggableModal>
        )}
      </>
    );
  }
}

AddAudio.propTypes = {
  wrapperClassname: PropTypes.string,
  removeOldOnChange: PropTypes.bool,
  filename: PropTypes.string,
  withId: PropTypes.bool,
  saveAfterChange: PropTypes.bool,
  withOverlay: PropTypes.bool,
  isUrl: PropTypes.bool,
  // Required
  path: PropTypes.string.isRequired,
  current: PropTypes.string,
  onChangeHandler: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  // from redux
  uploadAudioFile: PropTypes.func.isRequired,
  saveEditingActivity: PropTypes.func.isRequired,
  playingSrc: PropTypes.string,
  isPlaying: PropTypes.bool,
};

const mapStateToProps = ({ status: { translate }, audio: { audioSrc, isPlaying } }) => ({
  translate,
  playingSrc: audioSrc,
  isPlaying
});

export default compose(
  connect(mapStateToProps, {
    uploadAudioFile,
    saveEditingActivity,
  })
)(AddAudio);
