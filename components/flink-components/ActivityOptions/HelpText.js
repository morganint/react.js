import React from 'react';

export default () => (
  <dl>
    <dt>LENGTH OF TIME:</dt>
    <dd>
      This is the amount of time the correct answer remains on the screen after
      it is selected by the student or shown by the computer. After the number
      of seconds selected, the next problem displays. The dealt is 3 seconds.
    </dd>
    <dt>FONT:</dt>
    <dd>
      Choose the type of font, its size and its color. The defaults are Verdana,
      14 and Black.
    </dd>
    <dt>PROBLEM ORDER:</dt>
    <dd>
      Choose Random if you want to create greater replayability and As Written
      if you want to order problems from less to more challenging. The default
      is Random.
    </dd>
    <dt>SHOW QUESTION:</dt>
    <dd>
      Decide whether children see the question or just hear the audio file. The
      default is Show Question.
    </dd>
    <dt>SHOW ANSWER:</dt>
    <dd>
      You can also decide if children can see the correct answer. If not, select
      n/a in the Number of Tries drop-down menu. If you want to show the correct
      answer, decide after how many incorrect tries (select a number in the
      Number of Tries drop-down menu). The default is 2 number of tries.
    </dd>
  </dl>
);
