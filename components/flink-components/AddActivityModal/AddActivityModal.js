import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { connect } from "react-redux";
import _ from "lodash";

import DraggableModal from "../DraggableModal/DraggableModal";
import classes from "./AddActivityModal.module.scss";
import { imagesURL } from "config";

const AddActivityModal = ({
  show,
  onClose,
  activityTemplates,
  onCreate,
  translate
}) => {
  const [selectedTemplate, setSelectedTemplate] = useState();
  const [activityName, setActivityName] = useState("");

  return (
    <DraggableModal
      show={show}
      onClose={onClose}
      title={translate(470, "New Activity")}
      withOverlay
    >
      <div className={classes["container"]}>
        <div className={classes.leftColumn}>
          <label htmlFor="activityName" className={classes["label"]}>
            {translate(450, "Activity Name:")}
          </label>
          <input
            autoFocus
            type="text"
            className={classes["input"]}
            value={activityName}
            onChange={e => setActivityName(_.trimStart(e.target.value))}
          />

          {selectedTemplate && (
            <div className={classes["template-descr"]}>
              <img
                src={`${imagesURL}/TemplateThumbnails/${selectedTemplate.thumbFilename}`}
                alt=""
              />
              <p>{translate(selectedTemplate.explanationString)}</p>
            </div>
          )}
        </div>
        <div className={classes.rightColumn}>
          <div className={classes["template-btns-wrapper"]}>
            {_.chain(activityTemplates)
              .filter(
                templ =>
                  !(templ.availableForWordlists && templ.onlyForWordlists)
              )
              .sortBy(templ => templ.activityTemplateName)
              .map(templ => (
                <button
                  className={classnames(classes["template-btn"], {
                    [classes["active"]]:
                      !!selectedTemplate && selectedTemplate._id === templ._id
                  })}
                  key={templ._id}
                  onClick={() => setSelectedTemplate(templ)}
                >
                  {translate(templ.nameString, templ.activityTemplateName)}
                </button>
              ))
              .value()}
          </div>

          <button
            className={classes["save-btn"]}
            disabled={!activityName || !selectedTemplate}
            onClick={() =>
              onCreate({ activityName, template: selectedTemplate })
            }
          >
            {translate(298, "Save")}
          </button>
        </div>
      </div>
    </DraggableModal>
  );
};

AddActivityModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onCreate: PropTypes.func.isRequired,
  activityTemplates: PropTypes.array.isRequired
};

const mapStateToProps = ({ status: { translate } }) => ({ translate });

export default connect(mapStateToProps)(AddActivityModal);
