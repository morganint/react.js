import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

const withTranslate = WrappedComponent => {
  const Wrapper = ({ translate, ...otherProps }) => {
    return <WrappedComponent {...otherProps} translate={translate} />;
  };

  Wrapper.propTypes = {
    translate: PropTypes.func.isRequired
  };

  const mapStateToProps = ({ status }) => ({
    translate: status.translate
  });

  return connect(mapStateToProps)(Wrapper);
};

export default withTranslate;

// import React, { useMemo } from 'react';
// import PropTypes from 'prop-types';
// import { connect } from 'react-redux';

// import { getTranslateFunc } from 'utils';

// const withTranslate = WrappedComponent => {
//   const Wrapper = ({ strings, locales, currentLang, ...otherProps }) => {
//     const translateFunc = useMemo(() => {
//       const lang = currentLang || locales.filter(l => l.code === 'en')[0];
//       return getTranslateFunc(strings, lang.code);
//     }, [strings, currentLang, locales]);

//     return <WrappedComponent {...otherProps} translate={translateFunc} />;
//   };

//   Wrapper.propTypes = {
//     strings: PropTypes.array.isRequired,
//     currentLang: PropTypes.object
//   };

//   const mapStateToProps = ({ common, status }) => ({
//     strings: common.strings,
//     locales: common.locales,
//     currentLang: status.lang
//   });

//   return connect(mapStateToProps)(Wrapper);
// };

// export default withTranslate;
