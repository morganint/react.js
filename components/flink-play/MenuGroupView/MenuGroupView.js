import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { WhiteBox } from 'components/flink-play';
import { StarIcon } from 'components/flink-play/FlinkPlayIcons/FlinkPlayIcons';
import classes from './MenuGroupView.module.scss';

const MenuGroupView = ({
  title,
  items,
  reports,
  teamReports,
  highlight,
  activityClickHandler,
  groupClickHandler,
}) => {
  return (
    <WhiteBox outerClass={classes.wrapper} innerClass={classes.innerWrapper}>
      {title && <h2 className={classes.title}>{title}</h2>}

      <ul className={classes.itemsList}>
        {items.map((ele) => {
          const { _id, isGroup } = ele;

          // const iconUrl = `${imagesURL}/FlinkMake/${
          // isGroup ? "activitygroupsmall.png" : "activitysmall.png"
          // }`;

          const feedback = reports[_id] || {};

          const feedbacks = [feedback];

          if (teamReports) {
            feedbacks.push(
              ...teamReports.map((reports) => {
                const report = reports && reports[_id];
                return report || {};
              })
            );
          }

          const clickHandler = isGroup
            ? groupClickHandler
            : activityClickHandler;

          return (
            <li key={_id}>
              <button
                onClick={() => clickHandler && clickHandler(_id)}
                className={classnames(classes.item, {
                  [classes.clickable]: !!clickHandler,
                  [classes.itemGroup]: isGroup,
                  [classes.itemActivity]: !isGroup,
                  [classes.previous]: !isGroup && _id === highlight,
                })}
              >
                {/* <span
                    className={classnames(classes.itemStar, {
                      [classes.partial]: feedback.partial && !feedback.mastered,
                      [classes.mastered]: feedback.mastered
                    })}
                  >

                    <span className={classes.itemStarStroke}>&#9734;</span>
                    {(feedback.partial || feedback.mastered) && (
                      <span className={classes.itemStarFill}>&#9733;</span>
                    )}
                  </span> */}

                {/* <span className={classes.itemIcon}>
                    <img src={iconUrl} alt="" />
                  </span> */}

                {feedbacks.map((f, idx) => {
                  return (
                    <div key={idx}>
                      <StarIcon {...f} />
                    </div>
                  );
                })}

                {ele.formattedName}
              </button>
            </li>
          );
        })}
      </ul>
    </WhiteBox>
  );
};

MenuGroupView.propTypes = {
  title: PropTypes.string,
  items: PropTypes.array.isRequired,
  reports: PropTypes.object.isRequired,
  activityClickHandler: PropTypes.func,
  groupClickHandler: PropTypes.func,
  teamReports: PropTypes.array,
  highlight: PropTypes.string,
};

export default MenuGroupView;
