import React from 'react';
import PropTypes from 'prop-types';

import { PreviewImage } from 'components/flink-components';
import { s3bucketPublicURL } from 'config';

const ActivityImage = ({ closeHandler, activity, show }) => {
  const { activityImage } = activity.data;
  const { contentFolder } = activity.activity;

  if (!activityImage) {
    return null;
  }

  const imageSrc = `${s3bucketPublicURL}/${contentFolder}/gamedata/${activityImage}`;

  // preload image
  const preloadedImage = new Image();
  preloadedImage.src = imageSrc;

  return (
    <PreviewImage
      maxWidth="80%"
      show={!!(imageSrc && show)}
      closeHandler={closeHandler}
      imageSrc={imageSrc}
      default={{x: 100, y: 150}}
    />
  );
};

ActivityImage.propTypes = {
  show: PropTypes.bool,
  closeHandler: PropTypes.func.isRequired,
  activity: PropTypes.shape({
    data: PropTypes.shape({
      activityImage: PropTypes.string
    }).isRequired
  }).isRequired
};

export default ActivityImage;
