import React from "react";

import { imagesURL } from "config";

const flinkPlayAssets = `${imagesURL}/FlinkPlay/`;

export const WhiteStar = () => (
  <img src={flinkPlayAssets + "Star_90x72White.png"} alt="" />
);

export const YellowStar = () => (
  <img src={flinkPlayAssets + "Star_90x72Yellow.png"} alt="" />
);

export const GreenStar = () => (
  <img src={flinkPlayAssets + "Star_90x72Green.png"} alt="" />
);

export const StarIcon = ({ mastered, partial }) => {
  return mastered ? <GreenStar /> : partial ? <YellowStar /> : <WhiteStar />;
};

export const MenuIcon = () => <img src={flinkPlayAssets + "menu.png"} alt="" />;

export const PlayAgainIcon = () => (
  <img src={flinkPlayAssets + "PlayAgain150.png"} alt="" />
);
