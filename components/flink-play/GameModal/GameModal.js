import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import WhiteBox from '../WhiteBox/WhiteBox';

import classes from './GameModal.module.scss';

const GameModal = ({
  title,
  children,
  buttons,
  centerButtons,
  buttonsColumn = false,
}) => {
  return (
    <>
      <div className={classes['overlay']}></div>

      <WhiteBox outerClass={classes.modal} innerClass={classes['modal-inner']}>
        <div className={classes['title']}>{title}</div>
        {children && <div className={classes['content']}>{children}</div>}
        <div
          className={classnames(classes.buttons, {
            [classes.column]: buttonsColumn,
          })}
          style={centerButtons ? { justifyContent: 'center' } : {}}
        >
          {buttons.map(
            (btn, idx) =>
              !btn.dontShow && (
                <button
                  key={idx}
                  disabled={btn.disabled}
                  onClick={btn.clickHandler}
                >
                  {btn.icon && <img src={btn.icon} alt="" />}
                  {btn.title}
                </button>
              )
          )}
        </div>
      </WhiteBox>
    </>
  );
};

GameModal.propTypes = {
  title: PropTypes.string,
  buttons: PropTypes.array.isRequired,
};

export default GameModal;
