import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import { openActivityHelp, showGameParentAdvice } from 'actions/gameActions';
import Buttons from '../Buttons/Buttons';
import { GO_BACK, HELP, SPEAK, PARENT_ADVICE } from 'consts/buttons';

const ActivityButtons = ({
  parentAdviceUrl,
  openActivityHelp,
  buttons,
  history,
  gameGoBackHandler,
  prevPages,
  showGameParentAdvice,
  gameParentAdvice,
}) => {
  const formattedButtons = useMemo(() => {
    const buttonsArray = [...buttons];

    if (
      !buttonsArray.includes(PARENT_ADVICE) &&
      !_.find(buttonsArray, { type: PARENT_ADVICE })
    ) {
      buttonsArray.push(PARENT_ADVICE);
    }

    return buttonsArray.map((btn) => {
      const btnObj = typeof btn === 'string' ? { type: btn } : { ...btn };

      if (btnObj.onClick) {
        return btnObj;
      }

      switch (btnObj.type) {
        case GO_BACK:
          const goBackHandler =
            gameGoBackHandler ||
            (() =>
              prevPages.length
                ? history.goBack()
                : history.push(
                    history.location.pathname.split('/').slice(0, -1).join('/')
                  ));

          btnObj.onClick = goBackHandler;
          break;
        case HELP:
          btnObj.onClick = openActivityHelp;
          break;
        case SPEAK:
          btnObj.onClick =
            btnObj.onClick ||
            (() => {
              const event = new Event('playInstructionsAudio');
              document.dispatchEvent(event);
            });
          break;
        case PARENT_ADVICE: {
          const trigger = !gameParentAdvice;

          btnObj.onClick = () => {
            if (btnObj.preClick) btnObj.preClick(trigger);
            showGameParentAdvice(trigger);
          };
          btnObj.dontShow = btnObj.dontShow || !parentAdviceUrl;
          break;
        }
        default:
          btnObj.onClick = () => console.log('No Handler');
      }

      return btnObj;
    });
  }, [
    buttons,
    gameGoBackHandler,
    showGameParentAdvice,
    history,
    openActivityHelp,
    prevPages,
    parentAdviceUrl,
    gameParentAdvice,
  ]);

  return <Buttons buttons={formattedButtons} />;
};

ActivityButtons.propTypes = {
  buttons: PropTypes.array.isRequired,
  prevPages: PropTypes.array,
  history: PropTypes.object.isRequired,
  gameGoBackHandler: PropTypes.func,
  openActivityHelp: PropTypes.func.isRequired,
  showGameParentAdvice: PropTypes.func.isRequired,
  parentAdviceUrl: PropTypes.string,
};

const mapStateToProps = ({ common, status, game, flinkPlay }) => ({
  parentAdviceUrl: flinkPlay.parentAdviceUrl,
  globalButtons: common.globalButtons,
  prevPages: status.prevPages,
  gameGoBackHandler: game.goBackHandler,
  gameParentAdvice: game.gameParentAdvice,
});

export default compose(
  withRouter,
  connect(mapStateToProps, { openActivityHelp, showGameParentAdvice })
)(ActivityButtons);
