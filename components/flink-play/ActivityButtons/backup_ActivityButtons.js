import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { openActivityHelp } from 'actions/gameActions';
import ButtonsWrapper from '../ButtonsWrapper/ButtonsWrapper';
import * as buttonsTypes from 'consts/buttons';
import { imagesURL } from 'config';
import { withTranslate } from 'components/hocs';

// translates
// Activity Builder	60
// Check Answer	59
// Clue	67
// Correct Answer	74
// Facebook	62
// Go Back	58
// Go	70
// Stop	73
// Help	66
// Image	71
// Next	68
// Print	69
// Remove Activity	72
// Report	64
// Settings	65
// Speak	75
// Vocabulary Notebook	63

class ActivityButtons extends Component {
  renderButton = btn => {
    if (!btn || btn.dontShow) return null;

    const { openActivityHelp, translate, game, history, status } = this.props;
    // const { isPreview } = game;

    switch (btn.type) {
      case buttonsTypes.GO_BACK: {
        const { prevPages } = status;
        const goBackHandler =
          btn.onClick ||
          game.goBackHandler ||
          (() =>
            prevPages.length
              ? history.goBack()
              : history.push(
                  history.location.pathname
                    .split('/')
                    .slice(0, -1)
                    .join('/')
                ));

        return (
          <button
            onClick={goBackHandler}
            disabled={btn.disabled}
            title={translate(58, 'Go Back', true)}
            style={{
              backgroundImage: `url(${imagesURL}/GlobalButtons/goback_enabled.png)`
            }}
          />
        );
      }
      case buttonsTypes.CHECK_ANSWER: {
        return (
          <button
            onClick={btn.onClick || (() => console.log('No handler'))}
            disabled={btn.disabled}
            title={translate(59, 'Check Answer', true)}
            style={{
              backgroundImage: `url(${imagesURL}/GlobalButtons/checkans_enabled.png)`
            }}
          />
        );
      }
      case buttonsTypes.SPEAK: {
        return (
          <button
            onClick={btn.onClick || (() => console.log('No handler'))}
            disabled={btn.disabled}
            title={translate(75, 'Speak', true)}
            style={{
              backgroundImage: `url(${imagesURL}/GlobalButtons/speak_enabled.png)`
            }}
          />
        );
      }
      case buttonsTypes.ACTIVITY_IMAGE: {
        return (
          <button
            onClick={btn.onClick || (() => console.log('No handler'))}
            disabled={btn.disabled}
            title={translate(71, 'Image', true)}
            style={{
              backgroundImage: `url(${imagesURL}/GlobalButtons/image_enabled.png)`
            }}
          />
        );
      }
      case buttonsTypes.CORRECT_ANSWER: {
        return (
          <button
            onClick={btn.onClick || (() => console.log('No handler'))}
            disabled={btn.disabled}
            title={translate(74, 'Correct Answer', true)}
            style={{
              backgroundImage: `url(${imagesURL}/GlobalButtons/showans_enabled.png)`
            }}
          />
        );
      }
      case buttonsTypes.HELP: {
        return (
          <button
            // @TODO check if activity opened then open activity help if not, open menu help
            onClick={btn.onClick || openActivityHelp}
            disabled={btn.disabled}
            title={translate(66, 'Help', true)}
            style={{
              backgroundImage: `url(${imagesURL}/GlobalButtons/help_enabled.png)`
            }}
          />
        );
      }
      default:
        console.error('Button type not found');
        return null;
    }
  };

  render() {
    const { buttons } = this.props;

    return (
      <ButtonsWrapper>
        {buttons.map((btn, index) => (
          <div key={btn ? btn.type : index}>{this.renderButton(btn)}</div>
        ))}
      </ButtonsWrapper>
    );
  }
}

ActivityButtons.propTypes = {
  openActivityHelp: PropTypes.func.isRequired,
  buttons: PropTypes.array.isRequired,
  translate: PropTypes.func.isRequired
};

const mapStateToProps = ({ game, status }) => ({ game, status });

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    { openActivityHelp }
  ),
  withTranslate
)(ActivityButtons);
