import React from "react";
import PropTypes from "prop-types";

import WhiteBox from "../WhiteBox/WhiteBox";
import classes from "./PlayRecorder.module.scss";

import { RecordAudio } from "utils";
import { imagesURL } from "config";

const PlayRecorder = ({
  currentUrl,
  deleteHandler,
  uploadHandler,
  classes: propsClasses
}) => {
  const wrapperClasses = `${classes.wrapper} ${propsClasses.wrapper || ""}`;
  const innerClasses = `${classes.inner} ${propsClasses.inner || ""}`;
  const buttonClasses = `${classes.button} ${propsClasses.button || ""}`;

  return (
    <WhiteBox outerClass={wrapperClasses} innerClass={innerClasses}>
      <RecordAudio onEndRecord={(url, blob) => uploadHandler(url, blob)}>
        {({
          recorder,
          processing,
          isRecording,
          isPlaying,
          stopRecordHandler,
          startRecordHandler,
          stopPlayHandler,
          playHandler,
          resetHandler
        }) => {
          if (!recorder) {
            return null;
          }

          function microphoneClickHandler() {
            if (isRecording) {
              stopRecordHandler();
            } else {
              startRecordHandler();
            }
          }

          function togglePlayHandler() {
            if (isPlaying) {
              stopPlayHandler();
            } else {
              playHandler(currentUrl);
            }
          }

          function trashClickHandler() {
            deleteHandler && deleteHandler();
            resetHandler();
          }

          return (
            <>
              <Button
                onClick={microphoneClickHandler}
                disabled={processing || currentUrl}
                image={isRecording ? "stop.png" : "microphone.png"}
                className={buttonClasses}
              />
              <Button
                onClick={togglePlayHandler}
                disabled={processing}
                hidden={isRecording || !currentUrl}
                image={isPlaying ? "stop.png" : "play.png"}
                className={buttonClasses}
              />
              <Button
                onClick={trashClickHandler}
                disabled={processing}
                hidden={!currentUrl}
                image="trash.png"
                className={buttonClasses}
              />
            </>
          );
        }}
      </RecordAudio>
    </WhiteBox>
  );
};

const Button = ({ image, ...otherProps }) => {
  return (
    <button {...otherProps}>
      <img src={`${imagesURL}/Images/Recorder/${image}`} alt="" />
    </button>
  );
};

PlayRecorder.defaultProps = {
  classes: {}
};

PlayRecorder.propTypes = {
  classes: PropTypes.object,
  uploadHandler: PropTypes.func,
  deleteHandler: PropTypes.func
};

export default PlayRecorder;
