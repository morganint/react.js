import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Buttons from '../Buttons/Buttons';
import {
  LOGOUT,
  SETTINGS,
  GO_BACK,
  HELP,
  REPORTS,
  SHARE,
  PARENT_ADVICE,
} from 'consts/buttons';
import { logout } from 'actions/authActions';
import {
  showSettings,
  showMenuHelp,
  goBack,
  showFamilyReports,
  showParentAdvice,
} from 'actions/flinkPlayActions';

const MenuButtons = ({
  buttons,
  logout,
  showSettings,
  goBack,
  solution,
  solutionMenu,
  groupsBreadcrumbs,
  familyReportsOpened,
  showFamilyReports,
  showParentAdvice,
  parentAdvice,
  parentAdviceUrl,
  showMenuHelp,
}) => {
  const formattedButtons = useMemo(
    () =>
      buttons.map((btn) => {
        const { activityBuilder } = solution || {};

        const btnObj = typeof btn === 'string' ? { type: btn } : btn;

        if (btnObj.onClick) {
          return btnObj;
        }

        switch (btnObj.type) {
          case SHARE:
            btnObj.dontShow = !activityBuilder || btnObj.dontShow;
            btnObj.onClick = () => console.log(btnObj.type, 'No Handler');

            break;
          case REPORTS:
            btnObj.dontShow = familyReportsOpened || btnObj.dontShow;
            btnObj.onClick = () => showFamilyReports(true);

            break;
          case GO_BACK: {
            if (
              solutionMenu &&
              solutionMenu.menuGroups &&
              solutionMenu.menuGroups.length === 1 &&
              !groupsBreadcrumbs.length
            ) {
              btnObj.type = LOGOUT;
              btnObj.onClick = logout;
            } else {
              btnObj.onClick = goBack;
            }
            break;
          }
          case LOGOUT:
            btnObj.onClick = logout;
            break;
          case HELP:
            btnObj.onClick = showMenuHelp;
            break;
          case PARENT_ADVICE: {
            btnObj.onClick = () => showParentAdvice(!parentAdvice);
            btnObj.dontShow = btnObj.dontShow || !parentAdviceUrl;
            break;
          }

          case SETTINGS:
            btnObj.onClick = showSettings;
            break;
          default:
            btnObj.onClick = () => console.log(btnObj.type, 'No Handler');
        }

        return btnObj;
      }),
    [
      buttons,
      logout,
      showSettings,
      showMenuHelp,
      solution,
      goBack,
      showFamilyReports,
      groupsBreadcrumbs.length,
      familyReportsOpened,
      solutionMenu,
      parentAdvice,
      parentAdviceUrl,
      showParentAdvice,
    ]
  );

  return <Buttons buttons={formattedButtons} />;
};

MenuButtons.propTypes = {
  buttons: PropTypes.array.isRequired,
  groupsBreadcrumbs: PropTypes.array.isRequired,
  logout: PropTypes.func.isRequired,
  showSettings: PropTypes.func.isRequired,
  showMenuHelp: PropTypes.func.isRequired,
  showFamilyReports: PropTypes.func.isRequired,
  showParentAdvice: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  familyReportsOpened: PropTypes.bool,
  solution: PropTypes.object,
  parentAdvice: PropTypes.bool,
  parentAdviceUrl: PropTypes.string,
};

const mapStateToProps = ({
  flinkPlay: {
    menuButtons,
    solution,
    solutionMenu,
    parentAdvice,
    parentAdviceUrl,
    familyReportsOpened,
    groupsBreadcrumbs,
  },
}) => ({
  buttons: menuButtons,
  solutionMenu,
  groupsBreadcrumbs,
  parentAdvice,
  parentAdviceUrl,
  solution,
  familyReportsOpened,
});

export default compose(
  connect(mapStateToProps, {
    logout,
    goBack,
    showSettings,
    showMenuHelp,
    showFamilyReports,
    showParentAdvice,
  })
)(MenuButtons);
