import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import _ from 'lodash';

import { viewDocumentById } from 'actions/adminActions';
import { imagesURL } from 'config';
import classes from './SettingsBigButtons.module.scss';

const iconsSrc = `${imagesURL}/SettingsDialog/icons/`;

const SettingsBigButtons = ({
  openFamilyForm,
  settings,
  viewDocumentById,
  solutions,
  changeHandler,
  // isTeam,
  learner,
  isCustomerFamily,
  translate,
  openAccount,
  currentLocaleCode = 'en',
  quickStartDocs,
}) => {
  const hasAnimations = useMemo(() => {
    if (
      !solutions ||
      !solutions.length ||
      !settings ||
      !settings.selectedSolutionId
    ) {
      return false;
    }

    const selectedSolution = _.find(solutions, {
      _id: settings.selectedSolutionId,
    });

    if (!selectedSolution) return false;

    const { animations } = selectedSolution;

    if (
      !animations ||
      (!animations.introduction && !animations.activityBuilder)
    ) {
      return;
    }

    const { introduction = {}, activityBuilder = {} } = animations;

    const hasIntroAnimation = !!introduction[currentLocaleCode];
    const hasBuilderAnimation = !!activityBuilder[currentLocaleCode];

    return hasIntroAnimation || hasBuilderAnimation;
  }, [solutions, settings, currentLocaleCode]);

  const quickStartDocumentId = useMemo(() => {
    if (!quickStartDocs) return null;

    const { isAdmin } = learner;

    const documents = isAdmin
      ? quickStartDocs.forAdmin
      : quickStartDocs.forMember;

    return documents && documents[currentLocaleCode];
  }, [currentLocaleCode, quickStartDocs, learner]);

  return (
    <>
      <div className={classes.wrapper}>
        <div className={classes.buttonWrapper}>
          {learner.isAdmin && (
            <Button
              onClick={openFamilyForm}
              title={translate(201, 'Family Members', true)}
              icon={iconsSrc + 'team.jpg'}
            />
          )}
        </div>
        <div className={classes.buttonWrapper}>
          {isCustomerFamily && learner.isAdmin && (
            <Button
              onClick={openAccount}
              title={translate(746, 'Manage', true)}
              icon={iconsSrc + 'account.jpg'}
            />
          )}
        </div>
        <div className={classes.buttonWrapper}>
          {quickStartDocumentId && (
            <Button
              onClick={() => viewDocumentById(quickStartDocumentId)}
              title={translate(200, 'Quick Start', true)}
              icon={iconsSrc + 'quickstart.png'}
            />
          )}
        </div>
        <div className={classes.buttonWrapper}>
          {hasAnimations && (
            <Button
              onClick={() => {
                changeHandler('autoAnimation', !settings.autoAnimation);
              }}
              off={!settings.autoAnimation}
              title={translate(45, 'Auto Animation', true)}
              icon={iconsSrc + 'animation.jpg'}
            />
          )}
        </div>
        <div className={classes.buttonWrapper}>
          <Button
            // disabled={isTeam && settings.teamHelp}
            onClick={() => {
              changeHandler('activityHelp', !settings.activityHelp);
            }}
            off={!settings.activityHelp}
            // off={!settings.activityHelp || (isTeam && settings.teamHelp)}
            title={translate(44, 'Auto Activity Help', true)}
            icon={iconsSrc + 'activity.jpg'}
          />
        </div>
        {/* <div className={classes.buttonWrapper}>
          {isTeam && (
            <Button
              onClick={() => {
                changeHandler("teamHelp", !settings.teamHelp);
              }}
              off={!settings.teamHelp}
              title={translate(245, "Team Help", true)}
              icon={iconsSrc + "team.jpg"}
            />
          )}
        </div> */}
      </div>
    </>
  );
};

SettingsBigButtons.propTypes = {
  openFamilyForm: PropTypes.func.isRequired,
  changeHandler: PropTypes.func.isRequired,
  viewDocumentById: PropTypes.func.isRequired,
  translate: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired,
  learner: PropTypes.object.isRequired,
  isTeam: PropTypes.bool.isRequired,
  isCustomerFamily: PropTypes.bool,
  solutions: PropTypes.array,
  currentLocaleCode: PropTypes.string,
  quickStartDocs: PropTypes.object,
};

const mapStateToProps = ({ auth, flinkPlay }) => ({
  isTeam: auth.isTeam,
  learner: flinkPlay.learner,
  isCustomerFamily: flinkPlay.family.isCustomer,
  quickStartDocs: flinkPlay.product.quickStartDocs,
  solutions: flinkPlay.product.solutions,
});

export default connect(mapStateToProps, { viewDocumentById })(
  SettingsBigButtons
);

const Button = ({ title, icon, off, ...props }) => {
  return (
    <button
      title={title}
      className={classnames(classes.button, { [classes.off]: off })}
      {...props}
    >
      <img src={icon} alt={title} />

      <span>{title}</span>
    </button>
  );
};
