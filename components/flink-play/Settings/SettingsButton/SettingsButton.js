import React from 'react';
import styled from 'styled-components';

const SettingsButton = (props) => {
  return <StyledButton {...props} />;
};

export default SettingsButton;

const StyledButton = styled.button`
  background-color: #972bc9;
  border-radius: 1vmin;
  padding: 1vmin 2vmin;
  color: #fff;
  text-transform: uppercase;
  font-weight: 500;
  opacity: 0.8;
  transition: all 0.2s ease;
  min-width: 15vmin;
  font-size: 1.4vmin;

  &:hover:not(:disabled) {
    opacity: 1;
  }

  &:disabled {
    opacity: 0.6;
    cursor: default;
  }
`;
