import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';

import {
  showHelp,
  closeSettings,
  saveSettingsAndCloseDialog,
  setMenuButtons,
  setPrevMenuButtons,
  familyUpdate,
} from 'actions/flinkPlayActions';
import { getFamilyMembersByFamily } from 'actions/familiesActions';

import { WhiteBox } from 'components/flink-play';
import SettingsButton from './SettingsButton/SettingsButton';
import SettingsSlider from './SettingsSlider/SettingsSlider';
import SettingsFamilyForm from './SettingsFamilyForm/SettingsFamilyForm';
import SettingsBigButtons from './SettingsBigButtons/SettingsBigButtons';
import SettingsManagePurchase from './SettingsManagePurchase/SettingsManagePurchase';
import { imagesURL, audioURL } from 'config';
import { LOGOUT, HELP, PARENT_ADVICE } from 'consts/buttons';
import classes from './Settings.module.scss';

const settingsDialogFolder = `${imagesURL}/SettingsDialog`;

const defaultSettings = {
  bySolution: {},
  autoAnimation: true,
  teamHelp: true,
  activityHelp: true,
};

export class Settings extends Component {
  state = {
    isVisible: false,
    familyFormOpened: false,
    settings: {},
  };

  componentDidMount() {
    const {
      saveSettingsAndCloseDialog,
      closeSettings,
      learner: { settings, isAdmin, _id: learnerId },
      setMenuButtons,
      locale,
      product,
      isInitialSettings,
    } = this.props;

    // Set menu buttons
    setMenuButtons([
      LOGOUT,
      PARENT_ADVICE,
      { type: HELP, onClick: () => this.showSettingsHelp() },
    ]);

    const {
      solutions,
      defaultSolutionsForAdmins,
      defaultSolutionsForMembers,
    } = product;

    const changedSettings = settings ? { ...settings } : { ...defaultSettings };

    let selectedSolution =
      changedSettings.selectedSolutionId &&
      _.find(solutions, {
        _id: changedSettings.selectedSolutionId,
      });

    if (!selectedSolution) {
      const defaultSolutions =
        (isAdmin ? defaultSolutionsForAdmins : defaultSolutionsForMembers) ||
        {};

      changedSettings.selectedSolutionId =
        defaultSolutions[locale.code] || (solutions[0] && solutions[0]._id);

      selectedSolution = _.find(solutions, {
        _id: changedSettings.selectedSolutionId,
      });
    }

    if (!selectedSolution) {
      return alert('No solutions in the product');
    }

    if (isInitialSettings && settings) {
      return closeSettings();
    }

    if (isInitialSettings && !settings) {
      changedSettings.bySolution[selectedSolution._id] = {
        theme: selectedSolution.defaultTheme,
        character: selectedSolution.defaultCharacter,
      };

      saveSettingsAndCloseDialog(learnerId, changedSettings)
      // if (isAdmin) {
      //   this.showSettingsHelp(() =>
      //     saveSettingsAndCloseDialog(learnerId, changedSettings)
      //   );
      // } else {
      //   saveSettingsAndCloseDialog(learnerId, changedSettings);
      // }

      return;
    }

    const solutionsOptions = _.chain(solutions)
      .map((sol) => ({
        label: sol.displayName,
        value: sol._id,
      }))
      .sortBy('label')
      .value();

    this.setState({
      isVisible: true,
      solutionsOptions,
      settings: changedSettings,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      settings: { selectedSolutionId },
    } = this.state;

    if (prevState.settings.selectedSolutionId !== selectedSolutionId) {
      this.onSelectedSolutionChanged();
    }
  }

  componentWillUnmount() {
    const { isInitialSettings, setPrevMenuButtons } = this.props;
    setPrevMenuButtons();

    if (isInitialSettings) {
      const event = new CustomEvent('initialSettingsClosed');
      document.dispatchEvent(event);
    }
  }

  showSettingsHelp = (cb) => {
    const { learner, showHelp, locale, translate } = this.props;

    const { isAdmin } = learner;

    const audioHelp = `${audioURL}/Generic/SettingsHelp/${
      locale.name
    }/InitialHelp${isAdmin ? 'Admin' : 'Member'}.mp3`;

    showHelp({
      title: translate(196, 'Settings Help'),
      stringNumber: isAdmin ? 77 : 49,
      cb,
      // stringNumber: !!settings ? 196 : isAdmin ? 77 : 49,
      audioHelp,
      customTranslate: translate,
    });
  };

  openFamilyForm = async () => {
    const { getFamilyMembersByFamily, family } = this.props;

    const familyMembers = await getFamilyMembersByFamily(family._id);

    if (!familyMembers) return;

    const familyFormInitialValues = {
      ...family,
      // Family admin should be first one
      familyMembers: familyMembers.sort((a) => (a.isAdmin ? -1 : 1)),
    };

    this.setState({ familyFormOpened: true, familyFormInitialValues });
  };

  familyFormOnSuccess = (family) => {
    const { familyUpdate } = this.props;
    familyUpdate(family);
    this.closeFamilyForm();
  };

  closeFamilyForm = () => {
    this.setState({ familyFormOpened: false, familyFormInitialValues: null });
  };

  changeSettingsInCurrentSolution = (prop, value) => {
    const { settings } = this.state;
    const { selectedSolutionId, bySolution = {} } = settings;

    const currentSolutionSettings = bySolution[selectedSolutionId] || {};
    currentSolutionSettings[prop] = value;

    this.setState({
      settings: {
        ...settings,
        bySolution: {
          ...bySolution,
          [selectedSolutionId]: currentSolutionSettings,
        },
      },
    });
  };

  changeSettings = (prop, value) => {
    const { settings } = this.state;

    this.setState({ settings: { ...settings, [prop]: value } });
  };

  changeSelectedSolution = (solutionId) => {
    this.setState((state) => ({
      settings: { ...state.settings, selectedSolutionId: solutionId },
    }));
  };

  onSelectedSolutionChanged = () => {
    const { settings } = this.state;
    const { selectedSolutionId, bySolution = {} } = settings;

    const { product } = this.props;

    const selectedSolution = _.find(product.solutions, {
      _id: selectedSolutionId,
    });

    const bySolutionSettings =
      (bySolution[selectedSolutionId] && {
        ...bySolution[selectedSolutionId],
      }) ||
      {};

    const themesOptions = _.chain(selectedSolution.themes)
      .map((theme) => ({ value: theme._id, label: theme.name }))
      .sortBy('label')
      .value();

    if (bySolutionSettings.theme) {
      const theme = _.find(selectedSolution.themes, {
        _id: bySolutionSettings.theme,
      });

      if (!theme) {
        bySolutionSettings.theme = selectedSolution.defaultTheme;
      }
    } else if (bySolutionSettings.theme === undefined) {
      bySolutionSettings.theme = selectedSolution.defaultTheme;
    }

    const charactersOptions = _.chain(selectedSolution.characters)
      .map((character) => ({ value: character._id, label: character.name }))
      .sortBy('label')
      .value();

    if (bySolutionSettings.character) {
      const character = _.find(selectedSolution.characters, {
        _id: bySolutionSettings.character,
      });

      if (!character) {
        bySolutionSettings.character = selectedSolution.defaultCharacter;
      }
    } else if (bySolutionSettings.character === undefined) {
      bySolutionSettings.character = selectedSolution.defaultCharacter;
    }

    this.setState({
      themesOptions,
      charactersOptions,
      settings: {
        ...settings,
        bySolution: { ...bySolution, [selectedSolutionId]: bySolutionSettings },
      },
    });
  };

  saveSettings = () => {
    const { saveSettingsAndCloseDialog, learner } = this.props;
    const { settings } = this.state;

    saveSettingsAndCloseDialog(learner._id, settings);
  };

  openManagePurchase = () => {
    this.setState({ managePurchaseOpened: true });
  };

  closeManagePurchase = () => {
    this.setState({ managePurchaseOpened: false });
  };

  render() {
    const {
      closeSettings,
      locale,
      translate,
      learner: { settings: currentSettings },
    } = this.props;

    const {
      isVisible,
      themesOptions,
      charactersOptions,
      solutionsOptions,
      settings,
      managePurchaseOpened,
      familyFormOpened,
      familyFormInitialValues,
    } = this.state;

    const { selectedSolutionId, bySolution } = settings;

    const currentSolutionSettings =
      (bySolution && bySolution[selectedSolutionId]) || {};

    if (!isVisible) return null;

    return (
      <div className={classes.overlay}>
        <WhiteBox
          outerClass={classes.settingsOuter}
          innerClass={classes.settingsInner}
        >
          {familyFormOpened && (
            <SettingsFamilyForm
              onSuccess={this.familyFormOnSuccess}
              closeHandler={this.closeFamilyForm}
              translate={translate}
              initialValues={familyFormInitialValues}
            />
          )}

          {managePurchaseOpened && (
            <div className={classes.popup}>
              <div className={classes.popupViewport}>
                <SettingsManagePurchase translate={translate} />
              </div>

              <div className={classes.popupActions}>
                <SettingsButton onClick={this.closeManagePurchase}>
                  {translate(58, 'Go Back')}
                </SettingsButton>
              </div>
            </div>
          )}

          {charactersOptions && !!charactersOptions.length && (
            <SettingsSlider>
              <div>
                <button
                  className={classnames(classes.itemButton, {
                    [classes.selectedItem]:
                      currentSolutionSettings.character === '',
                  })}
                  onClick={this.changeSettingsInCurrentSolution.bind(
                    null,
                    'character',
                    ''
                  )}
                  title="No Character"
                >
                  <div
                    className={classes.characterImgWrapper}
                    style={{
                      backgroundImage: `url(${settingsDialogFolder}/characters/NoCharacter.png)`,
                    }}
                  />
                </button>
              </div>
              {charactersOptions.map((opt) => {
                const filename = opt.label.replace(/\s/g, '') + '.png';

                return (
                  <div key={opt.value}>
                    <button
                      className={classnames(classes.itemButton, {
                        [classes.selectedItem]:
                          currentSolutionSettings.character === opt.value,
                      })}
                      onClick={this.changeSettingsInCurrentSolution.bind(
                        null,
                        'character',
                        opt.value
                      )}
                    >
                      <div
                        title={opt.label}
                        className={classes.characterImgWrapper}
                        style={{
                          backgroundImage: `url(${settingsDialogFolder}/characters/${filename})`,
                        }}
                      />
                    </button>
                  </div>
                );
              })}
            </SettingsSlider>
          )}

          {themesOptions && !!themesOptions.length && (
            <SettingsSlider>
              {themesOptions.map((opt) => {
                const filename = opt.label.replace(/\s/g, '') + '.png';

                return (
                  <div key={opt.value}>
                    <button
                      className={classnames(classes.itemButton, {
                        [classes.selectedItem]:
                          currentSolutionSettings.theme === opt.value,
                      })}
                      onClick={this.changeSettingsInCurrentSolution.bind(
                        null,
                        'theme',
                        opt.value
                      )}
                      title={opt.label}
                    >
                      <div
                        className={classes.themeImgWrapper}
                        style={{
                          backgroundImage: `url(${settingsDialogFolder}/themes/${filename})`,
                        }}
                      />
                    </button>
                  </div>
                );
              })}
            </SettingsSlider>
          )}

          <SettingsBigButtons
            openAccount={this.openManagePurchase}
            currentLocaleCode={locale.code}
            openFamilyForm={this.openFamilyForm}
            translate={translate}
            settings={settings}
            changeHandler={this.changeSettings}
          />

          <div className={classes.bottom}>
            {solutionsOptions && solutionsOptions.length > 1 && (
              <div className={classes.radioListWrapper}>
                <p className={classes.radioListLabel}>
                  {translate(338, 'Solution')}
                </p>

                <div className={classes.radioList}>
                  {solutionsOptions.map((opt) => (
                    <label key={opt.value}>
                      <input
                        checked={opt.value === selectedSolutionId}
                        value={opt.value}
                        onChange={(e) =>
                          this.changeSelectedSolution(e.target.value)
                        }
                        type="radio"
                      />
                      <span>{opt.label}</span>
                    </label>
                  ))}
                </div>
              </div>
            )}

            <div className={classes.settingsActions}>
              {currentSettings && (
                <SettingsButton onClick={closeSettings}>
                  {translate(52, 'Cancel')}
                </SettingsButton>
              )}
              <SettingsButton
                onClick={this.saveSettings}
                disabled={
                  currentSettings &&
                  JSON.stringify(currentSettings) === JSON.stringify(settings)
                }
              >
                {translate(298, 'Save')}
              </SettingsButton>
            </div>
          </div>
        </WhiteBox>
      </div>
    );
  }
}

Settings.propTypes = {
  isInitialSettings: PropTypes.bool,
  closeSettings: PropTypes.func.isRequired,
  familyUpdate: PropTypes.func.isRequired,
  getFamilyMembersByFamily: PropTypes.func.isRequired,
  saveSettingsAndCloseDialog: PropTypes.func.isRequired,
  showHelp: PropTypes.func.isRequired,
  product: PropTypes.object.isRequired,
  learner: PropTypes.object.isRequired,
  setMenuButtons: PropTypes.func.isRequired,
  setPrevMenuButtons: PropTypes.func.isRequired,
};

const mapStateToProps = ({
  flinkPlay: {
    product,
    family,
    isInitialSettings,
    solutionTranslate,
    solutionLocale,
    learner,
  },
  status,
}) => ({
  product,
  family,
  learner,
  isInitialSettings,
  translate: solutionTranslate || status.translate,
  locale: solutionLocale || status.lang,
});

export default compose(
  connect(mapStateToProps, {
    showHelp,
    closeSettings,
    familyUpdate,
    saveSettingsAndCloseDialog,
    getFamilyMembersByFamily,
    setMenuButtons,
    setPrevMenuButtons,
  })
)(Settings);
