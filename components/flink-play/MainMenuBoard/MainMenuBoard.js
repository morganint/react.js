import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import _ from 'lodash';

import { WhiteBox, CustomScrollbar } from 'components/flink-play';
import { StarIcon } from 'components/flink-play/FlinkPlayIcons/FlinkPlayIcons';
import { imagesURL } from 'config';
import classes from './MainMenuBoard.module.scss';

const MainMenuBoard = (props) => {
  const {
    title,
    subtitle,
    solutionMenu,
    translate,
    reports = {},
    learnerGoal,
    openMenuItem,
    scrollPosition,
    setScrollPosition,
    scrollbarRef,
    children,
    teamReports,
  } = props;
  const { menuGroups } = solutionMenu;

  const levels = useMemo(() => {
    return _.chain(menuGroups)
      .map((g) => (isNaN(+g.menuLevel) ? g.menuLevel : +g.menuLevel))
      .uniq()
      .sort((a, b) => {
        if (isNaN(a) && isNaN(b)) return a > b ? 1 : -1;
        if (isNaN(a)) return 1;
        if (isNaN(b)) return -1;
        return b - a;
      })
      .value();
  }, [menuGroups]);

  const firstCategories = useMemo(() => {
    return _.chain(menuGroups)
      .map((g) => g.firstCategory)
      .uniq()
      .map((id) => ({ id, ...solutionMenu.reports[id] }))
      .sortBy('order')
      .value();
  }, [menuGroups, solutionMenu]);

  return (
    <WhiteBox outerClass={classes.wrapper} innerClass={classes.innerWrapper}>
      <div className={classes.header}>
        <div className={classes.logoWrapper}>
          <img
            src={`${imagesURL}/Images/Login/FLCLogo.png`}
            alt="Flink logo"
          />
        </div>

        <h1 className={classes.menuTitle}>{title}</h1>
        <h2 className={classes.menuSubtitle}>{subtitle}</h2>
      </div>

      <div className={classes.table}>
        <div className={classes.tableHeading}>
          <div>
            <ul className={classes.legend}>
              <li>
                <span
                  style={{ color: 'yellow' }}
                  className={classes.legendStar}
                >
                  <StarIcon partial />
                </span>
                <span>{translate(220, 'Partial')}</span>
              </li>
              <li>
                <span style={{ color: 'green' }} className={classes.legendStar}>
                  <StarIcon mastered />
                </span>
                <span>{translate(221, 'Mastered')}</span>
              </li>
            </ul>
            <span
              className={classnames(classes.levelsIndicator, {
                [classes.hidden]: levels.length < 8,
              })}
            >
              {translate(107, 'Level')} {levels[levels.length - 1]} -{' '}
              {levels[0]}
            </span>
          </div>

          {firstCategories.map((cat) => {
            const name = translate(cat.name);

            return (
              <figure key={cat.id}>
                <img
                  src={
                    cat.image && `${imagesURL}/ReportMenuImages/${cat.image}`
                  }
                  alt={name}
                />
                <figcaption>{name}</figcaption>
              </figure>
            );
          })}
        </div>

        <CustomScrollbar
          ref={scrollbarRef}
          style={{ height: '48vmin' }}
          initialValue={scrollPosition}
          onChange={(scroll) => setScrollPosition && setScrollPosition(scroll)}
        >
          {levels.map((level) => {
            return (
              <div key={level} className={classes.levelRow}>
                <span className={classes.levelTitle}>{level}</span>

                {firstCategories.map((cat) => {
                  const menuGroup = _.find(menuGroups, {
                    firstCategory: cat.id,
                    menuLevel: level + '',
                  });

                  const feedbacks = [];

                  if (menuGroup) {
                    feedbacks.push(
                      (reports && reports[menuGroup.groupId]) || {}
                    );

                    if (teamReports) {
                      feedbacks.push(
                        ...teamReports.map((reports) => {
                          const report = reports && reports[menuGroup.groupId];
                          return report || {};
                        })
                      );
                    }
                  }

                  return (
                    <div key={cat.id} className={classes.cell}>
                      {menuGroup && (
                        <>
                          <button
                            onClick={() =>
                              openMenuItem &&
                              openMenuItem(menuGroup.groupId, true)
                            }
                            disabled={!openMenuItem}
                            className={classnames(classes.menuGroupBtn, {
                              [classes.highlight]:
                                learnerGoal &&
                                learnerGoal.level === level + '' &&
                                learnerGoal.subject === cat.id,
                              // [classes.partial]:
                              //   feedback.partial && !feedback.mastered,
                              // [classes.mastered]: feedback.mastered
                            })}
                          >
                            {feedbacks.map((f, idx) => {
                              const tooltip =
                                f.dateMastered && new Date(f.dateMastered);

                              return (
                                <div
                                  key={idx}
                                  title={
                                    (tooltip && 'Mastered at ' + tooltip) || ''
                                  }
                                >
                                  <StarIcon {...f} />
                                </div>
                              );
                            })}
                          </button>
                        </>
                      )}
                    </div>
                  );
                })}
              </div>
            );
          })}
        </CustomScrollbar>

        {/* <div className={classes.tableBody} ref={this.tableBodyRef}></div> */}
      </div>

      {children}
    </WhiteBox>
  );
};

MainMenuBoard.propTypes = {
  solutionMenu: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
  reports: PropTypes.object,
  scrollPosition: PropTypes.object,
  setScrollPosition: PropTypes.func,
  openMenuItem: PropTypes.func,
  teamReports: PropTypes.array,
  learnerGoal: PropTypes.object,
};

export default MainMenuBoard;
