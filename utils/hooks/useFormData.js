import { useState, useMemo } from 'react';
import _ from 'lodash';

export default (initialData, cb) => {
  const [data, setData] = useState(_.cloneDeep(initialData));

  function handleDataChange(editedProps) {
    const changedData = { ...data, ...editedProps };
    setData(changedData);
    cb && cb(changedData);
  }

  useMemo(() => {
    setData(_.cloneDeep(initialData));
  }, [initialData]);

  return [data, handleDataChange];
};
