export default (e, max) => {
  const val = +e.target.value;
  const key = +e.key;
  const concat = +('' + val + key);

  if (key === '-' || concat < 0 || concat > +max) {
    e.preventDefault();
  }
};