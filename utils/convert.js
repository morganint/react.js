export default {
  PXtoVW(size, originBlockWidth, currentBlockWidth, precision) {
    const parsedSize = parseFloat(size);
    const coef = currentBlockWidth / originBlockWidth;

    const convertedSize = ((parsedSize * coef) / window.innerWidth) * 100;

    return (
      (typeof precision === "number"
        ? convertedSize.toFixed(precision)
        : convertedSize) + "vw"
    );
  },

  VWtoPX(size, originBlockWidth, currentBlockWidth, precision) {
    let parsedSize = parseFloat(size);
    if (typeof precision === "number") {
      parsedSize = +parsedSize.toFixed(precision);
    }
    const coef = currentBlockWidth / originBlockWidth;

    const convertedSize = ((parsedSize / 100) * window.innerWidth) / coef;

    return (
      (typeof precision === "number"
        ? convertedSize.toFixed(precision)
        : convertedSize) + "vw"
    );
  },

  htmlPXtoVW(html, ...args) {
    const formattedHtml = (html || "")
      .replace(
        /:\s?(\d+(?:.\d+)?)(?:px)/gm,
        fontSize => ": " + this.PXtoVW(fontSize, ...args)
      )
      .replace(
        /width="(\d+(?:.\d+)?)"/gm,
        fontSize => 'width="' + this.PXtoVW(fontSize, ...args) + '"'
      );
    return formattedHtml;
  },

  htmlVWtoPX(html, ...args) {
    const formattedHtml = (html || "")
      .replace(
        /:\s?(\d+(?:.\d+)?)(?:vw)/gm,
        fontSize => ": " + this.VWtoPX(fontSize, ...args)
      )
      .replace(
        /width="(\d+(?:.\d+)?)"/gm,
        fontSize => 'width="' + this.VWtoPX(fontSize, ...args) + '"'
      );
    return formattedHtml;
  }
};
