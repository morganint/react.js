export default (firstname, lastname) =>
  ((firstname ? firstname : '') + (lastname ? ` ${lastname}` : '')).trim();
