// Num - number of characters in retrned value, max is 9
export default num =>
  Math.random()
    .toString(36)
    .substr(2, num || 9);
