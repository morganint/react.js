export default arg => {
  const elem = typeof arg === "string" ? document.querySelector(arg) : arg;
  
  const mywindow = window.open();
  mywindow.document.write("<html><head></head><body>");
  mywindow.document.write(elem.outerHTML);
  mywindow.document.write("</body></html>");

  mywindow.document.close(); // necessary for IE >= 10
  mywindow.focus(); // necessary for IE >= 10*/

  mywindow.print();

  mywindow.onafterprint = function() {
    mywindow.close();
  };
};
