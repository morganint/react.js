export default (queryString) =>
  queryString
    .replace('?', '')
    .split('&')
    .reduce((acc, set) => {
      const [key, value] = set.split('=');
      acc[key] = value;
      return acc;
    }, {});
