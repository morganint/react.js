import React, { Fragment } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';

import checkPermissions from './checkPermissions';
import { logout } from 'actions/authActions';

const CustomRoute = ({ component: Component, permissions, auth, logout, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        return checkPermissions(auth, permissions) ? (
          <Component {...props} />
        ) : auth.isAuthenticated ? (
          <Fragment>
            <p>
              You don't have permissions to access{' '}
              <b>{props.location.pathname}</b>
            </p>
            <button onClick={props.history.goBack}>Go back</button>
            <button onClick={logout}>Logout</button>
          </Fragment>
        ) : (
          <Redirect to="/" />
        );
      }}
    />
  );
};

CustomRoute.propTypes = {
  auth: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired
};

export default connect(
  null,
  { logout }
)(CustomRoute);
