import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from "classnames"

const styles = theme => ({
  root: {
    userSelect: 'none',
    width: '0.9em!important',
    height: '0.9em!important',
    display: 'inline-block',
    fill: 'currentColor',
    flexShrink: 0,
    fontSize: 24,
    transition: theme.transitions.create('fill', {
      duration: theme.transitions.duration.shorter
    })
  }
});

function MaterialFaIcon(props) {
  const { classes } = props;

  return (
      <FontAwesomeIcon {...props}  className={classNames(classes.root, props.className)}  />
  );
}

export default withStyles(styles)(MaterialFaIcon);
