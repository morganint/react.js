import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';

import { WithPageTitle } from 'components/hocs';
import checkPermissions from './checkPermissions';
import { logout } from 'actions/authActions';
// import { setCurrentPage } from 'actions/statusActions';

const CustomRoute = ({
  auth,
  logout,
  children,
  permissions,
  // setCurrentPage,
  component: Component,
  redirectTo = '/',
  ...rest
}) => {
  const hasPermissions = checkPermissions(auth, permissions);

  if (!hasPermissions) {
    auth.user && alert("You don't have permissions to access this page");
    return <Redirect to={redirectTo} />;
  }

  return (
    <Route {...rest}>
      {Component ? <WithPageTitle Component={Component} /> : children}
    </Route>
  );
};

CustomRoute.propTypes = {
  auth: PropTypes.object,
  logout: PropTypes.func.isRequired,
};

const mapStateToProps = ({ auth }) => ({ auth });

export default connect(mapStateToProps, {
  logout,
  // setCurrentPage
})(CustomRoute);
