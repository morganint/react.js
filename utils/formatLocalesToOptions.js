export default locales =>
  locales
    .map(locale => ({
      value: locale.code,
      label: locale.displayName
    }))
    .sort((a, b) => (a.label > b.label ? 1 : -1));
