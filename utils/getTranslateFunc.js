import React from "react";
import _ from "lodash";

export default (strings, locale) => (
  stringNumber,
  defaultText,
  isTooltip,
  forHelp
) => {
  const string = _.find(strings, {
    locale,
    stringNumber: stringNumber + ""
  });

  if (!string) {
    return defaultText || "Translate not found";
  }

  if (isTooltip) {
    return string.stringValue;
  }

  if (forHelp) {
    return string.prettyValue || string.stringValue;
  }

  return string.prettyValue ? (
    <span dangerouslySetInnerHTML={{ __html: string.prettyValue }} />
  ) : (
    string.stringValue
  );
};
