import axios from "axios";
import { apiURL } from "config";

const axiosAPI = axios.create({
  baseURL: apiURL,
  timeout: 60000
});

export default axiosAPI;
