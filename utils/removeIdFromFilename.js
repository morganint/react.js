export default filename => {
  return filename.replace(/_\S+?\./, ".");
};
