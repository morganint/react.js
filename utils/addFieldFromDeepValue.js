import { get } from "lodash";

export default (data, fieldName, valuePath) =>
  data.map(d => {
    d[fieldName] = get(d, valuePath);
    return d;
  });
