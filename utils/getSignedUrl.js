import axiosAPI from "./axiosAPI";

const getSignedUrl = async data => {
  const res = await axiosAPI.post("api/common/signed-url", data);

  return res.data;
};

export default getSignedUrl;
