const fallbackDivider = 7.68;

export default ({ html, data = {}, calc, units = "vmin", coef }) => {
  const convertFontSize = (fontSizeWithUnits, fontSize) => {
    let convertedSize;

    if (calc) {
      convertedSize = calc(fontSize);
    } else if (coef) {
      convertedSize = fontSize * coef;
    } else {
      convertedSize = fontSize / fallbackDivider;
    }

    return `: ${convertedSize}${units}`;
  };

  let formattedHtml = html
    ? html
        .replace(/:\s?(\d+(?:.\d+)?)(?:px)/gm, convertFontSize)
        .replace(/width="(\d+(?:.\d+)?)"/gm, (whole, size) => {
          return `width="${calc ? calc(size) : size}"`;
        })
        .replace(/font-size:\s?(?:large|small|medium|larger|smaller)/gm, "")
    : "";
  // .replace(/<br>/gm, '<br><span style="font-size: 20em;">&nbsp;</span>');

  // Replace variables
  const match = formattedHtml.match(/\{\{([\s\S]+?)\}\}/g);

  if (match) {
    match.forEach(m => {
      const variable = m.replace(/\{\{(\s+)?/, "").replace(/(\s+)?\}\}/, "");
      const value =
        data[variable] !== undefined ? data[variable] : "Value not found";

      const regExp = new RegExp(m, "g");
      formattedHtml = formattedHtml.replace(regExp, value);
    });
  }

  return formattedHtml;
};
