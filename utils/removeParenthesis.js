export default string =>
  string.indexOf('(') === -1 ? string : string.split('(')[0];
