export const apiURL =
  process.env.REACT_APP_API_URL ||
  window.location.origin.replace('5000', '3000');

// FS Bucket
export const filesURL = 'https://s3.us-east-2.amazonaws.com/flink-2.0-fs';
export const imagesURL = `${filesURL}/Images`;
export const audioURL = `${filesURL}/Audio`;

// Uploads Bucket
export const uploadsURL = process.env.REACT_APP_UPLOADS_BUCKET;

export const publicKey = 'public';
export const activitiesKey = `${publicKey}/Activities`;
export const usersKey = `${publicKey}/Users`;

export const s3bucketPublicURL = `${uploadsURL}/${publicKey}`;
export const activitiesURL = `${uploadsURL}/${activitiesKey}`;
