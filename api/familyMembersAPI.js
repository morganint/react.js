import { axiosAPI, responseHandler } from 'utils';

export default {
  getById(id) {
    return responseHandler(axiosAPI.get(`api/family-members/${id}`));
  },
};
