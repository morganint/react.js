import { axiosAPI, responseHandler } from 'utils';

export default {
  getReports(params) {
    return responseHandler(axiosAPI.post(`api/reports`, params));
  },
  geByPurchaseConfig(id, params) {
    return responseHandler(
      axiosAPI.get(`api/reports/by-purchase-config/${id}`, { params })
    );
  },
  getRevenueReport(params) {
    return responseHandler(axiosAPI.get('api/reports/revenue', { params }));
  },
  byReportGroup(reportGroupId, params) {
    return responseHandler(
      axiosAPI.post(`api/reports/by-report-group/${reportGroupId}`, params)
    );
  },
  byFamily(familyId, params) {
    return responseHandler(
      axiosAPI.get(`api/reports/by-family/${familyId}`, { params })
    );
  },
};
