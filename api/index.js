import activity from './activityAPI';
import game from './gameAPI';
import config from './configAPI';
import storage from './storageAPI';
import products from './productsAPI';
import reports from './reportsAPI';
import purchase from './purchaseAPI';
import activityGroups from './activityGroupsAPI';
import solutions from './solutionsAPI';
import familyMembers from './familyMembersAPI';
import templates from './templatesAPI';

const api = {
  activity,
  game,
  config,
  storage,
  products,
  reports,
  purchase,
  activityGroups,
  solutions,
  familyMembers,
  templates
};

export default api;
